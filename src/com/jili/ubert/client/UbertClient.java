/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client;

import com.jili.ubert.client.ui.UINode;
import com.jili.ubert.client.ui.workspace3.main2.table.AccountBailUIController;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import com.jili.ubert.clientapi.ClientAPI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author ChengJili
 */
public class UbertClient extends Application {

    static ClientAPI clientapi;
    private static final Log log = LogFactory.getLog(UbertClient.class);
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        UINode.setStage(stage);
        log.debug("开始运行");
        //    Stage stage = UINode.getStage();
        clientapi = new ClientAPI();
        //正常启动代码
        /*
        Scene scene = new Scene(UINode.getLogin(clientapi).getUINode());
        stage.setScene(scene);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.show();
                */
        //测试启动代码
        
        Scene scene1 = new Scene(UINode.getCancelUI(clientapi).getUINode());
    //    AccountBailUIController as = new AccountBailUIController();
    //    Scene scene1 = new Scene(as.getUINode());
        stage.setScene(scene1);
        stage.initStyle(StageStyle.DECORATED);
        log.info(stage.getStyle().getClass());
        stage.show();
    }
}
