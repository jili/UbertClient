/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.antotest;

import com.jili.ubert.client.ui.UINode;
import com.jili.ubert.client.ui.i18n.I18N;
import com.jili.ubert.client.ui.workspace3.main2.table.AccountBailUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.AccountFareUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.AccountIcodeUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.AccountInfoUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.AccountInvestUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.AlterOrderUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.BailTempObjUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.BailTempUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.CancelOrderUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ClearingCurrencyRatesUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ClearingDayBookUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ClearingExRightUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ClearingPriceUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ClearingProtfolioposUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.FareTempUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.FareTempeObjUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.FundStateUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.FundTransferUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.HistoryClearingCurrencyRatesUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.HistoryIOPVAccountUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.HistoryIOPVProductUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.HistoryIOPVProductUnitUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.HistoryLogLoginInfoUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.HistoryObjExRightInfoUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.HistoryTradeFundTransferUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.HistoryTradesetCalendarUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.IOPVAccountUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.IOPVProductUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.IOPVProductUnitUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.LogLoginInfoUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.NewOrderUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ObjClassCodeAtrrUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ObjClassCodeUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ObjETFListUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ObjETFUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ObjExRightInfoUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ObjIndexListUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ObjIndexUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ObjInfoUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ObjNewStockUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ObjPriceTickUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ObjProtfolioListUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ObjProtfolioUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ObjStructuredFundBaseUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ObjStructuredFundPartUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ObjUnderlyingCodeUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.OrderExecuteProgressUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.PosDetailUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ProductAccountUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ProductInfoUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.ProductUnitUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentClearingAccountBailUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentClearingAccountFareUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentClearingDayBookUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentClearingExRightUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentClearingFundStateUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentClearingPosDetailUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentClearingPriceUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentClearingProtfolioposUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentObjETFListUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentObjETFUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentObjIndexListUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentObjIndexUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentTradeAlterOrderUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentTradeCancelOrderUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentTradeExecuteOrderUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentTradeNewOrderUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RecentTradeReportDetailUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RiskCtrlAssetsUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RiskIndexUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.RiskSetsUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.SafeForbidLoginUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.SafeOtherAccessUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceAccountBailUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceAccountFareUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceAccountIcodeUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceAccountInfoUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceAccountInvestUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceBailTempObjUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceBailTempUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceFareTempUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceFareTempeObjUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceObjInfoUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceObjProtfolioListUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceObjProtfolioUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceObjUnderlyingUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceProductAccountUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceProductInfoUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceProductUnitUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceRiskCtrlAssetsUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceRiskSetsUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceSafeForbidLoginUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceSafeOtherAccessUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceTradesetExchangeLimitUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceUserAccessUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceUserAccountUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceUserInfoUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceUserPrivateInfoUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceWorkFlowPathUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceWorkGroupUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceWorkGroupUserUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TraceWorkProductFlowUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TradeReportDetailUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TradesetBrokerUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TradesetCalendarUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TradesetExchangeLimitUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.TradesetPipUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.UserAccessUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.UserAccountUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.UserInfoUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.UserPrivateInfoUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.WorkFlowPathUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.WorkGroupUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.WorkGroupUserUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.WorkProductFlowUIController;
import com.jili.ubert.clientapi.ClientAPI;
import com.jili.ubert.code.server2client.MsgResult;
import com.panemu.tiwulfx.control.DetachableTabPane;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author dragon
 */
public class ShowAllTable extends Application {

    static ClientAPI clientapi;static DetachableTabPane tabpane;
    private static final Log log = LogFactory.getLog(ShowAllTable.class);
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        log.debug("开始运行");
        clientapi = new ClientAPI();
        MsgResult ret;
        ret = clientapi.ConnectTradeServer("127.0.0.1", 3773);
        if (ret.isSuccess()) {
            log.info("登陆成功，开始界面");
  //          clientapi.getVerlidCode(handler);
  //          login_Version.setText("连接服务器成功");
        } else {
            log.info("登陆失败");
   //         login_Version.setText("连接服务器失败");
        }
        tabpane = new DetachableTabPane();
        tabpane.setSceneFactory(new Callback<DetachableTabPane, Scene>() {
			@Override
			public Scene call(DetachableTabPane p) {
				//create your scene here
				Label lbl = new Label("TiwulFX Demo 2.0");
				lbl.setId("app-title");
				p.setPrefSize(600, 600);
				VBox vbox = new VBox();
//			hbox
				HBox hbox = new HBox();
				hbox.setAlignment(Pos.CENTER_RIGHT);
				hbox.setHgrow(p, Priority.ALWAYS);
				hbox.setPrefHeight(-1.0);
				hbox.setPrefWidth(-1.0);
				hbox.setSpacing(10.0);
				hbox.setPadding(new Insets(10));
				hbox.getStyleClass().add("top-panel");
				hbox.getChildren().add(lbl);
				
				vbox.getChildren().add(hbox);
				vbox.getChildren().add(p);
				VBox.setVgrow(p, Priority.ALWAYS);
				Scene scene = new Scene(vbox);
				return scene;
			}
		});  
        /*
        ShowAllTable ss =new ShowAllTable();
        AccountBailUIController ta = new AccountBailUIController(clientapi);
        Pane pane = new Pane();
        pane.getChildren().add(ta.getUINode());
        ShowAllTable.showAsTab(pane,"我是测试");
        */
    //    antoTabUI();
        showTable();
        log.info("我是");
        Scene scene1 = new Scene(tabpane);
        stage.setScene(scene1);
        stage.initStyle(StageStyle.DECORATED);
        log.info(stage.getStyle().getClass());
        stage.show();
    }
    public void showTable(){
        OrderExecuteProgressUIController re = new OrderExecuteProgressUIController(ShowAllTable.clientapi);
        UINode.showAsTab(tabpane, re.getUINode(),I18N.getString("tablename.OrderExecuteProgress"));
    }
    public void antoTabUI(){
        ClearingProtfolioposUIController clearingprotfoliopos = new ClearingProtfolioposUIController(clientapi);UINode.showAsTab(tabpane, clearingprotfoliopos.getUINode(),I18N.getString("tablename.ClearingProtfoliopos"));
AccountBailUIController accountbail = new AccountBailUIController(clientapi);UINode.showAsTab(tabpane, accountbail.getUINode(),I18N.getString("tablename.AccountBail"));
AccountFareUIController accountfare = new AccountFareUIController(clientapi);UINode.showAsTab(tabpane, accountfare.getUINode(),I18N.getString("tablename.AccountFare"));
AccountIcodeUIController accounticode = new AccountIcodeUIController(clientapi);UINode.showAsTab(tabpane, accounticode.getUINode(),I18N.getString("tablename.AccountIcode"));
AccountInfoUIController accountinfo = new AccountInfoUIController(clientapi);UINode.showAsTab(tabpane, accountinfo.getUINode(),I18N.getString("tablename.AccountInfo"));
AccountInvestUIController accountinvest = new AccountInvestUIController(clientapi);UINode.showAsTab(tabpane, accountinvest.getUINode(),I18N.getString("tablename.AccountInvest"));
AlterOrderUIController alterorder = new AlterOrderUIController(clientapi);UINode.showAsTab(tabpane, alterorder.getUINode(),I18N.getString("tablename.AlterOrder"));
BailTempUIController bailtemp = new BailTempUIController(clientapi);UINode.showAsTab(tabpane, bailtemp.getUINode(),I18N.getString("tablename.BailTemp"));
BailTempObjUIController bailtempobj = new BailTempObjUIController(clientapi);UINode.showAsTab(tabpane, bailtempobj.getUINode(),I18N.getString("tablename.BailTempObj"));
CancelOrderUIController cancelorder = new CancelOrderUIController(clientapi);UINode.showAsTab(tabpane, cancelorder.getUINode(),I18N.getString("tablename.CancelOrder"));
ClearingCurrencyRatesUIController clearingcurrencyrates = new ClearingCurrencyRatesUIController(clientapi);UINode.showAsTab(tabpane, clearingcurrencyrates.getUINode(),I18N.getString("tablename.ClearingCurrencyRates"));
ClearingDayBookUIController clearingdaybook = new ClearingDayBookUIController(clientapi);UINode.showAsTab(tabpane, clearingdaybook.getUINode(),I18N.getString("tablename.ClearingDayBook"));
ClearingExRightUIController clearingexright = new ClearingExRightUIController(clientapi);UINode.showAsTab(tabpane, clearingexright.getUINode(),I18N.getString("tablename.ClearingExRight"));
ClearingPriceUIController clearingprice = new ClearingPriceUIController(clientapi);UINode.showAsTab(tabpane, clearingprice.getUINode(),I18N.getString("tablename.ClearingPrice"));
FareTempUIController faretemp = new FareTempUIController(clientapi);UINode.showAsTab(tabpane, faretemp.getUINode(),I18N.getString("tablename.FareTemp"));
FareTempeObjUIController faretempeobj = new FareTempeObjUIController(clientapi);UINode.showAsTab(tabpane, faretempeobj.getUINode(),I18N.getString("tablename.FareTempeObj"));
FundStateUIController fundstate = new FundStateUIController(clientapi);UINode.showAsTab(tabpane, fundstate.getUINode(),I18N.getString("tablename.FundState"));
FundTransferUIController fundtransfer = new FundTransferUIController(clientapi);UINode.showAsTab(tabpane, fundtransfer.getUINode(),I18N.getString("tablename.FundTransfer"));
HistoryClearingCurrencyRatesUIController historyclearingcurrencyrates = new HistoryClearingCurrencyRatesUIController(clientapi);UINode.showAsTab(tabpane, historyclearingcurrencyrates.getUINode(),I18N.getString("tablename.HistoryClearingCurrencyRates"));
HistoryIOPVAccountUIController historyiopvaccount = new HistoryIOPVAccountUIController(clientapi);UINode.showAsTab(tabpane, historyiopvaccount.getUINode(),I18N.getString("tablename.HistoryIOPVAccount"));
HistoryIOPVProductUIController historyiopvproduct = new HistoryIOPVProductUIController(clientapi);UINode.showAsTab(tabpane, historyiopvproduct.getUINode(),I18N.getString("tablename.HistoryIOPVProduct"));
HistoryIOPVProductUnitUIController historyiopvproductunit = new HistoryIOPVProductUnitUIController(clientapi);UINode.showAsTab(tabpane, historyiopvproductunit.getUINode(),I18N.getString("tablename.HistoryIOPVProductUnit"));
HistoryLogLoginInfoUIController historyloglogininfo = new HistoryLogLoginInfoUIController(clientapi);UINode.showAsTab(tabpane, historyloglogininfo.getUINode(),I18N.getString("tablename.HistoryLogLoginInfo"));
HistoryObjExRightInfoUIController historyobjexrightinfo = new HistoryObjExRightInfoUIController(clientapi);UINode.showAsTab(tabpane, historyobjexrightinfo.getUINode(),I18N.getString("tablename.HistoryObjExRightInfo"));
HistoryTradeFundTransferUIController historytradefundtransfer = new HistoryTradeFundTransferUIController(clientapi);UINode.showAsTab(tabpane, historytradefundtransfer.getUINode(),I18N.getString("tablename.HistoryTradeFundTransfer"));
HistoryTradesetCalendarUIController historytradesetcalendar = new HistoryTradesetCalendarUIController(clientapi);UINode.showAsTab(tabpane, historytradesetcalendar.getUINode(),I18N.getString("tablename.HistoryTradesetCalendar"));
IOPVAccountUIController iopvaccount = new IOPVAccountUIController(clientapi);UINode.showAsTab(tabpane, iopvaccount.getUINode(),I18N.getString("tablename.IOPVAccount"));
IOPVProductUIController iopvproduct = new IOPVProductUIController(clientapi);UINode.showAsTab(tabpane, iopvproduct.getUINode(),I18N.getString("tablename.IOPVProduct"));
IOPVProductUnitUIController iopvproductunit = new IOPVProductUnitUIController(clientapi);UINode.showAsTab(tabpane, iopvproductunit.getUINode(),I18N.getString("tablename.IOPVProductUnit"));
LogLoginInfoUIController loglogininfo = new LogLoginInfoUIController(clientapi);UINode.showAsTab(tabpane, loglogininfo.getUINode(),I18N.getString("tablename.LogLoginInfo"));
NewOrderUIController neworder = new NewOrderUIController(clientapi);UINode.showAsTab(tabpane, neworder.getUINode(),I18N.getString("tablename.NewOrder"));
ObjClassCodeUIController objclasscode = new ObjClassCodeUIController(clientapi);UINode.showAsTab(tabpane, objclasscode.getUINode(),I18N.getString("tablename.ObjClassCode"));
ObjClassCodeAtrrUIController objclasscodeatrr = new ObjClassCodeAtrrUIController(clientapi);UINode.showAsTab(tabpane, objclasscodeatrr.getUINode(),I18N.getString("tablename.ObjClassCodeAtrr"));
ObjETFUIController objetf = new ObjETFUIController(clientapi);UINode.showAsTab(tabpane, objetf.getUINode(),I18N.getString("tablename.ObjETF"));
ObjETFListUIController objetflist = new ObjETFListUIController(clientapi);UINode.showAsTab(tabpane, objetflist.getUINode(),I18N.getString("tablename.ObjETFList"));
ObjExRightInfoUIController objexrightinfo = new ObjExRightInfoUIController(clientapi);UINode.showAsTab(tabpane, objexrightinfo.getUINode(),I18N.getString("tablename.ObjExRightInfo"));
ObjIndexUIController objindex = new ObjIndexUIController(clientapi);UINode.showAsTab(tabpane, objindex.getUINode(),I18N.getString("tablename.ObjIndex"));
ObjIndexListUIController objindexlist = new ObjIndexListUIController(clientapi);UINode.showAsTab(tabpane, objindexlist.getUINode(),I18N.getString("tablename.ObjIndexList"));
ObjInfoUIController objinfo = new ObjInfoUIController(clientapi);UINode.showAsTab(tabpane, objinfo.getUINode(),I18N.getString("tablename.ObjInfo"));
ObjNewStockUIController objnewstock = new ObjNewStockUIController(clientapi);UINode.showAsTab(tabpane, objnewstock.getUINode(),I18N.getString("tablename.ObjNewStock"));
ObjPriceTickUIController objpricetick = new ObjPriceTickUIController(clientapi);UINode.showAsTab(tabpane, objpricetick.getUINode(),I18N.getString("tablename.ObjPriceTick"));
ObjProtfolioUIController objprotfolio = new ObjProtfolioUIController(clientapi);UINode.showAsTab(tabpane, objprotfolio.getUINode(),I18N.getString("tablename.ObjProtfolio"));
ObjProtfolioListUIController objprotfoliolist = new ObjProtfolioListUIController(clientapi);UINode.showAsTab(tabpane, objprotfoliolist.getUINode(),I18N.getString("tablename.ObjProtfolioList"));
ObjStructuredFundBaseUIController objstructuredfundbase = new ObjStructuredFundBaseUIController(clientapi);UINode.showAsTab(tabpane, objstructuredfundbase.getUINode(),I18N.getString("tablename.ObjStructuredFundBase"));
ObjStructuredFundPartUIController objstructuredfundpart = new ObjStructuredFundPartUIController(clientapi);UINode.showAsTab(tabpane, objstructuredfundpart.getUINode(),I18N.getString("tablename.ObjStructuredFundPart"));
ObjUnderlyingCodeUIController objunderlyingcode = new ObjUnderlyingCodeUIController(clientapi);UINode.showAsTab(tabpane, objunderlyingcode.getUINode(),I18N.getString("tablename.ObjUnderlyingCode"));
OrderExecuteProgressUIController orderexecuteprogress = new OrderExecuteProgressUIController(clientapi);UINode.showAsTab(tabpane, orderexecuteprogress.getUINode(),I18N.getString("tablename.OrderExecuteProgress"));
PosDetailUIController posdetail = new PosDetailUIController(clientapi);UINode.showAsTab(tabpane, posdetail.getUINode(),I18N.getString("tablename.PosDetail"));
ProductAccountUIController productaccount = new ProductAccountUIController(clientapi);UINode.showAsTab(tabpane, productaccount.getUINode(),I18N.getString("tablename.ProductAccount"));
ProductInfoUIController productinfo = new ProductInfoUIController(clientapi);UINode.showAsTab(tabpane, productinfo.getUINode(),I18N.getString("tablename.ProductInfo"));
ProductUnitUIController productunit = new ProductUnitUIController(clientapi);UINode.showAsTab(tabpane, productunit.getUINode(),I18N.getString("tablename.ProductUnit"));
RecentClearingAccountBailUIController recentclearingaccountbail = new RecentClearingAccountBailUIController(clientapi);UINode.showAsTab(tabpane, recentclearingaccountbail.getUINode(),I18N.getString("tablename.RecentClearingAccountBail"));
RecentClearingAccountFareUIController recentclearingaccountfare = new RecentClearingAccountFareUIController(clientapi);UINode.showAsTab(tabpane, recentclearingaccountfare.getUINode(),I18N.getString("tablename.RecentClearingAccountFare"));
RecentClearingDayBookUIController recentclearingdaybook = new RecentClearingDayBookUIController(clientapi);UINode.showAsTab(tabpane, recentclearingdaybook.getUINode(),I18N.getString("tablename.RecentClearingDayBook"));
RecentClearingExRightUIController recentclearingexright = new RecentClearingExRightUIController(clientapi);UINode.showAsTab(tabpane, recentclearingexright.getUINode(),I18N.getString("tablename.RecentClearingExRight"));
RecentClearingFundStateUIController recentclearingfundstate = new RecentClearingFundStateUIController(clientapi);UINode.showAsTab(tabpane, recentclearingfundstate.getUINode(),I18N.getString("tablename.RecentClearingFundState"));
RecentClearingPosDetailUIController recentclearingposdetail = new RecentClearingPosDetailUIController(clientapi);UINode.showAsTab(tabpane, recentclearingposdetail.getUINode(),I18N.getString("tablename.RecentClearingPosDetail"));
RecentClearingPriceUIController recentclearingprice = new RecentClearingPriceUIController(clientapi);UINode.showAsTab(tabpane, recentclearingprice.getUINode(),I18N.getString("tablename.RecentClearingPrice"));
RecentClearingProtfolioposUIController recentclearingprotfoliopos = new RecentClearingProtfolioposUIController(clientapi);UINode.showAsTab(tabpane, recentclearingprotfoliopos.getUINode(),I18N.getString("tablename.RecentClearingProtfoliopos"));
RecentObjETFUIController recentobjetf = new RecentObjETFUIController(clientapi);UINode.showAsTab(tabpane, recentobjetf.getUINode(),I18N.getString("tablename.RecentObjETF"));
RecentObjETFListUIController recentobjetflist = new RecentObjETFListUIController(clientapi);UINode.showAsTab(tabpane, recentobjetflist.getUINode(),I18N.getString("tablename.RecentObjETFList"));
RecentObjIndexUIController recentobjindex = new RecentObjIndexUIController(clientapi);UINode.showAsTab(tabpane, recentobjindex.getUINode(),I18N.getString("tablename.RecentObjIndex"));
RecentObjIndexListUIController recentobjindexlist = new RecentObjIndexListUIController(clientapi);UINode.showAsTab(tabpane, recentobjindexlist.getUINode(),I18N.getString("tablename.RecentObjIndexList"));
RecentTradeAlterOrderUIController recenttradealterorder = new RecentTradeAlterOrderUIController(clientapi);UINode.showAsTab(tabpane, recenttradealterorder.getUINode(),I18N.getString("tablename.RecentTradeAlterOrder"));
RecentTradeCancelOrderUIController recenttradecancelorder = new RecentTradeCancelOrderUIController(clientapi);UINode.showAsTab(tabpane, recenttradecancelorder.getUINode(),I18N.getString("tablename.RecentTradeCancelOrder"));
RecentTradeExecuteOrderUIController recenttradeexecuteorder = new RecentTradeExecuteOrderUIController(clientapi);UINode.showAsTab(tabpane, recenttradeexecuteorder.getUINode(),I18N.getString("tablename.RecentTradeExecuteOrder"));
RecentTradeNewOrderUIController recenttradeneworder = new RecentTradeNewOrderUIController(clientapi);UINode.showAsTab(tabpane, recenttradeneworder.getUINode(),I18N.getString("tablename.RecentTradeNewOrder"));
RecentTradeReportDetailUIController recenttradereportdetail = new RecentTradeReportDetailUIController(clientapi);UINode.showAsTab(tabpane, recenttradereportdetail.getUINode(),I18N.getString("tablename.RecentTradeReportDetail"));
RiskCtrlAssetsUIController riskctrlassets = new RiskCtrlAssetsUIController(clientapi);UINode.showAsTab(tabpane, riskctrlassets.getUINode(),I18N.getString("tablename.RiskCtrlAssets"));
RiskIndexUIController riskindex = new RiskIndexUIController(clientapi);UINode.showAsTab(tabpane, riskindex.getUINode(),I18N.getString("tablename.RiskIndex"));
RiskSetsUIController risksets = new RiskSetsUIController(clientapi);UINode.showAsTab(tabpane, risksets.getUINode(),I18N.getString("tablename.RiskSets"));
SafeForbidLoginUIController safeforbidlogin = new SafeForbidLoginUIController(clientapi);UINode.showAsTab(tabpane, safeforbidlogin.getUINode(),I18N.getString("tablename.SafeForbidLogin"));
SafeOtherAccessUIController safeotheraccess = new SafeOtherAccessUIController(clientapi);UINode.showAsTab(tabpane, safeotheraccess.getUINode(),I18N.getString("tablename.SafeOtherAccess"));
TraceAccountBailUIController traceaccountbail = new TraceAccountBailUIController(clientapi);UINode.showAsTab(tabpane, traceaccountbail.getUINode(),I18N.getString("tablename.TraceAccountBail"));
TraceAccountFareUIController traceaccountfare = new TraceAccountFareUIController(clientapi);UINode.showAsTab(tabpane, traceaccountfare.getUINode(),I18N.getString("tablename.TraceAccountFare"));
TraceAccountIcodeUIController traceaccounticode = new TraceAccountIcodeUIController(clientapi);UINode.showAsTab(tabpane, traceaccounticode.getUINode(),I18N.getString("tablename.TraceAccountIcode"));
TraceAccountInfoUIController traceaccountinfo = new TraceAccountInfoUIController(clientapi);UINode.showAsTab(tabpane, traceaccountinfo.getUINode(),I18N.getString("tablename.TraceAccountInfo"));
TraceAccountInvestUIController traceaccountinvest = new TraceAccountInvestUIController(clientapi);UINode.showAsTab(tabpane, traceaccountinvest.getUINode(),I18N.getString("tablename.TraceAccountInvest"));
TraceBailTempUIController tracebailtemp = new TraceBailTempUIController(clientapi);UINode.showAsTab(tabpane, tracebailtemp.getUINode(),I18N.getString("tablename.TraceBailTemp"));
TraceBailTempObjUIController tracebailtempobj = new TraceBailTempObjUIController(clientapi);UINode.showAsTab(tabpane, tracebailtempobj.getUINode(),I18N.getString("tablename.TraceBailTempObj"));
TraceFareTempUIController tracefaretemp = new TraceFareTempUIController(clientapi);UINode.showAsTab(tabpane, tracefaretemp.getUINode(),I18N.getString("tablename.TraceFareTemp"));
TraceFareTempeObjUIController tracefaretempeobj = new TraceFareTempeObjUIController(clientapi);UINode.showAsTab(tabpane, tracefaretempeobj.getUINode(),I18N.getString("tablename.TraceFareTempeObj"));
TraceObjInfoUIController traceobjinfo = new TraceObjInfoUIController(clientapi);UINode.showAsTab(tabpane, traceobjinfo.getUINode(),I18N.getString("tablename.TraceObjInfo"));
TraceObjProtfolioUIController traceobjprotfolio = new TraceObjProtfolioUIController(clientapi);UINode.showAsTab(tabpane, traceobjprotfolio.getUINode(),I18N.getString("tablename.TraceObjProtfolio"));
TraceObjProtfolioListUIController traceobjprotfoliolist = new TraceObjProtfolioListUIController(clientapi);UINode.showAsTab(tabpane, traceobjprotfoliolist.getUINode(),I18N.getString("tablename.TraceObjProtfolioList"));
TraceObjUnderlyingUIController traceobjunderlying = new TraceObjUnderlyingUIController(clientapi);UINode.showAsTab(tabpane, traceobjunderlying.getUINode(),I18N.getString("tablename.TraceObjUnderlying"));
TraceProductAccountUIController traceproductaccount = new TraceProductAccountUIController(clientapi);UINode.showAsTab(tabpane, traceproductaccount.getUINode(),I18N.getString("tablename.TraceProductAccount"));
TraceProductInfoUIController traceproductinfo = new TraceProductInfoUIController(clientapi);UINode.showAsTab(tabpane, traceproductinfo.getUINode(),I18N.getString("tablename.TraceProductInfo"));
TraceProductUnitUIController traceproductunit = new TraceProductUnitUIController(clientapi);UINode.showAsTab(tabpane, traceproductunit.getUINode(),I18N.getString("tablename.TraceProductUnit"));
TraceRiskCtrlAssetsUIController traceriskctrlassets = new TraceRiskCtrlAssetsUIController(clientapi);UINode.showAsTab(tabpane, traceriskctrlassets.getUINode(),I18N.getString("tablename.TraceRiskCtrlAssets"));
TraceRiskSetsUIController tracerisksets = new TraceRiskSetsUIController(clientapi);UINode.showAsTab(tabpane, tracerisksets.getUINode(),I18N.getString("tablename.TraceRiskSets"));
TraceSafeForbidLoginUIController tracesafeforbidlogin = new TraceSafeForbidLoginUIController(clientapi);UINode.showAsTab(tabpane, tracesafeforbidlogin.getUINode(),I18N.getString("tablename.TraceSafeForbidLogin"));
TraceSafeOtherAccessUIController tracesafeotheraccess = new TraceSafeOtherAccessUIController(clientapi);UINode.showAsTab(tabpane, tracesafeotheraccess.getUINode(),I18N.getString("tablename.TraceSafeOtherAccess"));
TraceTradesetExchangeLimitUIController tracetradesetexchangelimit = new TraceTradesetExchangeLimitUIController(clientapi);UINode.showAsTab(tabpane, tracetradesetexchangelimit.getUINode(),I18N.getString("tablename.TraceTradesetExchangeLimit"));
TraceUserAccessUIController traceuseraccess = new TraceUserAccessUIController(clientapi);UINode.showAsTab(tabpane, traceuseraccess.getUINode(),I18N.getString("tablename.TraceUserAccess"));
TraceUserAccountUIController traceuseraccount = new TraceUserAccountUIController(clientapi);UINode.showAsTab(tabpane, traceuseraccount.getUINode(),I18N.getString("tablename.TraceUserAccount"));
TraceUserInfoUIController traceuserinfo = new TraceUserInfoUIController(clientapi);UINode.showAsTab(tabpane, traceuserinfo.getUINode(),I18N.getString("tablename.TraceUserInfo"));
TraceUserPrivateInfoUIController traceuserprivateinfo = new TraceUserPrivateInfoUIController(clientapi);UINode.showAsTab(tabpane, traceuserprivateinfo.getUINode(),I18N.getString("tablename.TraceUserPrivateInfo"));
TraceWorkFlowPathUIController traceworkflowpath = new TraceWorkFlowPathUIController(clientapi);UINode.showAsTab(tabpane, traceworkflowpath.getUINode(),I18N.getString("tablename.TraceWorkFlowPath"));
TraceWorkGroupUIController traceworkgroup = new TraceWorkGroupUIController(clientapi);UINode.showAsTab(tabpane, traceworkgroup.getUINode(),I18N.getString("tablename.TraceWorkGroup"));
TraceWorkGroupUserUIController traceworkgroupuser = new TraceWorkGroupUserUIController(clientapi);UINode.showAsTab(tabpane, traceworkgroupuser.getUINode(),I18N.getString("tablename.TraceWorkGroupUser"));
TraceWorkProductFlowUIController traceworkproductflow = new TraceWorkProductFlowUIController(clientapi);UINode.showAsTab(tabpane, traceworkproductflow.getUINode(),I18N.getString("tablename.TraceWorkProductFlow"));
TradeReportDetailUIController tradereportdetail = new TradeReportDetailUIController(clientapi);UINode.showAsTab(tabpane, tradereportdetail.getUINode(),I18N.getString("tablename.TradeReportDetail"));
TradesetBrokerUIController tradesetbroker = new TradesetBrokerUIController(clientapi);UINode.showAsTab(tabpane, tradesetbroker.getUINode(),I18N.getString("tablename.TradesetBroker"));
TradesetCalendarUIController tradesetcalendar = new TradesetCalendarUIController(clientapi);UINode.showAsTab(tabpane, tradesetcalendar.getUINode(),I18N.getString("tablename.TradesetCalendar"));
TradesetExchangeLimitUIController tradesetexchangelimit = new TradesetExchangeLimitUIController(clientapi);UINode.showAsTab(tabpane, tradesetexchangelimit.getUINode(),I18N.getString("tablename.TradesetExchangeLimit"));
TradesetPipUIController tradesetpip = new TradesetPipUIController(clientapi);UINode.showAsTab(tabpane, tradesetpip.getUINode(),I18N.getString("tablename.TradesetPip"));
//TradeSimOrderqueneUIController tradesimorderquene = new TradeSimOrderqueneUIController(clientapi);UINode.showAsTab(tabpane, tradesimorderquene.getUINode(),I18N.getString("tablename.TradeSimOrderquene"));
UserAccessUIController useraccess = new UserAccessUIController(clientapi);UINode.showAsTab(tabpane, useraccess.getUINode(),I18N.getString("tablename.UserAccess"));
UserAccountUIController useraccount = new UserAccountUIController(clientapi);UINode.showAsTab(tabpane, useraccount.getUINode(),I18N.getString("tablename.UserAccount"));
UserInfoUIController userinfo = new UserInfoUIController(clientapi);UINode.showAsTab(tabpane, userinfo.getUINode(),I18N.getString("tablename.UserInfo"));
UserPrivateInfoUIController userprivateinfo = new UserPrivateInfoUIController(clientapi);UINode.showAsTab(tabpane, userprivateinfo.getUINode(),I18N.getString("tablename.UserPrivateInfo"));
WorkFlowPathUIController workflowpath = new WorkFlowPathUIController(clientapi);UINode.showAsTab(tabpane, workflowpath.getUINode(),I18N.getString("tablename.WorkFlowPath"));
WorkGroupUIController workgroup = new WorkGroupUIController(clientapi);UINode.showAsTab(tabpane, workgroup.getUINode(),I18N.getString("tablename.WorkGroup"));
WorkGroupUserUIController workgroupuser = new WorkGroupUserUIController(clientapi);UINode.showAsTab(tabpane, workgroupuser.getUINode(),I18N.getString("tablename.WorkGroupUser"));
WorkProductFlowUIController workproductflow = new WorkProductFlowUIController(clientapi);UINode.showAsTab(tabpane, workproductflow.getUINode(),I18N.getString("tablename.WorkProductFlow"));

    }
    public void antoTab(){
        ClearingProtfolioposUIController clearingprotfoliopos = new ClearingProtfolioposUIController(clientapi);Pane clearingprotfolioposP = new Pane();clearingprotfolioposP.getChildren().add(clearingprotfoliopos.getUINode());ShowAllTable.showAsTab(clearingprotfolioposP,I18N.getString("tablename.ClearingProtfoliopos"));
FareTempUIController faretemp = new FareTempUIController(clientapi);Pane faretempP = new Pane();faretempP.getChildren().add(faretemp.getUINode());ShowAllTable.showAsTab(faretempP,I18N.getString("tablename.FareTemp"));
FareTempeObjUIController faretempeobj = new FareTempeObjUIController(clientapi);Pane faretempeobjP = new Pane();faretempeobjP.getChildren().add(faretempeobj.getUINode());ShowAllTable.showAsTab(faretempeobjP,I18N.getString("tablename.FareTempeObj"));
HistoryClearingCurrencyRatesUIController historyclearingcurrencyrates = new HistoryClearingCurrencyRatesUIController(clientapi);Pane historyclearingcurrencyratesP = new Pane();historyclearingcurrencyratesP.getChildren().add(historyclearingcurrencyrates.getUINode());ShowAllTable.showAsTab(historyclearingcurrencyratesP,I18N.getString("tablename.HistoryClearingCurrencyRates"));
HistoryIOPVAccountUIController historyiopvaccount = new HistoryIOPVAccountUIController(clientapi);Pane historyiopvaccountP = new Pane();historyiopvaccountP.getChildren().add(historyiopvaccount.getUINode());ShowAllTable.showAsTab(historyiopvaccountP,I18N.getString("tablename.HistoryIOPVAccount"));
HistoryIOPVProductUIController historyiopvproduct = new HistoryIOPVProductUIController(clientapi);Pane historyiopvproductP = new Pane();historyiopvproductP.getChildren().add(historyiopvproduct.getUINode());ShowAllTable.showAsTab(historyiopvproductP,I18N.getString("tablename.HistoryIOPVProduct"));
HistoryIOPVProductUnitUIController historyiopvproductunit = new HistoryIOPVProductUnitUIController(clientapi);Pane historyiopvproductunitP = new Pane();historyiopvproductunitP.getChildren().add(historyiopvproductunit.getUINode());ShowAllTable.showAsTab(historyiopvproductunitP,I18N.getString("tablename.HistoryIOPVProductUnit"));
HistoryLogLoginInfoUIController historyloglogininfo = new HistoryLogLoginInfoUIController(clientapi);Pane historyloglogininfoP = new Pane();historyloglogininfoP.getChildren().add(historyloglogininfo.getUINode());ShowAllTable.showAsTab(historyloglogininfoP,I18N.getString("tablename.HistoryLogLoginInfo"));
HistoryObjExRightInfoUIController historyobjexrightinfo = new HistoryObjExRightInfoUIController(clientapi);Pane historyobjexrightinfoP = new Pane();historyobjexrightinfoP.getChildren().add(historyobjexrightinfo.getUINode());ShowAllTable.showAsTab(historyobjexrightinfoP,I18N.getString("tablename.HistoryObjExRightInfo"));
HistoryTradeFundTransferUIController historytradefundtransfer = new HistoryTradeFundTransferUIController(clientapi);Pane historytradefundtransferP = new Pane();historytradefundtransferP.getChildren().add(historytradefundtransfer.getUINode());ShowAllTable.showAsTab(historytradefundtransferP,I18N.getString("tablename.HistoryTradeFundTransfer"));
HistoryTradesetCalendarUIController historytradesetcalendar = new HistoryTradesetCalendarUIController(clientapi);Pane historytradesetcalendarP = new Pane();historytradesetcalendarP.getChildren().add(historytradesetcalendar.getUINode());ShowAllTable.showAsTab(historytradesetcalendarP,I18N.getString("tablename.HistoryTradesetCalendar"));
IOPVAccountUIController iopvaccount = new IOPVAccountUIController(clientapi);Pane iopvaccountP = new Pane();iopvaccountP.getChildren().add(iopvaccount.getUINode());ShowAllTable.showAsTab(iopvaccountP,I18N.getString("tablename.IOPVAccount"));
IOPVProductUIController iopvproduct = new IOPVProductUIController(clientapi);Pane iopvproductP = new Pane();iopvproductP.getChildren().add(iopvproduct.getUINode());ShowAllTable.showAsTab(iopvproductP,I18N.getString("tablename.IOPVProduct"));
IOPVProductUnitUIController iopvproductunit = new IOPVProductUnitUIController(clientapi);Pane iopvproductunitP = new Pane();iopvproductunitP.getChildren().add(iopvproductunit.getUINode());ShowAllTable.showAsTab(iopvproductunitP,I18N.getString("tablename.IOPVProductUnit"));
LogLoginInfoUIController loglogininfo = new LogLoginInfoUIController(clientapi);Pane loglogininfoP = new Pane();loglogininfoP.getChildren().add(loglogininfo.getUINode());ShowAllTable.showAsTab(loglogininfoP,I18N.getString("tablename.LogLoginInfo"));
ObjClassCodeUIController objclasscode = new ObjClassCodeUIController(clientapi);Pane objclasscodeP = new Pane();objclasscodeP.getChildren().add(objclasscode.getUINode());ShowAllTable.showAsTab(objclasscodeP,I18N.getString("tablename.ObjClassCode"));
ObjClassCodeAtrrUIController objclasscodeatrr = new ObjClassCodeAtrrUIController(clientapi);Pane objclasscodeatrrP = new Pane();objclasscodeatrrP.getChildren().add(objclasscodeatrr.getUINode());ShowAllTable.showAsTab(objclasscodeatrrP,I18N.getString("tablename.ObjClassCodeAtrr"));
ObjETFUIController objetf = new ObjETFUIController(clientapi);Pane objetfP = new Pane();objetfP.getChildren().add(objetf.getUINode());ShowAllTable.showAsTab(objetfP,I18N.getString("tablename.ObjETF"));
ObjETFListUIController objetflist = new ObjETFListUIController(clientapi);Pane objetflistP = new Pane();objetflistP.getChildren().add(objetflist.getUINode());ShowAllTable.showAsTab(objetflistP,I18N.getString("tablename.ObjETFList"));
ObjExRightInfoUIController objexrightinfo = new ObjExRightInfoUIController(clientapi);Pane objexrightinfoP = new Pane();objexrightinfoP.getChildren().add(objexrightinfo.getUINode());ShowAllTable.showAsTab(objexrightinfoP,I18N.getString("tablename.ObjExRightInfo"));
ObjIndexUIController objindex = new ObjIndexUIController(clientapi);Pane objindexP = new Pane();objindexP.getChildren().add(objindex.getUINode());ShowAllTable.showAsTab(objindexP,I18N.getString("tablename.ObjIndex"));
ObjIndexListUIController objindexlist = new ObjIndexListUIController(clientapi);Pane objindexlistP = new Pane();objindexlistP.getChildren().add(objindexlist.getUINode());ShowAllTable.showAsTab(objindexlistP,I18N.getString("tablename.ObjIndexList"));
ObjInfoUIController objinfo = new ObjInfoUIController(clientapi);Pane objinfoP = new Pane();objinfoP.getChildren().add(objinfo.getUINode());ShowAllTable.showAsTab(objinfoP,I18N.getString("tablename.ObjInfo"));
ObjNewStockUIController objnewstock = new ObjNewStockUIController(clientapi);Pane objnewstockP = new Pane();objnewstockP.getChildren().add(objnewstock.getUINode());ShowAllTable.showAsTab(objnewstockP,I18N.getString("tablename.ObjNewStock"));
ObjPriceTickUIController objpricetick = new ObjPriceTickUIController(clientapi);Pane objpricetickP = new Pane();objpricetickP.getChildren().add(objpricetick.getUINode());ShowAllTable.showAsTab(objpricetickP,I18N.getString("tablename.ObjPriceTick"));
ObjProtfolioUIController objprotfolio = new ObjProtfolioUIController(clientapi);Pane objprotfolioP = new Pane();objprotfolioP.getChildren().add(objprotfolio.getUINode());ShowAllTable.showAsTab(objprotfolioP,I18N.getString("tablename.ObjProtfolio"));
ObjProtfolioListUIController objprotfoliolist = new ObjProtfolioListUIController(clientapi);Pane objprotfoliolistP = new Pane();objprotfoliolistP.getChildren().add(objprotfoliolist.getUINode());ShowAllTable.showAsTab(objprotfoliolistP,I18N.getString("tablename.ObjProtfolioList"));
ObjStructuredFundBaseUIController objstructuredfundbase = new ObjStructuredFundBaseUIController(clientapi);Pane objstructuredfundbaseP = new Pane();objstructuredfundbaseP.getChildren().add(objstructuredfundbase.getUINode());ShowAllTable.showAsTab(objstructuredfundbaseP,I18N.getString("tablename.ObjStructuredFundBase"));
ObjStructuredFundPartUIController objstructuredfundpart = new ObjStructuredFundPartUIController(clientapi);Pane objstructuredfundpartP = new Pane();objstructuredfundpartP.getChildren().add(objstructuredfundpart.getUINode());ShowAllTable.showAsTab(objstructuredfundpartP,I18N.getString("tablename.ObjStructuredFundPart"));
ObjUnderlyingCodeUIController objunderlyingcode = new ObjUnderlyingCodeUIController(clientapi);Pane objunderlyingcodeP = new Pane();objunderlyingcodeP.getChildren().add(objunderlyingcode.getUINode());ShowAllTable.showAsTab(objunderlyingcodeP,I18N.getString("tablename.ObjUnderlyingCode"));
ProductAccountUIController productaccount = new ProductAccountUIController(clientapi);Pane productaccountP = new Pane();productaccountP.getChildren().add(productaccount.getUINode());ShowAllTable.showAsTab(productaccountP,I18N.getString("tablename.ProductAccount"));
ProductInfoUIController productinfo = new ProductInfoUIController(clientapi);Pane productinfoP = new Pane();productinfoP.getChildren().add(productinfo.getUINode());ShowAllTable.showAsTab(productinfoP,I18N.getString("tablename.ProductInfo"));
ProductUnitUIController productunit = new ProductUnitUIController(clientapi);Pane productunitP = new Pane();productunitP.getChildren().add(productunit.getUINode());ShowAllTable.showAsTab(productunitP,I18N.getString("tablename.ProductUnit"));
RecentClearingAccountBailUIController recentclearingaccountbail = new RecentClearingAccountBailUIController(clientapi);Pane recentclearingaccountbailP = new Pane();recentclearingaccountbailP.getChildren().add(recentclearingaccountbail.getUINode());ShowAllTable.showAsTab(recentclearingaccountbailP,I18N.getString("tablename.RecentClearingAccountBail"));
RecentClearingAccountFareUIController recentclearingaccountfare = new RecentClearingAccountFareUIController(clientapi);Pane recentclearingaccountfareP = new Pane();recentclearingaccountfareP.getChildren().add(recentclearingaccountfare.getUINode());ShowAllTable.showAsTab(recentclearingaccountfareP,I18N.getString("tablename.RecentClearingAccountFare"));
RecentClearingDayBookUIController recentclearingdaybook = new RecentClearingDayBookUIController(clientapi);Pane recentclearingdaybookP = new Pane();recentclearingdaybookP.getChildren().add(recentclearingdaybook.getUINode());ShowAllTable.showAsTab(recentclearingdaybookP,I18N.getString("tablename.RecentClearingDayBook"));
RecentClearingExRightUIController recentclearingexright = new RecentClearingExRightUIController(clientapi);Pane recentclearingexrightP = new Pane();recentclearingexrightP.getChildren().add(recentclearingexright.getUINode());ShowAllTable.showAsTab(recentclearingexrightP,I18N.getString("tablename.RecentClearingExRight"));
RecentClearingFundStateUIController recentclearingfundstate = new RecentClearingFundStateUIController(clientapi);Pane recentclearingfundstateP = new Pane();recentclearingfundstateP.getChildren().add(recentclearingfundstate.getUINode());ShowAllTable.showAsTab(recentclearingfundstateP,I18N.getString("tablename.RecentClearingFundState"));
RecentClearingPosDetailUIController recentclearingposdetail = new RecentClearingPosDetailUIController(clientapi);Pane recentclearingposdetailP = new Pane();recentclearingposdetailP.getChildren().add(recentclearingposdetail.getUINode());ShowAllTable.showAsTab(recentclearingposdetailP,I18N.getString("tablename.RecentClearingPosDetail"));
RecentClearingPriceUIController recentclearingprice = new RecentClearingPriceUIController(clientapi);Pane recentclearingpriceP = new Pane();recentclearingpriceP.getChildren().add(recentclearingprice.getUINode());ShowAllTable.showAsTab(recentclearingpriceP,I18N.getString("tablename.RecentClearingPrice"));
RecentClearingProtfolioposUIController recentclearingprotfoliopos = new RecentClearingProtfolioposUIController(clientapi);Pane recentclearingprotfolioposP = new Pane();recentclearingprotfolioposP.getChildren().add(recentclearingprotfoliopos.getUINode());ShowAllTable.showAsTab(recentclearingprotfolioposP,I18N.getString("tablename.RecentClearingProtfoliopos"));
RecentObjETFUIController recentobjetf = new RecentObjETFUIController(clientapi);Pane recentobjetfP = new Pane();recentobjetfP.getChildren().add(recentobjetf.getUINode());ShowAllTable.showAsTab(recentobjetfP,I18N.getString("tablename.RecentObjETF"));
RecentObjETFListUIController recentobjetflist = new RecentObjETFListUIController(clientapi);Pane recentobjetflistP = new Pane();recentobjetflistP.getChildren().add(recentobjetflist.getUINode());ShowAllTable.showAsTab(recentobjetflistP,I18N.getString("tablename.RecentObjETFList"));
RecentObjIndexUIController recentobjindex = new RecentObjIndexUIController(clientapi);Pane recentobjindexP = new Pane();recentobjindexP.getChildren().add(recentobjindex.getUINode());ShowAllTable.showAsTab(recentobjindexP,I18N.getString("tablename.RecentObjIndex"));
RecentObjIndexListUIController recentobjindexlist = new RecentObjIndexListUIController(clientapi);Pane recentobjindexlistP = new Pane();recentobjindexlistP.getChildren().add(recentobjindexlist.getUINode());ShowAllTable.showAsTab(recentobjindexlistP,I18N.getString("tablename.RecentObjIndexList"));
RecentTradeAlterOrderUIController recenttradealterorder = new RecentTradeAlterOrderUIController(clientapi);Pane recenttradealterorderP = new Pane();recenttradealterorderP.getChildren().add(recenttradealterorder.getUINode());ShowAllTable.showAsTab(recenttradealterorderP,I18N.getString("tablename.RecentTradeAlterOrder"));
RecentTradeCancelOrderUIController recenttradecancelorder = new RecentTradeCancelOrderUIController(clientapi);Pane recenttradecancelorderP = new Pane();recenttradecancelorderP.getChildren().add(recenttradecancelorder.getUINode());ShowAllTable.showAsTab(recenttradecancelorderP,I18N.getString("tablename.RecentTradeCancelOrder"));
RecentTradeExecuteOrderUIController recenttradeexecuteorder = new RecentTradeExecuteOrderUIController(clientapi);Pane recenttradeexecuteorderP = new Pane();recenttradeexecuteorderP.getChildren().add(recenttradeexecuteorder.getUINode());ShowAllTable.showAsTab(recenttradeexecuteorderP,I18N.getString("tablename.RecentTradeExecuteOrder"));
RecentTradeNewOrderUIController recenttradeneworder = new RecentTradeNewOrderUIController(clientapi);Pane recenttradeneworderP = new Pane();recenttradeneworderP.getChildren().add(recenttradeneworder.getUINode());ShowAllTable.showAsTab(recenttradeneworderP,I18N.getString("tablename.RecentTradeNewOrder"));
RecentTradeReportDetailUIController recenttradereportdetail = new RecentTradeReportDetailUIController(clientapi);Pane recenttradereportdetailP = new Pane();recenttradereportdetailP.getChildren().add(recenttradereportdetail.getUINode());ShowAllTable.showAsTab(recenttradereportdetailP,I18N.getString("tablename.RecentTradeReportDetail"));
RiskCtrlAssetsUIController riskctrlassets = new RiskCtrlAssetsUIController(clientapi);Pane riskctrlassetsP = new Pane();riskctrlassetsP.getChildren().add(riskctrlassets.getUINode());ShowAllTable.showAsTab(riskctrlassetsP,I18N.getString("tablename.RiskCtrlAssets"));
RiskIndexUIController riskindex = new RiskIndexUIController(clientapi);Pane riskindexP = new Pane();riskindexP.getChildren().add(riskindex.getUINode());ShowAllTable.showAsTab(riskindexP,I18N.getString("tablename.RiskIndex"));
RiskSetsUIController risksets = new RiskSetsUIController(clientapi);Pane risksetsP = new Pane();risksetsP.getChildren().add(risksets.getUINode());ShowAllTable.showAsTab(risksetsP,I18N.getString("tablename.RiskSets"));
SafeForbidLoginUIController safeforbidlogin = new SafeForbidLoginUIController(clientapi);Pane safeforbidloginP = new Pane();safeforbidloginP.getChildren().add(safeforbidlogin.getUINode());ShowAllTable.showAsTab(safeforbidloginP,I18N.getString("tablename.SafeForbidLogin"));
SafeOtherAccessUIController safeotheraccess = new SafeOtherAccessUIController(clientapi);Pane safeotheraccessP = new Pane();safeotheraccessP.getChildren().add(safeotheraccess.getUINode());ShowAllTable.showAsTab(safeotheraccessP,I18N.getString("tablename.SafeOtherAccess"));
TraceAccountBailUIController traceaccountbail = new TraceAccountBailUIController(clientapi);Pane traceaccountbailP = new Pane();traceaccountbailP.getChildren().add(traceaccountbail.getUINode());ShowAllTable.showAsTab(traceaccountbailP,I18N.getString("tablename.TraceAccountBail"));
TraceAccountFareUIController traceaccountfare = new TraceAccountFareUIController(clientapi);Pane traceaccountfareP = new Pane();traceaccountfareP.getChildren().add(traceaccountfare.getUINode());ShowAllTable.showAsTab(traceaccountfareP,I18N.getString("tablename.TraceAccountFare"));
TraceAccountIcodeUIController traceaccounticode = new TraceAccountIcodeUIController(clientapi);Pane traceaccounticodeP = new Pane();traceaccounticodeP.getChildren().add(traceaccounticode.getUINode());ShowAllTable.showAsTab(traceaccounticodeP,I18N.getString("tablename.TraceAccountIcode"));
TraceAccountInfoUIController traceaccountinfo = new TraceAccountInfoUIController(clientapi);Pane traceaccountinfoP = new Pane();traceaccountinfoP.getChildren().add(traceaccountinfo.getUINode());ShowAllTable.showAsTab(traceaccountinfoP,I18N.getString("tablename.TraceAccountInfo"));
TraceAccountInvestUIController traceaccountinvest = new TraceAccountInvestUIController(clientapi);Pane traceaccountinvestP = new Pane();traceaccountinvestP.getChildren().add(traceaccountinvest.getUINode());ShowAllTable.showAsTab(traceaccountinvestP,I18N.getString("tablename.TraceAccountInvest"));
TraceBailTempUIController tracebailtemp = new TraceBailTempUIController(clientapi);Pane tracebailtempP = new Pane();tracebailtempP.getChildren().add(tracebailtemp.getUINode());ShowAllTable.showAsTab(tracebailtempP,I18N.getString("tablename.TraceBailTemp"));
TraceBailTempObjUIController tracebailtempobj = new TraceBailTempObjUIController(clientapi);Pane tracebailtempobjP = new Pane();tracebailtempobjP.getChildren().add(tracebailtempobj.getUINode());ShowAllTable.showAsTab(tracebailtempobjP,I18N.getString("tablename.TraceBailTempObj"));
TraceFareTempUIController tracefaretemp = new TraceFareTempUIController(clientapi);Pane tracefaretempP = new Pane();tracefaretempP.getChildren().add(tracefaretemp.getUINode());ShowAllTable.showAsTab(tracefaretempP,I18N.getString("tablename.TraceFareTemp"));
TraceFareTempeObjUIController tracefaretempeobj = new TraceFareTempeObjUIController(clientapi);Pane tracefaretempeobjP = new Pane();tracefaretempeobjP.getChildren().add(tracefaretempeobj.getUINode());ShowAllTable.showAsTab(tracefaretempeobjP,I18N.getString("tablename.TraceFareTempeObj"));
TraceObjInfoUIController traceobjinfo = new TraceObjInfoUIController(clientapi);Pane traceobjinfoP = new Pane();traceobjinfoP.getChildren().add(traceobjinfo.getUINode());ShowAllTable.showAsTab(traceobjinfoP,I18N.getString("tablename.TraceObjInfo"));
TraceObjProtfolioUIController traceobjprotfolio = new TraceObjProtfolioUIController(clientapi);Pane traceobjprotfolioP = new Pane();traceobjprotfolioP.getChildren().add(traceobjprotfolio.getUINode());ShowAllTable.showAsTab(traceobjprotfolioP,I18N.getString("tablename.TraceObjProtfolio"));
TraceObjProtfolioListUIController traceobjprotfoliolist = new TraceObjProtfolioListUIController(clientapi);Pane traceobjprotfoliolistP = new Pane();traceobjprotfoliolistP.getChildren().add(traceobjprotfoliolist.getUINode());ShowAllTable.showAsTab(traceobjprotfoliolistP,I18N.getString("tablename.TraceObjProtfolioList"));
TraceObjUnderlyingUIController traceobjunderlying = new TraceObjUnderlyingUIController(clientapi);Pane traceobjunderlyingP = new Pane();traceobjunderlyingP.getChildren().add(traceobjunderlying.getUINode());ShowAllTable.showAsTab(traceobjunderlyingP,I18N.getString("tablename.TraceObjUnderlying"));
TraceProductAccountUIController traceproductaccount = new TraceProductAccountUIController(clientapi);Pane traceproductaccountP = new Pane();traceproductaccountP.getChildren().add(traceproductaccount.getUINode());ShowAllTable.showAsTab(traceproductaccountP,I18N.getString("tablename.TraceProductAccount"));
TraceProductInfoUIController traceproductinfo = new TraceProductInfoUIController(clientapi);Pane traceproductinfoP = new Pane();traceproductinfoP.getChildren().add(traceproductinfo.getUINode());ShowAllTable.showAsTab(traceproductinfoP,I18N.getString("tablename.TraceProductInfo"));
TraceProductUnitUIController traceproductunit = new TraceProductUnitUIController(clientapi);Pane traceproductunitP = new Pane();traceproductunitP.getChildren().add(traceproductunit.getUINode());ShowAllTable.showAsTab(traceproductunitP,I18N.getString("tablename.TraceProductUnit"));
TraceRiskCtrlAssetsUIController traceriskctrlassets = new TraceRiskCtrlAssetsUIController(clientapi);Pane traceriskctrlassetsP = new Pane();traceriskctrlassetsP.getChildren().add(traceriskctrlassets.getUINode());ShowAllTable.showAsTab(traceriskctrlassetsP,I18N.getString("tablename.TraceRiskCtrlAssets"));
TraceRiskSetsUIController tracerisksets = new TraceRiskSetsUIController(clientapi);Pane tracerisksetsP = new Pane();tracerisksetsP.getChildren().add(tracerisksets.getUINode());ShowAllTable.showAsTab(tracerisksetsP,I18N.getString("tablename.TraceRiskSets"));
TraceSafeForbidLoginUIController tracesafeforbidlogin = new TraceSafeForbidLoginUIController(clientapi);Pane tracesafeforbidloginP = new Pane();tracesafeforbidloginP.getChildren().add(tracesafeforbidlogin.getUINode());ShowAllTable.showAsTab(tracesafeforbidloginP,I18N.getString("tablename.TraceSafeForbidLogin"));
TraceSafeOtherAccessUIController tracesafeotheraccess = new TraceSafeOtherAccessUIController(clientapi);Pane tracesafeotheraccessP = new Pane();tracesafeotheraccessP.getChildren().add(tracesafeotheraccess.getUINode());ShowAllTable.showAsTab(tracesafeotheraccessP,I18N.getString("tablename.TraceSafeOtherAccess"));
TraceTradesetExchangeLimitUIController tracetradesetexchangelimit = new TraceTradesetExchangeLimitUIController(clientapi);Pane tracetradesetexchangelimitP = new Pane();tracetradesetexchangelimitP.getChildren().add(tracetradesetexchangelimit.getUINode());ShowAllTable.showAsTab(tracetradesetexchangelimitP,I18N.getString("tablename.TraceTradesetExchangeLimit"));
TraceUserAccessUIController traceuseraccess = new TraceUserAccessUIController(clientapi);Pane traceuseraccessP = new Pane();traceuseraccessP.getChildren().add(traceuseraccess.getUINode());ShowAllTable.showAsTab(traceuseraccessP,I18N.getString("tablename.TraceUserAccess"));
TraceUserAccountUIController traceuseraccount = new TraceUserAccountUIController(clientapi);Pane traceuseraccountP = new Pane();traceuseraccountP.getChildren().add(traceuseraccount.getUINode());ShowAllTable.showAsTab(traceuseraccountP,I18N.getString("tablename.TraceUserAccount"));
TraceUserInfoUIController traceuserinfo = new TraceUserInfoUIController(clientapi);Pane traceuserinfoP = new Pane();traceuserinfoP.getChildren().add(traceuserinfo.getUINode());ShowAllTable.showAsTab(traceuserinfoP,I18N.getString("tablename.TraceUserInfo"));
TraceUserPrivateInfoUIController traceuserprivateinfo = new TraceUserPrivateInfoUIController(clientapi);Pane traceuserprivateinfoP = new Pane();traceuserprivateinfoP.getChildren().add(traceuserprivateinfo.getUINode());ShowAllTable.showAsTab(traceuserprivateinfoP,I18N.getString("tablename.TraceUserPrivateInfo"));
TraceWorkFlowPathUIController traceworkflowpath = new TraceWorkFlowPathUIController(clientapi);Pane traceworkflowpathP = new Pane();traceworkflowpathP.getChildren().add(traceworkflowpath.getUINode());ShowAllTable.showAsTab(traceworkflowpathP,I18N.getString("tablename.TraceWorkFlowPath"));
TraceWorkGroupUIController traceworkgroup = new TraceWorkGroupUIController(clientapi);Pane traceworkgroupP = new Pane();traceworkgroupP.getChildren().add(traceworkgroup.getUINode());ShowAllTable.showAsTab(traceworkgroupP,I18N.getString("tablename.TraceWorkGroup"));
TraceWorkGroupUserUIController traceworkgroupuser = new TraceWorkGroupUserUIController(clientapi);Pane traceworkgroupuserP = new Pane();traceworkgroupuserP.getChildren().add(traceworkgroupuser.getUINode());ShowAllTable.showAsTab(traceworkgroupuserP,I18N.getString("tablename.TraceWorkGroupUser"));
TraceWorkProductFlowUIController traceworkproductflow = new TraceWorkProductFlowUIController(clientapi);Pane traceworkproductflowP = new Pane();traceworkproductflowP.getChildren().add(traceworkproductflow.getUINode());ShowAllTable.showAsTab(traceworkproductflowP,I18N.getString("tablename.TraceWorkProductFlow"));
AlterOrderUIController alterorder = new AlterOrderUIController(clientapi);Pane alterorderP = new Pane();alterorderP.getChildren().add(alterorder.getUINode());ShowAllTable.showAsTab(alterorderP,I18N.getString("tablename.AlterOrder"));
CancelOrderUIController cancelorder = new CancelOrderUIController(clientapi);Pane cancelorderP = new Pane();cancelorderP.getChildren().add(cancelorder.getUINode());ShowAllTable.showAsTab(cancelorderP,I18N.getString("tablename.CancelOrder"));
OrderExecuteProgressUIController orderexecuteprogress = new OrderExecuteProgressUIController(clientapi);Pane orderexecuteprogressP = new Pane();orderexecuteprogressP.getChildren().add(orderexecuteprogress.getUINode());ShowAllTable.showAsTab(orderexecuteprogressP,I18N.getString("tablename.OrderExecuteProgress"));
FundTransferUIController fundtransfer = new FundTransferUIController(clientapi);Pane fundtransferP = new Pane();fundtransferP.getChildren().add(fundtransfer.getUINode());ShowAllTable.showAsTab(fundtransferP,I18N.getString("tablename.FundTransfer"));
NewOrderUIController neworder = new NewOrderUIController(clientapi);Pane neworderP = new Pane();neworderP.getChildren().add(neworder.getUINode());ShowAllTable.showAsTab(neworderP,I18N.getString("tablename.NewOrder"));
TradeReportDetailUIController tradereportdetail = new TradeReportDetailUIController(clientapi);Pane tradereportdetailP = new Pane();tradereportdetailP.getChildren().add(tradereportdetail.getUINode());ShowAllTable.showAsTab(tradereportdetailP,I18N.getString("tablename.TradeReportDetail"));
TradesetBrokerUIController tradesetbroker = new TradesetBrokerUIController(clientapi);Pane tradesetbrokerP = new Pane();tradesetbrokerP.getChildren().add(tradesetbroker.getUINode());ShowAllTable.showAsTab(tradesetbrokerP,I18N.getString("tablename.TradesetBroker"));
TradesetCalendarUIController tradesetcalendar = new TradesetCalendarUIController(clientapi);Pane tradesetcalendarP = new Pane();tradesetcalendarP.getChildren().add(tradesetcalendar.getUINode());ShowAllTable.showAsTab(tradesetcalendarP,I18N.getString("tablename.TradesetCalendar"));
TradesetExchangeLimitUIController tradesetexchangelimit = new TradesetExchangeLimitUIController(clientapi);Pane tradesetexchangelimitP = new Pane();tradesetexchangelimitP.getChildren().add(tradesetexchangelimit.getUINode());ShowAllTable.showAsTab(tradesetexchangelimitP,I18N.getString("tablename.TradesetExchangeLimit"));
TradesetPipUIController tradesetpip = new TradesetPipUIController(clientapi);Pane tradesetpipP = new Pane();tradesetpipP.getChildren().add(tradesetpip.getUINode());ShowAllTable.showAsTab(tradesetpipP,I18N.getString("tablename.TradesetPip"));
//TradeSimOrderqueneUIController tradesimorderquene = new TradeSimOrderqueneUIController(clientapi);Pane tradesimorderqueneP = new Pane();tradesimorderqueneP.getChildren().add(tradesimorderquene.getUINode());ShowAllTable.showAsTab(tradesimorderqueneP,I18N.getString("tablename.TradeSimOrderquene"));
UserAccessUIController useraccess = new UserAccessUIController(clientapi);Pane useraccessP = new Pane();useraccessP.getChildren().add(useraccess.getUINode());ShowAllTable.showAsTab(useraccessP,I18N.getString("tablename.UserAccess"));
UserAccountUIController useraccount = new UserAccountUIController(clientapi);Pane useraccountP = new Pane();useraccountP.getChildren().add(useraccount.getUINode());ShowAllTable.showAsTab(useraccountP,I18N.getString("tablename.UserAccount"));
UserInfoUIController userinfo = new UserInfoUIController(clientapi);Pane userinfoP = new Pane();userinfoP.getChildren().add(userinfo.getUINode());ShowAllTable.showAsTab(userinfoP,I18N.getString("tablename.UserInfo"));
UserPrivateInfoUIController userprivateinfo = new UserPrivateInfoUIController(clientapi);Pane userprivateinfoP = new Pane();userprivateinfoP.getChildren().add(userprivateinfo.getUINode());ShowAllTable.showAsTab(userprivateinfoP,I18N.getString("tablename.UserPrivateInfo"));
WorkFlowPathUIController workflowpath = new WorkFlowPathUIController(clientapi);Pane workflowpathP = new Pane();workflowpathP.getChildren().add(workflowpath.getUINode());ShowAllTable.showAsTab(workflowpathP,I18N.getString("tablename.WorkFlowPath"));
WorkGroupUIController workgroup = new WorkGroupUIController(clientapi);Pane workgroupP = new Pane();workgroupP.getChildren().add(workgroup.getUINode());ShowAllTable.showAsTab(workgroupP,I18N.getString("tablename.WorkGroup"));
WorkGroupUserUIController workgroupuser = new WorkGroupUserUIController(clientapi);Pane workgroupuserP = new Pane();workgroupuserP.getChildren().add(workgroupuser.getUINode());ShowAllTable.showAsTab(workgroupuserP,I18N.getString("tablename.WorkGroupUser"));
WorkProductFlowUIController workproductflow = new WorkProductFlowUIController(clientapi);Pane workproductflowP = new Pane();workproductflowP.getChildren().add(workproductflow.getUINode());ShowAllTable.showAsTab(workproductflowP,I18N.getString("tablename.WorkProductFlow"));
AccountBailUIController accountbail = new AccountBailUIController(clientapi);Pane accountbailP = new Pane();accountbailP.getChildren().add(accountbail.getUINode());ShowAllTable.showAsTab(accountbailP,I18N.getString("tablename.AccountBail"));
AccountFareUIController accountfare = new AccountFareUIController(clientapi);Pane accountfareP = new Pane();accountfareP.getChildren().add(accountfare.getUINode());ShowAllTable.showAsTab(accountfareP,I18N.getString("tablename.AccountFare"));
AccountIcodeUIController accounticode = new AccountIcodeUIController(clientapi);Pane accounticodeP = new Pane();accounticodeP.getChildren().add(accounticode.getUINode());ShowAllTable.showAsTab(accounticodeP,I18N.getString("tablename.AccountIcode"));
AccountInfoUIController accountinfo = new AccountInfoUIController(clientapi);Pane accountinfoP = new Pane();accountinfoP.getChildren().add(accountinfo.getUINode());ShowAllTable.showAsTab(accountinfoP,I18N.getString("tablename.AccountInfo"));
AccountInvestUIController accountinvest = new AccountInvestUIController(clientapi);Pane accountinvestP = new Pane();accountinvestP.getChildren().add(accountinvest.getUINode());ShowAllTable.showAsTab(accountinvestP,I18N.getString("tablename.AccountInvest"));
BailTempUIController bailtemp = new BailTempUIController(clientapi);Pane bailtempP = new Pane();bailtempP.getChildren().add(bailtemp.getUINode());ShowAllTable.showAsTab(bailtempP,I18N.getString("tablename.BailTemp"));
BailTempObjUIController bailtempobj = new BailTempObjUIController(clientapi);Pane bailtempobjP = new Pane();bailtempobjP.getChildren().add(bailtempobj.getUINode());ShowAllTable.showAsTab(bailtempobjP,I18N.getString("tablename.BailTempObj"));
ClearingCurrencyRatesUIController clearingcurrencyrates = new ClearingCurrencyRatesUIController(clientapi);Pane clearingcurrencyratesP = new Pane();clearingcurrencyratesP.getChildren().add(clearingcurrencyrates.getUINode());ShowAllTable.showAsTab(clearingcurrencyratesP,I18N.getString("tablename.ClearingCurrencyRates"));
ClearingDayBookUIController clearingdaybook = new ClearingDayBookUIController(clientapi);Pane clearingdaybookP = new Pane();clearingdaybookP.getChildren().add(clearingdaybook.getUINode());ShowAllTable.showAsTab(clearingdaybookP,I18N.getString("tablename.ClearingDayBook"));
ClearingExRightUIController clearingexright = new ClearingExRightUIController(clientapi);Pane clearingexrightP = new Pane();clearingexrightP.getChildren().add(clearingexright.getUINode());ShowAllTable.showAsTab(clearingexrightP,I18N.getString("tablename.ClearingExRight"));
FundStateUIController fundstate = new FundStateUIController(clientapi);Pane fundstateP = new Pane();fundstateP.getChildren().add(fundstate.getUINode());ShowAllTable.showAsTab(fundstateP,I18N.getString("tablename.FundState"));
PosDetailUIController posdetail = new PosDetailUIController(clientapi);Pane posdetailP = new Pane();posdetailP.getChildren().add(posdetail.getUINode());ShowAllTable.showAsTab(posdetailP,I18N.getString("tablename.PosDetail"));
ClearingPriceUIController clearingprice = new ClearingPriceUIController(clientapi);Pane clearingpriceP = new Pane();clearingpriceP.getChildren().add(clearingprice.getUINode());ShowAllTable.showAsTab(clearingpriceP,I18N.getString("tablename.ClearingPrice"));

    }

    public static void showAsTab(Pane frm, String label) {
		final Tab tab = new Tab(label);
		tab.setClosable(true);
		tab.setContent(frm);
		tabpane.getTabs().add(tab);
		tabpane.getSelectionModel().select(tab);

		/**
		 * Workaround for TabPane memory leak
		 */
		tab.setOnClosed(new EventHandler<Event>() {

			@Override
			public void handle(Event t) {
				tab.setContent(null);
			}
		});

	}
    
}
