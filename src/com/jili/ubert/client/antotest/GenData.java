/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.antotest;


import com.jili.ubert.dao.db.AccountBail;
import com.jili.ubert.dao.db.AccountFare;
import com.jili.ubert.dao.db.AccountIcode;
import com.jili.ubert.dao.db.AccountInfo;
import com.jili.ubert.dao.db.AccountInvest;
import com.jili.ubert.dao.db.AlterOrder;
import com.jili.ubert.dao.db.BailTemp;
import com.jili.ubert.dao.db.BailTempObj;
import com.jili.ubert.dao.db.CancelOrder;
import com.jili.ubert.dao.db.ClearingCurrencyRates;
import com.jili.ubert.dao.db.ClearingDayBook;
import com.jili.ubert.dao.db.ClearingExRight;
import com.jili.ubert.dao.db.ClearingPrice;
import com.jili.ubert.dao.db.ClearingProtfoliopos;
import com.jili.ubert.dao.db.FareTemp;
import com.jili.ubert.dao.db.FareTempeObj;
import com.jili.ubert.dao.db.FundState;
import com.jili.ubert.dao.db.FundTransfer;
import com.jili.ubert.dao.db.IOPVAccount;
import com.jili.ubert.dao.db.IOPVProduct;
import com.jili.ubert.dao.db.IOPVProductUnit;
import com.jili.ubert.dao.db.LogLoginInfo;
import com.jili.ubert.dao.db.NewOrder;
import com.jili.ubert.dao.db.ObjClassCode;
import com.jili.ubert.dao.db.ObjClassCodeAtrr;
import com.jili.ubert.dao.db.ObjETF;
import com.jili.ubert.dao.db.ObjETFList;
import com.jili.ubert.dao.db.ObjExRightInfo;
import com.jili.ubert.dao.db.ObjIndex;
import com.jili.ubert.dao.db.ObjIndexList;
import com.jili.ubert.dao.db.ObjInfo;
import com.jili.ubert.dao.db.ObjNewStock;
import com.jili.ubert.dao.db.ObjPriceTick;
import com.jili.ubert.dao.db.ObjProtfolio;
import com.jili.ubert.dao.db.ObjProtfolioList;
import com.jili.ubert.dao.db.ObjStructuredFundBase;
import com.jili.ubert.dao.db.ObjStructuredFundPart;
import com.jili.ubert.dao.db.ObjUnderlyingCode;
import com.jili.ubert.dao.db.OrderExecuteProgress;
import com.jili.ubert.dao.db.PosDetail;
import com.jili.ubert.dao.db.ProductAccount;
import com.jili.ubert.dao.db.ProductInfo;
import com.jili.ubert.dao.db.ProductUnit;
import com.jili.ubert.dao.db.RiskCtrlAssets;
import com.jili.ubert.dao.db.RiskIndex;
import com.jili.ubert.dao.db.RiskSets;
import com.jili.ubert.dao.db.SafeForbidLogin;
import com.jili.ubert.dao.db.SafeOtherAccess;
import com.jili.ubert.dao.db.TradeReportDetail;
import com.jili.ubert.dao.db.TradesetBroker;
import com.jili.ubert.dao.db.TradesetCalendar;
import com.jili.ubert.dao.db.TradesetExchangeLimit;
import com.jili.ubert.dao.db.TradesetPip;
import com.jili.ubert.dao.db.UserAccess;
import com.jili.ubert.dao.db.UserAccount;
import com.jili.ubert.dao.db.UserInfo;
import com.jili.ubert.dao.db.UserPrivateInfo;
import com.jili.ubert.dao.db.WorkFlowPath;
import com.jili.ubert.dao.db.WorkGroup;
import com.jili.ubert.dao.db.WorkGroupUser;
import com.jili.ubert.dao.db.WorkProductFlow;
import com.jili.ubert.dao.history.HistoryClearingCurrencyRates;
import com.jili.ubert.dao.history.HistoryIOPVAccount;
import com.jili.ubert.dao.history.HistoryIOPVProduct;
import com.jili.ubert.dao.history.HistoryIOPVProductUnit;
import com.jili.ubert.dao.history.HistoryLogLoginInfo;
import com.jili.ubert.dao.history.HistoryObjExRightInfo;
import com.jili.ubert.dao.history.HistoryTradeFundTransfer;
import com.jili.ubert.dao.history.HistoryTradesetCalendar;
import com.jili.ubert.dao.recent.RecentClearingAccountBail;
import com.jili.ubert.dao.recent.RecentClearingAccountFare;
import com.jili.ubert.dao.recent.RecentClearingDayBook;
import com.jili.ubert.dao.recent.RecentClearingExRight;
import com.jili.ubert.dao.recent.RecentClearingFundState;
import com.jili.ubert.dao.recent.RecentClearingPosDetail;
import com.jili.ubert.dao.recent.RecentClearingPrice;
import com.jili.ubert.dao.recent.RecentClearingProtfoliopos;
import com.jili.ubert.dao.recent.RecentObjETF;
import com.jili.ubert.dao.recent.RecentObjETFList;
import com.jili.ubert.dao.recent.RecentObjIndex;
import com.jili.ubert.dao.recent.RecentObjIndexList;
import com.jili.ubert.dao.recent.RecentTradeAlterOrder;
import com.jili.ubert.dao.recent.RecentTradeCancelOrder;
import com.jili.ubert.dao.recent.RecentTradeExecuteOrder;
import com.jili.ubert.dao.recent.RecentTradeNewOrder;
import com.jili.ubert.dao.recent.RecentTradeReportDetail;
import com.jili.ubert.dao.trace.TraceAccountBail;
import com.jili.ubert.dao.trace.TraceAccountFare;
import com.jili.ubert.dao.trace.TraceAccountIcode;
import com.jili.ubert.dao.trace.TraceAccountInfo;
import com.jili.ubert.dao.trace.TraceAccountInvest;
import com.jili.ubert.dao.trace.TraceBailTemp;
import com.jili.ubert.dao.trace.TraceBailTempObj;
import com.jili.ubert.dao.trace.TraceFareTemp;
import com.jili.ubert.dao.trace.TraceFareTempeObj;
import com.jili.ubert.dao.trace.TraceObjInfo;
import com.jili.ubert.dao.trace.TraceObjProtfolio;
import com.jili.ubert.dao.trace.TraceObjProtfolioList;
import com.jili.ubert.dao.trace.TraceObjUnderlying;
import com.jili.ubert.dao.trace.TraceProductAccount;
import com.jili.ubert.dao.trace.TraceProductInfo;
import com.jili.ubert.dao.trace.TraceProductUnit;
import com.jili.ubert.dao.trace.TraceRiskCtrlAssets;
import com.jili.ubert.dao.trace.TraceRiskSets;
import com.jili.ubert.dao.trace.TraceSafeForbidLogin;
import com.jili.ubert.dao.trace.TraceSafeOtherAccess;
import com.jili.ubert.dao.trace.TraceTradesetExchangeLimit;
import com.jili.ubert.dao.trace.TraceUserAccess;
import com.jili.ubert.dao.trace.TraceUserAccount;
import com.jili.ubert.dao.trace.TraceUserInfo;
import com.jili.ubert.dao.trace.TraceUserPrivateInfo;
import com.jili.ubert.dao.trace.TraceWorkFlowPath;
import com.jili.ubert.dao.trace.TraceWorkGroup;
import com.jili.ubert.dao.trace.TraceWorkGroupUser;
import com.jili.ubert.dao.trace.TraceWorkProductFlow;
import java.util.Date;

/**
 *
 * @author dragon
 */
public class GenData {
    public GenData(){}

    public ClearingProtfoliopos genClearingProtfoliopos() {
        ClearingProtfoliopos clearingprotfoliopos = new ClearingProtfoliopos();
        clearingprotfoliopos.setId(1000);
        clearingprotfoliopos.setTimeStamp(new Date());
        clearingprotfoliopos.setClearingDate(new Date());
        clearingprotfoliopos.setAccountCode("jili成");
        clearingprotfoliopos.setPosID("jili成");
        clearingprotfoliopos.setQty(1000);
        clearingprotfoliopos.setAvlQty(1000);
        clearingprotfoliopos.setFrozeQty(1000);
        clearingprotfoliopos.setClosePrice((float) 15.23);
        clearingprotfoliopos.setCostPriceA((float) 15.23);
        clearingprotfoliopos.setCostPriceB((float) 15.23);
        clearingprotfoliopos.setOpencostprice((float) 15.23);
        clearingprotfoliopos.setHedgeFlag("jili成");
        clearingprotfoliopos.setCurrency("jili成");
        clearingprotfoliopos.setPl((float) 15.23);
        clearingprotfoliopos.setCostPL((float) 15.23);
        clearingprotfoliopos.setTotalPL((float) 15.23);
        clearingprotfoliopos.setMargin((float) 15.23);
        clearingprotfoliopos.setTotalFare((float) 15.23);
        clearingprotfoliopos.setInvestID("jili成");
        clearingprotfoliopos.setOwnUserID("jili成");
        clearingprotfoliopos.setAlterUserID("jili成");
        clearingprotfoliopos.setCreateTime(new Date());
        return clearingprotfoliopos;
    }

    public FareTemp genFareTemp() {
        FareTemp faretemp = new FareTemp();
        faretemp.setTempID(1000);
        faretemp.setTimeStamp(new Date());
        faretemp.setTempName("jili成");
        faretemp.setByOwnGroupID(1000);
        faretemp.setReMark("jili成");
        faretemp.setOwnUserID("jili成");
        faretemp.setAlterUserID("jili成");
        faretemp.setCreateTime(new Date());
        return faretemp;
    }

    public FareTempeObj genFareTempeObj() {
        FareTempeObj faretempeobj = new FareTempeObj();
        faretempeobj.setId(1000);
        faretempeobj.setTimeStamp(new Date());
        faretempeobj.setTempID(1000);
        faretempeobj.setMarketCode("jili成");
        faretempeobj.setClassCode("jil");
        faretempeobj.setUnderlyingCode("jili成");
        faretempeobj.setObj("jili成");
        faretempeobj.setBs("jili成");
        faretempeobj.setOpenClose("jili成");
        faretempeobj.setCommission0((float) 15.23);
        faretempeobj.setCommission1((float) 15.23);
        faretempeobj.setMinCommission0((float) 15.23);
        faretempeobj.setMinCommission1((float) 15.23);
        faretempeobj.setDeliverFare0((float) 15.23);
        faretempeobj.setDeliverFare1((float) 15.23);
        faretempeobj.setTax0((float) 15.23);
        faretempeobj.setTax1((float) 15.23);
        faretempeobj.setEtax1((float) 15.23);
        faretempeobj.setEtax0((float) 15.23);
        faretempeobj.setCost0((float) 15.23);
        faretempeobj.setCost1((float) 15.23);
        faretempeobj.setOwnUserID("jili成");
        faretempeobj.setAlterUserID("jili成");
        faretempeobj.setCreateTime(new Date());
        return faretempeobj;
    }

    public HistoryClearingCurrencyRates genHistoryClearingCurrencyRates() {
        HistoryClearingCurrencyRates historyclearingcurrencyrates = new HistoryClearingCurrencyRates();
        historyclearingcurrencyrates.setId(1000);
        historyclearingcurrencyrates.setTimeStamp(new Date());
        historyclearingcurrencyrates.setClearingDate(new Date());
        historyclearingcurrencyrates.setAskCurrency("jili成");
        historyclearingcurrencyrates.setBidCurrency("jili成");
        historyclearingcurrencyrates.setRatePrice((float) 15.23);
        historyclearingcurrencyrates.setOwnUserID("jili成");
        historyclearingcurrencyrates.setAlterUserID("jili成");
        historyclearingcurrencyrates.setCreateTime(new Date());
        return historyclearingcurrencyrates;
    }

    public HistoryIOPVAccount genHistoryIOPVAccount() {
        HistoryIOPVAccount historyiopvaccount = new HistoryIOPVAccount();
        historyiopvaccount.setId(1000);
        historyiopvaccount.setTimeStamp(new Date());
        historyiopvaccount.setDate(new Date());
        historyiopvaccount.setProductID(1000);
        historyiopvaccount.setUnitID(1000);
        historyiopvaccount.setAccountCode("jili成");
        historyiopvaccount.setNet((float) 15.23);
        historyiopvaccount.setIopv((float) 15.23);
        historyiopvaccount.setRealIOPV((float) 15.23);
        historyiopvaccount.setCurrentLot(1000000L);
        historyiopvaccount.setDeltLot(1000);
        historyiopvaccount.setFeeOfDay((float) 15.23);
        historyiopvaccount.setClearFeeaAcumulation((float) 15.23);
        historyiopvaccount.setTotalAssetValue((float) 15.23);
        historyiopvaccount.setPosRate((float) 15.23);
        historyiopvaccount.setDebtAssetValue((float) 15.23);
        historyiopvaccount.setOwnUserID("jili成");
        historyiopvaccount.setAlterUserID("jili成");
        historyiopvaccount.setCreateTime(new Date());
        return historyiopvaccount;
    }

    public HistoryIOPVProduct genHistoryIOPVProduct() {
        HistoryIOPVProduct historyiopvproduct = new HistoryIOPVProduct();
        historyiopvproduct.setId(1000);
        historyiopvproduct.setTimeStamp(new Date());
        historyiopvproduct.setDate(new Date());
        historyiopvproduct.setProductID(1000);
        historyiopvproduct.setNet((float) 15.23);
        historyiopvproduct.setIopv((float) 15.23);
        historyiopvproduct.setRealIOPV((float) 15.23);
        historyiopvproduct.setCurrentLot(1000000L);
        historyiopvproduct.setDeltLot(1000);
        historyiopvproduct.setFeeOfDay((float) 15.23);
        historyiopvproduct.setClearFeeaAcumulation((float) 15.23);
        historyiopvproduct.setTotalAssetValue((float) 15.23);
        historyiopvproduct.setPosRate((float) 15.23);
        historyiopvproduct.setDebtAssetValue((float) 15.23);
        historyiopvproduct.setOwnUserID("jili成");
        historyiopvproduct.setAlterUserID("jili成");
        historyiopvproduct.setCreateTime(new Date());
        return historyiopvproduct;
    }

    public HistoryIOPVProductUnit genHistoryIOPVProductUnit() {
        HistoryIOPVProductUnit historyiopvproductunit = new HistoryIOPVProductUnit();
        historyiopvproductunit.setId(1000);
        historyiopvproductunit.setTimeStamp(new Date());
        historyiopvproductunit.setDate(new Date());
        historyiopvproductunit.setProductID(1000);
        historyiopvproductunit.setUnitID(1000);
        historyiopvproductunit.setNet((float) 15.23);
        historyiopvproductunit.setIopv((float) 15.23);
        historyiopvproductunit.setRealIOPV((float) 15.23);
        historyiopvproductunit.setCurrentLot(1000000L);
        historyiopvproductunit.setDeltLot(1000);
        historyiopvproductunit.setFeeOfDay((float) 15.23);
        historyiopvproductunit.setClearFeeaAcumulation((float) 15.23);
        historyiopvproductunit.setTotalAssetValue((float) 15.23);
        historyiopvproductunit.setPosRate((float) 15.23);
        historyiopvproductunit.setDebtAssetValue((float) 15.23);
        historyiopvproductunit.setOwnUserID("jili成");
        historyiopvproductunit.setAlterUserID("jili成");
        historyiopvproductunit.setCreateTime(new Date());
        return historyiopvproductunit;
    }

    public HistoryLogLoginInfo genHistoryLogLoginInfo() {
        HistoryLogLoginInfo historyloglogininfo = new HistoryLogLoginInfo();
        historyloglogininfo.setId(1000);
        historyloglogininfo.setTimeStamp(new Date());
        historyloglogininfo.setUserID("jili成");
        historyloglogininfo.setLoginType("jili成");
        historyloglogininfo.setLogoutType("jili成");
        historyloglogininfo.setLoginDateTime(new Date());
        historyloglogininfo.setLogoutDateTime(new Date());
        historyloglogininfo.setMac("jili成");
        historyloglogininfo.setIp4(1000);
//        historyloglogininfo.setIp6("jili".getBytes());
        historyloglogininfo.setAddress("jili成");
        historyloglogininfo.setClientType("jili成");
        historyloglogininfo.setOSType("jili成");
        historyloglogininfo.setClientVersion("jili成");
        historyloglogininfo.setCreateTime(new Date());
        return historyloglogininfo;
    }

    public HistoryObjExRightInfo genHistoryObjExRightInfo() {
        HistoryObjExRightInfo historyobjexrightinfo = new HistoryObjExRightInfo();
        historyobjexrightinfo.setId(1000);
        historyobjexrightinfo.setTimeStamp(new Date());
        historyobjexrightinfo.setRegionID("jili成");
        historyobjexrightinfo.setObj("jili成");
        historyobjexrightinfo.setMarketCode("jili成");
        historyobjexrightinfo.setClassCode("jil");
        historyobjexrightinfo.setExRightDate(new Date());
        historyobjexrightinfo.setRecordeDate(new Date());
        historyobjexrightinfo.setCheckInRightDate(new Date());
        historyobjexrightinfo.setCheckInProfitsDate(new Date());
        historyobjexrightinfo.setDenominator(1000);
        historyobjexrightinfo.setMemberA(1000);
        historyobjexrightinfo.setMemberB(1000);
        historyobjexrightinfo.setProfits((float) 15.23);
        historyobjexrightinfo.setReMark("jili成");
        historyobjexrightinfo.setOwnUserID("jili成");
        historyobjexrightinfo.setAlterUserID("jili成");
        historyobjexrightinfo.setCreateTime(new Date());
        return historyobjexrightinfo;
    }

    public HistoryTradeFundTransfer genHistoryTradeFundTransfer() {
        HistoryTradeFundTransfer historytradefundtransfer = new HistoryTradeFundTransfer();
        historytradefundtransfer.setId(1000);
        historytradefundtransfer.setTimeStamp(new Date());
        historytradefundtransfer.setRegionID("jili成");
        historytradefundtransfer.setAccountCode("jili成");
        historytradefundtransfer.setDirect("jili成");
        historytradefundtransfer.setAmount(45.123);
        historytradefundtransfer.setDate(new Date());
        historytradefundtransfer.setTime(new Date());
        historytradefundtransfer.setDateTime(new Date());
        historytradefundtransfer.setCurrency("jili成");
        historytradefundtransfer.setChecker("jili成");
        historytradefundtransfer.setReMark("jili成");
        historytradefundtransfer.setIsEffectived("jili成");
        historytradefundtransfer.setReplyMsg("jili成");
        historytradefundtransfer.setOwnUserID("jili成");
        historytradefundtransfer.setAlterUserID("jili成");
        historytradefundtransfer.setCreateTime(new Date());
        return historytradefundtransfer;
    }

    public HistoryTradesetCalendar genHistoryTradesetCalendar() {
        HistoryTradesetCalendar historytradesetcalendar = new HistoryTradesetCalendar();
        historytradesetcalendar.setId(1000);
        historytradesetcalendar.setTimeStamp(new Date());
        historytradesetcalendar.setRegionID("jili成");
        historytradesetcalendar.setDate(new Date());
        historytradesetcalendar.setIsTrade("jili成");
        historytradesetcalendar.setReMark("jili成");
        historytradesetcalendar.setOwnUserID("jili成");
        historytradesetcalendar.setAlterUserID("jili成");
        historytradesetcalendar.setCreateTime(new Date());
        return historytradesetcalendar;
    }

    public IOPVAccount genIOPVAccount() {
        IOPVAccount iopvaccount = new IOPVAccount();
        iopvaccount.setId(1000);
        iopvaccount.setTimeStamp(new Date());
        iopvaccount.setDate(new Date());
        iopvaccount.setProductID(1000);
        iopvaccount.setUnitID(1000);
        iopvaccount.setAccountCode("jili成");
        iopvaccount.setNet((float) 15.23);
        iopvaccount.setIopv((float) 15.23);
        iopvaccount.setRealIOPV((float) 15.23);
        iopvaccount.setCurrentLot(1000000L);
        iopvaccount.setDeltLot(1000);
        iopvaccount.setFeeOfDay((float) 15.23);
        iopvaccount.setClearFeeaAcumulation((float) 15.23);
        iopvaccount.setTotalAssetValue((float) 15.23);
        iopvaccount.setPosRate((float) 15.23);
        iopvaccount.setDebtAssetValue((float) 15.23);
        iopvaccount.setOwnUserID("jili成");
        iopvaccount.setAlterUserID("jili成");
        iopvaccount.setCreateTime(new Date());
        return iopvaccount;
    }

    public IOPVProduct genIOPVProduct() {
        IOPVProduct iopvproduct = new IOPVProduct();
        iopvproduct.setId(1000);
        iopvproduct.setTimeStamp(new Date());
        iopvproduct.setDate(new Date());
        iopvproduct.setProductID(1000);
        iopvproduct.setNet((float) 15.23);
        iopvproduct.setIopv((float) 15.23);
        iopvproduct.setRealIOPV((float) 15.23);
        iopvproduct.setCurrentLot(1000000L);
        iopvproduct.setDeltLot(1000);
        iopvproduct.setFeeOfDay((float) 15.23);
        iopvproduct.setClearFeeaAcumulation((float) 15.23);
        iopvproduct.setTotalAssetValue((float) 15.23);
        iopvproduct.setPosRate((float) 15.23);
        iopvproduct.setDebtAssetValue((float) 15.23);
        iopvproduct.setOwnUserID("jili成");
        iopvproduct.setAlterUserID("jili成");
        iopvproduct.setCreateTime(new Date());
        return iopvproduct;
    }

    public IOPVProductUnit genIOPVProductUnit() {
        IOPVProductUnit iopvproductunit = new IOPVProductUnit();
        iopvproductunit.setId(1000);
        iopvproductunit.setTimeStamp(new Date());
        iopvproductunit.setDate(new Date());
        iopvproductunit.setProductID(1000);
        iopvproductunit.setUnitID(1000);
        iopvproductunit.setNet((float) 15.23);
        iopvproductunit.setIopv((float) 15.23);
        iopvproductunit.setRealIOPV((float) 15.23);
        iopvproductunit.setCurrentLot(1000000L);
        iopvproductunit.setDeltLot(1000);
        iopvproductunit.setFeeOfDay((float) 15.23);
        iopvproductunit.setClearFeeaAcumulation((float) 15.23);
        iopvproductunit.setTotalAssetValue((float) 15.23);
        iopvproductunit.setPosRate((float) 15.23);
        iopvproductunit.setDebtAssetValue((float) 15.23);
        iopvproductunit.setOwnUserID("jili成");
        iopvproductunit.setAlterUserID("jili成");
        iopvproductunit.setCreateTime(new Date());
        return iopvproductunit;
    }

    public LogLoginInfo genLogLoginInfo() {
        LogLoginInfo loglogininfo = new LogLoginInfo();
        loglogininfo.setId(1000);
        loglogininfo.setTimeStamp(new Date());
        loglogininfo.setUserID("jili成");
        loglogininfo.setLoginType("jili成");
        loglogininfo.setLogoutType("jili成");
        loglogininfo.setLoginDateTime(new Date());
        loglogininfo.setLogoutDateTime(new Date());
        loglogininfo.setMac("jili成");
        loglogininfo.setIp4(1000);
//        loglogininfo.setIp6("jili".getBytes());
        loglogininfo.setAddress("jili成");
        loglogininfo.setClientType("jili成");
        loglogininfo.setOSType("jili成");
        loglogininfo.setClientVersion("jili成");
        loglogininfo.setCreateTime(new Date());
        return loglogininfo;
    }

    public ObjClassCode genObjClassCode() {
        ObjClassCode objclasscode = new ObjClassCode();
        objclasscode.setClassCode("jii");
        objclasscode.setTimeStamp(new Date());
        objclasscode.setUnderlyingCode("jili成");
        objclasscode.setClassName("jili成");
        objclasscode.setReMark("jili成");
        objclasscode.setAlterUserID("jili成");
        objclasscode.setCreateTime(new Date());
        return objclasscode;
    }

    public ObjClassCodeAtrr genObjClassCodeAtrr() {
        ObjClassCodeAtrr objclasscodeatrr = new ObjClassCodeAtrr();
        objclasscodeatrr.setId(1000);
        objclasscodeatrr.setTimeStamp(new Date());
        objclasscodeatrr.setClassCode("jil");
        objclasscodeatrr.setMarketCode("jili成");
        objclasscodeatrr.setUnderlyingCode("jili成");
        objclasscodeatrr.setUpDownLimit((float) 15.23);
        objclasscodeatrr.setContractSize(1000);
        objclasscodeatrr.setTradeType("jili成");
        objclasscodeatrr.setClearType("jili成");
        objclasscodeatrr.setAlterUserID("jili成");
        objclasscodeatrr.setCreateTime(new Date());
        return objclasscodeatrr;
    }

    public ObjETF genObjETF() {
        ObjETF objetf = new ObjETF();
        objetf.setId(1000);
        objetf.setTimeStamp(new Date());
        objetf.setEtfid("jili成");
        objetf.setObj("jili成");
        objetf.setPrimaryObj("jili成");
        objetf.setCashObj("jili成");
        objetf.setName("jili成");
        objetf.setMarketCode("jili成");
        objetf.setFundManagementCompany("jili成");
        objetf.setUnderlyingIndex("jili成");
        objetf.setTradeUnit(1000);
        objetf.setEstimateCashCompoment((float) 15.23);
        objetf.setMaxCashRatio((float) 15.23);
        objetf.setPublish("jili成");
        objetf.setCreation("jili成");
        objetf.setRedemption("jili成");
        objetf.setRecordeNum(1000);
        objetf.setListingDate(new Date());
        objetf.setCashComponent((float) 15.23);
        objetf.setNavPercu((float) 15.23);
        objetf.setNav((float) 15.23);
        objetf.setDividendPercu((float) 15.23);
        objetf.setOwnUserID("jili成");
        objetf.setAlterUserID("jili成");
        objetf.setCreateTime(new Date());
        return objetf;
    }

    public ObjETFList genObjETFList() {
        ObjETFList objetflist = new ObjETFList();
        objetflist.setId(1000);
        objetflist.setTimeStamp(new Date());
        objetflist.setEtfid("jili成");
        objetflist.setListingDate(new Date());
        objetflist.setRegionID("jili成");
        objetflist.setObj("jili成");
        objetflist.setMarketCode("jili成");
        objetflist.setClassCode("jil");
        objetflist.setQty(1000);
        objetflist.setCanCashRepl("jili成");
        objetflist.setCashBuffer((float) 15.23);
        objetflist.setCashAmount((float) 15.23);
        objetflist.setReMark("jili成");
        objetflist.setOwnUserID("jili成");
        objetflist.setAlterUserID("jili成");
        objetflist.setCreateTime(new Date());
        return objetflist;
    }

    public ObjExRightInfo genObjExRightInfo() {
        ObjExRightInfo objexrightinfo = new ObjExRightInfo();
        objexrightinfo.setId(1000);
        objexrightinfo.setTimeStamp(new Date());
        objexrightinfo.setRegionID("jili成");
        objexrightinfo.setObj("jili成");
        objexrightinfo.setMarketCode("jili成");
        objexrightinfo.setClassCode("jil");
        objexrightinfo.setExRightDate(new Date());
        objexrightinfo.setRecordeDate(new Date());
        objexrightinfo.setCheckInRightDate(new Date());
        objexrightinfo.setCheckInProfitsDate(new Date());
        objexrightinfo.setDenominator(1000);
        objexrightinfo.setMemberA(1000);
        objexrightinfo.setMemberB(1000);
        objexrightinfo.setProfits((float) 15.23);
        objexrightinfo.setReMark("jili成");
        objexrightinfo.setOwnUserID("jili成");
        objexrightinfo.setAlterUserID("jili成");
        objexrightinfo.setCreateTime(new Date());
        return objexrightinfo;
    }

    public ObjIndex genObjIndex() {
        ObjIndex objindex = new ObjIndex();
        objindex.setIndexID(1000);
        objindex.setTimeStamp(new Date());
        objindex.setIndexObj("jili成");
        objindex.setListingDate(new Date());
        objindex.setIndexName("jili成");
        objindex.setRegionID("jili成");
        objindex.setMarketCode("jili成");
        objindex.setOwnUserID("jili成");
        objindex.setAlterUserID("jili成");
        objindex.setCreateTime(new Date());
        return objindex;
    }

    public ObjIndexList genObjIndexList() {
        ObjIndexList objindexlist = new ObjIndexList();
        objindexlist.setId(1000);
        objindexlist.setTimeStamp(new Date());
        objindexlist.setIndexID(1000);
        objindexlist.setIndexObj("jili成");
        objindexlist.setObj("jili成");
        objindexlist.setRegionID("jili成");
        objindexlist.setMarketCode("jili成");
        objindexlist.setClassCode("jil");
        objindexlist.setQty(1000);
        objindexlist.setRate((float) 15.23);
        objindexlist.setListingDate(new Date());
        objindexlist.setOwnUserID("jili成");
        objindexlist.setAlterUserID("jili成");
        objindexlist.setCreateTime(new Date());
        return objindexlist;
    }

    public ObjInfo genObjInfo() {
        ObjInfo objinfo = new ObjInfo();
        objinfo.setId(1000);
        objinfo.setTimeStamp(new Date());
        objinfo.setObjID("jili成");
        objinfo.setRegionID("jili成");
        objinfo.setObj("jili成");
        objinfo.setMarketCode("jili成");
        objinfo.setClassCode("jil");
        objinfo.setName("jili成");
        objinfo.setEName("jili成");
        objinfo.setLongName("jili成");
        objinfo.setListingDate(new Date());
        objinfo.setFaceValue(1000);
        objinfo.setTradeUnit(1000);
        objinfo.setCurrency("jili成");
        objinfo.setContractSize(1000);
        objinfo.setTradeType("jili成");
        objinfo.setClearType("jili成");
        objinfo.setContractMonth("jili成");
        objinfo.setExpirationDate(new Date());
        objinfo.setUnderlyingCode("jili成");
        objinfo.setUnderlyingobj("jili成");
        objinfo.setFirst("jili成");
        objinfo.setSecend("jili成");
        objinfo.setThird("jili成");
        objinfo.setPutCall("jili成");
        objinfo.setStrikePrice((float) 15.23);
        objinfo.setOptionRate((float) 15.23);
        objinfo.setOwnUserID("jili成");
        objinfo.setAlterUserID("jili成");
        objinfo.setCreateTime(new Date());
        return objinfo;
    }

    public ObjNewStock genObjNewStock() {
        ObjNewStock objnewstock = new ObjNewStock();
        objnewstock.setId(1000);
        objnewstock.setTimeStamp(new Date());
        objnewstock.setObj("jili成");
        objnewstock.setPrimaryObj("jili成");
        objnewstock.setName("jili成");
        objnewstock.setRegionID("jili成");
        objnewstock.setMarketCode("jili成");
        objnewstock.setClassCode("jil");
        objnewstock.setTradeUnit(1000);
        objnewstock.setMaxQty(1000);
        objnewstock.setPrimaryTradeDate(new Date());
        objnewstock.setTradeDate(new Date());
        objnewstock.setOwnUserID("jili成");
        objnewstock.setAlterUserID("jili成");
        objnewstock.setCreateTime(new Date());
        return objnewstock;
    }

    public ObjPriceTick genObjPriceTick() {
        ObjPriceTick objpricetick = new ObjPriceTick();
        objpricetick.setId(1000);
        objpricetick.setTimeStamp(new Date());
        objpricetick.setClassCode("jil");
        objpricetick.setMarketCode("jili成");
        objpricetick.setUnderlyingCode("jili成");
        objpricetick.setLowerRange((float) 15.23);
        objpricetick.setUpperRange((float) 15.23);
        objpricetick.setTick((float) 15.23);
        objpricetick.setAlterUserID("jili成");
        objpricetick.setCreateTime(new Date());
        return objpricetick;
    }

    public ObjProtfolio genObjProtfolio() {
        ObjProtfolio objprotfolio = new ObjProtfolio();
        objprotfolio.setProtfolioID(1000);
        objprotfolio.setTimeStamp(new Date());
        objprotfolio.setName("jili成");
        objprotfolio.setIsPublic("jili成");
        objprotfolio.setOwnUserID("jili成");
        objprotfolio.setAlterUserID("jili成");
        objprotfolio.setCreateTime(new Date());
        return objprotfolio;
    }

    public ObjProtfolioList genObjProtfolioList() {
        ObjProtfolioList objprotfoliolist = new ObjProtfolioList();
        objprotfoliolist.setId(1000);
        objprotfoliolist.setTimeStamp(new Date());
        objprotfoliolist.setProtfolioID(1000);
        objprotfoliolist.setObj("jili成");
        objprotfoliolist.setMarketCode("jili成");
        objprotfoliolist.setClassCode("jil");
        objprotfoliolist.setRegionID("jili成");
        objprotfoliolist.setQty(1000);
        objprotfoliolist.setRate((float) 15.23);
        objprotfoliolist.setReMark("jili成");
        objprotfoliolist.setOwnUserID("jili成");
        objprotfoliolist.setAlterUserID("jili成");
        objprotfoliolist.setCreateTime(new Date());
        return objprotfoliolist;
    }

    public ObjStructuredFundBase genObjStructuredFundBase() {
        ObjStructuredFundBase objstructuredfundbase = new ObjStructuredFundBase();
        objstructuredfundbase.setObj("jili成");
        objstructuredfundbase.setTimeStamp(new Date());
        objstructuredfundbase.setName("jili成");
        objstructuredfundbase.setMarketCode("jili成");
        objstructuredfundbase.setFundManagementCompany("jili成");
        objstructuredfundbase.setUnderlyingIndex("jili成");
        objstructuredfundbase.setTradingdate(new Date());
        objstructuredfundbase.setOwnUserID("jili成");
        objstructuredfundbase.setAlterUserID("jili成");
        objstructuredfundbase.setCreateTime(new Date());
        return objstructuredfundbase;
    }

    public ObjStructuredFundPart genObjStructuredFundPart() {
        ObjStructuredFundPart objstructuredfundpart = new ObjStructuredFundPart();
        objstructuredfundpart.setObj("jili成");
        objstructuredfundpart.setTimeStamp(new Date());
        objstructuredfundpart.setBaseObj("jili成");
        objstructuredfundpart.setMarketCode("jili成");
        objstructuredfundpart.setType("jili成");
        objstructuredfundpart.setRate((float) 15.23);
        objstructuredfundpart.setOwnUserID("jili成");
        objstructuredfundpart.setAlterUserID("jili成");
        objstructuredfundpart.setCreateTime(new Date());
        return objstructuredfundpart;
    }

    public ObjUnderlyingCode genObjUnderlyingCode() {
        ObjUnderlyingCode objunderlyingcode = new ObjUnderlyingCode();
        objunderlyingcode.setId(1000);
        objunderlyingcode.setTimeStamp(new Date());
        objunderlyingcode.setUnderlyingCode("jili成");
        objunderlyingcode.setRegionID("jili成");
        objunderlyingcode.setMarketCode("jili成");
        objunderlyingcode.setClassCode("jil");
        objunderlyingcode.setUnderlyingName("jili成");
        objunderlyingcode.setCurrency("jili成");
        objunderlyingcode.setTick((float) 15.23);
        objunderlyingcode.setTradeUnit(1000);
        objunderlyingcode.setContractSize(1000);
        objunderlyingcode.setOwnUserID("jili成");
        objunderlyingcode.setAlterUserID("jili成");
        objunderlyingcode.setCreateTime(new Date());
        return objunderlyingcode;
    }

    public ProductAccount genProductAccount() {
        ProductAccount productaccount = new ProductAccount();
        productaccount.setId(1000);
        productaccount.setTimeStamp(new Date());
        productaccount.setProductID(1000);
        productaccount.setUnitID(1000);
        productaccount.setAccountCode("jili成");
        productaccount.setNet((float) 15.23);
        productaccount.setShare(1000000L);
        productaccount.setStartDate(new Date());
        productaccount.setEndDate(new Date());
        productaccount.setBelongGroupID(1000);
        productaccount.setOwnUserID("jili成");
        productaccount.setAlterUserID("jili成");
        productaccount.setCreateTime(new Date());
        return productaccount;
    }

    public ProductInfo genProductInfo() {
        ProductInfo productinfo = new ProductInfo();
        productinfo.setProductID(1000);
        productinfo.setTimeStamp(new Date());
        productinfo.setProductName("jili成");
        productinfo.setNet((float) 15.23);
        productinfo.setShare(1000000L);
        productinfo.setStartDate(new Date());
        productinfo.setEndDate(new Date());
        productinfo.setBelongGroupID(1000);
        productinfo.setType("jili成");
        productinfo.setReMark("jili成");
        productinfo.setOwnUserID("jili成");
        productinfo.setAlterUserID("jili成");
        productinfo.setCreateTime(new Date());
        return productinfo;
    }

    public ProductUnit genProductUnit() {
        ProductUnit productunit = new ProductUnit();
        productunit.setUnitID(1000);
        productunit.setTimeStamp(new Date());
        productunit.setProductID(1000);
        productunit.setUnitName("jili成");
        productunit.setNet((float) 15.23);
        productunit.setShare(1000000L);
        productunit.setStartDate(new Date());
        productunit.setEndDate(new Date());
        productunit.setBelongGroupID(1000);
        productunit.setOwnUserID("jili成");
        productunit.setAlterUserID("jili成");
        productunit.setCreateTime(new Date());
        return productunit;
    }

    public RecentClearingAccountBail genRecentClearingAccountBail() {
        RecentClearingAccountBail recentclearingaccountbail = new RecentClearingAccountBail();
        recentclearingaccountbail.setId(1000000L);
        recentclearingaccountbail.setTimeStamp(new Date());
        recentclearingaccountbail.setClearingDate(new Date());
        recentclearingaccountbail.setAccountCode("jili成");
        recentclearingaccountbail.setTempID(1000);
        recentclearingaccountbail.setMarketCode("jili成");
        recentclearingaccountbail.setClassCode("jil");
        recentclearingaccountbail.setBs("jili成");
        recentclearingaccountbail.setUnderlyingCode("jili成");
        recentclearingaccountbail.setObj("jili成");
        recentclearingaccountbail.setBail0((float) 15.23);
        recentclearingaccountbail.setBail1((float) 15.23);
        recentclearingaccountbail.setBail2((float) 15.23);
        recentclearingaccountbail.setBail0Rate((float) 15.23);
        recentclearingaccountbail.setBail1Rate((float) 15.23);
        recentclearingaccountbail.setBail2Rate((float) 15.23);
        recentclearingaccountbail.setOwnUserID("jili成");
        recentclearingaccountbail.setAlterUserID("jili成");
        recentclearingaccountbail.setCreateTime(new Date());
        return recentclearingaccountbail;
    }

    public RecentClearingAccountFare genRecentClearingAccountFare() {
        RecentClearingAccountFare recentclearingaccountfare = new RecentClearingAccountFare();
        recentclearingaccountfare.setId(1000000L);
        recentclearingaccountfare.setTimeStamp(new Date());
        recentclearingaccountfare.setClearingDate(new Date());
        recentclearingaccountfare.setAccountCode("jili成");
        recentclearingaccountfare.setTempID(1000);
        recentclearingaccountfare.setMarketCode("jili成");
        recentclearingaccountfare.setClassCode("jil");
        recentclearingaccountfare.setUnderlyingCode("jili成");
        recentclearingaccountfare.setObj("jili成");
        recentclearingaccountfare.setBs("jili成");
        recentclearingaccountfare.setOpenClose("jili成");
        recentclearingaccountfare.setCommission0((float) 15.23);
        recentclearingaccountfare.setCommission1((float) 15.23);
        recentclearingaccountfare.setMinCommission0((float) 15.23);
        recentclearingaccountfare.setMinCommission1((float) 15.23);
        recentclearingaccountfare.setDeliverFare0((float) 15.23);
        recentclearingaccountfare.setDeliverFare1((float) 15.23);
        recentclearingaccountfare.setTax0((float) 15.23);
        recentclearingaccountfare.setTax1((float) 15.23);
        recentclearingaccountfare.setEtax1((float) 15.23);
        recentclearingaccountfare.setEtax0((float) 15.23);
        recentclearingaccountfare.setCost0((float) 15.23);
        recentclearingaccountfare.setCost1((float) 15.23);
        recentclearingaccountfare.setOwnUserID("jili成");
        recentclearingaccountfare.setAlterUserID("jili成");
        recentclearingaccountfare.setCreateTime(new Date());
        return recentclearingaccountfare;
    }

    public RecentClearingDayBook genRecentClearingDayBook() {
        RecentClearingDayBook recentclearingdaybook = new RecentClearingDayBook();
        recentclearingdaybook.setId(1000000L);
        recentclearingdaybook.setTimeStamp(new Date());
        recentclearingdaybook.setDayid(1000000L);
        recentclearingdaybook.setBookDate(new Date());
        recentclearingdaybook.setAccountCode("jili成");
        recentclearingdaybook.setBookID(1000);
        recentclearingdaybook.setExeTime(new Date());
        recentclearingdaybook.setTradeDate(new Date());
        recentclearingdaybook.setAccountTitle("jili成");
        recentclearingdaybook.setAmountReceived(45.123);
        recentclearingdaybook.setAmountPayed(45.123);
        recentclearingdaybook.setBalance(45.123);
        recentclearingdaybook.setMarketCode("jili成");
        recentclearingdaybook.setClassCode("jil");
        recentclearingdaybook.setObj("jili成");
        recentclearingdaybook.setMac("jili成");
        recentclearingdaybook.setIp4(1000);
        recentclearingdaybook.setOrderTypeEmit("jili成");
        recentclearingdaybook.setTradeOperate(1000);
        recentclearingdaybook.setCloseType("jili成");
        recentclearingdaybook.setBs("jili成");
        recentclearingdaybook.setOpenClose("jili成");
        recentclearingdaybook.setHedgeFlag("jili成");
        recentclearingdaybook.setPosID("jili成");
        recentclearingdaybook.setExeSequenceNo(1000);
        recentclearingdaybook.setOutsideExeNo("jili成");
        recentclearingdaybook.setExeQty(1000);
        recentclearingdaybook.setExePrice((float) 15.23);
        recentclearingdaybook.setCurrency("jili成");
        recentclearingdaybook.setExeAmount((float) 15.23);
        recentclearingdaybook.setTotalFare((float) 15.23);
        recentclearingdaybook.setCommissionFare((float) 15.23);
        recentclearingdaybook.setDeliverFare((float) 15.23);
        recentclearingdaybook.setTax((float) 15.23);
        recentclearingdaybook.setOtherCost((float) 15.23);
        recentclearingdaybook.setEsCheckinAmount(45.123);
        recentclearingdaybook.setInvestID("jili成");
        recentclearingdaybook.setOwnUserID("jili成");
        recentclearingdaybook.setAlterUserID("jili成");
        recentclearingdaybook.setCreateTime(new Date());
        return recentclearingdaybook;
    }

    public RecentClearingExRight genRecentClearingExRight() {
        RecentClearingExRight recentclearingexright = new RecentClearingExRight();
        recentclearingexright.setId(1000000L);
        recentclearingexright.setTimeStamp(new Date());
        recentclearingexright.setAccountCode("jili成");
        recentclearingexright.setClearingDate(new Date());
        recentclearingexright.setObj("jili成");
        recentclearingexright.setMarketCode("jili成");
        recentclearingexright.setClassCode("jil");
        recentclearingexright.setRecordeDate(new Date());
        recentclearingexright.setPosQty(1000);
        recentclearingexright.setProfitsAmount((float) 15.23);
        recentclearingexright.setShareA(1000);
        recentclearingexright.setShareB(1000);
        recentclearingexright.setTax((float) 15.23);
        recentclearingexright.setNetProfitsAmount((float) 15.23);
        recentclearingexright.setReMark("jili成");
        recentclearingexright.setOwnUserID("jili成");
        recentclearingexright.setAlterUserID("jili成");
        recentclearingexright.setCreateTime(new Date());
        return recentclearingexright;
    }

    public RecentClearingFundState genRecentClearingFundState() {
        RecentClearingFundState recentclearingfundstate = new RecentClearingFundState();
        recentclearingfundstate.setId(1000000L);
        recentclearingfundstate.setTimeStamp(new Date());
        recentclearingfundstate.setClearingDate(new Date());
        recentclearingfundstate.setAccountCode("jili成");
        recentclearingfundstate.setFundTotal((float) 15.23);
        recentclearingfundstate.setFundAvl((float) 15.23);
        recentclearingfundstate.setFundFroze((float) 15.23);
        recentclearingfundstate.setAssetTotal((float) 15.23);
        recentclearingfundstate.setAssetNet((float) 15.23);
        recentclearingfundstate.setAssetCredit((float) 15.23);
        recentclearingfundstate.setPosTotalValue((float) 15.23);
        recentclearingfundstate.setPosNetValue((float) 15.23);
        recentclearingfundstate.setPosCreditValue((float) 15.23);
        recentclearingfundstate.setCurrency("jili成");
        recentclearingfundstate.setRate((float) 15.23);
        recentclearingfundstate.setEqualFundAvl((float) 15.23);
        recentclearingfundstate.setBefAssetTotal((float) 15.23);
        recentclearingfundstate.setBefFundAvl((float) 15.23);
        recentclearingfundstate.setOwnUserID("jili成");
        recentclearingfundstate.setAlterUserID("jili成");
        recentclearingfundstate.setCreateTime(new Date());
        return recentclearingfundstate;
    }

    public RecentClearingPosDetail genRecentClearingPosDetail() {
        RecentClearingPosDetail recentclearingposdetail = new RecentClearingPosDetail();
        recentclearingposdetail.setId(1000000L);
        recentclearingposdetail.setTimeStamp(new Date());
        recentclearingposdetail.setClearingDate(new Date());
        recentclearingposdetail.setAccountCode("jili成");
        recentclearingposdetail.setPosID("jili成");
        recentclearingposdetail.setObj("jili成");
        recentclearingposdetail.setMarketCode("jili成");
        recentclearingposdetail.setClassCode("jil");
        recentclearingposdetail.setBs("jili成");
        recentclearingposdetail.setQty(1000);
        recentclearingposdetail.setAvlQty(1000);
        recentclearingposdetail.setFrozeQty(1000);
        recentclearingposdetail.setClosePrice((float) 15.23);
        recentclearingposdetail.setCostPriceA((float) 15.23);
        recentclearingposdetail.setCostPriceB((float) 15.23);
        recentclearingposdetail.setOpencostprice((float) 15.23);
        recentclearingposdetail.setAvgCostPrice((float) 15.23);
        recentclearingposdetail.setHedgeFlag("jili成");
        recentclearingposdetail.setPosType("jili成");
        recentclearingposdetail.setPutCall("jili成");
        recentclearingposdetail.setCurrency("jili成");
        recentclearingposdetail.setPl((float) 15.23);
        recentclearingposdetail.setCostPL((float) 15.23);
        recentclearingposdetail.setTotalPL((float) 15.23);
        recentclearingposdetail.setMargin((float) 15.23);
        recentclearingposdetail.setTotalFare((float) 15.23);
        recentclearingposdetail.setInvestID("jili成");
        recentclearingposdetail.setOwnUserID("jili成");
        recentclearingposdetail.setAlterUserID("jili成");
        recentclearingposdetail.setCreateTime(new Date());
        return recentclearingposdetail;
    }

    public RecentClearingPrice genRecentClearingPrice() {
        RecentClearingPrice recentclearingprice = new RecentClearingPrice();
        recentclearingprice.setId(1000000L);
        recentclearingprice.setTimeStamp(new Date());
        recentclearingprice.setClearingDate(new Date());
        recentclearingprice.setRegionID("jili成");
        recentclearingprice.setObj("jili成");
        recentclearingprice.setClassCode("jil");
        recentclearingprice.setMarketCode("jili成");
        recentclearingprice.setClosePrice((float) 15.23);
        recentclearingprice.setClearprice((float) 15.23);
        recentclearingprice.setALCPrice((float) 15.23);
        recentclearingprice.setOwnUserID("jili成");
        recentclearingprice.setAlterUserID("jili成");
        recentclearingprice.setCreateTime(new Date());
        return recentclearingprice;
    }

    public RecentClearingProtfoliopos genRecentClearingProtfoliopos() {
        RecentClearingProtfoliopos recentclearingprotfoliopos = new RecentClearingProtfoliopos();
        recentclearingprotfoliopos.setId(1000);
        recentclearingprotfoliopos.setTimeStamp(new Date());
        recentclearingprotfoliopos.setClearingDate(new Date());
        recentclearingprotfoliopos.setAccountCode("jili成");
        recentclearingprotfoliopos.setPosID("jili成");
        recentclearingprotfoliopos.setQty(1000);
        recentclearingprotfoliopos.setAvlQty(1000);
        recentclearingprotfoliopos.setFrozeQty(1000);
        recentclearingprotfoliopos.setClosePrice((float) 15.23);
        recentclearingprotfoliopos.setCostPriceA((float) 15.23);
        recentclearingprotfoliopos.setCostPriceB((float) 15.23);
        recentclearingprotfoliopos.setOpencostprice((float) 15.23);
        recentclearingprotfoliopos.setHedgeFlag("jili成");
        recentclearingprotfoliopos.setCurrency("jili成");
        recentclearingprotfoliopos.setPl((float) 15.23);
        recentclearingprotfoliopos.setCostPL((float) 15.23);
        recentclearingprotfoliopos.setTotalPL((float) 15.23);
        recentclearingprotfoliopos.setMargin((float) 15.23);
        recentclearingprotfoliopos.setTotalFare((float) 15.23);
        recentclearingprotfoliopos.setInvestID("jili成");
        recentclearingprotfoliopos.setOwnUserID("jili成");
        recentclearingprotfoliopos.setAlterUserID("jili成");
        recentclearingprotfoliopos.setCreateTime(new Date());
        return recentclearingprotfoliopos;
    }

    public RecentObjETF genRecentObjETF() {
        RecentObjETF recentobjetf = new RecentObjETF();
        recentobjetf.setId(1000);
        recentobjetf.setTimeStamp(new Date());
        recentobjetf.setListingDate(new Date());
        recentobjetf.setEtfid("jili成");
        recentobjetf.setObj("jili成");
        recentobjetf.setPrimaryObj("jili成");
        recentobjetf.setCashObj("jili成");
        recentobjetf.setName("jili成");
        recentobjetf.setMarketCode("jili成");
        recentobjetf.setFundManagementCompany("jili成");
        recentobjetf.setUnderlyingIndex("jili成");
        recentobjetf.setTradeUnit(1000);
        recentobjetf.setEstimateCashCompoment((float) 15.23);
        recentobjetf.setMaxCashRatio((float) 15.23);
        recentobjetf.setPublish("jili成");
        recentobjetf.setCreation("jili成");
        recentobjetf.setRedemption("jili成");
        recentobjetf.setRecordeNum(1000);
        recentobjetf.setCashComponent((float) 15.23);
        recentobjetf.setNavPercu((float) 15.23);
        recentobjetf.setNav((float) 15.23);
        recentobjetf.setDividendPercu((float) 15.23);
        recentobjetf.setOwnUserID("jili成");
        recentobjetf.setAlterUserID("jili成");
        recentobjetf.setCreateTime(new Date());
        return recentobjetf;
    }

    public RecentObjETFList genRecentObjETFList() {
        RecentObjETFList recentobjetflist = new RecentObjETFList();
        recentobjetflist.setId(1000);
        recentobjetflist.setTimeStamp(new Date());
        recentobjetflist.setEtfid("jili成");
        recentobjetflist.setListingDate(new Date());
        recentobjetflist.setObj("jili成");
        recentobjetflist.setMarketCode("jili成");
        recentobjetflist.setClassCode("jil");
        recentobjetflist.setQty(1000);
        recentobjetflist.setCanCashRepl("jili成");
        recentobjetflist.setCashBuffer((float) 15.23);
        recentobjetflist.setCashAmount((float) 15.23);
        recentobjetflist.setReMark("jili成");
        recentobjetflist.setOwnUserID("jili成");
        recentobjetflist.setAlterUserID("jili成");
        recentobjetflist.setCreateTime(new Date());
        return recentobjetflist;
    }

    public RecentObjIndex genRecentObjIndex() {
        RecentObjIndex recentobjindex = new RecentObjIndex();
        recentobjindex.setIndexID(1000);
        recentobjindex.setTimeStamp(new Date());
        recentobjindex.setListingDate(new Date());
        recentobjindex.setIndexObj("jili成");
        recentobjindex.setIndexName("jili成");
        recentobjindex.setRegionID("jili成");
        recentobjindex.setMarketCode("jili成");
        recentobjindex.setOwnUserID("jili成");
        recentobjindex.setAlterUserID("jili成");
        recentobjindex.setCreateTime(new Date());
        return recentobjindex;
    }

    public RecentObjIndexList genRecentObjIndexList() {
        RecentObjIndexList recentobjindexlist = new RecentObjIndexList();
        recentobjindexlist.setId(1000);
        recentobjindexlist.setTimeStamp(new Date());
        recentobjindexlist.setIndexID(1000);
        recentobjindexlist.setIndexObj("jili成");
        recentobjindexlist.setObj("jili成");
        recentobjindexlist.setRegionID("jili成");
        recentobjindexlist.setMarketCode("jili成");
        recentobjindexlist.setClassCode("jil");
        recentobjindexlist.setQty(1000);
        recentobjindexlist.setRate((float) 15.23);
        recentobjindexlist.setListingDate(new Date());
        recentobjindexlist.setOwnUserID("jili成");
        recentobjindexlist.setAlterUserID("jili成");
        recentobjindexlist.setCreateTime(new Date());
        return recentobjindexlist;
    }

    public RecentTradeAlterOrder genRecentTradeAlterOrder() {
        RecentTradeAlterOrder recenttradealterorder = new RecentTradeAlterOrder();
        recenttradealterorder.setId(1000000L);
        recenttradealterorder.setTimeStamp(new Date());
        recenttradealterorder.setOrderID(1000);
        recenttradealterorder.setDealerUserID("jili成");
        recenttradealterorder.setAccountCode("jili成");
        recenttradealterorder.setTime(new Date());
        recenttradealterorder.setTradeDate(new Date());
        recenttradealterorder.setDateTime(new Date());
        recenttradealterorder.setMac("jili成");
        recenttradealterorder.setIp4(1000);
        recenttradealterorder.setOrderTypeEmit("jili成");
        recenttradealterorder.setTradeOperate(1000);
        recenttradealterorder.setEntrustNo(1000);
        recenttradealterorder.setEntrustkey("jili成");
        recenttradealterorder.setAlterQty(1000);
        recenttradealterorder.setAlterPrice((float) 15.23);
        recenttradealterorder.setWorkStationID("jili成");
        recenttradealterorder.setOwnUserID("jili成");
        recenttradealterorder.setAlterUserID("jili成");
        recenttradealterorder.setCreateTime(new Date());
        return recenttradealterorder;
    }

    public RecentTradeCancelOrder genRecentTradeCancelOrder() {
        RecentTradeCancelOrder recenttradecancelorder = new RecentTradeCancelOrder();
        recenttradecancelorder.setId(1000000L);
        recenttradecancelorder.setTimeStamp(new Date());
        recenttradecancelorder.setOrderID(1000);
        recenttradecancelorder.setDealerUserID("jili成");
        recenttradecancelorder.setAccountCode("jili成");
        recenttradecancelorder.setTime(new Date());
        recenttradecancelorder.setTradeDate(new Date());
        recenttradecancelorder.setDateTime(new Date());
        recenttradecancelorder.setMac("jili成");
        recenttradecancelorder.setIp4(1000);
        recenttradecancelorder.setOrderTypeEmit("jili成");
        recenttradecancelorder.setTradeOperate(1000);
        recenttradecancelorder.setEntrustNo(1000);
        recenttradecancelorder.setEntrustkey("jili成");
        recenttradecancelorder.setCancelQty(1000);
        recenttradecancelorder.setWorkStationID("jili成");
        recenttradecancelorder.setOwnUserID("jili成");
        recenttradecancelorder.setAlterUserID("jili成");
        recenttradecancelorder.setCreateTime(new Date());
        return recenttradecancelorder;
    }

    public RecentTradeExecuteOrder genRecentTradeExecuteOrder() {
        RecentTradeExecuteOrder recenttradeexecuteorder = new RecentTradeExecuteOrder();
        recenttradeexecuteorder.setId(1000000L);
        recenttradeexecuteorder.setTimeStamp(new Date());
        recenttradeexecuteorder.setOrderID(1000);
        recenttradeexecuteorder.setAccountCode("jili成");
        recenttradeexecuteorder.setDealerUserID("jili成");
        recenttradeexecuteorder.setSubmitTime(new Date());
        recenttradeexecuteorder.setTradeDate(new Date());
        recenttradeexecuteorder.setDateTime(new Date());
        recenttradeexecuteorder.setMac("jili成");
        recenttradeexecuteorder.setIp4(1000);
        recenttradeexecuteorder.setOrderTypeEmit("jili成");
        recenttradeexecuteorder.setTradeOperate(1000);
        recenttradeexecuteorder.setMarketCode("jili成");
        recenttradeexecuteorder.setClassCode("jil");
        recenttradeexecuteorder.setObj("jili成");
        recenttradeexecuteorder.setBs("jili成");
        recenttradeexecuteorder.setOpenClose("jili成");
        recenttradeexecuteorder.setHedgeFlag("jili成");
        recenttradeexecuteorder.setPosID("jili成");
        recenttradeexecuteorder.setCustomCode("jili成");
        recenttradeexecuteorder.setCloseType("jili成");
        recenttradeexecuteorder.setBasketID(1000);
        recenttradeexecuteorder.setBasketTatol(1000);
        recenttradeexecuteorder.setSeqNum(1000);
        recenttradeexecuteorder.setExeTime(new Date());
        recenttradeexecuteorder.setExeSequenceNo(1000);
        recenttradeexecuteorder.setOutsideExeNo("jili成");
        recenttradeexecuteorder.setExeQty(1000);
        recenttradeexecuteorder.setExePrice((float) 15.23);
        recenttradeexecuteorder.setCurrency("jili成");
        recenttradeexecuteorder.setExeAmount((float) 15.23);
        recenttradeexecuteorder.setCancelQty(1000);
        recenttradeexecuteorder.setOrderQty(1000);
        recenttradeexecuteorder.setWorkingQty(1000);
        recenttradeexecuteorder.setCompleteQty(1000);
        recenttradeexecuteorder.setOrderStatus("jili成");
        recenttradeexecuteorder.setTotalFare((float) 15.23);
        recenttradeexecuteorder.setCommissionFare((float) 15.23);
        recenttradeexecuteorder.setDeliverFare((float) 15.23);
        recenttradeexecuteorder.setTax((float) 15.23);
        recenttradeexecuteorder.setOtherCost((float) 15.23);
        recenttradeexecuteorder.setEsCheckinAmount(45.123);
        recenttradeexecuteorder.setReMark("jili成");
        recenttradeexecuteorder.setInvestID("jili成");
        recenttradeexecuteorder.setOwnUserID("jili成");
        recenttradeexecuteorder.setAlterUserID("jili成");
        recenttradeexecuteorder.setCreateTime(new Date());
        return recenttradeexecuteorder;
    }

    public RecentTradeNewOrder genRecentTradeNewOrder() {
        RecentTradeNewOrder recenttradeneworder = new RecentTradeNewOrder();
        recenttradeneworder.setId(1000000L);
        recenttradeneworder.setTimeStamp(new Date());
        recenttradeneworder.setOrderID(1000);
        recenttradeneworder.setAccountCode("jili成");
        recenttradeneworder.setDealerUserID("jili成");
        recenttradeneworder.setSubmitTime(new Date());
        recenttradeneworder.setTradeDate(new Date());
        recenttradeneworder.setDateTime(new Date());
        recenttradeneworder.setMac("jili成");
        recenttradeneworder.setIp4(1000);
        recenttradeneworder.setOrderTypeEmit("jili成");
        recenttradeneworder.setTradeOperate(1000);
        recenttradeneworder.setObj("jili成");
        recenttradeneworder.setMarketCode("jili成");
        recenttradeneworder.setClassCode("jil");
        recenttradeneworder.setBs("jili成");
        recenttradeneworder.setOpenClose("jili成");
        recenttradeneworder.setPriceType("jili成");
        recenttradeneworder.setOrderPrice((float) 15.23);
        recenttradeneworder.setOrderQty(1000);
        recenttradeneworder.setHedgeFlag("jili成");
        recenttradeneworder.setPosID("jili成");
        recenttradeneworder.setCustomCode("jili成");
        recenttradeneworder.setCloseType("jili成");
        recenttradeneworder.setBasketID(1000);
        recenttradeneworder.setBasketTatol(1000);
        recenttradeneworder.setSeqNum(1000);
        recenttradeneworder.setWorkStationID("jili成");
        recenttradeneworder.setOwnUserID("jili成");
        recenttradeneworder.setAlterUserID("jili成");
        recenttradeneworder.setCreateTime(new Date());
        return recenttradeneworder;
    }

    public RecentTradeReportDetail genRecentTradeReportDetail() {
        RecentTradeReportDetail recenttradereportdetail = new RecentTradeReportDetail();
        recenttradereportdetail.setId(1000000L);
        recenttradereportdetail.setTimeStamp(new Date());
        recenttradereportdetail.setMsgType("jili成");
        recenttradereportdetail.setOrderID(1000);
        recenttradereportdetail.setReportNo(1000);
        recenttradereportdetail.setReportContent("jili成");
        recenttradereportdetail.setEntrustNo(1000);
        recenttradereportdetail.setEntrustkey("jili成");
        recenttradereportdetail.setWorkingQty(1000);
        recenttradereportdetail.setUnWorkingQty(1000);
        recenttradereportdetail.setCancelQty(1000);
        recenttradereportdetail.setExeQtyTatol(1000);
        recenttradereportdetail.setExePriceAvg((float) 15.23);
        recenttradereportdetail.setExeAmountTatol(45.123);
        recenttradereportdetail.setPrice((float) 15.23);
        recenttradereportdetail.setExexSeq(1000);
        recenttradereportdetail.setExeSequenceNo(1000);
        recenttradereportdetail.setOutsideExeNo("jili成");
        recenttradereportdetail.setExeQty(1000);
        recenttradereportdetail.setExePrice((float) 15.23);
        recenttradereportdetail.setCurrency("jili成");
        recenttradereportdetail.setExeAmount((float) 15.23);
        recenttradereportdetail.setAccountCode("jili成");
        recenttradereportdetail.setMarketCode("jili成");
        recenttradereportdetail.setClassCode("jil");
        recenttradereportdetail.setObj("jili成");
        recenttradereportdetail.setBs("jili成");
        recenttradereportdetail.setOpenClose("jili成");
        recenttradereportdetail.setHedgeFlag("jili成");
        recenttradereportdetail.setCloseType("jili成");
        recenttradereportdetail.setExeTime(new Date());
        recenttradereportdetail.setEntrustTime(new Date());
        recenttradereportdetail.setSubmitTime(new Date());
        recenttradereportdetail.setTradeDate(new Date());
        recenttradereportdetail.setInvestID("jili成");
        recenttradereportdetail.setReMark("jili成");
        recenttradereportdetail.setOwnUserID("jili成");
        recenttradereportdetail.setAlterUserID("jili成");
        recenttradereportdetail.setCreateTime(new Date());
        return recenttradereportdetail;
    }

    public RiskCtrlAssets genRiskCtrlAssets() {
        RiskCtrlAssets riskctrlassets = new RiskCtrlAssets();
        riskctrlassets.setId(1000);
        riskctrlassets.setTimeStamp(new Date());
        riskctrlassets.setCtrlAssetsCode("jili成");
        riskctrlassets.setObj("jili成");
        riskctrlassets.setIsSingleObj("jili成");
        riskctrlassets.setCalcDirect("jili成");
        riskctrlassets.setOwnUserID("jili成");
        riskctrlassets.setAlterUserID("jili成");
        riskctrlassets.setCreateTime(new Date());
        return riskctrlassets;
    }

    public RiskIndex genRiskIndex() {
        RiskIndex riskindex = new RiskIndex();
        riskindex.setIndexCode("jili成");
        riskindex.setTimeStamp(new Date());
        riskindex.setIndexName("jili成");
        riskindex.setTypeA("jili成");
        riskindex.setTypeB("jili成");
        riskindex.setTypeC("jili成");
        riskindex.setReMark("jili成");
        riskindex.setUnit("jili成");
        riskindex.setIsHaveThreshold("jili成");
        riskindex.setThresholdNum((short) 23);
        riskindex.setThresholdSplitSig("jili成");
        riskindex.setAssetsScope("jili成");
        riskindex.setCanNumerator("jili成");
        riskindex.setCanDenominator("jili成");
        riskindex.setLevelType("jili成");
        riskindex.setIsRateOrAbs("jili成");
        riskindex.setIsForceDone("jili成");
        riskindex.setSceneType("jili成");
        riskindex.setCaseShow("jili成");
        riskindex.setOwnUserID("jili成");
        riskindex.setAlterUserID("jili成");
        riskindex.setCreateTime(new Date());
        return riskindex;
    }

    public RiskSets genRiskSets() {
        RiskSets risksets = new RiskSets();
        risksets.setId(1000);
        risksets.setTimeStamp(new Date());
        risksets.setRiskSetCode("jili成");
        risksets.setIndexCode("jili成");
        risksets.setCtrlLevel("jili成");
        risksets.setCtrlObject("jili成");
        risksets.setCtrlAssetsCode("jili成");
        risksets.setOwnGroupID(1000);
        risksets.setCtrlScene("jili成");
        risksets.setIsForceIntercept("jili成");
        risksets.setStartDate(new Date());
        risksets.setEndDate(new Date());
        risksets.setGateValueA((float) 15.23);
        risksets.setGateValueB((float) 15.23);
        risksets.setGateValueC((float) 15.23);
        risksets.setGateDirectionA("jili成");
        risksets.setGateDirectionB("jili成");
        risksets.setGateDirectionC("jili成");
        risksets.setGateDirectionN("jili成");
        risksets.setGateValueN("jili成");
        risksets.setOwnUserID("jili成");
        risksets.setAlterUserID("jili成");
        risksets.setCreateTime(new Date());
        return risksets;
    }

    public SafeForbidLogin genSafeForbidLogin() {
        SafeForbidLogin safeforbidlogin = new SafeForbidLogin();
        safeforbidlogin.setId(1000);
        safeforbidlogin.setTimeStamp(new Date());
        safeforbidlogin.setMac("jili成");
        safeforbidlogin.setIp4(1000);
        safeforbidlogin.setUserID("jili成");
        safeforbidlogin.setReMark("jili成");
        safeforbidlogin.setOwnUserID("jili成");
        safeforbidlogin.setAlterUserID("jili成");
        safeforbidlogin.setCreateTime(new Date());
        return safeforbidlogin;
    }

    public SafeOtherAccess genSafeOtherAccess() {
        SafeOtherAccess safeotheraccess = new SafeOtherAccess();
        safeotheraccess.setUserID("jili成");
        safeotheraccess.setTimeStamp(new Date());
        safeotheraccess.setMaxOnline(1000);
        safeotheraccess.setLevelPrice("jili成");
        safeotheraccess.setDev("jili成");
        safeotheraccess.setReseach("jili成");
        safeotheraccess.setReBack("jili成");
        safeotheraccess.setTrade("jili成");
        safeotheraccess.setOwnUserID("jili成");
        safeotheraccess.setAlterUserID("jili成");
        safeotheraccess.setCreateTime(new Date());
        return safeotheraccess;
    }

    public TraceAccountBail genTraceAccountBail() {
        TraceAccountBail traceaccountbail = new TraceAccountBail();
        traceaccountbail.setSid(1000);
        traceaccountbail.setTimeStamp(new Date());
        traceaccountbail.setTraceType("jili成");
        traceaccountbail.setId(1000);
        traceaccountbail.setAccountCode("jili成");
        traceaccountbail.setTempID(1000);
        traceaccountbail.setMarketCode("jili成");
        traceaccountbail.setClassCode("jil");
        traceaccountbail.setBs("jili成");
        traceaccountbail.setUnderlyingCode("jili成");
        traceaccountbail.setObj("jili成");
        traceaccountbail.setBail0((float) 15.23);
        traceaccountbail.setBail1((float) 15.23);
        traceaccountbail.setBail2((float) 15.23);
        traceaccountbail.setBail0Rate((float) 15.23);
        traceaccountbail.setBail1Rate((float) 15.23);
        traceaccountbail.setBail2Rate((float) 15.23);
        traceaccountbail.setOwnUserID("jili成");
        traceaccountbail.setAlterUserID("jili成");
        traceaccountbail.setCreateTime(new Date());
        return traceaccountbail;
    }

    public TraceAccountFare genTraceAccountFare() {
        TraceAccountFare traceaccountfare = new TraceAccountFare();
        traceaccountfare.setSid(1000);
        traceaccountfare.setTimeStamp(new Date());
        traceaccountfare.setTraceType("jili成");
        traceaccountfare.setId(1000);
        traceaccountfare.setAccountCode("jili成");
        traceaccountfare.setTempID(1000);
        traceaccountfare.setMarketCode("jili成");
        traceaccountfare.setClassCode("jil");
        traceaccountfare.setUnderlyingCode("jili成");
        traceaccountfare.setObj("jili成");
        traceaccountfare.setBs("jili成");
        traceaccountfare.setOpenClose("jili成");
        traceaccountfare.setCommission0((float) 15.23);
        traceaccountfare.setCommission1((float) 15.23);
        traceaccountfare.setMinCommission0((float) 15.23);
        traceaccountfare.setMinCommission1((float) 15.23);
        traceaccountfare.setDeliverFare0((float) 15.23);
        traceaccountfare.setDeliverFare1((float) 15.23);
        traceaccountfare.setTax0((float) 15.23);
        traceaccountfare.setTax1((float) 15.23);
        traceaccountfare.setEtax1((float) 15.23);
        traceaccountfare.setEtax0((float) 15.23);
        traceaccountfare.setCost0((float) 15.23);
        traceaccountfare.setCost1((float) 15.23);
        traceaccountfare.setOwnUserID("jili成");
        traceaccountfare.setAlterUserID("jili成");
        traceaccountfare.setCreateTime(new Date());
        return traceaccountfare;
    }

    public TraceAccountIcode genTraceAccountIcode() {
        TraceAccountIcode traceaccounticode = new TraceAccountIcode();
        traceaccounticode.setSid(1000);
        traceaccounticode.setTimeStamp(new Date());
        traceaccounticode.setTraceType("jili成");
        traceaccounticode.setId(1000);
        traceaccounticode.setIid(1000);
        traceaccounticode.setInvestID("jili成");
        traceaccounticode.setType("jili成");
        traceaccounticode.setCode("jili成");
        traceaccounticode.setAlterUserID("jili成");
        traceaccounticode.setCreateTime(new Date());
        return traceaccounticode;
    }

    public TraceAccountInfo genTraceAccountInfo() {
        TraceAccountInfo traceaccountinfo = new TraceAccountInfo();
        traceaccountinfo.setSid(1000);
        traceaccountinfo.setTimeStamp(new Date());
        traceaccountinfo.setTraceType("jili成");
        traceaccountinfo.setId(1000);
        traceaccountinfo.setAccountCode("jili成");
        traceaccountinfo.setAccountName("jili成");
        traceaccountinfo.setPipID(1000);
        traceaccountinfo.setPwd("jili成");
        traceaccountinfo.setIsRisk("jili成");
        traceaccountinfo.setIid(1000);
        traceaccountinfo.setType("jili成");
        traceaccountinfo.setInvestID("jili成");
        traceaccountinfo.setOwnUserID("jili成");
        traceaccountinfo.setAlterUserID("jili成");
        traceaccountinfo.setCreateTime(new Date());
        return traceaccountinfo;
    }

    public TraceAccountInvest genTraceAccountInvest() {
        TraceAccountInvest traceaccountinvest = new TraceAccountInvest();
        traceaccountinvest.setSid(1000);
        traceaccountinvest.setTimeStamp(new Date());
        traceaccountinvest.setTraceType("jili成");
        traceaccountinvest.setId(1000);
        traceaccountinvest.setInvestID("jili成");
        traceaccountinvest.setRegionID("jili成");
        traceaccountinvest.setInvestName("jili成");
        traceaccountinvest.setBrokerID("jili成");
        traceaccountinvest.setPwd("jili成");
        traceaccountinvest.setType("jili成");
        traceaccountinvest.setOwnUserID("jili成");
        traceaccountinvest.setAlterUserID("jili成");
        traceaccountinvest.setCreateTime(new Date());
        return traceaccountinvest;
    }

    public TraceBailTemp genTraceBailTemp() {
        TraceBailTemp tracebailtemp = new TraceBailTemp();
        tracebailtemp.setSid(1000);
        tracebailtemp.setTimeStamp(new Date());
        tracebailtemp.setTraceType("jili成");
        tracebailtemp.setTempID(1000);
        tracebailtemp.setTempName("jili成");
        tracebailtemp.setByOwnGroupID(1000);
        tracebailtemp.setReMark("jili成");
        tracebailtemp.setOwnUserID("jili成");
        tracebailtemp.setAlterUserID("jili成");
        tracebailtemp.setCreateTime(new Date());
        return tracebailtemp;
    }

    public TraceBailTempObj genTraceBailTempObj() {
        TraceBailTempObj tracebailtempobj = new TraceBailTempObj();
        tracebailtempobj.setSid(1000);
        tracebailtempobj.setTimeStamp(new Date());
        tracebailtempobj.setTraceType("jili成");
        tracebailtempobj.setId(1000);
        tracebailtempobj.setTempID(1000);
        tracebailtempobj.setMarketCode("jili成");
        tracebailtempobj.setClassCode("jil");
        tracebailtempobj.setBs("jili成");
        tracebailtempobj.setUnderlyingCode("jili成");
        tracebailtempobj.setObj("jili成");
        tracebailtempobj.setBail0((float) 15.23);
        tracebailtempobj.setBail1((float) 15.23);
        tracebailtempobj.setBail2((float) 15.23);
        tracebailtempobj.setBail0Rate((float) 15.23);
        tracebailtempobj.setBail1Rate((float) 15.23);
        tracebailtempobj.setBail2Rate((float) 15.23);
        tracebailtempobj.setOwnUserID("jili成");
        tracebailtempobj.setAlterUserID("jili成");
        tracebailtempobj.setCreateTime(new Date());
        return tracebailtempobj;
    }

    public TraceFareTemp genTraceFareTemp() {
        TraceFareTemp tracefaretemp = new TraceFareTemp();
        tracefaretemp.setSid(1000);
        tracefaretemp.setTimeStamp(new Date());
        tracefaretemp.setTraceType("jili成");
        tracefaretemp.setTempID(1000);
        tracefaretemp.setTempName("jili成");
        tracefaretemp.setByOwnGroupID(1000);
        tracefaretemp.setReMark("jili成");
        tracefaretemp.setOwnUserID("jili成");
        tracefaretemp.setAlterUserID("jili成");
        tracefaretemp.setCreateTime(new Date());
        return tracefaretemp;
    }

    public TraceFareTempeObj genTraceFareTempeObj() {
        TraceFareTempeObj tracefaretempeobj = new TraceFareTempeObj();
        tracefaretempeobj.setSid(1000);
        tracefaretempeobj.setTimeStamp(new Date());
        tracefaretempeobj.setTraceType("jili成");
        tracefaretempeobj.setId(1000);
        tracefaretempeobj.setTempID(1000);
        tracefaretempeobj.setMarketCode("jili成");
        tracefaretempeobj.setClassCode("jil");
        tracefaretempeobj.setUnderlyingCode("jili成");
        tracefaretempeobj.setObj("jili成");
        tracefaretempeobj.setBs("jili成");
        tracefaretempeobj.setOpenClose("jili成");
        tracefaretempeobj.setCommission0((float) 15.23);
        tracefaretempeobj.setCommission1((float) 15.23);
        tracefaretempeobj.setMinCommission0((float) 15.23);
        tracefaretempeobj.setMinCommission1((float) 15.23);
        tracefaretempeobj.setDeliverFare0((float) 15.23);
        tracefaretempeobj.setDeliverFare1((float) 15.23);
        tracefaretempeobj.setTax0((float) 15.23);
        tracefaretempeobj.setTax1((float) 15.23);
        tracefaretempeobj.setEtax1((float) 15.23);
        tracefaretempeobj.setEtax0((float) 15.23);
        tracefaretempeobj.setCost0((float) 15.23);
        tracefaretempeobj.setCost1((float) 15.23);
        tracefaretempeobj.setOwnUserID("jili成");
        tracefaretempeobj.setAlterUserID("jili成");
        tracefaretempeobj.setCreateTime(new Date());
        return tracefaretempeobj;
    }

    public TraceObjInfo genTraceObjInfo() {
        TraceObjInfo traceobjinfo = new TraceObjInfo();
        traceobjinfo.setSid(1000);
        traceobjinfo.setTimeStamp(new Date());
        traceobjinfo.setTraceType("jili成");
        traceobjinfo.setObjID("jili成");
        traceobjinfo.setRegionID("jili成");
        traceobjinfo.setObj("jili成");
        traceobjinfo.setMarketCode("jili成");
        traceobjinfo.setClassCode("jil");
        traceobjinfo.setName("jili成");
        traceobjinfo.setEName("jili成");
        traceobjinfo.setLongName("jili成");
        traceobjinfo.setListingDate(new Date());
        traceobjinfo.setFaceValue(1000);
        traceobjinfo.setTradeUnit(1000);
        traceobjinfo.setCurrency("jili成");
        traceobjinfo.setContractSize(1000);
        traceobjinfo.setTradeType("jili成");
        traceobjinfo.setClearType("jili成");
        traceobjinfo.setContractMonth("jili成");
        traceobjinfo.setExpirationDate(new Date());
        traceobjinfo.setUnderlyingCode("jili成");
        traceobjinfo.setUnderlyingobj("jili成");
        traceobjinfo.setFirst("jili成");
        traceobjinfo.setSecend("jili成");
        traceobjinfo.setThird("jili成");
        traceobjinfo.setPutCall("jili成");
        traceobjinfo.setStrikePrice((float) 15.23);
        traceobjinfo.setOptionRate((float) 15.23);
        traceobjinfo.setOwnUserID("jili成");
        traceobjinfo.setAlterUserID("jili成");
        traceobjinfo.setCreateTime(new Date());
        return traceobjinfo;
    }

    public TraceObjProtfolio genTraceObjProtfolio() {
        TraceObjProtfolio traceobjprotfolio = new TraceObjProtfolio();
        traceobjprotfolio.setSid(1000);
        traceobjprotfolio.setTimeStamp(new Date());
        traceobjprotfolio.setTraceType("jili成");
        traceobjprotfolio.setProtfolioID(1000);
        traceobjprotfolio.setName("jili成");
        traceobjprotfolio.setIsPublic("jili成");
        traceobjprotfolio.setOwnUserID("jili成");
        traceobjprotfolio.setAlterUserID("jili成");
        traceobjprotfolio.setCreateTime(new Date());
        return traceobjprotfolio;
    }

    public TraceObjProtfolioList genTraceObjProtfolioList() {
        TraceObjProtfolioList traceobjprotfoliolist = new TraceObjProtfolioList();
        traceobjprotfoliolist.setSid(1000);
        traceobjprotfoliolist.setTimeStamp(new Date());
        traceobjprotfoliolist.setTraceType("jili成");
        traceobjprotfoliolist.setId(1000);
        traceobjprotfoliolist.setProtfolioID(1000);
        traceobjprotfoliolist.setObj("jili成");
        traceobjprotfoliolist.setMarketCode("jili成");
        traceobjprotfoliolist.setClassCode("jil");
        traceobjprotfoliolist.setRegionID("jili成");
        traceobjprotfoliolist.setQty(1000);
        traceobjprotfoliolist.setRate((float) 15.23);
        traceobjprotfoliolist.setReMark("jili成");
        traceobjprotfoliolist.setOwnUserID("jili成");
        traceobjprotfoliolist.setAlterUserID("jili成");
        traceobjprotfoliolist.setCreateTime(new Date());
        return traceobjprotfoliolist;
    }

    public TraceObjUnderlying genTraceObjUnderlying() {
        TraceObjUnderlying traceobjunderlying = new TraceObjUnderlying();
        traceobjunderlying.setSid(1000);
        traceobjunderlying.setTimeStamp(new Date());
        traceobjunderlying.setTraceType("jili成");
        traceobjunderlying.setId(1000);
        traceobjunderlying.setUnderlyingCode("jili成");
        traceobjunderlying.setRegionID("jili成");
        traceobjunderlying.setMarketCode("jili成");
        traceobjunderlying.setClassCode("jil");
        traceobjunderlying.setUnderlyingName("jili成");
        traceobjunderlying.setCurrency("jili成");
        traceobjunderlying.setTick((float) 15.23);
        traceobjunderlying.setTradeUnit(1000);
        traceobjunderlying.setContractSize(1000);
        traceobjunderlying.setOwnUserID("jili成");
        traceobjunderlying.setAlterUserID("jili成");
        traceobjunderlying.setCreateTime(new Date());
        return traceobjunderlying;
    }

    public TraceProductAccount genTraceProductAccount() {
        TraceProductAccount traceproductaccount = new TraceProductAccount();
        traceproductaccount.setSid(1000);
        traceproductaccount.setTimeStamp(new Date());
        traceproductaccount.setTraceType("jili成");
        traceproductaccount.setId(1000);
        traceproductaccount.setProductID(1000);
        traceproductaccount.setUnitID(1000);
        traceproductaccount.setAccountCode("jili成");
        traceproductaccount.setNet((float) 15.23);
        traceproductaccount.setShare(1000000L);
        traceproductaccount.setStartDate(new Date());
        traceproductaccount.setEndDate(new Date());
        traceproductaccount.setBelongGroupID(1000);
        traceproductaccount.setOwnUserID("jili成");
        traceproductaccount.setAlterUserID("jili成");
        traceproductaccount.setCreateTime(new Date());
        return traceproductaccount;
    }

    public TraceProductInfo genTraceProductInfo() {
        TraceProductInfo traceproductinfo = new TraceProductInfo();
        traceproductinfo.setSid(1000);
        traceproductinfo.setTimeStamp(new Date());
        traceproductinfo.setTraceType("jili成");
        traceproductinfo.setProductID(1000);
        traceproductinfo.setProductName("jili成");
        traceproductinfo.setNet((float) 15.23);
        traceproductinfo.setShare(1000000L);
        traceproductinfo.setStartDate(new Date());
        traceproductinfo.setEndDate(new Date());
        traceproductinfo.setBelongGroupID(1000);
        traceproductinfo.setType("jili成");
        traceproductinfo.setReMark("jili成");
        traceproductinfo.setOwnUserID("jili成");
        traceproductinfo.setAlterUserID("jili成");
        traceproductinfo.setCreateTime(new Date());
        return traceproductinfo;
    }

    public TraceProductUnit genTraceProductUnit() {
        TraceProductUnit traceproductunit = new TraceProductUnit();
        traceproductunit.setSid(1000);
        traceproductunit.setTimeStamp(new Date());
        traceproductunit.setTraceType("jili成");
        traceproductunit.setUnitID(1000);
        traceproductunit.setProductID(1000);
        traceproductunit.setUnitName("jili成");
        traceproductunit.setNet((float) 15.23);
        traceproductunit.setShare(1000000L);
        traceproductunit.setStartDate(new Date());
        traceproductunit.setEndDate(new Date());
        traceproductunit.setBelongGroupID(1000);
        traceproductunit.setOwnUserID("jili成");
        traceproductunit.setAlterUserID("jili成");
        traceproductunit.setCreateTime(new Date());
        return traceproductunit;
    }

    public TraceRiskCtrlAssets genTraceRiskCtrlAssets() {
        TraceRiskCtrlAssets traceriskctrlassets = new TraceRiskCtrlAssets();
        traceriskctrlassets.setSid(1000);
        traceriskctrlassets.setTimeStamp(new Date());
        traceriskctrlassets.setTraceType("jili成");
        traceriskctrlassets.setId(1000);
        traceriskctrlassets.setCtrlAssetsCode("jili成");
        traceriskctrlassets.setObj("jili成");
        traceriskctrlassets.setIsSingleObj("jili成");
        traceriskctrlassets.setCalcDirect("jili成");
        traceriskctrlassets.setOwnUserID("jili成");
        traceriskctrlassets.setAlterUserID("jili成");
        traceriskctrlassets.setCreateTime(new Date());
        return traceriskctrlassets;
    }

    public TraceRiskSets genTraceRiskSets() {
        TraceRiskSets tracerisksets = new TraceRiskSets();
        tracerisksets.setSid(1000);
        tracerisksets.setTimeStamp(new Date());
        tracerisksets.setTraceType("jili成");
        tracerisksets.setId(1000);
        tracerisksets.setRiskSetCode("jili成");
        tracerisksets.setIndexCode("jili成");
        tracerisksets.setCtrlLevel("jili成");
        tracerisksets.setCtrlObject("jili成");
        tracerisksets.setCtrlAssetsCode("jili成");
        tracerisksets.setOwnGroupID(1000);
        tracerisksets.setCtrlScene("jili成");
        tracerisksets.setIsForceIntercept("jili成");
        tracerisksets.setStartDate(new Date());
        tracerisksets.setEndDate(new Date());
        tracerisksets.setGateValueA((float) 15.23);
        tracerisksets.setGateValueB((float) 15.23);
        tracerisksets.setGateValueC((float) 15.23);
        tracerisksets.setGateDirectionA("jili成");
        tracerisksets.setGateDirectionB("jili成");
        tracerisksets.setGateDirectionC("jili成");
        tracerisksets.setGateDirectionN("jili成");
        tracerisksets.setGateValueN("jili成");
        tracerisksets.setOwnUserID("jili成");
        tracerisksets.setAlterUserID("jili成");
        tracerisksets.setCreateTime(new Date());
        return tracerisksets;
    }

    public TraceSafeForbidLogin genTraceSafeForbidLogin() {
        TraceSafeForbidLogin tracesafeforbidlogin = new TraceSafeForbidLogin();
        tracesafeforbidlogin.setSid(1000);
        tracesafeforbidlogin.setTimeStamp(new Date());
        tracesafeforbidlogin.setTraceType("jili成");
        tracesafeforbidlogin.setId(1000);
        tracesafeforbidlogin.setMac("jili成");
        tracesafeforbidlogin.setIp4(1000);
        tracesafeforbidlogin.setUserID("jili成");
        tracesafeforbidlogin.setReMark("jili成");
        tracesafeforbidlogin.setOwnUserID("jili成");
        tracesafeforbidlogin.setAlterUserID("jili成");
        tracesafeforbidlogin.setCreateTime(new Date());
        return tracesafeforbidlogin;
    }

    public TraceSafeOtherAccess genTraceSafeOtherAccess() {
        TraceSafeOtherAccess tracesafeotheraccess = new TraceSafeOtherAccess();
        tracesafeotheraccess.setSid(1000);
        tracesafeotheraccess.setTimeStamp(new Date());
        tracesafeotheraccess.setTraceType("jili成");
        tracesafeotheraccess.setUserID("jili成");
        tracesafeotheraccess.setMaxOnline(1000);
        tracesafeotheraccess.setLevelPrice("jili成");
        tracesafeotheraccess.setDev("jili成");
        tracesafeotheraccess.setReseach("jili成");
        tracesafeotheraccess.setReBack("jili成");
        tracesafeotheraccess.setTrade("jili成");
        tracesafeotheraccess.setOwnUserID("jili成");
        tracesafeotheraccess.setAlterUserID("jili成");
        tracesafeotheraccess.setCreateTime(new Date());
        return tracesafeotheraccess;
    }

    public TraceTradesetExchangeLimit genTraceTradesetExchangeLimit() {
        TraceTradesetExchangeLimit tracetradesetexchangelimit = new TraceTradesetExchangeLimit();
        tracetradesetexchangelimit.setSid(1000);
        tracetradesetexchangelimit.setTimeStamp(new Date());
        tracetradesetexchangelimit.setTraceType("jili成");
        tracetradesetexchangelimit.setId(1000);
        tracetradesetexchangelimit.setMarketCode("jili成");
        tracetradesetexchangelimit.setUnderlyingObj("jili成");
        tracetradesetexchangelimit.setCancelLimit(1000);
        tracetradesetexchangelimit.setOrderLimit(1000);
        tracetradesetexchangelimit.setOnceMaxQty(1000);
        tracetradesetexchangelimit.setReMark("jili成");
        tracetradesetexchangelimit.setOwnUserID("jili成");
        tracetradesetexchangelimit.setAlterUserID("jili成");
        tracetradesetexchangelimit.setCreateTime(new Date());
        return tracetradesetexchangelimit;
    }

    public TraceUserAccess genTraceUserAccess() {
        TraceUserAccess traceuseraccess = new TraceUserAccess();
        traceuseraccess.setSid(1000);
        traceuseraccess.setTimeStamp(new Date());
        traceuseraccess.setTraceType("jili成");
        traceuseraccess.setId(1000);
        traceuseraccess.setUserID("jili成");
        traceuseraccess.setAccessUserID("jili成");
        traceuseraccess.setAccessID("jili成");
        traceuseraccess.setReMark("jili成");
        traceuseraccess.setAlterUserID("jili成");
        traceuseraccess.setCreateTime(new Date());
        return traceuseraccess;
    }

    public TraceUserAccount genTraceUserAccount() {
        TraceUserAccount traceuseraccount = new TraceUserAccount();
        traceuseraccount.setSid(1000);
        traceuseraccount.setTimeStamp(new Date());
        traceuseraccount.setTraceType("jili成");
        traceuseraccount.setId(1000);
        traceuseraccount.setUserID("jili成");
        traceuseraccount.setAccountCode("jili成");
        traceuseraccount.setAccessID("jili成");
        traceuseraccount.setReMark("jili成");
        traceuseraccount.setAlterUserID("jili成");
        traceuseraccount.setCreateTime(new Date());
        return traceuseraccount;
    }

    public TraceUserInfo genTraceUserInfo() {
        TraceUserInfo traceuserinfo = new TraceUserInfo();
        traceuserinfo.setSid(1000);
        traceuserinfo.setTimeStamp(new Date());
        traceuserinfo.setTraceType("jili成");
        traceuserinfo.setId(1000);
        traceuserinfo.setUserID("jili成");
        traceuserinfo.setUserName("jili成");
        traceuserinfo.setPwd("jili成");
        traceuserinfo.setStat("jili成");
        traceuserinfo.setType("jili成");
        traceuserinfo.setLoginStat("jili成");
        traceuserinfo.setCorrectNum(1000);
        traceuserinfo.setIsUSBKey("jili成");
        traceuserinfo.setRoleID("jili成");
        traceuserinfo.setAlterUserID("jili成");
        traceuserinfo.setCreateTime(new Date());
        return traceuserinfo;
    }

    public TraceUserPrivateInfo genTraceUserPrivateInfo() {
        TraceUserPrivateInfo traceuserprivateinfo = new TraceUserPrivateInfo();
        traceuserprivateinfo.setSid(1000);
        traceuserprivateinfo.setTimeStamp(new Date());
        traceuserprivateinfo.setTraceType("jili成");
        traceuserprivateinfo.setId(1000);
        traceuserprivateinfo.setUserID("jili成");
        traceuserprivateinfo.setUserName("jili成");
        traceuserprivateinfo.setSfID("jili成");
        traceuserprivateinfo.setAlterUserID("jili成");
        traceuserprivateinfo.setCreateTime(new Date());
        return traceuserprivateinfo;
    }

    public TraceWorkFlowPath genTraceWorkFlowPath() {
        TraceWorkFlowPath traceworkflowpath = new TraceWorkFlowPath();
        traceworkflowpath.setSid(1000);
        traceworkflowpath.setTimeStamp(new Date());
        traceworkflowpath.setTraceType("jili成");
        traceworkflowpath.setId(1000);
        traceworkflowpath.setGroupID(1000);
        traceworkflowpath.setAnchorCode("jili成");
        traceworkflowpath.setFlowSeqNo(1000);
        traceworkflowpath.setUserID("jili成");
        traceworkflowpath.setRoleID("jili成");
        traceworkflowpath.setReMark("jili成");
        traceworkflowpath.setOwnUserID("jili成");
        traceworkflowpath.setAlterUserID("jili成");
        traceworkflowpath.setCreateTime(new Date());
        return traceworkflowpath;
    }

    public TraceWorkGroup genTraceWorkGroup() {
        TraceWorkGroup traceworkgroup = new TraceWorkGroup();
        traceworkgroup.setSid(1000);
        traceworkgroup.setTimeStamp(new Date());
        traceworkgroup.setTraceType("jili成");
        traceworkgroup.setGroupID(1000);
        traceworkgroup.setGroupName("jili成");
        traceworkgroup.setManagerUserID("jili成");
        traceworkgroup.setType("jili成");
        traceworkgroup.setUpGroupID(1000);
        traceworkgroup.setFlag("jili成");
        traceworkgroup.setRole("jili成");
        traceworkgroup.setOwnUserID("jili成");
        traceworkgroup.setAlterUserID("jili成");
        traceworkgroup.setCreateTime(new Date());
        return traceworkgroup;
    }

    public TraceWorkGroupUser genTraceWorkGroupUser() {
        TraceWorkGroupUser traceworkgroupuser = new TraceWorkGroupUser();
        traceworkgroupuser.setSid(1000);
        traceworkgroupuser.setTimeStamp(new Date());
        traceworkgroupuser.setTraceType("jili成");
        traceworkgroupuser.setId(1000);
        traceworkgroupuser.setGroupID(1000);
        traceworkgroupuser.setUserID("jili成");
        traceworkgroupuser.setRoleID("jili成");
        traceworkgroupuser.setReMark("jili成");
        traceworkgroupuser.setOwnUserID("jili成");
        traceworkgroupuser.setAlterUserID("jili成");
        traceworkgroupuser.setCreateTime(new Date());
        return traceworkgroupuser;
    }

    public TraceWorkProductFlow genTraceWorkProductFlow() {
        TraceWorkProductFlow traceworkproductflow = new TraceWorkProductFlow();
        traceworkproductflow.setSid(1000);
        traceworkproductflow.setTimeStamp(new Date());
        traceworkproductflow.setTraceType("jili成");
        traceworkproductflow.setId(1000);
        traceworkproductflow.setProductID(1000);
        traceworkproductflow.setAnchorCode("jili成");
        traceworkproductflow.setFlowSeqNo(1000);
        traceworkproductflow.setUserID("jili成");
        traceworkproductflow.setRoleID("jili成");
        traceworkproductflow.setBlongGroupID(1000);
        traceworkproductflow.setReMark("jili成");
        traceworkproductflow.setOwnUserID("jili成");
        traceworkproductflow.setAlterUserID("jili成");
        traceworkproductflow.setCreateTime(new Date());
        return traceworkproductflow;
    }

    public AlterOrder genAlterOrder() {
        AlterOrder alterorder = new AlterOrder();
        alterorder.setOrderID(1000);
        alterorder.setTimeStamp(new Date());
        alterorder.setDealerUserID("jili成");
        alterorder.setAccountCode("jili成");
        alterorder.setTime(new Date());
        alterorder.setTradeDate(new Date());
        alterorder.setDateTime(new Date());
        alterorder.setMac("jili成");
        alterorder.setIp4(1000);
        alterorder.setOrderTypeEmit("jili成");
        alterorder.setTradeOperate(1000);
        alterorder.setEntrustNo(1000);
        alterorder.setEntrustkey("jili成");
        alterorder.setAlterQty(1000);
        alterorder.setAlterPrice((float) 15.23);
        alterorder.setWorkStationID("jili成");
        alterorder.setOwnUserID("jili成");
        alterorder.setAlterUserID("jili成");
        alterorder.setCreateTime(new Date());
        return alterorder;
    }

    public CancelOrder genCancelOrder() {
        CancelOrder cancelorder = new CancelOrder();
        cancelorder.setOrderID(1000);
        cancelorder.setTimeStamp(new Date());
        cancelorder.setDealerUserID("jili成");
        cancelorder.setAccountCode("jili成");
        cancelorder.setTime(new Date());
        cancelorder.setTradeDate(new Date());
        cancelorder.setDateTime(new Date());
        cancelorder.setMac("jili成");
        cancelorder.setIp4(1000);
        cancelorder.setOrderTypeEmit("jili成");
        cancelorder.setTradeOperate(1000);
        cancelorder.setEntrustNo(1000);
        cancelorder.setEntrustkey("jili成");
        cancelorder.setCancelQty(1000);
        cancelorder.setWorkStationID("jili成");
        cancelorder.setOwnUserID("jili成");
        cancelorder.setAlterUserID("jili成");
        cancelorder.setCreateTime(new Date());
        return cancelorder;
    }

    public OrderExecuteProgress genOrderExecuteProgress() {
        OrderExecuteProgress orderexecuteprogress = new OrderExecuteProgress();
        orderexecuteprogress.setOrderID(1000);
        orderexecuteprogress.setTimeStamp(new Date());
        orderexecuteprogress.setAccountCode("jili成");
        orderexecuteprogress.setDealerUserID("jili成");
        orderexecuteprogress.setSubmitTime(new Date());
        orderexecuteprogress.setTradeDate(new Date());
        orderexecuteprogress.setDateTime(new Date());
        orderexecuteprogress.setMac("jili成");
        orderexecuteprogress.setIp4(1000);
        orderexecuteprogress.setOrderTypeEmit("jili成");
        orderexecuteprogress.setTradeOperate(1000);
        orderexecuteprogress.setMarketCode("jili成");
        orderexecuteprogress.setClassCode("jil");
        orderexecuteprogress.setObj("jili成");
        orderexecuteprogress.setBs("jili成");
        orderexecuteprogress.setOpenClose("jili成");
        orderexecuteprogress.setHedgeFlag("jili成");
        orderexecuteprogress.setPosID("jili成");
        orderexecuteprogress.setCustomCode("jili成");
        orderexecuteprogress.setCloseType("jili成");
        orderexecuteprogress.setBasketID(1000);
        orderexecuteprogress.setBasketTatol(1000);
        orderexecuteprogress.setSeqNum(1000);
        orderexecuteprogress.setExeTime(new Date());
        orderexecuteprogress.setExeSequenceNo(1000);
        orderexecuteprogress.setOutsideExeNo("jili成");
        orderexecuteprogress.setExeQty(1000);
        orderexecuteprogress.setExePrice((float) 15.23);
        orderexecuteprogress.setCurrency("jili成");
        orderexecuteprogress.setExeAmount((float) 15.23);
        orderexecuteprogress.setCancelQty(1000);
        orderexecuteprogress.setOrderQty(1000);
        orderexecuteprogress.setWorkingQty(1000);
        orderexecuteprogress.setCompleteQty(1000);
        orderexecuteprogress.setOrderStatus("jili成");
        orderexecuteprogress.setTotalFare((float) 15.23);
        orderexecuteprogress.setCommissionFare((float) 15.23);
        orderexecuteprogress.setDeliverFare((float) 15.23);
        orderexecuteprogress.setTax((float) 15.23);
        orderexecuteprogress.setOtherCost((float) 15.23);
        orderexecuteprogress.setEsCheckinAmount(45.123);
        orderexecuteprogress.setReMark("jili成");
        orderexecuteprogress.setInvestID("jili成");
        orderexecuteprogress.setOwnUserID("jili成");
        orderexecuteprogress.setAlterUserID("jili成");
        orderexecuteprogress.setCreateTime(new Date());
        return orderexecuteprogress;
    }

    public FundTransfer genFundTransfer() {
        FundTransfer fundtransfer = new FundTransfer();
        fundtransfer.setId(1000);
        fundtransfer.setTimeStamp(new Date());
        fundtransfer.setRegionID("jili成");
        fundtransfer.setAccountCode("jili成");
        fundtransfer.setDirect("jili成");
        fundtransfer.setAmount(45.123);
        fundtransfer.setDate(new Date());
        fundtransfer.setTime(new Date());
        fundtransfer.setDateTime(new Date());
        fundtransfer.setCurrency("jili成");
        fundtransfer.setChecker("jili成");
        fundtransfer.setReMark("jili成");
        fundtransfer.setIsEffectived("jili成");
        fundtransfer.setReplyMsg("jili成");
        fundtransfer.setOwnUserID("jili成");
        fundtransfer.setAlterUserID("jili成");
        fundtransfer.setCreateTime(new Date());
        return fundtransfer;
    }

    public NewOrder genNewOrder() {
        NewOrder neworder = new NewOrder();
        neworder.setOrderID(1000);
        neworder.setTimeStamp(new Date());
        neworder.setAccountCode("jili成");
        neworder.setDealerUserID("jili成");
        neworder.setSubmitTime(new Date());
        neworder.setTradeDate(new Date());
        neworder.setDateTime(new Date());
        neworder.setMac("jili成");
        neworder.setIp4(1000);
        neworder.setOrderTypeEmit("jili成");
        neworder.setTradeOperate(1000);
        neworder.setObj("jili成");
        neworder.setMarketCode("jili成");
        neworder.setClassCode("jil");
        neworder.setBs("jili成");
        neworder.setOpenClose("jili成");
        neworder.setPriceType("jili成");
        neworder.setOrderPrice((float) 15.23);
        neworder.setOrderQty(1000);
        neworder.setHedgeFlag("jili成");
        neworder.setPosID("jili成");
        neworder.setCustomCode("jili成");
        neworder.setCloseType("jili成");
        neworder.setBasketID(1000);
        neworder.setBasketTatol(1000);
        neworder.setSeqNum(1000);
        neworder.setWorkStationID("jili成");
        neworder.setOwnUserID("jili成");
        neworder.setAlterUserID("jili成");
        neworder.setCreateTime(new Date());
        return neworder;
    }

    public TradeReportDetail genTradeReportDetail() {
        TradeReportDetail tradereportdetail = new TradeReportDetail();
        tradereportdetail.setId(1000);
        tradereportdetail.setTimeStamp(new Date());
        tradereportdetail.setMsgType("jili成");
        tradereportdetail.setOrderID(1000);
        tradereportdetail.setReportNo(1000);
        tradereportdetail.setReportContent("jili成");
        tradereportdetail.setEntrustNo(1000);
        tradereportdetail.setEntrustkey("jili成");
        tradereportdetail.setWorkingQty(1000);
        tradereportdetail.setUnWorkingQty(1000);
        tradereportdetail.setCancelQty(1000);
        tradereportdetail.setExeQtyTatol(1000);
        tradereportdetail.setExePriceAvg((float) 15.23);
        tradereportdetail.setExeAmountTatol(45.123);
        tradereportdetail.setPrice((float) 15.23);
        tradereportdetail.setExexSeq(1000);
        tradereportdetail.setExeSequenceNo(1000);
        tradereportdetail.setOutsideExeNo("jili成");
        tradereportdetail.setExeQty(1000);
        tradereportdetail.setExePrice((float) 15.23);
        tradereportdetail.setCurrency("jili成");
        tradereportdetail.setExeAmount((float) 15.23);
        tradereportdetail.setAccountCode("jili成");
        tradereportdetail.setMarketCode("jili成");
        tradereportdetail.setClassCode("jil");
        tradereportdetail.setObj("jili成");
        tradereportdetail.setBs("jili成");
        tradereportdetail.setOpenClose("jili成");
        tradereportdetail.setHedgeFlag("jili成");
        tradereportdetail.setCloseType("jili成");
        tradereportdetail.setExeTime(new Date());
        tradereportdetail.setEntrustTime(new Date());
        tradereportdetail.setSubmitTime(new Date());
        tradereportdetail.setTradeDate(new Date());
        tradereportdetail.setInvestID("jili成");
        tradereportdetail.setReMark("jili成");
        tradereportdetail.setOwnUserID("jili成");
        tradereportdetail.setAlterUserID("jili成");
        tradereportdetail.setCreateTime(new Date());
        return tradereportdetail;
    }

    public TradesetBroker genTradesetBroker() {
        TradesetBroker tradesetbroker = new TradesetBroker();
        tradesetbroker.setBrokerID("jili成");
        tradesetbroker.setTimeStamp(new Date());
        tradesetbroker.setRegionID("jili成");
        tradesetbroker.setType("jili成");
        tradesetbroker.setMarketCodeIss("jili成");
        tradesetbroker.setOwnUserID("jili成");
        tradesetbroker.setAlterUserID("jili成");
        tradesetbroker.setCreateTime(new Date());
        return tradesetbroker;
    }

    public TradesetCalendar genTradesetCalendar() {
        TradesetCalendar tradesetcalendar = new TradesetCalendar();
        tradesetcalendar.setId(1000);
        tradesetcalendar.setTimeStamp(new Date());
        tradesetcalendar.setRegionID("jili成");
        tradesetcalendar.setDate(new Date());
        tradesetcalendar.setIsTrade("jili成");
        tradesetcalendar.setReMark("jili成");
        tradesetcalendar.setOwnUserID("jili成");
        tradesetcalendar.setAlterUserID("jili成");
        tradesetcalendar.setCreateTime(new Date());
        return tradesetcalendar;
    }

    public TradesetExchangeLimit genTradesetExchangeLimit() {
        TradesetExchangeLimit tradesetexchangelimit = new TradesetExchangeLimit();
        tradesetexchangelimit.setId(1000);
        tradesetexchangelimit.setTimeStamp(new Date());
        tradesetexchangelimit.setMarketCode("jili成");
        tradesetexchangelimit.setUnderlyingObj("jili成");
        tradesetexchangelimit.setCancelLimit(1000);
        tradesetexchangelimit.setOrderLimit(1000);
        tradesetexchangelimit.setOnceMaxQty(1000);
        tradesetexchangelimit.setReMark("jili成");
        tradesetexchangelimit.setOwnUserID("jili成");
        tradesetexchangelimit.setAlterUserID("jili成");
        tradesetexchangelimit.setCreateTime(new Date());
        return tradesetexchangelimit;
    }

    public TradesetPip genTradesetPip() {
        TradesetPip tradesetpip = new TradesetPip();
        tradesetpip.setPipID(1000);
        tradesetpip.setTimeStamp(new Date());
        tradesetpip.setPipName("jili成");
        tradesetpip.setType("jili成");
        tradesetpip.setRegionID("jili成");
        tradesetpip.setMarketCode("jili成");
        tradesetpip.setClassCode("jil");
        tradesetpip.setBrokerID("jili成");
        tradesetpip.setLimitA(1000);
        tradesetpip.setLimitB(1000);
        tradesetpip.setLimitC((float) 15.23);
        tradesetpip.setOwnUserID("jili成");
        tradesetpip.setAlterUserID("jili成");
        tradesetpip.setCreateTime(new Date());
        return tradesetpip;
    }
/*
    public TradeSimOrderquene genTradeSimOrderquene() {
        TradeSimOrderquene tradesimorderquene = new TradeSimOrderquene();
        tradesimorderquene.setOrderID(1000);
        tradesimorderquene.setOrdertype("jili成");
        tradesimorderquene.setAccountCode("jili成");
        tradesimorderquene.setDealerUserID("jili成");
        tradesimorderquene.setTime(new Date());
        tradesimorderquene.setTradeDate(new Date());
        tradesimorderquene.setDateTime(new Date());
        tradesimorderquene.setMac("jili成");
        tradesimorderquene.setIp4(1000);
        tradesimorderquene.setObj("jili成");
        tradesimorderquene.setMarketCode("jili成");
        tradesimorderquene.setClassCode("jil");
        tradesimorderquene.setBs("jili成");
        tradesimorderquene.setOpenClose("jili成");
        tradesimorderquene.setPriceType("jili成");
        tradesimorderquene.setOrderPrice((float) 15.23);
        tradesimorderquene.setOrderQty(1000);
        tradesimorderquene.setHedgeFlag("jili成");
        tradesimorderquene.setPosID("jili成");
        tradesimorderquene.setExexSeq(1000);
        tradesimorderquene.setWorkingQty(1000);
        tradesimorderquene.setUnWorkingQty(1000);
        tradesimorderquene.setCancelQty(1000);
        tradesimorderquene.setExeQty(1000);
        return tradesimorderquene;
    }
    */

    public UserAccess genUserAccess() {
        UserAccess useraccess = new UserAccess();
        useraccess.setId(1000);
        useraccess.setTimeStamp(new Date());
        useraccess.setUserID("jili成");
        useraccess.setAccessUserID("jili成");
        useraccess.setAccessID("jili成");
        useraccess.setReMark("jili成");
        useraccess.setAlterUserID("jili成");
        useraccess.setCreateTime(new Date());
        return useraccess;
    }

    public UserAccount genUserAccount() {
        UserAccount useraccount = new UserAccount();
        useraccount.setId(1000);
        useraccount.setTimeStamp(new Date());
        useraccount.setUserID("jili成");
        useraccount.setAccountCode("jili成");
        useraccount.setAccessID("jili成");
        useraccount.setReMark("jili成");
        useraccount.setAlterUserID("jili成");
        useraccount.setCreateTime(new Date());
        return useraccount;
    }

    public UserInfo genUserInfo() {
        UserInfo userinfo = new UserInfo();
        userinfo.setId(1000);
        userinfo.setTimeStamp(new Date());
        userinfo.setUserID("jili成");
        userinfo.setUserName("jili成");
        userinfo.setPwd("jili成");
        userinfo.setStat("jili成");
        userinfo.setType("jili成");
        userinfo.setLoginStat("jili成");
        userinfo.setCorrectNum(1000);
        userinfo.setIsUSBKey("jili成");
        userinfo.setRoleID("jili成");
        userinfo.setAlterUserID("jili成");
        userinfo.setCreateTime(new Date());
        return userinfo;
    }

    public UserPrivateInfo genUserPrivateInfo() {
        UserPrivateInfo userprivateinfo = new UserPrivateInfo();
        userprivateinfo.setId(1000);
        userprivateinfo.setTimeStamp(new Date());
        userprivateinfo.setUserID("jili成");
        userprivateinfo.setUserName("jili成");
        userprivateinfo.setSfID("jili成");
        userprivateinfo.setAlterUserID("jili成");
        userprivateinfo.setCreateTime(new Date());
        return userprivateinfo;
    }

    public WorkFlowPath genWorkFlowPath() {
        WorkFlowPath workflowpath = new WorkFlowPath();
        workflowpath.setId(1000);
        workflowpath.setTimeStamp(new Date());
        workflowpath.setGroupID(1000);
        workflowpath.setAnchorCode("jili成");
        workflowpath.setFlowSeqNo(1000);
        workflowpath.setUserID("jili成");
        workflowpath.setRoleID("jili成");
        workflowpath.setReMark("jili成");
        workflowpath.setOwnUserID("jili成");
        workflowpath.setAlterUserID("jili成");
        workflowpath.setCreateTime(new Date());
        return workflowpath;
    }

    public WorkGroup genWorkGroup() {
        WorkGroup workgroup = new WorkGroup();
        workgroup.setGroupID(1000);
        workgroup.setTimeStamp(new Date());
        workgroup.setGroupName("jili成");
        workgroup.setManagerUserID("jili成");
        workgroup.setType("jili成");
        workgroup.setUpGroupID(1000);
        workgroup.setFlag("jili成");
        workgroup.setRole("jili成");
        workgroup.setOwnUserID("jili成");
        workgroup.setAlterUserID("jili成");
        workgroup.setCreateTime(new Date());
        return workgroup;
    }

    public WorkGroupUser genWorkGroupUser() {
        WorkGroupUser workgroupuser = new WorkGroupUser();
        workgroupuser.setId(1000);
        workgroupuser.setTimeStamp(new Date());
        workgroupuser.setGroupID(1000);
        workgroupuser.setUserID("jili成");
        workgroupuser.setRoleID("jili成");
        workgroupuser.setReMark("jili成");
        workgroupuser.setOwnUserID("jili成");
        workgroupuser.setAlterUserID("jili成");
        workgroupuser.setCreateTime(new Date());
        return workgroupuser;
    }

    public WorkProductFlow genWorkProductFlow() {
        WorkProductFlow workproductflow = new WorkProductFlow();
        workproductflow.setId(1000);
        workproductflow.setTimeStamp(new Date());
        workproductflow.setProductID(1000);
        workproductflow.setAnchorCode("jili成");
        workproductflow.setFlowSeqNo(1000);
        workproductflow.setUserID("jili成");
        workproductflow.setRoleID("jili成");
        workproductflow.setBlongGroupID(1000);
        workproductflow.setReMark("jili成");
        workproductflow.setOwnUserID("jili成");
        workproductflow.setAlterUserID("jili成");
        workproductflow.setCreateTime(new Date());
        return workproductflow;
    }

    public AccountBail genAccountBail() {
        AccountBail accountbail = new AccountBail();
        accountbail.setId(1000);
        accountbail.setTimeStamp(new Date());
        accountbail.setAccountCode("jili成");
        accountbail.setTempID(1000);
        accountbail.setMarketCode("jili成");
        accountbail.setClassCode("jil");
        accountbail.setBs("jili成");
        accountbail.setUnderlyingCode("jili成");
        accountbail.setObj("jili成");
        accountbail.setBail0((float) 15.23);
        accountbail.setBail1((float) 15.23);
        accountbail.setBail2((float) 15.23);
        accountbail.setBail0Rate((float) 15.23);
        accountbail.setBail1Rate((float) 15.23);
        accountbail.setBail2Rate((float) 15.23);
        accountbail.setOwnUserID("jili成");
        accountbail.setAlterUserID("jili成");
        accountbail.setCreateTime(new Date());
        return accountbail;
    }

    public AccountFare genAccountFare() {
        AccountFare accountfare = new AccountFare();
        accountfare.setId(1000);
        accountfare.setTimeStamp(new Date());
        accountfare.setAccountCode("jili成");
        accountfare.setTempID(1000);
        accountfare.setMarketCode("jili成");
        accountfare.setClassCode("jil");
        accountfare.setUnderlyingCode("jili成");
        accountfare.setObj("jili成");
        accountfare.setBs("jili成");
        accountfare.setOpenClose("jili成");
        accountfare.setCommission0((float) 15.23);
        accountfare.setCommission1((float) 15.23);
        accountfare.setMinCommission0((float) 15.23);
        accountfare.setMinCommission1((float) 15.23);
        accountfare.setDeliverFare0((float) 15.23);
        accountfare.setDeliverFare1((float) 15.23);
        accountfare.setTax0((float) 15.23);
        accountfare.setTax1((float) 15.23);
        accountfare.setEtax1((float) 15.23);
        accountfare.setEtax0((float) 15.23);
        accountfare.setCost0((float) 15.23);
        accountfare.setCost1((float) 15.23);
        accountfare.setOwnUserID("jili成");
        accountfare.setAlterUserID("jili成");
        accountfare.setCreateTime(new Date());
        return accountfare;
    }

    public AccountIcode genAccountIcode() {
        AccountIcode accounticode = new AccountIcode();
        accounticode.setId(1000);
        accounticode.setTimeStamp(new Date());
        accounticode.setIid(1000);
        accounticode.setInvestID("jili成");
        accounticode.setType("jili成");
        accounticode.setCode("jili成");
        accounticode.setAlterUserID("jili成");
        accounticode.setCreateTime(new Date());
        return accounticode;
    }

    public AccountInfo genAccountInfo() {
        AccountInfo accountinfo = new AccountInfo();
        accountinfo.setId(1000);
        accountinfo.setTimeStamp(new Date());
        accountinfo.setAccountCode("jili成");
        accountinfo.setAccountName("jili成");
        accountinfo.setPipID(1000);
        accountinfo.setPwd("jili成");
        accountinfo.setIsRisk("jili成");
        accountinfo.setIid(1000);
        accountinfo.setType("jili成");
        accountinfo.setInvestID("jili成");
        accountinfo.setOwnUserID("jili成");
        accountinfo.setAlterUserID("jili成");
        accountinfo.setCreateTime(new Date());
        return accountinfo;
    }

    public AccountInvest genAccountInvest() {
        AccountInvest accountinvest = new AccountInvest();
        accountinvest.setId(1000);
        accountinvest.setTimeStamp(new Date());
        accountinvest.setInvestID("jili成");
        accountinvest.setRegionID("jili成");
        accountinvest.setInvestName("jili成");
        accountinvest.setBrokerID("jili成");
        accountinvest.setPwd("jili成");
        accountinvest.setType("jili成");
        accountinvest.setOwnUserID("jili成");
        accountinvest.setAlterUserID("jili成");
        accountinvest.setCreateTime(new Date());
        return accountinvest;
    }

    public BailTemp genBailTemp() {
        BailTemp bailtemp = new BailTemp();
        bailtemp.setTempID(1000);
        bailtemp.setTimeStamp(new Date());
        bailtemp.setTempName("jili成");
        bailtemp.setByOwnGroupID(1000);
        bailtemp.setReMark("jili成");
        bailtemp.setOwnUserID("jili成");
        bailtemp.setAlterUserID("jili成");
        bailtemp.setCreateTime(new Date());
        return bailtemp;
    }

    public BailTempObj genBailTempObj() {
        BailTempObj bailtempobj = new BailTempObj();
        bailtempobj.setId(1000);
        bailtempobj.setTimeStamp(new Date());
        bailtempobj.setTempID(1000);
        bailtempobj.setMarketCode("jili成");
        bailtempobj.setClassCode("jil");
        bailtempobj.setBs("jili成");
        bailtempobj.setUnderlyingCode("jili成");
        bailtempobj.setObj("jili成");
        bailtempobj.setBail0((float) 15.23);
        bailtempobj.setBail1((float) 15.23);
        bailtempobj.setBail2((float) 15.23);
        bailtempobj.setBail0Rate((float) 15.23);
        bailtempobj.setBail1Rate((float) 15.23);
        bailtempobj.setBail2Rate((float) 15.23);
        bailtempobj.setOwnUserID("jili成");
        bailtempobj.setAlterUserID("jili成");
        bailtempobj.setCreateTime(new Date());
        return bailtempobj;
    }

    public ClearingCurrencyRates genClearingCurrencyRates() {
        ClearingCurrencyRates clearingcurrencyrates = new ClearingCurrencyRates();
        clearingcurrencyrates.setId(1000);
        clearingcurrencyrates.setTimeStamp(new Date());
        clearingcurrencyrates.setClearingDate(new Date());
        clearingcurrencyrates.setAskCurrency("jili成");
        clearingcurrencyrates.setBidCurrency("jili成");
        clearingcurrencyrates.setRatePrice((float) 15.23);
        clearingcurrencyrates.setOwnUserID("jili成");
        clearingcurrencyrates.setAlterUserID("jili成");
        clearingcurrencyrates.setCreateTime(new Date());
        return clearingcurrencyrates;
    }

    public ClearingDayBook genClearingDayBook() {
        ClearingDayBook clearingdaybook = new ClearingDayBook();
        clearingdaybook.setDayid(1000000L);
        clearingdaybook.setTimeStamp(new Date());
        clearingdaybook.setBookDate(new Date());
        clearingdaybook.setAccountCode("jili成");
        clearingdaybook.setBookID(1000);
        clearingdaybook.setExeTime(new Date());
        clearingdaybook.setTradeDate(new Date());
        clearingdaybook.setAccountTitle("jili成");
        clearingdaybook.setAmountReceived(45.123);
        clearingdaybook.setAmountPayed(45.123);
        clearingdaybook.setBalance(45.123);
        clearingdaybook.setMarketCode("jili成");
        clearingdaybook.setClassCode("jil");
        clearingdaybook.setObj("jili成");
        clearingdaybook.setMac("jili成");
        clearingdaybook.setIp4(1000);
        clearingdaybook.setOrderTypeEmit("jili成");
        clearingdaybook.setTradeOperate(1000);
        clearingdaybook.setCloseType("jili成");
        clearingdaybook.setBs("jili成");
        clearingdaybook.setOpenClose("jili成");
        clearingdaybook.setHedgeFlag("jili成");
        clearingdaybook.setPosID("jili成");
        clearingdaybook.setExeSequenceNo(1000);
        clearingdaybook.setOutsideExeNo("jili成");
        clearingdaybook.setExeQty(1000);
        clearingdaybook.setExePrice((float) 15.23);
        clearingdaybook.setCurrency("jili成");
        clearingdaybook.setExeAmount((float) 15.23);
        clearingdaybook.setTotalFare((float) 15.23);
        clearingdaybook.setCommissionFare((float) 15.23);
        clearingdaybook.setDeliverFare((float) 15.23);
        clearingdaybook.setTax((float) 15.23);
        clearingdaybook.setOtherCost((float) 15.23);
        clearingdaybook.setEsCheckinAmount(45.123);
        clearingdaybook.setInvestID("jili成");
        clearingdaybook.setOwnUserID("jili成");
        clearingdaybook.setAlterUserID("jili成");
        clearingdaybook.setCreateTime(new Date());
        return clearingdaybook;
    }

    public ClearingExRight genClearingExRight() {
        ClearingExRight clearingexright = new ClearingExRight();
        clearingexright.setId(1000);
        clearingexright.setTimeStamp(new Date());
        clearingexright.setAccountCode("jili成");
        clearingexright.setClearingDate(new Date());
        clearingexright.setObj("jili成");
        clearingexright.setMarketCode("jili成");
        clearingexright.setClassCode("jil");
        clearingexright.setRecordeDate(new Date());
        clearingexright.setPosQty(1000);
        clearingexright.setProfitsAmount((float) 15.23);
        clearingexright.setShareA(1000);
        clearingexright.setShareB(1000);
        clearingexright.setTax((float) 15.23);
        clearingexright.setNetProfitsAmount((float) 15.23);
        clearingexright.setReMark("jili成");
        clearingexright.setOwnUserID("jili成");
        clearingexright.setAlterUserID("jili成");
        clearingexright.setCreateTime(new Date());
        return clearingexright;
    }

    public FundState genFundState() {
        FundState fundstate = new FundState();
        fundstate.setId(1000);
        fundstate.setTimeStamp(new Date());
        fundstate.setClearingDate(new Date());
        fundstate.setAccountCode("jili成");
        fundstate.setFundTotal((float) 15.23);
        fundstate.setFundAvl((float) 15.23);
        fundstate.setFundFroze((float) 15.23);
        fundstate.setAssetTotal((float) 15.23);
        fundstate.setAssetNet((float) 15.23);
        fundstate.setAssetCredit((float) 15.23);
        fundstate.setPosTotalValue((float) 15.23);
        fundstate.setPosNetValue((float) 15.23);
        fundstate.setPosCreditValue((float) 15.23);
        fundstate.setCurrency("jili成");
        fundstate.setRate((float) 15.23);
        fundstate.setEqualFundAvl((float) 15.23);
        fundstate.setBefAssetTotal((float) 15.23);
        fundstate.setBefFundAvl((float) 15.23);
        fundstate.setOwnUserID("jili成");
        fundstate.setAlterUserID("jili成");
        fundstate.setCreateTime(new Date());
        return fundstate;
    }

    public PosDetail genPosDetail() {
        PosDetail posdetail = new PosDetail();
        posdetail.setId(1000);
        posdetail.setTimeStamp(new Date());
        posdetail.setClearingDate(new Date());
        posdetail.setAccountCode("jili成");
        posdetail.setPosID("jili成");
        posdetail.setObj("jili成");
        posdetail.setMarketCode("jili成");
        posdetail.setClassCode("jil");
        posdetail.setBs("jili成");
        posdetail.setQty(1000);
        posdetail.setAvlQty(1000);
        posdetail.setFrozeQty(1000);
        posdetail.setClosePrice((float) 15.23);
        posdetail.setCostPriceA((float) 15.23);
        posdetail.setCostPriceB((float) 15.23);
        posdetail.setOpencostprice((float) 15.23);
        posdetail.setAvgCostPrice((float) 15.23);
        posdetail.setHedgeFlag("jili成");
        posdetail.setPosType("jili成");
        posdetail.setPutCall("jili成");
        posdetail.setCurrency("jili成");
        posdetail.setPl((float) 15.23);
        posdetail.setCostPL((float) 15.23);
        posdetail.setTotalPL((float) 15.23);
        posdetail.setMargin((float) 15.23);
        posdetail.setTotalFare((float) 15.23);
        posdetail.setInvestID("jili成");
        posdetail.setOwnUserID("jili成");
        posdetail.setAlterUserID("jili成");
        posdetail.setCreateTime(new Date());
        return posdetail;
    }

    public ClearingPrice genClearingPrice() {
        ClearingPrice clearingprice = new ClearingPrice();
        clearingprice.setId(1000);
        clearingprice.setTimeStamp(new Date());
        clearingprice.setClearingDate(new Date());
        clearingprice.setRegionID("jili成");
        clearingprice.setObj("jili成");
        clearingprice.setClassCode("jil");
        clearingprice.setMarketCode("jili成");
        clearingprice.setClosePrice((float) 15.23);
        clearingprice.setClearprice((float) 15.23);
        clearingprice.setALCPrice((float) 15.23);
        clearingprice.setOwnUserID("jili成");
        clearingprice.setAlterUserID("jili成");
        clearingprice.setCreateTime(new Date());
        return clearingprice;
    }

}
