/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.antotest;

import com.jili.ubert.dao.db.AccountBail;
import com.jili.ubert.dao.db.AccountFare;
import com.jili.ubert.dao.db.AccountIcode;
import com.jili.ubert.dao.db.AccountInfo;
import com.jili.ubert.dao.db.AccountInvest;
import com.jili.ubert.dao.db.AlterOrder;
import com.jili.ubert.dao.db.BailTemp;
import com.jili.ubert.dao.db.BailTempObj;
import com.jili.ubert.dao.db.CancelOrder;
import com.jili.ubert.dao.db.ClearingCurrencyRates;
import com.jili.ubert.dao.db.ClearingDayBook;
import com.jili.ubert.dao.db.ClearingExRight;
import com.jili.ubert.dao.db.ClearingPrice;
import com.jili.ubert.dao.db.ClearingProtfoliopos;
import com.jili.ubert.dao.db.FareTemp;
import com.jili.ubert.dao.db.FareTempeObj;
import com.jili.ubert.dao.db.FundState;
import com.jili.ubert.dao.db.FundTransfer;
import com.jili.ubert.dao.db.IOPVAccount;
import com.jili.ubert.dao.db.IOPVProduct;
import com.jili.ubert.dao.db.IOPVProductUnit;
import com.jili.ubert.dao.db.LogLoginInfo;
import com.jili.ubert.dao.db.NewOrder;
import com.jili.ubert.dao.db.ObjClassCode;
import com.jili.ubert.dao.db.ObjClassCodeAtrr;
import com.jili.ubert.dao.db.ObjETF;
import com.jili.ubert.dao.db.ObjETFList;
import com.jili.ubert.dao.db.ObjExRightInfo;
import com.jili.ubert.dao.db.ObjIndex;
import com.jili.ubert.dao.db.ObjIndexList;
import com.jili.ubert.dao.db.ObjInfo;
import com.jili.ubert.dao.db.ObjNewStock;
import com.jili.ubert.dao.db.ObjPriceTick;
import com.jili.ubert.dao.db.ObjProtfolio;
import com.jili.ubert.dao.db.ObjProtfolioList;
import com.jili.ubert.dao.db.ObjStructuredFundBase;
import com.jili.ubert.dao.db.ObjStructuredFundPart;
import com.jili.ubert.dao.db.ObjUnderlyingCode;
import com.jili.ubert.dao.db.OrderExecuteProgress;
import com.jili.ubert.dao.db.PosDetail;
import com.jili.ubert.dao.db.ProductAccount;
import com.jili.ubert.dao.db.ProductInfo;
import com.jili.ubert.dao.db.ProductUnit;
import com.jili.ubert.dao.db.RiskCtrlAssets;
import com.jili.ubert.dao.db.RiskIndex;
import com.jili.ubert.dao.db.RiskSets;
import com.jili.ubert.dao.db.SafeForbidLogin;
import com.jili.ubert.dao.db.SafeOtherAccess;
import com.jili.ubert.dao.db.TradeReportDetail;
import com.jili.ubert.dao.db.TradesetBroker;
import com.jili.ubert.dao.db.TradesetCalendar;
import com.jili.ubert.dao.db.TradesetExchangeLimit;
import com.jili.ubert.dao.db.TradesetPip;
import com.jili.ubert.dao.db.UserAccess;
import com.jili.ubert.dao.db.UserAccount;
import com.jili.ubert.dao.db.UserInfo;
import com.jili.ubert.dao.db.UserPrivateInfo;
import com.jili.ubert.dao.db.WorkFlowPath;
import com.jili.ubert.dao.db.WorkGroup;
import com.jili.ubert.dao.db.WorkGroupUser;
import com.jili.ubert.dao.db.WorkProductFlow;
import com.jili.ubert.dao.history.HistoryClearingCurrencyRates;
import com.jili.ubert.dao.history.HistoryIOPVAccount;
import com.jili.ubert.dao.history.HistoryIOPVProduct;
import com.jili.ubert.dao.history.HistoryIOPVProductUnit;
import com.jili.ubert.dao.history.HistoryLogLoginInfo;
import com.jili.ubert.dao.history.HistoryObjExRightInfo;
import com.jili.ubert.dao.history.HistoryTradeFundTransfer;
import com.jili.ubert.dao.history.HistoryTradesetCalendar;
import com.jili.ubert.dao.recent.RecentClearingAccountBail;
import com.jili.ubert.dao.recent.RecentClearingAccountFare;
import com.jili.ubert.dao.recent.RecentClearingDayBook;
import com.jili.ubert.dao.recent.RecentClearingExRight;
import com.jili.ubert.dao.recent.RecentClearingFundState;
import com.jili.ubert.dao.recent.RecentClearingPosDetail;
import com.jili.ubert.dao.recent.RecentClearingPrice;
import com.jili.ubert.dao.recent.RecentClearingProtfoliopos;
import com.jili.ubert.dao.recent.RecentObjETF;
import com.jili.ubert.dao.recent.RecentObjETFList;
import com.jili.ubert.dao.recent.RecentObjIndex;
import com.jili.ubert.dao.recent.RecentObjIndexList;
import com.jili.ubert.dao.recent.RecentTradeAlterOrder;
import com.jili.ubert.dao.recent.RecentTradeCancelOrder;
import com.jili.ubert.dao.recent.RecentTradeExecuteOrder;
import com.jili.ubert.dao.recent.RecentTradeNewOrder;
import com.jili.ubert.dao.recent.RecentTradeReportDetail;
import com.jili.ubert.dao.trace.TraceAccountBail;
import com.jili.ubert.dao.trace.TraceAccountFare;
import com.jili.ubert.dao.trace.TraceAccountIcode;
import com.jili.ubert.dao.trace.TraceAccountInfo;
import com.jili.ubert.dao.trace.TraceAccountInvest;
import com.jili.ubert.dao.trace.TraceBailTemp;
import com.jili.ubert.dao.trace.TraceBailTempObj;
import com.jili.ubert.dao.trace.TraceFareTemp;
import com.jili.ubert.dao.trace.TraceFareTempeObj;
import com.jili.ubert.dao.trace.TraceObjInfo;
import com.jili.ubert.dao.trace.TraceObjProtfolio;
import com.jili.ubert.dao.trace.TraceObjProtfolioList;
import com.jili.ubert.dao.trace.TraceObjUnderlying;
import com.jili.ubert.dao.trace.TraceProductAccount;
import com.jili.ubert.dao.trace.TraceProductInfo;
import com.jili.ubert.dao.trace.TraceProductUnit;
import com.jili.ubert.dao.trace.TraceRiskCtrlAssets;
import com.jili.ubert.dao.trace.TraceRiskSets;
import com.jili.ubert.dao.trace.TraceSafeForbidLogin;
import com.jili.ubert.dao.trace.TraceSafeOtherAccess;
import com.jili.ubert.dao.trace.TraceTradesetExchangeLimit;
import com.jili.ubert.dao.trace.TraceUserAccess;
import com.jili.ubert.dao.trace.TraceUserAccount;
import com.jili.ubert.dao.trace.TraceUserInfo;
import com.jili.ubert.dao.trace.TraceUserPrivateInfo;
import com.jili.ubert.dao.trace.TraceWorkFlowPath;
import com.jili.ubert.dao.trace.TraceWorkGroup;
import com.jili.ubert.dao.trace.TraceWorkGroupUser;
import com.jili.ubert.dao.trace.TraceWorkProductFlow;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author dragon
 */
public class GenList {
    public GenList(){}

    public List<ClearingProtfoliopos> genClearingProtfolioposList(int n) {
        List<ClearingProtfoliopos> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ClearingProtfoliopos clearingprotfoliopos = new ClearingProtfoliopos();
            clearingprotfoliopos.setId(i);
            clearingprotfoliopos.setTimeStamp(new Date());
            clearingprotfoliopos.setClearingDate(new Date());
            clearingprotfoliopos.setAccountCode("jili成");
            clearingprotfoliopos.setPosID("jili成");
            clearingprotfoliopos.setQty(i);
            clearingprotfoliopos.setAvlQty(i);
            clearingprotfoliopos.setFrozeQty(i);
            clearingprotfoliopos.setClosePrice((float) 15.23);
            clearingprotfoliopos.setCostPriceA((float) 15.23);
            clearingprotfoliopos.setCostPriceB((float) 15.23);
            clearingprotfoliopos.setOpencostprice((float) 15.23);
            clearingprotfoliopos.setHedgeFlag("jili成");
            clearingprotfoliopos.setCurrency("jili成");
            clearingprotfoliopos.setPl((float) 15.23);
            clearingprotfoliopos.setCostPL((float) 15.23);
            clearingprotfoliopos.setTotalPL((float) 15.23);
            clearingprotfoliopos.setMargin((float) 15.23);
            clearingprotfoliopos.setTotalFare((float) 15.23);
            clearingprotfoliopos.setInvestID("jili成");
            clearingprotfoliopos.setOwnUserID("jili成");
            clearingprotfoliopos.setAlterUserID("jili成");
            clearingprotfoliopos.setCreateTime(new Date());
            ls.add(clearingprotfoliopos);
        }
        return ls;
    }

    public List<FareTemp> genFareTempList(int n) {
        List<FareTemp> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            FareTemp faretemp = new FareTemp();
            faretemp.setTempID(i);
            faretemp.setTimeStamp(new Date());
            faretemp.setTempName("jili成");
            faretemp.setByOwnGroupID(i);
            faretemp.setReMark("jili成");
            faretemp.setOwnUserID("jili成");
            faretemp.setAlterUserID("jili成");
            faretemp.setCreateTime(new Date());
            ls.add(faretemp);
        }
        return ls;
    }

    public List<FareTempeObj> genFareTempeObjList(int n) {
        List<FareTempeObj> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            FareTempeObj faretempeobj = new FareTempeObj();
            faretempeobj.setId(i);
            faretempeobj.setTimeStamp(new Date());
            faretempeobj.setTempID(i);
            faretempeobj.setMarketCode("jili成");
            faretempeobj.setClassCode("jil");
            faretempeobj.setUnderlyingCode("jili成");
            faretempeobj.setObj("jili成"+i);
            faretempeobj.setBs("jili成");
            faretempeobj.setOpenClose("jili成");
            faretempeobj.setCommission0((float) 15.23);
            faretempeobj.setCommission1((float) 15.23);
            faretempeobj.setMinCommission0((float) 15.23);
            faretempeobj.setMinCommission1((float) 15.23);
            faretempeobj.setDeliverFare0((float) 15.23);
            faretempeobj.setDeliverFare1((float) 15.23);
            faretempeobj.setTax0((float) 15.23);
            faretempeobj.setTax1((float) 15.23);
            faretempeobj.setEtax1((float) 15.23);
            faretempeobj.setEtax0((float) 15.23);
            faretempeobj.setCost0((float) 15.23);
            faretempeobj.setCost1((float) 15.23);
            faretempeobj.setOwnUserID("jili成");
            faretempeobj.setAlterUserID("jili成");
            faretempeobj.setCreateTime(new Date());
            ls.add(faretempeobj);
        }
        return ls;
    }

    public List<HistoryClearingCurrencyRates> genHistoryClearingCurrencyRatesList(int n) {
        List<HistoryClearingCurrencyRates> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            HistoryClearingCurrencyRates historyclearingcurrencyrates = new HistoryClearingCurrencyRates();
            historyclearingcurrencyrates.setId(i);
            historyclearingcurrencyrates.setTimeStamp(new Date());
            historyclearingcurrencyrates.setClearingDate(new Date());
            historyclearingcurrencyrates.setAskCurrency("jili成");
            historyclearingcurrencyrates.setBidCurrency("jili成");
            historyclearingcurrencyrates.setRatePrice((float) 15.23);
            historyclearingcurrencyrates.setOwnUserID("jili成");
            historyclearingcurrencyrates.setAlterUserID("jili成");
            historyclearingcurrencyrates.setCreateTime(new Date());
            ls.add(historyclearingcurrencyrates);
        }
        return ls;
    }

    public List<HistoryIOPVAccount> genHistoryIOPVAccountList(int n) {
        List<HistoryIOPVAccount> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            HistoryIOPVAccount historyiopvaccount = new HistoryIOPVAccount();
            historyiopvaccount.setId(i);
            historyiopvaccount.setTimeStamp(new Date());
            historyiopvaccount.setDate(new Date());
            historyiopvaccount.setProductID(i);
            historyiopvaccount.setUnitID(i);
            historyiopvaccount.setAccountCode("jili成");
            historyiopvaccount.setNet((float) 15.23);
            historyiopvaccount.setIopv((float) 15.23);
            historyiopvaccount.setRealIOPV((float) 15.23);
            historyiopvaccount.setCurrentLot((long)i);
            historyiopvaccount.setDeltLot(i);
            historyiopvaccount.setFeeOfDay((float) 15.23);
            historyiopvaccount.setClearFeeaAcumulation((float) 15.23);
            historyiopvaccount.setTotalAssetValue((float) 15.23);
            historyiopvaccount.setPosRate((float) 15.23);
            historyiopvaccount.setDebtAssetValue((float) 15.23);
            historyiopvaccount.setOwnUserID("jili成");
            historyiopvaccount.setAlterUserID("jili成");
            historyiopvaccount.setCreateTime(new Date());
            ls.add(historyiopvaccount);
        }
        return ls;
    }

    public List<HistoryIOPVProduct> genHistoryIOPVProductList(int n) {
        List<HistoryIOPVProduct> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            HistoryIOPVProduct historyiopvproduct = new HistoryIOPVProduct();
            historyiopvproduct.setId(i);
            historyiopvproduct.setTimeStamp(new Date());
            historyiopvproduct.setDate(new Date());
            historyiopvproduct.setProductID(i);
            historyiopvproduct.setNet((float) 15.23);
            historyiopvproduct.setIopv((float) 15.23);
            historyiopvproduct.setRealIOPV((float) 15.23);
            historyiopvproduct.setCurrentLot((long)i);
            historyiopvproduct.setDeltLot(i);
            historyiopvproduct.setFeeOfDay((float) 15.23);
            historyiopvproduct.setClearFeeaAcumulation((float) 15.23);
            historyiopvproduct.setTotalAssetValue((float) 15.23);
            historyiopvproduct.setPosRate((float) 15.23);
            historyiopvproduct.setDebtAssetValue((float) 15.23);
            historyiopvproduct.setOwnUserID("jili成");
            historyiopvproduct.setAlterUserID("jili成");
            historyiopvproduct.setCreateTime(new Date());
            ls.add(historyiopvproduct);
        }
        return ls;
    }

    public List<HistoryIOPVProductUnit> genHistoryIOPVProductUnitList(int n) {
        List<HistoryIOPVProductUnit> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            HistoryIOPVProductUnit historyiopvproductunit = new HistoryIOPVProductUnit();
            historyiopvproductunit.setId(i);
            historyiopvproductunit.setTimeStamp(new Date());
            historyiopvproductunit.setDate(new Date());
            historyiopvproductunit.setProductID(i);
            historyiopvproductunit.setUnitID(i);
            historyiopvproductunit.setNet((float) 15.23);
            historyiopvproductunit.setIopv((float) 15.23);
            historyiopvproductunit.setRealIOPV((float) 15.23);
            historyiopvproductunit.setCurrentLot((long)i);
            historyiopvproductunit.setDeltLot(i);
            historyiopvproductunit.setFeeOfDay((float) 15.23);
            historyiopvproductunit.setClearFeeaAcumulation((float) 15.23);
            historyiopvproductunit.setTotalAssetValue((float) 15.23);
            historyiopvproductunit.setPosRate((float) 15.23);
            historyiopvproductunit.setDebtAssetValue((float) 15.23);
            historyiopvproductunit.setOwnUserID("jili成");
            historyiopvproductunit.setAlterUserID("jili成");
            historyiopvproductunit.setCreateTime(new Date());
            ls.add(historyiopvproductunit);
        }
        return ls;
    }

    public List<HistoryLogLoginInfo> genHistoryLogLoginInfoList(int n) {
        List<HistoryLogLoginInfo> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            HistoryLogLoginInfo historyloglogininfo = new HistoryLogLoginInfo();
            historyloglogininfo.setId(i);
            historyloglogininfo.setTimeStamp(new Date());
            historyloglogininfo.setUserID("jili成"+i);
            historyloglogininfo.setLoginType("jili成");
            historyloglogininfo.setLogoutType("jili成");
            historyloglogininfo.setLoginDateTime(new Date());
            historyloglogininfo.setLogoutDateTime(new Date());
            historyloglogininfo.setMac("jili成");
            historyloglogininfo.setIp4(i);
//            historyloglogininfo.setIp6("jili".getBytes());
            historyloglogininfo.setAddress("jili成");
            historyloglogininfo.setClientType("jili成");
            historyloglogininfo.setOSType("jili成");
            historyloglogininfo.setClientVersion("jili成");
            historyloglogininfo.setCreateTime(new Date());
            ls.add(historyloglogininfo);
        }
        return ls;
    }

    public List<HistoryObjExRightInfo> genHistoryObjExRightInfoList(int n) {
        List<HistoryObjExRightInfo> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            HistoryObjExRightInfo historyobjexrightinfo = new HistoryObjExRightInfo();
            historyobjexrightinfo.setId(i);
            historyobjexrightinfo.setTimeStamp(new Date());
            historyobjexrightinfo.setRegionID("jili成");
            historyobjexrightinfo.setObj("jili成"+i);
            historyobjexrightinfo.setMarketCode("jili成");
            historyobjexrightinfo.setClassCode("jil");
            historyobjexrightinfo.setExRightDate(new Date());
            historyobjexrightinfo.setRecordeDate(new Date());
            historyobjexrightinfo.setCheckInRightDate(new Date());
            historyobjexrightinfo.setCheckInProfitsDate(new Date());
            historyobjexrightinfo.setDenominator(i);
            historyobjexrightinfo.setMemberA(i);
            historyobjexrightinfo.setMemberB(i);
            historyobjexrightinfo.setProfits((float) 15.23);
            historyobjexrightinfo.setReMark("jili成");
            historyobjexrightinfo.setOwnUserID("jili成");
            historyobjexrightinfo.setAlterUserID("jili成");
            historyobjexrightinfo.setCreateTime(new Date());
            ls.add(historyobjexrightinfo);
        }
        return ls;
    }

    public List<HistoryTradeFundTransfer> genHistoryTradeFundTransferList(int n) {
        List<HistoryTradeFundTransfer> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            HistoryTradeFundTransfer historytradefundtransfer = new HistoryTradeFundTransfer();
            historytradefundtransfer.setId(i);
            historytradefundtransfer.setTimeStamp(new Date());
            historytradefundtransfer.setRegionID("jili成");
            historytradefundtransfer.setAccountCode("jili成");
            historytradefundtransfer.setDirect("jili成");
            historytradefundtransfer.setAmount(45.123);
            historytradefundtransfer.setDate(new Date());
            historytradefundtransfer.setTime(new Date());
            historytradefundtransfer.setDateTime(new Date());
            historytradefundtransfer.setCurrency("jili成");
            historytradefundtransfer.setChecker("jili成");
            historytradefundtransfer.setReMark("jili成");
            historytradefundtransfer.setIsEffectived("jili成");
            historytradefundtransfer.setReplyMsg("jili成");
            historytradefundtransfer.setOwnUserID("jili成");
            historytradefundtransfer.setAlterUserID("jili成");
            historytradefundtransfer.setCreateTime(new Date());
            ls.add(historytradefundtransfer);
        }
        return ls;
    }

    public List<HistoryTradesetCalendar> genHistoryTradesetCalendarList(int n) {
        List<HistoryTradesetCalendar> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            HistoryTradesetCalendar historytradesetcalendar = new HistoryTradesetCalendar();
            historytradesetcalendar.setId(i);
            historytradesetcalendar.setTimeStamp(new Date());
            historytradesetcalendar.setRegionID("jili成");
            historytradesetcalendar.setDate(new Date());
            historytradesetcalendar.setIsTrade("jili成");
            historytradesetcalendar.setReMark("jili成");
            historytradesetcalendar.setOwnUserID("jili成");
            historytradesetcalendar.setAlterUserID("jili成");
            historytradesetcalendar.setCreateTime(new Date());
            ls.add(historytradesetcalendar);
        }
        return ls;
    }

    public List<IOPVAccount> genIOPVAccountList(int n) {
        List<IOPVAccount> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            IOPVAccount iopvaccount = new IOPVAccount();
            iopvaccount.setId(i);
            iopvaccount.setTimeStamp(new Date());
            iopvaccount.setDate(new Date());
            iopvaccount.setProductID(i);
            iopvaccount.setUnitID(i);
            iopvaccount.setAccountCode("jili成");
            iopvaccount.setNet((float) 15.23);
            iopvaccount.setIopv((float) 15.23);
            iopvaccount.setRealIOPV((float) 15.23);
            iopvaccount.setCurrentLot((long)i);
            iopvaccount.setDeltLot(i);
            iopvaccount.setFeeOfDay((float) 15.23);
            iopvaccount.setClearFeeaAcumulation((float) 15.23);
            iopvaccount.setTotalAssetValue((float) 15.23);
            iopvaccount.setPosRate((float) 15.23);
            iopvaccount.setDebtAssetValue((float) 15.23);
            iopvaccount.setOwnUserID("jili成");
            iopvaccount.setAlterUserID("jili成");
            iopvaccount.setCreateTime(new Date());
            ls.add(iopvaccount);
        }
        return ls;
    }

    public List<IOPVProduct> genIOPVProductList(int n) {
        List<IOPVProduct> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            IOPVProduct iopvproduct = new IOPVProduct();
            iopvproduct.setId(i);
            iopvproduct.setTimeStamp(new Date());
            iopvproduct.setDate(new Date());
            iopvproduct.setProductID(i);
            iopvproduct.setNet((float) 15.23);
            iopvproduct.setIopv((float) 15.23);
            iopvproduct.setRealIOPV((float) 15.23);
            iopvproduct.setCurrentLot((long)i);
            iopvproduct.setDeltLot(i);
            iopvproduct.setFeeOfDay((float) 15.23);
            iopvproduct.setClearFeeaAcumulation((float) 15.23);
            iopvproduct.setTotalAssetValue((float) 15.23);
            iopvproduct.setPosRate((float) 15.23);
            iopvproduct.setDebtAssetValue((float) 15.23);
            iopvproduct.setOwnUserID("jili成");
            iopvproduct.setAlterUserID("jili成");
            iopvproduct.setCreateTime(new Date());
            ls.add(iopvproduct);
        }
        return ls;
    }

    public List<IOPVProductUnit> genIOPVProductUnitList(int n) {
        List<IOPVProductUnit> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            IOPVProductUnit iopvproductunit = new IOPVProductUnit();
            iopvproductunit.setId(i);
            iopvproductunit.setTimeStamp(new Date());
            iopvproductunit.setDate(new Date());
            iopvproductunit.setProductID(i);
            iopvproductunit.setUnitID(i);
            iopvproductunit.setNet((float) 15.23);
            iopvproductunit.setIopv((float) 15.23);
            iopvproductunit.setRealIOPV((float) 15.23);
            iopvproductunit.setCurrentLot((long)i);
            iopvproductunit.setDeltLot(i);
            iopvproductunit.setFeeOfDay((float) 15.23);
            iopvproductunit.setClearFeeaAcumulation((float) 15.23);
            iopvproductunit.setTotalAssetValue((float) 15.23);
            iopvproductunit.setPosRate((float) 15.23);
            iopvproductunit.setDebtAssetValue((float) 15.23);
            iopvproductunit.setOwnUserID("jili成");
            iopvproductunit.setAlterUserID("jili成");
            iopvproductunit.setCreateTime(new Date());
            ls.add(iopvproductunit);
        }
        return ls;
    }

    public List<LogLoginInfo> genLogLoginInfoList(int n) {
        List<LogLoginInfo> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            LogLoginInfo loglogininfo = new LogLoginInfo();
            loglogininfo.setId(i);
            loglogininfo.setTimeStamp(new Date());
            loglogininfo.setUserID("jili成"+i);
            loglogininfo.setLoginType("jili成");
            loglogininfo.setLogoutType("jili成");
            loglogininfo.setLoginDateTime(new Date());
            loglogininfo.setLogoutDateTime(new Date());
            loglogininfo.setMac("jili成");
            loglogininfo.setIp4(i);
//            loglogininfo.setIp6("jili".getBytes());
            loglogininfo.setAddress("jili成");
            loglogininfo.setClientType("jili成");
            loglogininfo.setOSType("jili成");
            loglogininfo.setClientVersion("jili成");
            loglogininfo.setCreateTime(new Date());
            ls.add(loglogininfo);
        }
        return ls;
    }

    public List<ObjClassCode> genObjClassCodeList(int n) {
        if (n>999){
            n=999;
        }
        List<ObjClassCode> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ObjClassCode objclasscode = new ObjClassCode();
            objclasscode.setClassCode(""+i);
            objclasscode.setTimeStamp(new Date());
            objclasscode.setUnderlyingCode("jili成");
            objclasscode.setClassName("jili成");
            objclasscode.setReMark("jili成");
            objclasscode.setAlterUserID("jili成");
            objclasscode.setCreateTime(new Date());
            ls.add(objclasscode);
        }
        return ls;
    }

    public List<ObjClassCodeAtrr> genObjClassCodeAtrrList(int n) {
        List<ObjClassCodeAtrr> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ObjClassCodeAtrr objclasscodeatrr = new ObjClassCodeAtrr();
            objclasscodeatrr.setId(i);
            objclasscodeatrr.setTimeStamp(new Date());
            objclasscodeatrr.setClassCode("jil");
            objclasscodeatrr.setMarketCode("jili成");
            objclasscodeatrr.setUnderlyingCode("jili成");
            objclasscodeatrr.setUpDownLimit((float) 15.23);
            objclasscodeatrr.setContractSize(i);
            objclasscodeatrr.setTradeType("jili成");
            objclasscodeatrr.setClearType("jili成");
            objclasscodeatrr.setAlterUserID("jili成");
            objclasscodeatrr.setCreateTime(new Date());
            ls.add(objclasscodeatrr);
        }
        return ls;
    }

    public List<ObjETF> genObjETFList(int n) {
        List<ObjETF> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ObjETF objetf = new ObjETF();
            objetf.setId(i);
            objetf.setTimeStamp(new Date());
            objetf.setEtfid("jili成");
            objetf.setObj("jili成"+i);
            objetf.setPrimaryObj("jili成");
            objetf.setCashObj("jili成");
            objetf.setName("jili成");
            objetf.setMarketCode("jili成");
            objetf.setFundManagementCompany("jili成");
            objetf.setUnderlyingIndex("jili成");
            objetf.setTradeUnit(i);
            objetf.setEstimateCashCompoment((float) 15.23);
            objetf.setMaxCashRatio((float) 15.23);
            objetf.setPublish("jili成");
            objetf.setCreation("jili成");
            objetf.setRedemption("jili成");
            objetf.setRecordeNum(i);
            objetf.setListingDate(new Date());
            objetf.setCashComponent((float) 15.23);
            objetf.setNavPercu((float) 15.23);
            objetf.setNav((float) 15.23);
            objetf.setDividendPercu((float) 15.23);
            objetf.setOwnUserID("jili成");
            objetf.setAlterUserID("jili成");
            objetf.setCreateTime(new Date());
            ls.add(objetf);
        }
        return ls;
    }

    public List<ObjETFList> genObjETFListList(int n) {
        List<ObjETFList> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ObjETFList objetflist = new ObjETFList();
            objetflist.setId(i);
            objetflist.setTimeStamp(new Date());
            objetflist.setEtfid("jili成");
            objetflist.setListingDate(new Date());
            objetflist.setRegionID("jili成");
            objetflist.setObj("jili成"+i);
            objetflist.setMarketCode("jili成");
            objetflist.setClassCode("jil");
            objetflist.setQty(i);
            objetflist.setCanCashRepl("jili成");
            objetflist.setCashBuffer((float) 15.23);
            objetflist.setCashAmount((float) 15.23);
            objetflist.setReMark("jili成");
            objetflist.setOwnUserID("jili成");
            objetflist.setAlterUserID("jili成");
            objetflist.setCreateTime(new Date());
            ls.add(objetflist);
        }
        return ls;
    }

    public List<ObjExRightInfo> genObjExRightInfoList(int n) {
        List<ObjExRightInfo> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ObjExRightInfo objexrightinfo = new ObjExRightInfo();
            objexrightinfo.setId(i);
            objexrightinfo.setTimeStamp(new Date());
            objexrightinfo.setRegionID("jili成");
            objexrightinfo.setObj("jili成"+i);
            objexrightinfo.setMarketCode("jili成");
            objexrightinfo.setClassCode("jil");
            objexrightinfo.setExRightDate(new Date());
            objexrightinfo.setRecordeDate(new Date());
            objexrightinfo.setCheckInRightDate(new Date());
            objexrightinfo.setCheckInProfitsDate(new Date());
            objexrightinfo.setDenominator(i);
            objexrightinfo.setMemberA(i);
            objexrightinfo.setMemberB(i);
            objexrightinfo.setProfits((float) 15.23);
            objexrightinfo.setReMark("jili成");
            objexrightinfo.setOwnUserID("jili成");
            objexrightinfo.setAlterUserID("jili成");
            objexrightinfo.setCreateTime(new Date());
            ls.add(objexrightinfo);
        }
        return ls;
    }

    public List<ObjIndex> genObjIndexList(int n) {
        List<ObjIndex> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ObjIndex objindex = new ObjIndex();
            objindex.setIndexID(i);
            objindex.setTimeStamp(new Date());
            objindex.setIndexObj("jili成");
            objindex.setListingDate(new Date());
            objindex.setIndexName("jili成");
            objindex.setRegionID("jili成");
            objindex.setMarketCode("jili成");
            objindex.setOwnUserID("jili成");
            objindex.setAlterUserID("jili成");
            objindex.setCreateTime(new Date());
            ls.add(objindex);
        }
        return ls;
    }

    public List<ObjIndexList> genObjIndexListList(int n) {
        List<ObjIndexList> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ObjIndexList objindexlist = new ObjIndexList();
            objindexlist.setId(i);
            objindexlist.setTimeStamp(new Date());
            objindexlist.setIndexID(i);
            objindexlist.setIndexObj("jili成");
            objindexlist.setObj("jili成"+i);
            objindexlist.setRegionID("jili成");
            objindexlist.setMarketCode("jili成");
            objindexlist.setClassCode("jil");
            objindexlist.setQty(i);
            objindexlist.setRate((float) 15.23);
            objindexlist.setListingDate(new Date());
            objindexlist.setOwnUserID("jili成");
            objindexlist.setAlterUserID("jili成");
            objindexlist.setCreateTime(new Date());
            ls.add(objindexlist);
        }
        return ls;
    }

    public List<ObjInfo> genObjInfoList(int n) {
        List<ObjInfo> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ObjInfo objinfo = new ObjInfo();
            objinfo.setId(i);
            objinfo.setTimeStamp(new Date());
            objinfo.setObjID("jili成");
            objinfo.setRegionID("jili成");
            objinfo.setObj("jili成"+i);
            objinfo.setMarketCode("jili成");
            objinfo.setClassCode("jil");
            objinfo.setName("jili成");
            objinfo.setEName("jili成");
            objinfo.setLongName("jili成");
            objinfo.setListingDate(new Date());
            objinfo.setFaceValue(i);
            objinfo.setTradeUnit(i);
            objinfo.setCurrency("jili成");
            objinfo.setContractSize(i);
            objinfo.setTradeType("jili成");
            objinfo.setClearType("jili成");
            objinfo.setContractMonth("jili成");
            objinfo.setExpirationDate(new Date());
            objinfo.setUnderlyingCode("jili成");
            objinfo.setUnderlyingobj("jili成");
            objinfo.setFirst("jili成");
            objinfo.setSecend("jili成");
            objinfo.setThird("jili成");
            objinfo.setPutCall("jili成");
            objinfo.setStrikePrice((float) 15.23);
            objinfo.setOptionRate((float) 15.23);
            objinfo.setOwnUserID("jili成");
            objinfo.setAlterUserID("jili成");
            objinfo.setCreateTime(new Date());
            ls.add(objinfo);
        }
        return ls;
    }

    public List<ObjNewStock> genObjNewStockList(int n) {
        List<ObjNewStock> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ObjNewStock objnewstock = new ObjNewStock();
            objnewstock.setId(i);
            objnewstock.setTimeStamp(new Date());
            objnewstock.setObj("jili成"+i);
            objnewstock.setPrimaryObj("jili成");
            objnewstock.setName("jili成");
            objnewstock.setRegionID("jili成");
            objnewstock.setMarketCode("jili成");
            objnewstock.setClassCode("jil");
            objnewstock.setTradeUnit(i);
            objnewstock.setMaxQty(i);
            objnewstock.setPrimaryTradeDate(new Date());
            objnewstock.setTradeDate(new Date());
            objnewstock.setOwnUserID("jili成");
            objnewstock.setAlterUserID("jili成");
            objnewstock.setCreateTime(new Date());
            ls.add(objnewstock);
        }
        return ls;
    }

    public List<ObjPriceTick> genObjPriceTickList(int n) {
        List<ObjPriceTick> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ObjPriceTick objpricetick = new ObjPriceTick();
            objpricetick.setId(i);
            objpricetick.setTimeStamp(new Date());
            objpricetick.setClassCode("jil");
            objpricetick.setMarketCode("jili成");
            objpricetick.setUnderlyingCode("jili成");
            objpricetick.setLowerRange((float) 15.23);
            objpricetick.setUpperRange((float) 15.23);
            objpricetick.setTick((float) 15.23);
            objpricetick.setAlterUserID("jili成");
            objpricetick.setCreateTime(new Date());
            ls.add(objpricetick);
        }
        return ls;
    }

    public List<ObjProtfolio> genObjProtfolioList(int n) {
        List<ObjProtfolio> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ObjProtfolio objprotfolio = new ObjProtfolio();
            objprotfolio.setProtfolioID(i);
            objprotfolio.setTimeStamp(new Date());
            objprotfolio.setName("jili成");
            objprotfolio.setIsPublic("jili成");
            objprotfolio.setOwnUserID("jili成");
            objprotfolio.setAlterUserID("jili成");
            objprotfolio.setCreateTime(new Date());
            ls.add(objprotfolio);
        }
        return ls;
    }

    public List<ObjProtfolioList> genObjProtfolioListList(int n) {
        List<ObjProtfolioList> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ObjProtfolioList objprotfoliolist = new ObjProtfolioList();
            objprotfoliolist.setId(i);
            objprotfoliolist.setTimeStamp(new Date());
            objprotfoliolist.setProtfolioID(i);
            objprotfoliolist.setObj("jili成"+i);
            objprotfoliolist.setMarketCode("jili成");
            objprotfoliolist.setClassCode("jil");
            objprotfoliolist.setRegionID("jili成");
            objprotfoliolist.setQty(i);
            objprotfoliolist.setRate((float) 15.23);
            objprotfoliolist.setReMark("jili成");
            objprotfoliolist.setOwnUserID("jili成");
            objprotfoliolist.setAlterUserID("jili成");
            objprotfoliolist.setCreateTime(new Date());
            ls.add(objprotfoliolist);
        }
        return ls;
    }

    public List<ObjStructuredFundBase> genObjStructuredFundBaseList(int n) {
        List<ObjStructuredFundBase> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ObjStructuredFundBase objstructuredfundbase = new ObjStructuredFundBase();
            objstructuredfundbase.setObj("jili成"+i);
            objstructuredfundbase.setTimeStamp(new Date());
            objstructuredfundbase.setName("jili成");
            objstructuredfundbase.setMarketCode("jili成");
            objstructuredfundbase.setFundManagementCompany("jili成");
            objstructuredfundbase.setUnderlyingIndex("jili成");
            objstructuredfundbase.setTradingdate(new Date());
            objstructuredfundbase.setOwnUserID("jili成");
            objstructuredfundbase.setAlterUserID("jili成");
            objstructuredfundbase.setCreateTime(new Date());
            ls.add(objstructuredfundbase);
        }
        return ls;
    }

    public List<ObjStructuredFundPart> genObjStructuredFundPartList(int n) {
        List<ObjStructuredFundPart> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ObjStructuredFundPart objstructuredfundpart = new ObjStructuredFundPart();
            objstructuredfundpart.setObj("jili成"+i);
            objstructuredfundpart.setTimeStamp(new Date());
            objstructuredfundpart.setBaseObj("jili成");
            objstructuredfundpart.setMarketCode("jili成");
            objstructuredfundpart.setType("jili成");
            objstructuredfundpart.setRate((float) 15.23);
            objstructuredfundpart.setOwnUserID("jili成");
            objstructuredfundpart.setAlterUserID("jili成");
            objstructuredfundpart.setCreateTime(new Date());
            ls.add(objstructuredfundpart);
        }
        return ls;
    }

    public List<ObjUnderlyingCode> genObjUnderlyingCodeList(int n) {
        List<ObjUnderlyingCode> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ObjUnderlyingCode objunderlyingcode = new ObjUnderlyingCode();
            objunderlyingcode.setId(i);
            objunderlyingcode.setTimeStamp(new Date());
            objunderlyingcode.setUnderlyingCode("jili成");
            objunderlyingcode.setRegionID("jili成");
            objunderlyingcode.setMarketCode("jili成");
            objunderlyingcode.setClassCode("jil");
            objunderlyingcode.setUnderlyingName("jili成");
            objunderlyingcode.setCurrency("jili成");
            objunderlyingcode.setTick((float) 15.23);
            objunderlyingcode.setTradeUnit(i);
            objunderlyingcode.setContractSize(i);
            objunderlyingcode.setOwnUserID("jili成");
            objunderlyingcode.setAlterUserID("jili成");
            objunderlyingcode.setCreateTime(new Date());
            ls.add(objunderlyingcode);
        }
        return ls;
    }

    public List<ProductAccount> genProductAccountList(int n) {
        List<ProductAccount> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ProductAccount productaccount = new ProductAccount();
            productaccount.setId(i);
            productaccount.setTimeStamp(new Date());
            productaccount.setProductID(i);
            productaccount.setUnitID(i);
            productaccount.setAccountCode("jili成");
            productaccount.setNet((float) 15.23);
            productaccount.setShare((long)i);
            productaccount.setStartDate(new Date());
            productaccount.setEndDate(new Date());
            productaccount.setBelongGroupID(i);
            productaccount.setOwnUserID("jili成");
            productaccount.setAlterUserID("jili成");
            productaccount.setCreateTime(new Date());
            ls.add(productaccount);
        }
        return ls;
    }

    public List<ProductInfo> genProductInfoList(int n) {
        List<ProductInfo> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ProductInfo productinfo = new ProductInfo();
            productinfo.setProductID(i);
            productinfo.setTimeStamp(new Date());
            productinfo.setProductName("jili成");
            productinfo.setNet((float) 15.23);
            productinfo.setShare((long)i);
            productinfo.setStartDate(new Date());
            productinfo.setEndDate(new Date());
            productinfo.setBelongGroupID(i);
            productinfo.setType("jili成");
            productinfo.setReMark("jili成");
            productinfo.setOwnUserID("jili成");
            productinfo.setAlterUserID("jili成");
            productinfo.setCreateTime(new Date());
            ls.add(productinfo);
        }
        return ls;
    }

    public List<ProductUnit> genProductUnitList(int n) {
        List<ProductUnit> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ProductUnit productunit = new ProductUnit();
            productunit.setUnitID(i);
            productunit.setTimeStamp(new Date());
            productunit.setProductID(i);
            productunit.setUnitName("jili成");
            productunit.setNet((float) 15.23);
            productunit.setShare((long)i);
            productunit.setStartDate(new Date());
            productunit.setEndDate(new Date());
            productunit.setBelongGroupID(i);
            productunit.setOwnUserID("jili成");
            productunit.setAlterUserID("jili成");
            productunit.setCreateTime(new Date());
            ls.add(productunit);
        }
        return ls;
    }

    public List<RecentClearingAccountBail> genRecentClearingAccountBailList(int n) {
        List<RecentClearingAccountBail> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentClearingAccountBail recentclearingaccountbail = new RecentClearingAccountBail();
            recentclearingaccountbail.setId((long)i);
            recentclearingaccountbail.setTimeStamp(new Date());
            recentclearingaccountbail.setClearingDate(new Date());
            recentclearingaccountbail.setAccountCode("jili成");
            recentclearingaccountbail.setTempID(i);
            recentclearingaccountbail.setMarketCode("jili成");
            recentclearingaccountbail.setClassCode("jil");
            recentclearingaccountbail.setBs("jili成");
            recentclearingaccountbail.setUnderlyingCode("jili成");
            recentclearingaccountbail.setObj("jili成"+i);
            recentclearingaccountbail.setBail0((float) 15.23);
            recentclearingaccountbail.setBail1((float) 15.23);
            recentclearingaccountbail.setBail2((float) 15.23);
            recentclearingaccountbail.setBail0Rate((float) 15.23);
            recentclearingaccountbail.setBail1Rate((float) 15.23);
            recentclearingaccountbail.setBail2Rate((float) 15.23);
            recentclearingaccountbail.setOwnUserID("jili成");
            recentclearingaccountbail.setAlterUserID("jili成");
            recentclearingaccountbail.setCreateTime(new Date());
            ls.add(recentclearingaccountbail);
        }
        return ls;
    }

    public List<RecentClearingAccountFare> genRecentClearingAccountFareList(int n) {
        List<RecentClearingAccountFare> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentClearingAccountFare recentclearingaccountfare = new RecentClearingAccountFare();
            recentclearingaccountfare.setId((long)i);
            recentclearingaccountfare.setTimeStamp(new Date());
            recentclearingaccountfare.setClearingDate(new Date());
            recentclearingaccountfare.setAccountCode("jili成");
            recentclearingaccountfare.setTempID(i);
            recentclearingaccountfare.setMarketCode("jili成");
            recentclearingaccountfare.setClassCode("jil");
            recentclearingaccountfare.setUnderlyingCode("jili成");
            recentclearingaccountfare.setObj("jili成"+i);
            recentclearingaccountfare.setBs("jili成");
            recentclearingaccountfare.setOpenClose("jili成");
            recentclearingaccountfare.setCommission0((float) 15.23);
            recentclearingaccountfare.setCommission1((float) 15.23);
            recentclearingaccountfare.setMinCommission0((float) 15.23);
            recentclearingaccountfare.setMinCommission1((float) 15.23);
            recentclearingaccountfare.setDeliverFare0((float) 15.23);
            recentclearingaccountfare.setDeliverFare1((float) 15.23);
            recentclearingaccountfare.setTax0((float) 15.23);
            recentclearingaccountfare.setTax1((float) 15.23);
            recentclearingaccountfare.setEtax1((float) 15.23);
            recentclearingaccountfare.setEtax0((float) 15.23);
            recentclearingaccountfare.setCost0((float) 15.23);
            recentclearingaccountfare.setCost1((float) 15.23);
            recentclearingaccountfare.setOwnUserID("jili成");
            recentclearingaccountfare.setAlterUserID("jili成");
            recentclearingaccountfare.setCreateTime(new Date());
            ls.add(recentclearingaccountfare);
        }
        return ls;
    }

    public List<RecentClearingDayBook> genRecentClearingDayBookList(int n) {
        List<RecentClearingDayBook> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentClearingDayBook recentclearingdaybook = new RecentClearingDayBook();
            recentclearingdaybook.setId((long)i);
            recentclearingdaybook.setTimeStamp(new Date());
            recentclearingdaybook.setDayid((long)i);
            recentclearingdaybook.setBookDate(new Date());
            recentclearingdaybook.setAccountCode("jili成");
            recentclearingdaybook.setBookID(i);
            recentclearingdaybook.setExeTime(new Date());
            recentclearingdaybook.setTradeDate(new Date());
            recentclearingdaybook.setAccountTitle("jili成");
            recentclearingdaybook.setAmountReceived(45.123);
            recentclearingdaybook.setAmountPayed(45.123);
            recentclearingdaybook.setBalance(45.123);
            recentclearingdaybook.setMarketCode("jili成");
            recentclearingdaybook.setClassCode("jil");
            recentclearingdaybook.setObj("jili成"+i);
            recentclearingdaybook.setMac("jili成");
            recentclearingdaybook.setIp4(i);
            recentclearingdaybook.setOrderTypeEmit("jili成");
            recentclearingdaybook.setTradeOperate(i);
            recentclearingdaybook.setCloseType("jili成");
            recentclearingdaybook.setBs("jili成");
            recentclearingdaybook.setOpenClose("jili成");
            recentclearingdaybook.setHedgeFlag("jili成");
            recentclearingdaybook.setPosID("jili成");
            recentclearingdaybook.setExeSequenceNo(i);
            recentclearingdaybook.setOutsideExeNo("jili成");
            recentclearingdaybook.setExeQty(i);
            recentclearingdaybook.setExePrice((float) 15.23);
            recentclearingdaybook.setCurrency("jili成");
            recentclearingdaybook.setExeAmount((float) 15.23);
            recentclearingdaybook.setTotalFare((float) 15.23);
            recentclearingdaybook.setCommissionFare((float) 15.23);
            recentclearingdaybook.setDeliverFare((float) 15.23);
            recentclearingdaybook.setTax((float) 15.23);
            recentclearingdaybook.setOtherCost((float) 15.23);
            recentclearingdaybook.setEsCheckinAmount(45.123);
            recentclearingdaybook.setInvestID("jili成");
            recentclearingdaybook.setOwnUserID("jili成");
            recentclearingdaybook.setAlterUserID("jili成");
            recentclearingdaybook.setCreateTime(new Date());
            ls.add(recentclearingdaybook);
        }
        return ls;
    }

    public List<RecentClearingExRight> genRecentClearingExRightList(int n) {
        List<RecentClearingExRight> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentClearingExRight recentclearingexright = new RecentClearingExRight();
            recentclearingexright.setId((long)i);
            recentclearingexright.setTimeStamp(new Date());
            recentclearingexright.setAccountCode("jili成");
            recentclearingexright.setClearingDate(new Date());
            recentclearingexright.setObj("jili成"+i);
            recentclearingexright.setMarketCode("jili成");
            recentclearingexright.setClassCode("jil");
            recentclearingexright.setRecordeDate(new Date());
            recentclearingexright.setPosQty(i);
            recentclearingexright.setProfitsAmount((float) 15.23);
            recentclearingexright.setShareA(i);
            recentclearingexright.setShareB(i);
            recentclearingexright.setTax((float) 15.23);
            recentclearingexright.setNetProfitsAmount((float) 15.23);
            recentclearingexright.setReMark("jili成");
            recentclearingexright.setOwnUserID("jili成");
            recentclearingexright.setAlterUserID("jili成");
            recentclearingexright.setCreateTime(new Date());
            ls.add(recentclearingexright);
        }
        return ls;
    }

    public List<RecentClearingFundState> genRecentClearingFundStateList(int n) {
        List<RecentClearingFundState> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentClearingFundState recentclearingfundstate = new RecentClearingFundState();
            recentclearingfundstate.setId((long)i);
            recentclearingfundstate.setTimeStamp(new Date());
            recentclearingfundstate.setClearingDate(new Date());
            recentclearingfundstate.setAccountCode("jili成");
            recentclearingfundstate.setFundTotal((float) 15.23);
            recentclearingfundstate.setFundAvl((float) 15.23);
            recentclearingfundstate.setFundFroze((float) 15.23);
            recentclearingfundstate.setAssetTotal((float) 15.23);
            recentclearingfundstate.setAssetNet((float) 15.23);
            recentclearingfundstate.setAssetCredit((float) 15.23);
            recentclearingfundstate.setPosTotalValue((float) 15.23);
            recentclearingfundstate.setPosNetValue((float) 15.23);
            recentclearingfundstate.setPosCreditValue((float) 15.23);
            recentclearingfundstate.setCurrency("jili成");
            recentclearingfundstate.setRate((float) 15.23);
            recentclearingfundstate.setEqualFundAvl((float) 15.23);
            recentclearingfundstate.setBefAssetTotal((float) 15.23);
            recentclearingfundstate.setBefFundAvl((float) 15.23);
            recentclearingfundstate.setOwnUserID("jili成");
            recentclearingfundstate.setAlterUserID("jili成");
            recentclearingfundstate.setCreateTime(new Date());
            ls.add(recentclearingfundstate);
        }
        return ls;
    }

    public List<RecentClearingPosDetail> genRecentClearingPosDetailList(int n) {
        List<RecentClearingPosDetail> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentClearingPosDetail recentclearingposdetail = new RecentClearingPosDetail();
            recentclearingposdetail.setId((long)i);
            recentclearingposdetail.setTimeStamp(new Date());
            recentclearingposdetail.setClearingDate(new Date());
            recentclearingposdetail.setAccountCode("jili成");
            recentclearingposdetail.setPosID("jili成");
            recentclearingposdetail.setObj("jili成"+i);
            recentclearingposdetail.setMarketCode("jili成");
            recentclearingposdetail.setClassCode("jil");
            recentclearingposdetail.setBs("jili成");
            recentclearingposdetail.setQty(i);
            recentclearingposdetail.setAvlQty(i);
            recentclearingposdetail.setFrozeQty(i);
            recentclearingposdetail.setClosePrice((float) 15.23);
            recentclearingposdetail.setCostPriceA((float) 15.23);
            recentclearingposdetail.setCostPriceB((float) 15.23);
            recentclearingposdetail.setOpencostprice((float) 15.23);
            recentclearingposdetail.setAvgCostPrice((float) 15.23);
            recentclearingposdetail.setHedgeFlag("jili成");
            recentclearingposdetail.setPosType("jili成");
            recentclearingposdetail.setPutCall("jili成");
            recentclearingposdetail.setCurrency("jili成");
            recentclearingposdetail.setPl((float) 15.23);
            recentclearingposdetail.setCostPL((float) 15.23);
            recentclearingposdetail.setTotalPL((float) 15.23);
            recentclearingposdetail.setMargin((float) 15.23);
            recentclearingposdetail.setTotalFare((float) 15.23);
            recentclearingposdetail.setInvestID("jili成");
            recentclearingposdetail.setOwnUserID("jili成");
            recentclearingposdetail.setAlterUserID("jili成");
            recentclearingposdetail.setCreateTime(new Date());
            ls.add(recentclearingposdetail);
        }
        return ls;
    }

    public List<RecentClearingPrice> genRecentClearingPriceList(int n) {
        List<RecentClearingPrice> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentClearingPrice recentclearingprice = new RecentClearingPrice();
            recentclearingprice.setId((long)i);
            recentclearingprice.setTimeStamp(new Date());
            recentclearingprice.setClearingDate(new Date());
            recentclearingprice.setRegionID("jili成");
            recentclearingprice.setObj("jili成"+i);
            recentclearingprice.setClassCode("jil");
            recentclearingprice.setMarketCode("jili成");
            recentclearingprice.setClosePrice((float) 15.23);
            recentclearingprice.setClearprice((float) 15.23);
            recentclearingprice.setALCPrice((float) 15.23);
            recentclearingprice.setOwnUserID("jili成");
            recentclearingprice.setAlterUserID("jili成");
            recentclearingprice.setCreateTime(new Date());
            ls.add(recentclearingprice);
        }
        return ls;
    }

    public List<RecentClearingProtfoliopos> genRecentClearingProtfolioposList(int n) {
        List<RecentClearingProtfoliopos> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentClearingProtfoliopos recentclearingprotfoliopos = new RecentClearingProtfoliopos();
            recentclearingprotfoliopos.setId(i);
            recentclearingprotfoliopos.setTimeStamp(new Date());
            recentclearingprotfoliopos.setClearingDate(new Date());
            recentclearingprotfoliopos.setAccountCode("jili成");
            recentclearingprotfoliopos.setPosID("jili成");
            recentclearingprotfoliopos.setQty(i);
            recentclearingprotfoliopos.setAvlQty(i);
            recentclearingprotfoliopos.setFrozeQty(i);
            recentclearingprotfoliopos.setClosePrice((float) 15.23);
            recentclearingprotfoliopos.setCostPriceA((float) 15.23);
            recentclearingprotfoliopos.setCostPriceB((float) 15.23);
            recentclearingprotfoliopos.setOpencostprice((float) 15.23);
            recentclearingprotfoliopos.setHedgeFlag("jili成");
            recentclearingprotfoliopos.setCurrency("jili成");
            recentclearingprotfoliopos.setPl((float) 15.23);
            recentclearingprotfoliopos.setCostPL((float) 15.23);
            recentclearingprotfoliopos.setTotalPL((float) 15.23);
            recentclearingprotfoliopos.setMargin((float) 15.23);
            recentclearingprotfoliopos.setTotalFare((float) 15.23);
            recentclearingprotfoliopos.setInvestID("jili成");
            recentclearingprotfoliopos.setOwnUserID("jili成");
            recentclearingprotfoliopos.setAlterUserID("jili成");
            recentclearingprotfoliopos.setCreateTime(new Date());
            ls.add(recentclearingprotfoliopos);
        }
        return ls;
    }

    public List<RecentObjETF> genRecentObjETFList(int n) {
        List<RecentObjETF> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentObjETF recentobjetf = new RecentObjETF();
            recentobjetf.setId(i);
            recentobjetf.setTimeStamp(new Date());
            recentobjetf.setListingDate(new Date());
            recentobjetf.setEtfid("jili成");
            recentobjetf.setObj("jili成"+i);
            recentobjetf.setPrimaryObj("jili成");
            recentobjetf.setCashObj("jili成");
            recentobjetf.setName("jili成");
            recentobjetf.setMarketCode("jili成");
            recentobjetf.setFundManagementCompany("jili成");
            recentobjetf.setUnderlyingIndex("jili成");
            recentobjetf.setTradeUnit(i);
            recentobjetf.setEstimateCashCompoment((float) 15.23);
            recentobjetf.setMaxCashRatio((float) 15.23);
            recentobjetf.setPublish("jili成");
            recentobjetf.setCreation("jili成");
            recentobjetf.setRedemption("jili成");
            recentobjetf.setRecordeNum(i);
            recentobjetf.setCashComponent((float) 15.23);
            recentobjetf.setNavPercu((float) 15.23);
            recentobjetf.setNav((float) 15.23);
            recentobjetf.setDividendPercu((float) 15.23);
            recentobjetf.setOwnUserID("jili成");
            recentobjetf.setAlterUserID("jili成");
            recentobjetf.setCreateTime(new Date());
            ls.add(recentobjetf);
        }
        return ls;
    }

    public List<RecentObjETFList> genRecentObjETFListList(int n) {
        List<RecentObjETFList> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentObjETFList recentobjetflist = new RecentObjETFList();
            recentobjetflist.setId(i);
            recentobjetflist.setTimeStamp(new Date());
            recentobjetflist.setEtfid("jili成");
            recentobjetflist.setListingDate(new Date());
            recentobjetflist.setObj("jili成"+i);
            recentobjetflist.setMarketCode("jili成");
            recentobjetflist.setClassCode("jil");
            recentobjetflist.setQty(i);
            recentobjetflist.setCanCashRepl("jili成");
            recentobjetflist.setCashBuffer((float) 15.23);
            recentobjetflist.setCashAmount((float) 15.23);
            recentobjetflist.setReMark("jili成");
            recentobjetflist.setOwnUserID("jili成");
            recentobjetflist.setAlterUserID("jili成");
            recentobjetflist.setCreateTime(new Date());
            ls.add(recentobjetflist);
        }
        return ls;
    }

    public List<RecentObjIndex> genRecentObjIndexList(int n) {
        List<RecentObjIndex> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentObjIndex recentobjindex = new RecentObjIndex();
            recentobjindex.setIndexID(i);
            recentobjindex.setTimeStamp(new Date());
            recentobjindex.setListingDate(new Date());
            recentobjindex.setIndexObj("jili成");
            recentobjindex.setIndexName("jili成");
            recentobjindex.setRegionID("jili成");
            recentobjindex.setMarketCode("jili成");
            recentobjindex.setOwnUserID("jili成");
            recentobjindex.setAlterUserID("jili成");
            recentobjindex.setCreateTime(new Date());
            ls.add(recentobjindex);
        }
        return ls;
    }

    public List<RecentObjIndexList> genRecentObjIndexListList(int n) {
        List<RecentObjIndexList> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentObjIndexList recentobjindexlist = new RecentObjIndexList();
            recentobjindexlist.setId(i);
            recentobjindexlist.setTimeStamp(new Date());
            recentobjindexlist.setIndexID(i);
            recentobjindexlist.setIndexObj("jili成");
            recentobjindexlist.setObj("jili成"+i);
            recentobjindexlist.setRegionID("jili成");
            recentobjindexlist.setMarketCode("jili成");
            recentobjindexlist.setClassCode("jil");
            recentobjindexlist.setQty(i);
            recentobjindexlist.setRate((float) 15.23);
            recentobjindexlist.setListingDate(new Date());
            recentobjindexlist.setOwnUserID("jili成");
            recentobjindexlist.setAlterUserID("jili成");
            recentobjindexlist.setCreateTime(new Date());
            ls.add(recentobjindexlist);
        }
        return ls;
    }

    public List<RecentTradeAlterOrder> genRecentTradeAlterOrderList(int n) {
        List<RecentTradeAlterOrder> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentTradeAlterOrder recenttradealterorder = new RecentTradeAlterOrder();
            recenttradealterorder.setId((long)i);
            recenttradealterorder.setTimeStamp(new Date());
            recenttradealterorder.setOrderID(i);
            recenttradealterorder.setDealerUserID("jili成");
            recenttradealterorder.setAccountCode("jili成");
            recenttradealterorder.setTime(new Date());
            recenttradealterorder.setTradeDate(new Date());
            recenttradealterorder.setDateTime(new Date());
            recenttradealterorder.setMac("jili成");
            recenttradealterorder.setIp4(i);
            recenttradealterorder.setOrderTypeEmit("jili成");
            recenttradealterorder.setTradeOperate(i);
            recenttradealterorder.setEntrustNo(i);
            recenttradealterorder.setEntrustkey("jili成");
            recenttradealterorder.setAlterQty(i);
            recenttradealterorder.setAlterPrice((float) 15.23);
            recenttradealterorder.setWorkStationID("jili成");
            recenttradealterorder.setOwnUserID("jili成");
            recenttradealterorder.setAlterUserID("jili成");
            recenttradealterorder.setCreateTime(new Date());
            ls.add(recenttradealterorder);
        }
        return ls;
    }

    public List<RecentTradeCancelOrder> genRecentTradeCancelOrderList(int n) {
        List<RecentTradeCancelOrder> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentTradeCancelOrder recenttradecancelorder = new RecentTradeCancelOrder();
            recenttradecancelorder.setId((long)i);
            recenttradecancelorder.setTimeStamp(new Date());
            recenttradecancelorder.setOrderID(i);
            recenttradecancelorder.setDealerUserID("jili成");
            recenttradecancelorder.setAccountCode("jili成");
            recenttradecancelorder.setTime(new Date());
            recenttradecancelorder.setTradeDate(new Date());
            recenttradecancelorder.setDateTime(new Date());
            recenttradecancelorder.setMac("jili成");
            recenttradecancelorder.setIp4(i);
            recenttradecancelorder.setOrderTypeEmit("jili成");
            recenttradecancelorder.setTradeOperate(i);
            recenttradecancelorder.setEntrustNo(i);
            recenttradecancelorder.setEntrustkey("jili成");
            recenttradecancelorder.setCancelQty(i);
            recenttradecancelorder.setWorkStationID("jili成");
            recenttradecancelorder.setOwnUserID("jili成");
            recenttradecancelorder.setAlterUserID("jili成");
            recenttradecancelorder.setCreateTime(new Date());
            ls.add(recenttradecancelorder);
        }
        return ls;
    }

    public List<RecentTradeExecuteOrder> genRecentTradeExecuteOrderList(int n) {
        List<RecentTradeExecuteOrder> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentTradeExecuteOrder recenttradeexecuteorder = new RecentTradeExecuteOrder();
            recenttradeexecuteorder.setId((long)i);
            recenttradeexecuteorder.setTimeStamp(new Date());
            recenttradeexecuteorder.setOrderID(i);
            recenttradeexecuteorder.setAccountCode("jili成");
            recenttradeexecuteorder.setDealerUserID("jili成");
            recenttradeexecuteorder.setSubmitTime(new Date());
            recenttradeexecuteorder.setTradeDate(new Date());
            recenttradeexecuteorder.setDateTime(new Date());
            recenttradeexecuteorder.setMac("jili成");
            recenttradeexecuteorder.setIp4(i);
            recenttradeexecuteorder.setOrderTypeEmit("jili成");
            recenttradeexecuteorder.setTradeOperate(i);
            recenttradeexecuteorder.setMarketCode("jili成");
            recenttradeexecuteorder.setClassCode("jil");
            recenttradeexecuteorder.setObj("jili成"+i);
            recenttradeexecuteorder.setBs("jili成");
            recenttradeexecuteorder.setOpenClose("jili成");
            recenttradeexecuteorder.setHedgeFlag("jili成");
            recenttradeexecuteorder.setPosID("jili成");
            recenttradeexecuteorder.setCustomCode("jili成");
            recenttradeexecuteorder.setCloseType("jili成");
            recenttradeexecuteorder.setBasketID(i);
            recenttradeexecuteorder.setBasketTatol(i);
            recenttradeexecuteorder.setSeqNum(i);
            recenttradeexecuteorder.setExeTime(new Date());
            recenttradeexecuteorder.setExeSequenceNo(i);
            recenttradeexecuteorder.setOutsideExeNo("jili成");
            recenttradeexecuteorder.setExeQty(i);
            recenttradeexecuteorder.setExePrice((float) 15.23);
            recenttradeexecuteorder.setCurrency("jili成");
            recenttradeexecuteorder.setExeAmount((float) 15.23);
            recenttradeexecuteorder.setCancelQty(i);
            recenttradeexecuteorder.setOrderQty(i);
            recenttradeexecuteorder.setWorkingQty(i);
            recenttradeexecuteorder.setCompleteQty(i);
            recenttradeexecuteorder.setOrderStatus("jili成");
            recenttradeexecuteorder.setTotalFare((float) 15.23);
            recenttradeexecuteorder.setCommissionFare((float) 15.23);
            recenttradeexecuteorder.setDeliverFare((float) 15.23);
            recenttradeexecuteorder.setTax((float) 15.23);
            recenttradeexecuteorder.setOtherCost((float) 15.23);
            recenttradeexecuteorder.setEsCheckinAmount(45.123);
            recenttradeexecuteorder.setReMark("jili成");
            recenttradeexecuteorder.setInvestID("jili成");
            recenttradeexecuteorder.setOwnUserID("jili成");
            recenttradeexecuteorder.setAlterUserID("jili成");
            recenttradeexecuteorder.setCreateTime(new Date());
            ls.add(recenttradeexecuteorder);
        }
        return ls;
    }

    public List<RecentTradeNewOrder> genRecentTradeNewOrderList(int n) {
        List<RecentTradeNewOrder> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentTradeNewOrder recenttradeneworder = new RecentTradeNewOrder();
            recenttradeneworder.setId((long)i);
            recenttradeneworder.setTimeStamp(new Date());
            recenttradeneworder.setOrderID(i);
            recenttradeneworder.setAccountCode("jili成");
            recenttradeneworder.setDealerUserID("jili成");
            recenttradeneworder.setSubmitTime(new Date());
            recenttradeneworder.setTradeDate(new Date());
            recenttradeneworder.setDateTime(new Date());
            recenttradeneworder.setMac("jili成");
            recenttradeneworder.setIp4(i);
            recenttradeneworder.setOrderTypeEmit("jili成");
            recenttradeneworder.setTradeOperate(i);
            recenttradeneworder.setObj("jili成"+i);
            recenttradeneworder.setMarketCode("jili成");
            recenttradeneworder.setClassCode("jil");
            recenttradeneworder.setBs("jili成");
            recenttradeneworder.setOpenClose("jili成");
            recenttradeneworder.setPriceType("jili成");
            recenttradeneworder.setOrderPrice((float) 15.23);
            recenttradeneworder.setOrderQty(i);
            recenttradeneworder.setHedgeFlag("jili成");
            recenttradeneworder.setPosID("jili成");
            recenttradeneworder.setCustomCode("jili成");
            recenttradeneworder.setCloseType("jili成");
            recenttradeneworder.setBasketID(i);
            recenttradeneworder.setBasketTatol(i);
            recenttradeneworder.setSeqNum(i);
            recenttradeneworder.setWorkStationID("jili成");
            recenttradeneworder.setOwnUserID("jili成");
            recenttradeneworder.setAlterUserID("jili成");
            recenttradeneworder.setCreateTime(new Date());
            ls.add(recenttradeneworder);
        }
        return ls;
    }

    public List<RecentTradeReportDetail> genRecentTradeReportDetailList(int n) {
        List<RecentTradeReportDetail> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RecentTradeReportDetail recenttradereportdetail = new RecentTradeReportDetail();
            recenttradereportdetail.setId((long)i);
            recenttradereportdetail.setTimeStamp(new Date());
            recenttradereportdetail.setMsgType("jili成");
            recenttradereportdetail.setOrderID(i);
            recenttradereportdetail.setReportNo(i);
            recenttradereportdetail.setReportContent("jili成");
            recenttradereportdetail.setEntrustNo(i);
            recenttradereportdetail.setEntrustkey("jili成");
            recenttradereportdetail.setWorkingQty(i);
            recenttradereportdetail.setUnWorkingQty(i);
            recenttradereportdetail.setCancelQty(i);
            recenttradereportdetail.setExeQtyTatol(i);
            recenttradereportdetail.setExePriceAvg((float) 15.23);
            recenttradereportdetail.setExeAmountTatol(45.123);
            recenttradereportdetail.setPrice((float) 15.23);
            recenttradereportdetail.setExexSeq(i);
            recenttradereportdetail.setExeSequenceNo(i);
            recenttradereportdetail.setOutsideExeNo("jili成");
            recenttradereportdetail.setExeQty(i);
            recenttradereportdetail.setExePrice((float) 15.23);
            recenttradereportdetail.setCurrency("jili成");
            recenttradereportdetail.setExeAmount((float) 15.23);
            recenttradereportdetail.setAccountCode("jili成");
            recenttradereportdetail.setMarketCode("jili成");
            recenttradereportdetail.setClassCode("jil");
            recenttradereportdetail.setObj("jili成"+i);
            recenttradereportdetail.setBs("jili成");
            recenttradereportdetail.setOpenClose("jili成");
            recenttradereportdetail.setHedgeFlag("jili成");
            recenttradereportdetail.setCloseType("jili成");
            recenttradereportdetail.setExeTime(new Date());
            recenttradereportdetail.setEntrustTime(new Date());
            recenttradereportdetail.setSubmitTime(new Date());
            recenttradereportdetail.setTradeDate(new Date());
            recenttradereportdetail.setInvestID("jili成");
            recenttradereportdetail.setReMark("jili成");
            recenttradereportdetail.setOwnUserID("jili成");
            recenttradereportdetail.setAlterUserID("jili成");
            recenttradereportdetail.setCreateTime(new Date());
            ls.add(recenttradereportdetail);
        }
        return ls;
    }

    public List<RiskCtrlAssets> genRiskCtrlAssetsList(int n) {
        List<RiskCtrlAssets> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RiskCtrlAssets riskctrlassets = new RiskCtrlAssets();
            riskctrlassets.setId(i);
            riskctrlassets.setTimeStamp(new Date());
            riskctrlassets.setCtrlAssetsCode("jili成");
            riskctrlassets.setObj("jili成"+i);
            riskctrlassets.setIsSingleObj("jili成");
            riskctrlassets.setCalcDirect("jili成");
            riskctrlassets.setOwnUserID("jili成");
            riskctrlassets.setAlterUserID("jili成");
            riskctrlassets.setCreateTime(new Date());
            ls.add(riskctrlassets);
        }
        return ls;
    }

    public List<RiskIndex> genRiskIndexList(int n) {
        List<RiskIndex> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RiskIndex riskindex = new RiskIndex();
            riskindex.setIndexCode("jili成"+i);
            riskindex.setTimeStamp(new Date());
            riskindex.setIndexName("jili成");
            riskindex.setTypeA("jili成");
            riskindex.setTypeB("jili成");
            riskindex.setTypeC("jili成");
            riskindex.setReMark("jili成");
            riskindex.setUnit("jili成");
            riskindex.setIsHaveThreshold("jili成");
            riskindex.setThresholdNum((short) 23);
            riskindex.setThresholdSplitSig("jili成");
            riskindex.setAssetsScope("jili成");
            riskindex.setCanNumerator("jili成");
            riskindex.setCanDenominator("jili成");
            riskindex.setLevelType("jili成");
            riskindex.setIsRateOrAbs("jili成");
            riskindex.setIsForceDone("jili成");
            riskindex.setSceneType("jili成");
            riskindex.setCaseShow("jili成");
            riskindex.setOwnUserID("jili成");
            riskindex.setAlterUserID("jili成");
            riskindex.setCreateTime(new Date());
            ls.add(riskindex);
        }
        return ls;
    }

    public List<RiskSets> genRiskSetsList(int n) {
        List<RiskSets> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            RiskSets risksets = new RiskSets();
            risksets.setId(i);
            risksets.setTimeStamp(new Date());
            risksets.setRiskSetCode("jili成");
            risksets.setIndexCode("jili成"+i);
            risksets.setCtrlLevel("jili成");
            risksets.setCtrlObject("jili成");
            risksets.setCtrlAssetsCode("jili成");
            risksets.setOwnGroupID(i);
            risksets.setCtrlScene("jili成");
            risksets.setIsForceIntercept("jili成");
            risksets.setStartDate(new Date());
            risksets.setEndDate(new Date());
            risksets.setGateValueA((float) 15.23);
            risksets.setGateValueB((float) 15.23);
            risksets.setGateValueC((float) 15.23);
            risksets.setGateDirectionA("jili成");
            risksets.setGateDirectionB("jili成");
            risksets.setGateDirectionC("jili成");
            risksets.setGateDirectionN("jili成");
            risksets.setGateValueN("jili成");
            risksets.setOwnUserID("jili成");
            risksets.setAlterUserID("jili成");
            risksets.setCreateTime(new Date());
            ls.add(risksets);
        }
        return ls;
    }

    public List<SafeForbidLogin> genSafeForbidLoginList(int n) {
        List<SafeForbidLogin> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            SafeForbidLogin safeforbidlogin = new SafeForbidLogin();
            safeforbidlogin.setId(i);
            safeforbidlogin.setTimeStamp(new Date());
            safeforbidlogin.setMac("jili成");
            safeforbidlogin.setIp4(i);
            safeforbidlogin.setUserID("jili成"+i);
            safeforbidlogin.setReMark("jili成");
            safeforbidlogin.setOwnUserID("jili成");
            safeforbidlogin.setAlterUserID("jili成");
            safeforbidlogin.setCreateTime(new Date());
            ls.add(safeforbidlogin);
        }
        return ls;
    }

    public List<SafeOtherAccess> genSafeOtherAccessList(int n) {
        List<SafeOtherAccess> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            SafeOtherAccess safeotheraccess = new SafeOtherAccess();
            safeotheraccess.setUserID("jili成"+i);
            safeotheraccess.setTimeStamp(new Date());
            safeotheraccess.setMaxOnline(i);
            safeotheraccess.setLevelPrice("jili成");
            safeotheraccess.setDev("jili成");
            safeotheraccess.setReseach("jili成");
            safeotheraccess.setReBack("jili成");
            safeotheraccess.setTrade("jili成");
            safeotheraccess.setOwnUserID("jili成");
            safeotheraccess.setAlterUserID("jili成");
            safeotheraccess.setCreateTime(new Date());
            ls.add(safeotheraccess);
        }
        return ls;
    }

    public List<TraceAccountBail> genTraceAccountBailList(int n) {
        List<TraceAccountBail> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceAccountBail traceaccountbail = new TraceAccountBail();
            traceaccountbail.setSid(i);
            traceaccountbail.setTimeStamp(new Date());
            traceaccountbail.setTraceType("jili成");
            traceaccountbail.setId(i);
            traceaccountbail.setAccountCode("jili成");
            traceaccountbail.setTempID(i);
            traceaccountbail.setMarketCode("jili成");
            traceaccountbail.setClassCode("jil");
            traceaccountbail.setBs("jili成");
            traceaccountbail.setUnderlyingCode("jili成");
            traceaccountbail.setObj("jili成"+i);
            traceaccountbail.setBail0((float) 15.23);
            traceaccountbail.setBail1((float) 15.23);
            traceaccountbail.setBail2((float) 15.23);
            traceaccountbail.setBail0Rate((float) 15.23);
            traceaccountbail.setBail1Rate((float) 15.23);
            traceaccountbail.setBail2Rate((float) 15.23);
            traceaccountbail.setOwnUserID("jili成");
            traceaccountbail.setAlterUserID("jili成");
            traceaccountbail.setCreateTime(new Date());
            ls.add(traceaccountbail);
        }
        return ls;
    }

    public List<TraceAccountFare> genTraceAccountFareList(int n) {
        List<TraceAccountFare> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceAccountFare traceaccountfare = new TraceAccountFare();
            traceaccountfare.setSid(i);
            traceaccountfare.setTimeStamp(new Date());
            traceaccountfare.setTraceType("jili成");
            traceaccountfare.setId(i);
            traceaccountfare.setAccountCode("jili成");
            traceaccountfare.setTempID(i);
            traceaccountfare.setMarketCode("jili成");
            traceaccountfare.setClassCode("jil");
            traceaccountfare.setUnderlyingCode("jili成");
            traceaccountfare.setObj("jili成"+i);
            traceaccountfare.setBs("jili成");
            traceaccountfare.setOpenClose("jili成");
            traceaccountfare.setCommission0((float) 15.23);
            traceaccountfare.setCommission1((float) 15.23);
            traceaccountfare.setMinCommission0((float) 15.23);
            traceaccountfare.setMinCommission1((float) 15.23);
            traceaccountfare.setDeliverFare0((float) 15.23);
            traceaccountfare.setDeliverFare1((float) 15.23);
            traceaccountfare.setTax0((float) 15.23);
            traceaccountfare.setTax1((float) 15.23);
            traceaccountfare.setEtax1((float) 15.23);
            traceaccountfare.setEtax0((float) 15.23);
            traceaccountfare.setCost0((float) 15.23);
            traceaccountfare.setCost1((float) 15.23);
            traceaccountfare.setOwnUserID("jili成");
            traceaccountfare.setAlterUserID("jili成");
            traceaccountfare.setCreateTime(new Date());
            ls.add(traceaccountfare);
        }
        return ls;
    }

    public List<TraceAccountIcode> genTraceAccountIcodeList(int n) {
        List<TraceAccountIcode> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceAccountIcode traceaccounticode = new TraceAccountIcode();
            traceaccounticode.setSid(i);
            traceaccounticode.setTimeStamp(new Date());
            traceaccounticode.setTraceType("jili成");
            traceaccounticode.setId(i);
            traceaccounticode.setIid(i);
            traceaccounticode.setInvestID("jili成");
            traceaccounticode.setType("jili成");
            traceaccounticode.setCode("jili成");
            traceaccounticode.setAlterUserID("jili成");
            traceaccounticode.setCreateTime(new Date());
            ls.add(traceaccounticode);
        }
        return ls;
    }

    public List<TraceAccountInfo> genTraceAccountInfoList(int n) {
        List<TraceAccountInfo> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceAccountInfo traceaccountinfo = new TraceAccountInfo();
            traceaccountinfo.setSid(i);
            traceaccountinfo.setTimeStamp(new Date());
            traceaccountinfo.setTraceType("jili成");
            traceaccountinfo.setId(i);
            traceaccountinfo.setAccountCode("jili成");
            traceaccountinfo.setAccountName("jili成");
            traceaccountinfo.setPipID(i);
            traceaccountinfo.setPwd("jili成");
            traceaccountinfo.setIsRisk("jili成");
            traceaccountinfo.setIid(i);
            traceaccountinfo.setType("jili成");
            traceaccountinfo.setInvestID("jili成");
            traceaccountinfo.setOwnUserID("jili成");
            traceaccountinfo.setAlterUserID("jili成");
            traceaccountinfo.setCreateTime(new Date());
            ls.add(traceaccountinfo);
        }
        return ls;
    }

    public List<TraceAccountInvest> genTraceAccountInvestList(int n) {
        List<TraceAccountInvest> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceAccountInvest traceaccountinvest = new TraceAccountInvest();
            traceaccountinvest.setSid(i);
            traceaccountinvest.setTimeStamp(new Date());
            traceaccountinvest.setTraceType("jili成");
            traceaccountinvest.setId(i);
            traceaccountinvest.setInvestID("jili成");
            traceaccountinvest.setRegionID("jili成");
            traceaccountinvest.setInvestName("jili成");
            traceaccountinvest.setBrokerID("jili成"+i);
            traceaccountinvest.setPwd("jili成");
            traceaccountinvest.setType("jili成");
            traceaccountinvest.setOwnUserID("jili成");
            traceaccountinvest.setAlterUserID("jili成");
            traceaccountinvest.setCreateTime(new Date());
            ls.add(traceaccountinvest);
        }
        return ls;
    }

    public List<TraceBailTemp> genTraceBailTempList(int n) {
        List<TraceBailTemp> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceBailTemp tracebailtemp = new TraceBailTemp();
            tracebailtemp.setSid(i);
            tracebailtemp.setTimeStamp(new Date());
            tracebailtemp.setTraceType("jili成");
            tracebailtemp.setTempID(i);
            tracebailtemp.setTempName("jili成");
            tracebailtemp.setByOwnGroupID(i);
            tracebailtemp.setReMark("jili成");
            tracebailtemp.setOwnUserID("jili成");
            tracebailtemp.setAlterUserID("jili成");
            tracebailtemp.setCreateTime(new Date());
            ls.add(tracebailtemp);
        }
        return ls;
    }

    public List<TraceBailTempObj> genTraceBailTempObjList(int n) {
        List<TraceBailTempObj> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceBailTempObj tracebailtempobj = new TraceBailTempObj();
            tracebailtempobj.setSid(i);
            tracebailtempobj.setTimeStamp(new Date());
            tracebailtempobj.setTraceType("jili成");
            tracebailtempobj.setId(i);
            tracebailtempobj.setTempID(i);
            tracebailtempobj.setMarketCode("jili成");
            tracebailtempobj.setClassCode("jil");
            tracebailtempobj.setBs("jili成");
            tracebailtempobj.setUnderlyingCode("jili成");
            tracebailtempobj.setObj("jili成"+i);
            tracebailtempobj.setBail0((float) 15.23);
            tracebailtempobj.setBail1((float) 15.23);
            tracebailtempobj.setBail2((float) 15.23);
            tracebailtempobj.setBail0Rate((float) 15.23);
            tracebailtempobj.setBail1Rate((float) 15.23);
            tracebailtempobj.setBail2Rate((float) 15.23);
            tracebailtempobj.setOwnUserID("jili成");
            tracebailtempobj.setAlterUserID("jili成");
            tracebailtempobj.setCreateTime(new Date());
            ls.add(tracebailtempobj);
        }
        return ls;
    }

    public List<TraceFareTemp> genTraceFareTempList(int n) {
        List<TraceFareTemp> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceFareTemp tracefaretemp = new TraceFareTemp();
            tracefaretemp.setSid(i);
            tracefaretemp.setTimeStamp(new Date());
            tracefaretemp.setTraceType("jili成");
            tracefaretemp.setTempID(i);
            tracefaretemp.setTempName("jili成");
            tracefaretemp.setByOwnGroupID(i);
            tracefaretemp.setReMark("jili成");
            tracefaretemp.setOwnUserID("jili成");
            tracefaretemp.setAlterUserID("jili成");
            tracefaretemp.setCreateTime(new Date());
            ls.add(tracefaretemp);
        }
        return ls;
    }

    public List<TraceFareTempeObj> genTraceFareTempeObjList(int n) {
        List<TraceFareTempeObj> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceFareTempeObj tracefaretempeobj = new TraceFareTempeObj();
            tracefaretempeobj.setSid(i);
            tracefaretempeobj.setTimeStamp(new Date());
            tracefaretempeobj.setTraceType("jili成");
            tracefaretempeobj.setId(i);
            tracefaretempeobj.setTempID(i);
            tracefaretempeobj.setMarketCode("jili成");
            tracefaretempeobj.setClassCode("jil");
            tracefaretempeobj.setUnderlyingCode("jili成");
            tracefaretempeobj.setObj("jili成"+i);
            tracefaretempeobj.setBs("jili成");
            tracefaretempeobj.setOpenClose("jili成");
            tracefaretempeobj.setCommission0((float) 15.23);
            tracefaretempeobj.setCommission1((float) 15.23);
            tracefaretempeobj.setMinCommission0((float) 15.23);
            tracefaretempeobj.setMinCommission1((float) 15.23);
            tracefaretempeobj.setDeliverFare0((float) 15.23);
            tracefaretempeobj.setDeliverFare1((float) 15.23);
            tracefaretempeobj.setTax0((float) 15.23);
            tracefaretempeobj.setTax1((float) 15.23);
            tracefaretempeobj.setEtax1((float) 15.23);
            tracefaretempeobj.setEtax0((float) 15.23);
            tracefaretempeobj.setCost0((float) 15.23);
            tracefaretempeobj.setCost1((float) 15.23);
            tracefaretempeobj.setOwnUserID("jili成");
            tracefaretempeobj.setAlterUserID("jili成");
            tracefaretempeobj.setCreateTime(new Date());
            ls.add(tracefaretempeobj);
        }
        return ls;
    }

    public List<TraceObjInfo> genTraceObjInfoList(int n) {
        List<TraceObjInfo> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceObjInfo traceobjinfo = new TraceObjInfo();
            traceobjinfo.setSid(i);
            traceobjinfo.setTimeStamp(new Date());
            traceobjinfo.setTraceType("jili成");
            traceobjinfo.setObjID("jili成");
            traceobjinfo.setRegionID("jili成");
            traceobjinfo.setObj("jili成"+i);
            traceobjinfo.setMarketCode("jili成");
            traceobjinfo.setClassCode("jil");
            traceobjinfo.setName("jili成");
            traceobjinfo.setEName("jili成");
            traceobjinfo.setLongName("jili成");
            traceobjinfo.setListingDate(new Date());
            traceobjinfo.setFaceValue(i);
            traceobjinfo.setTradeUnit(i);
            traceobjinfo.setCurrency("jili成");
            traceobjinfo.setContractSize(i);
            traceobjinfo.setTradeType("jili成");
            traceobjinfo.setClearType("jili成");
            traceobjinfo.setContractMonth("jili成");
            traceobjinfo.setExpirationDate(new Date());
            traceobjinfo.setUnderlyingCode("jili成");
            traceobjinfo.setUnderlyingobj("jili成");
            traceobjinfo.setFirst("jili成");
            traceobjinfo.setSecend("jili成");
            traceobjinfo.setThird("jili成");
            traceobjinfo.setPutCall("jili成");
            traceobjinfo.setStrikePrice((float) 15.23);
            traceobjinfo.setOptionRate((float) 15.23);
            traceobjinfo.setOwnUserID("jili成");
            traceobjinfo.setAlterUserID("jili成");
            traceobjinfo.setCreateTime(new Date());
            ls.add(traceobjinfo);
        }
        return ls;
    }

    public List<TraceObjProtfolio> genTraceObjProtfolioList(int n) {
        List<TraceObjProtfolio> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceObjProtfolio traceobjprotfolio = new TraceObjProtfolio();
            traceobjprotfolio.setSid(i);
            traceobjprotfolio.setTimeStamp(new Date());
            traceobjprotfolio.setTraceType("jili成");
            traceobjprotfolio.setProtfolioID(i);
            traceobjprotfolio.setName("jili成");
            traceobjprotfolio.setIsPublic("jili成");
            traceobjprotfolio.setOwnUserID("jili成");
            traceobjprotfolio.setAlterUserID("jili成");
            traceobjprotfolio.setCreateTime(new Date());
            ls.add(traceobjprotfolio);
        }
        return ls;
    }

    public List<TraceObjProtfolioList> genTraceObjProtfolioListList(int n) {
        List<TraceObjProtfolioList> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceObjProtfolioList traceobjprotfoliolist = new TraceObjProtfolioList();
            traceobjprotfoliolist.setSid(i);
            traceobjprotfoliolist.setTimeStamp(new Date());
            traceobjprotfoliolist.setTraceType("jili成");
            traceobjprotfoliolist.setId(i);
            traceobjprotfoliolist.setProtfolioID(i);
            traceobjprotfoliolist.setObj("jili成"+i);
            traceobjprotfoliolist.setMarketCode("jili成");
            traceobjprotfoliolist.setClassCode("jil");
            traceobjprotfoliolist.setRegionID("jili成");
            traceobjprotfoliolist.setQty(i);
            traceobjprotfoliolist.setRate((float) 15.23);
            traceobjprotfoliolist.setReMark("jili成");
            traceobjprotfoliolist.setOwnUserID("jili成");
            traceobjprotfoliolist.setAlterUserID("jili成");
            traceobjprotfoliolist.setCreateTime(new Date());
            ls.add(traceobjprotfoliolist);
        }
        return ls;
    }

    public List<TraceObjUnderlying> genTraceObjUnderlyingList(int n) {
        List<TraceObjUnderlying> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceObjUnderlying traceobjunderlying = new TraceObjUnderlying();
            traceobjunderlying.setSid(i);
            traceobjunderlying.setTimeStamp(new Date());
            traceobjunderlying.setTraceType("jili成");
            traceobjunderlying.setId(i);
            traceobjunderlying.setUnderlyingCode("jili成");
            traceobjunderlying.setRegionID("jili成");
            traceobjunderlying.setMarketCode("jili成");
            traceobjunderlying.setClassCode("jil");
            traceobjunderlying.setUnderlyingName("jili成");
            traceobjunderlying.setCurrency("jili成");
            traceobjunderlying.setTick((float) 15.23);
            traceobjunderlying.setTradeUnit(i);
            traceobjunderlying.setContractSize(i);
            traceobjunderlying.setOwnUserID("jili成");
            traceobjunderlying.setAlterUserID("jili成");
            traceobjunderlying.setCreateTime(new Date());
            ls.add(traceobjunderlying);
        }
        return ls;
    }

    public List<TraceProductAccount> genTraceProductAccountList(int n) {
        List<TraceProductAccount> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceProductAccount traceproductaccount = new TraceProductAccount();
            traceproductaccount.setSid(i);
            traceproductaccount.setTimeStamp(new Date());
            traceproductaccount.setTraceType("jili成");
            traceproductaccount.setId(i);
            traceproductaccount.setProductID(i);
            traceproductaccount.setUnitID(i);
            traceproductaccount.setAccountCode("jili成");
            traceproductaccount.setNet((float) 15.23);
            traceproductaccount.setShare((long)i);
            traceproductaccount.setStartDate(new Date());
            traceproductaccount.setEndDate(new Date());
            traceproductaccount.setBelongGroupID(i);
            traceproductaccount.setOwnUserID("jili成");
            traceproductaccount.setAlterUserID("jili成");
            traceproductaccount.setCreateTime(new Date());
            ls.add(traceproductaccount);
        }
        return ls;
    }

    public List<TraceProductInfo> genTraceProductInfoList(int n) {
        List<TraceProductInfo> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceProductInfo traceproductinfo = new TraceProductInfo();
            traceproductinfo.setSid(i);
            traceproductinfo.setTimeStamp(new Date());
            traceproductinfo.setTraceType("jili成");
            traceproductinfo.setProductID(i);
            traceproductinfo.setProductName("jili成");
            traceproductinfo.setNet((float) 15.23);
            traceproductinfo.setShare((long)i);
            traceproductinfo.setStartDate(new Date());
            traceproductinfo.setEndDate(new Date());
            traceproductinfo.setBelongGroupID(i);
            traceproductinfo.setType("jili成");
            traceproductinfo.setReMark("jili成");
            traceproductinfo.setOwnUserID("jili成");
            traceproductinfo.setAlterUserID("jili成");
            traceproductinfo.setCreateTime(new Date());
            ls.add(traceproductinfo);
        }
        return ls;
    }

    public List<TraceProductUnit> genTraceProductUnitList(int n) {
        List<TraceProductUnit> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceProductUnit traceproductunit = new TraceProductUnit();
            traceproductunit.setSid(i);
            traceproductunit.setTimeStamp(new Date());
            traceproductunit.setTraceType("jili成");
            traceproductunit.setUnitID(i);
            traceproductunit.setProductID(i);
            traceproductunit.setUnitName("jili成");
            traceproductunit.setNet((float) 15.23);
            traceproductunit.setShare((long)i);
            traceproductunit.setStartDate(new Date());
            traceproductunit.setEndDate(new Date());
            traceproductunit.setBelongGroupID(i);
            traceproductunit.setOwnUserID("jili成");
            traceproductunit.setAlterUserID("jili成");
            traceproductunit.setCreateTime(new Date());
            ls.add(traceproductunit);
        }
        return ls;
    }

    public List<TraceRiskCtrlAssets> genTraceRiskCtrlAssetsList(int n) {
        List<TraceRiskCtrlAssets> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceRiskCtrlAssets traceriskctrlassets = new TraceRiskCtrlAssets();
            traceriskctrlassets.setSid(i);
            traceriskctrlassets.setTimeStamp(new Date());
            traceriskctrlassets.setTraceType("jili成");
            traceriskctrlassets.setId(i);
            traceriskctrlassets.setCtrlAssetsCode("jili成");
            traceriskctrlassets.setObj("jili成"+i);
            traceriskctrlassets.setIsSingleObj("jili成");
            traceriskctrlassets.setCalcDirect("jili成");
            traceriskctrlassets.setOwnUserID("jili成");
            traceriskctrlassets.setAlterUserID("jili成");
            traceriskctrlassets.setCreateTime(new Date());
            ls.add(traceriskctrlassets);
        }
        return ls;
    }

    public List<TraceRiskSets> genTraceRiskSetsList(int n) {
        List<TraceRiskSets> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceRiskSets tracerisksets = new TraceRiskSets();
            tracerisksets.setSid(i);
            tracerisksets.setTimeStamp(new Date());
            tracerisksets.setTraceType("jili成");
            tracerisksets.setId(i);
            tracerisksets.setRiskSetCode("jili成");
            tracerisksets.setIndexCode("jili成"+i);
            tracerisksets.setCtrlLevel("jili成");
            tracerisksets.setCtrlObject("jili成");
            tracerisksets.setCtrlAssetsCode("jili成");
            tracerisksets.setOwnGroupID(i);
            tracerisksets.setCtrlScene("jili成");
            tracerisksets.setIsForceIntercept("jili成");
            tracerisksets.setStartDate(new Date());
            tracerisksets.setEndDate(new Date());
            tracerisksets.setGateValueA((float) 15.23);
            tracerisksets.setGateValueB((float) 15.23);
            tracerisksets.setGateValueC((float) 15.23);
            tracerisksets.setGateDirectionA("jili成");
            tracerisksets.setGateDirectionB("jili成");
            tracerisksets.setGateDirectionC("jili成");
            tracerisksets.setGateDirectionN("jili成");
            tracerisksets.setGateValueN("jili成");
            tracerisksets.setOwnUserID("jili成");
            tracerisksets.setAlterUserID("jili成");
            tracerisksets.setCreateTime(new Date());
            ls.add(tracerisksets);
        }
        return ls;
    }

    public List<TraceSafeForbidLogin> genTraceSafeForbidLoginList(int n) {
        List<TraceSafeForbidLogin> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceSafeForbidLogin tracesafeforbidlogin = new TraceSafeForbidLogin();
            tracesafeforbidlogin.setSid(i);
            tracesafeforbidlogin.setTimeStamp(new Date());
            tracesafeforbidlogin.setTraceType("jili成");
            tracesafeforbidlogin.setId(i);
            tracesafeforbidlogin.setMac("jili成");
            tracesafeforbidlogin.setIp4(i);
            tracesafeforbidlogin.setUserID("jili成"+i);
            tracesafeforbidlogin.setReMark("jili成");
            tracesafeforbidlogin.setOwnUserID("jili成");
            tracesafeforbidlogin.setAlterUserID("jili成");
            tracesafeforbidlogin.setCreateTime(new Date());
            ls.add(tracesafeforbidlogin);
        }
        return ls;
    }

    public List<TraceSafeOtherAccess> genTraceSafeOtherAccessList(int n) {
        List<TraceSafeOtherAccess> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceSafeOtherAccess tracesafeotheraccess = new TraceSafeOtherAccess();
            tracesafeotheraccess.setSid(i);
            tracesafeotheraccess.setTimeStamp(new Date());
            tracesafeotheraccess.setTraceType("jili成");
            tracesafeotheraccess.setUserID("jili成"+i);
            tracesafeotheraccess.setMaxOnline(i);
            tracesafeotheraccess.setLevelPrice("jili成");
            tracesafeotheraccess.setDev("jili成");
            tracesafeotheraccess.setReseach("jili成");
            tracesafeotheraccess.setReBack("jili成");
            tracesafeotheraccess.setTrade("jili成");
            tracesafeotheraccess.setOwnUserID("jili成");
            tracesafeotheraccess.setAlterUserID("jili成");
            tracesafeotheraccess.setCreateTime(new Date());
            ls.add(tracesafeotheraccess);
        }
        return ls;
    }

    public List<TraceTradesetExchangeLimit> genTraceTradesetExchangeLimitList(int n) {
        List<TraceTradesetExchangeLimit> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceTradesetExchangeLimit tracetradesetexchangelimit = new TraceTradesetExchangeLimit();
            tracetradesetexchangelimit.setSid(i);
            tracetradesetexchangelimit.setTimeStamp(new Date());
            tracetradesetexchangelimit.setTraceType("jili成");
            tracetradesetexchangelimit.setId(i);
            tracetradesetexchangelimit.setMarketCode("jili成");
            tracetradesetexchangelimit.setUnderlyingObj("jili成");
            tracetradesetexchangelimit.setCancelLimit(i);
            tracetradesetexchangelimit.setOrderLimit(i);
            tracetradesetexchangelimit.setOnceMaxQty(i);
            tracetradesetexchangelimit.setReMark("jili成");
            tracetradesetexchangelimit.setOwnUserID("jili成");
            tracetradesetexchangelimit.setAlterUserID("jili成");
            tracetradesetexchangelimit.setCreateTime(new Date());
            ls.add(tracetradesetexchangelimit);
        }
        return ls;
    }

    public List<TraceUserAccess> genTraceUserAccessList(int n) {
        List<TraceUserAccess> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceUserAccess traceuseraccess = new TraceUserAccess();
            traceuseraccess.setSid(i);
            traceuseraccess.setTimeStamp(new Date());
            traceuseraccess.setTraceType("jili成");
            traceuseraccess.setId(i);
            traceuseraccess.setUserID("jili成"+i);
            traceuseraccess.setAccessUserID("jili成");
            traceuseraccess.setAccessID("jili成");
            traceuseraccess.setReMark("jili成");
            traceuseraccess.setAlterUserID("jili成");
            traceuseraccess.setCreateTime(new Date());
            ls.add(traceuseraccess);
        }
        return ls;
    }

    public List<TraceUserAccount> genTraceUserAccountList(int n) {
        List<TraceUserAccount> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceUserAccount traceuseraccount = new TraceUserAccount();
            traceuseraccount.setSid(i);
            traceuseraccount.setTimeStamp(new Date());
            traceuseraccount.setTraceType("jili成");
            traceuseraccount.setId(i);
            traceuseraccount.setUserID("jili成"+i);
            traceuseraccount.setAccountCode("jili成");
            traceuseraccount.setAccessID("jili成");
            traceuseraccount.setReMark("jili成");
            traceuseraccount.setAlterUserID("jili成");
            traceuseraccount.setCreateTime(new Date());
            ls.add(traceuseraccount);
        }
        return ls;
    }

    public List<TraceUserInfo> genTraceUserInfoList(int n) {
        List<TraceUserInfo> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceUserInfo traceuserinfo = new TraceUserInfo();
            traceuserinfo.setSid(i);
            traceuserinfo.setTimeStamp(new Date());
            traceuserinfo.setTraceType("jili成");
            traceuserinfo.setId(i);
            traceuserinfo.setUserID("jili成"+i);
            traceuserinfo.setUserName("jili成");
            traceuserinfo.setPwd("jili成");
            traceuserinfo.setStat("jili成");
            traceuserinfo.setType("jili成");
            traceuserinfo.setLoginStat("jili成");
            traceuserinfo.setCorrectNum(i);
            traceuserinfo.setIsUSBKey("jili成");
            traceuserinfo.setRoleID("jili成");
            traceuserinfo.setAlterUserID("jili成");
            traceuserinfo.setCreateTime(new Date());
            ls.add(traceuserinfo);
        }
        return ls;
    }

    public List<TraceUserPrivateInfo> genTraceUserPrivateInfoList(int n) {
        List<TraceUserPrivateInfo> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceUserPrivateInfo traceuserprivateinfo = new TraceUserPrivateInfo();
            traceuserprivateinfo.setSid(i);
            traceuserprivateinfo.setTimeStamp(new Date());
            traceuserprivateinfo.setTraceType("jili成");
            traceuserprivateinfo.setId(i);
            traceuserprivateinfo.setUserID("jili成"+i);
            traceuserprivateinfo.setUserName("jili成");
            traceuserprivateinfo.setSfID("jili成");
            traceuserprivateinfo.setAlterUserID("jili成");
            traceuserprivateinfo.setCreateTime(new Date());
            ls.add(traceuserprivateinfo);
        }
        return ls;
    }

    public List<TraceWorkFlowPath> genTraceWorkFlowPathList(int n) {
        List<TraceWorkFlowPath> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceWorkFlowPath traceworkflowpath = new TraceWorkFlowPath();
            traceworkflowpath.setSid(i);
            traceworkflowpath.setTimeStamp(new Date());
            traceworkflowpath.setTraceType("jili成");
            traceworkflowpath.setId(i);
            traceworkflowpath.setGroupID(i);
            traceworkflowpath.setAnchorCode("jili成");
            traceworkflowpath.setFlowSeqNo(i);
            traceworkflowpath.setUserID("jili成"+i);
            traceworkflowpath.setRoleID("jili成");
            traceworkflowpath.setReMark("jili成");
            traceworkflowpath.setOwnUserID("jili成");
            traceworkflowpath.setAlterUserID("jili成");
            traceworkflowpath.setCreateTime(new Date());
            ls.add(traceworkflowpath);
        }
        return ls;
    }

    public List<TraceWorkGroup> genTraceWorkGroupList(int n) {
        List<TraceWorkGroup> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceWorkGroup traceworkgroup = new TraceWorkGroup();
            traceworkgroup.setSid(i);
            traceworkgroup.setTimeStamp(new Date());
            traceworkgroup.setTraceType("jili成");
            traceworkgroup.setGroupID(i);
            traceworkgroup.setGroupName("jili成");
            traceworkgroup.setManagerUserID("jili成");
            traceworkgroup.setType("jili成");
            traceworkgroup.setUpGroupID(i);
            traceworkgroup.setFlag("jili成");
            traceworkgroup.setRole("jili成");
            traceworkgroup.setOwnUserID("jili成");
            traceworkgroup.setAlterUserID("jili成");
            traceworkgroup.setCreateTime(new Date());
            ls.add(traceworkgroup);
        }
        return ls;
    }

    public List<TraceWorkGroupUser> genTraceWorkGroupUserList(int n) {
        List<TraceWorkGroupUser> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceWorkGroupUser traceworkgroupuser = new TraceWorkGroupUser();
            traceworkgroupuser.setSid(i);
            traceworkgroupuser.setTimeStamp(new Date());
            traceworkgroupuser.setTraceType("jili成");
            traceworkgroupuser.setId(i);
            traceworkgroupuser.setGroupID(i);
            traceworkgroupuser.setUserID("jili成"+i);
            traceworkgroupuser.setRoleID("jili成");
            traceworkgroupuser.setReMark("jili成");
            traceworkgroupuser.setOwnUserID("jili成");
            traceworkgroupuser.setAlterUserID("jili成");
            traceworkgroupuser.setCreateTime(new Date());
            ls.add(traceworkgroupuser);
        }
        return ls;
    }

    public List<TraceWorkProductFlow> genTraceWorkProductFlowList(int n) {
        List<TraceWorkProductFlow> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TraceWorkProductFlow traceworkproductflow = new TraceWorkProductFlow();
            traceworkproductflow.setSid(i);
            traceworkproductflow.setTimeStamp(new Date());
            traceworkproductflow.setTraceType("jili成");
            traceworkproductflow.setId(i);
            traceworkproductflow.setProductID(i);
            traceworkproductflow.setAnchorCode("jili成");
            traceworkproductflow.setFlowSeqNo(i);
            traceworkproductflow.setUserID("jili成"+i);
            traceworkproductflow.setRoleID("jili成");
            traceworkproductflow.setBlongGroupID(i);
            traceworkproductflow.setReMark("jili成");
            traceworkproductflow.setOwnUserID("jili成");
            traceworkproductflow.setAlterUserID("jili成");
            traceworkproductflow.setCreateTime(new Date());
            ls.add(traceworkproductflow);
        }
        return ls;
    }

    public List<AlterOrder> genAlterOrderList(int n) {
        List<AlterOrder> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            AlterOrder alterorder = new AlterOrder();
            alterorder.setOrderID(i);
            alterorder.setTimeStamp(new Date());
            alterorder.setDealerUserID("jili成");
            alterorder.setAccountCode("jili成");
            alterorder.setTime(new Date());
            alterorder.setTradeDate(new Date());
            alterorder.setDateTime(new Date());
            alterorder.setMac("jili成");
            alterorder.setIp4(i);
            alterorder.setOrderTypeEmit("jili成");
            alterorder.setTradeOperate(i);
            alterorder.setEntrustNo(i);
            alterorder.setEntrustkey("jili成");
            alterorder.setAlterQty(i);
            alterorder.setAlterPrice((float) 15.23);
            alterorder.setWorkStationID("jili成");
            alterorder.setOwnUserID("jili成");
            alterorder.setAlterUserID("jili成");
            alterorder.setCreateTime(new Date());
            ls.add(alterorder);
        }
        return ls;
    }

    public List<CancelOrder> genCancelOrderList(int n) {
        List<CancelOrder> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            CancelOrder cancelorder = new CancelOrder();
            cancelorder.setOrderID(i);
            cancelorder.setTimeStamp(new Date());
            cancelorder.setDealerUserID("jili成");
            cancelorder.setAccountCode("jili成");
            cancelorder.setTime(new Date());
            cancelorder.setTradeDate(new Date());
            cancelorder.setDateTime(new Date());
            cancelorder.setMac("jili成");
            cancelorder.setIp4(i);
            cancelorder.setOrderTypeEmit("jili成");
            cancelorder.setTradeOperate(i);
            cancelorder.setEntrustNo(i);
            cancelorder.setEntrustkey("jili成");
            cancelorder.setCancelQty(i);
            cancelorder.setWorkStationID("jili成");
            cancelorder.setOwnUserID("jili成");
            cancelorder.setAlterUserID("jili成");
            cancelorder.setCreateTime(new Date());
            ls.add(cancelorder);
        }
        return ls;
    }

    public List<OrderExecuteProgress> genOrderExecuteProgressList(int n) {
        List<OrderExecuteProgress> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            OrderExecuteProgress orderexecuteprogress = new OrderExecuteProgress();
            orderexecuteprogress.setOrderID(i);
            orderexecuteprogress.setTimeStamp(new Date());
            orderexecuteprogress.setAccountCode("jili成");
            orderexecuteprogress.setDealerUserID("jili成");
            orderexecuteprogress.setSubmitTime(new Date());
            orderexecuteprogress.setTradeDate(new Date());
            orderexecuteprogress.setDateTime(new Date());
            orderexecuteprogress.setMac("jili成");
            orderexecuteprogress.setIp4(i);
            orderexecuteprogress.setOrderTypeEmit("jili成");
            orderexecuteprogress.setTradeOperate(i);
            orderexecuteprogress.setMarketCode("jili成");
            orderexecuteprogress.setClassCode("jil");
            orderexecuteprogress.setObj("jili成"+i);
            orderexecuteprogress.setBs("jili成");
            orderexecuteprogress.setOpenClose("jili成");
            orderexecuteprogress.setHedgeFlag("jili成");
            orderexecuteprogress.setPosID("jili成");
            orderexecuteprogress.setCustomCode("jili成");
            orderexecuteprogress.setCloseType("jili成");
            orderexecuteprogress.setBasketID(i);
            orderexecuteprogress.setBasketTatol(i);
            orderexecuteprogress.setSeqNum(i);
            orderexecuteprogress.setExeTime(new Date());
            orderexecuteprogress.setExeSequenceNo(i);
            orderexecuteprogress.setOutsideExeNo("jili成");
            orderexecuteprogress.setExeQty(i);
            orderexecuteprogress.setExePrice((float) 15.23);
            orderexecuteprogress.setCurrency("jili成");
            orderexecuteprogress.setExeAmount((float) 15.23);
            orderexecuteprogress.setCancelQty(i);
            orderexecuteprogress.setOrderQty(i);
            orderexecuteprogress.setWorkingQty(i);
            orderexecuteprogress.setCompleteQty(i);
            orderexecuteprogress.setOrderStatus("jili成");
            orderexecuteprogress.setTotalFare((float) 15.23);
            orderexecuteprogress.setCommissionFare((float) 15.23);
            orderexecuteprogress.setDeliverFare((float) 15.23);
            orderexecuteprogress.setTax((float) 15.23);
            orderexecuteprogress.setOtherCost((float) 15.23);
            orderexecuteprogress.setEsCheckinAmount(45.123);
            orderexecuteprogress.setReMark("jili成");
            orderexecuteprogress.setInvestID("jili成");
            orderexecuteprogress.setOwnUserID("jili成");
            orderexecuteprogress.setAlterUserID("jili成");
            orderexecuteprogress.setCreateTime(new Date());
            ls.add(orderexecuteprogress);
        }
        return ls;
    }

    public List<FundTransfer> genFundTransferList(int n) {
        List<FundTransfer> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            FundTransfer fundtransfer = new FundTransfer();
            fundtransfer.setId(i);
            fundtransfer.setTimeStamp(new Date());
            fundtransfer.setRegionID("jili成");
            fundtransfer.setAccountCode("jili成");
            fundtransfer.setDirect("jili成");
            fundtransfer.setAmount(45.123);
            fundtransfer.setDate(new Date());
            fundtransfer.setTime(new Date());
            fundtransfer.setDateTime(new Date());
            fundtransfer.setCurrency("jili成");
            fundtransfer.setChecker("jili成");
            fundtransfer.setReMark("jili成");
            fundtransfer.setIsEffectived("jili成");
            fundtransfer.setReplyMsg("jili成");
            fundtransfer.setOwnUserID("jili成");
            fundtransfer.setAlterUserID("jili成");
            fundtransfer.setCreateTime(new Date());
            ls.add(fundtransfer);
        }
        return ls;
    }

    public List<NewOrder> genNewOrderList(int n) {
        List<NewOrder> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            NewOrder neworder = new NewOrder();
            neworder.setOrderID(i);
            neworder.setTimeStamp(new Date());
            neworder.setAccountCode("jili成");
            neworder.setDealerUserID("jili成");
            neworder.setSubmitTime(new Date());
            neworder.setTradeDate(new Date());
            neworder.setDateTime(new Date());
            neworder.setMac("jili成");
            neworder.setIp4(i);
            neworder.setOrderTypeEmit("jili成");
            neworder.setTradeOperate(i);
            neworder.setObj("jili成"+i);
            neworder.setMarketCode("jili成");
            neworder.setClassCode("jil");
            neworder.setBs("jili成");
            neworder.setOpenClose("jili成");
            neworder.setPriceType("jili成");
            neworder.setOrderPrice((float) 15.23);
            neworder.setOrderQty(i);
            neworder.setHedgeFlag("jili成");
            neworder.setPosID("jili成");
            neworder.setCustomCode("jili成");
            neworder.setCloseType("jili成");
            neworder.setBasketID(i);
            neworder.setBasketTatol(i);
            neworder.setSeqNum(i);
            neworder.setWorkStationID("jili成");
            neworder.setOwnUserID("jili成");
            neworder.setAlterUserID("jili成");
            neworder.setCreateTime(new Date());
            ls.add(neworder);
        }
        return ls;
    }

    public List<TradeReportDetail> genTradeReportDetailList(int n) {
        List<TradeReportDetail> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TradeReportDetail tradereportdetail = new TradeReportDetail();
            tradereportdetail.setId(i);
            tradereportdetail.setTimeStamp(new Date());
            tradereportdetail.setMsgType("jili成");
            tradereportdetail.setOrderID(i);
            tradereportdetail.setReportNo(i);
            tradereportdetail.setReportContent("jili成");
            tradereportdetail.setEntrustNo(i);
            tradereportdetail.setEntrustkey("jili成");
            tradereportdetail.setWorkingQty(i);
            tradereportdetail.setUnWorkingQty(i);
            tradereportdetail.setCancelQty(i);
            tradereportdetail.setExeQtyTatol(i);
            tradereportdetail.setExePriceAvg((float) 15.23);
            tradereportdetail.setExeAmountTatol(45.123);
            tradereportdetail.setPrice((float) 15.23);
            tradereportdetail.setExexSeq(i);
            tradereportdetail.setExeSequenceNo(i);
            tradereportdetail.setOutsideExeNo("jili成");
            tradereportdetail.setExeQty(i);
            tradereportdetail.setExePrice((float) 15.23);
            tradereportdetail.setCurrency("jili成");
            tradereportdetail.setExeAmount((float) 15.23);
            tradereportdetail.setAccountCode("jili成");
            tradereportdetail.setMarketCode("jili成");
            tradereportdetail.setClassCode("jil");
            tradereportdetail.setObj("jili成"+i);
            tradereportdetail.setBs("jili成");
            tradereportdetail.setOpenClose("jili成");
            tradereportdetail.setHedgeFlag("jili成");
            tradereportdetail.setCloseType("jili成");
            tradereportdetail.setExeTime(new Date());
            tradereportdetail.setEntrustTime(new Date());
            tradereportdetail.setSubmitTime(new Date());
            tradereportdetail.setTradeDate(new Date());
            tradereportdetail.setInvestID("jili成");
            tradereportdetail.setReMark("jili成");
            tradereportdetail.setOwnUserID("jili成");
            tradereportdetail.setAlterUserID("jili成");
            tradereportdetail.setCreateTime(new Date());
            ls.add(tradereportdetail);
        }
        return ls;
    }

    public List<TradesetBroker> genTradesetBrokerList(int n) {
        List<TradesetBroker> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TradesetBroker tradesetbroker = new TradesetBroker();
            tradesetbroker.setBrokerID("jili成"+i);
            tradesetbroker.setTimeStamp(new Date());
            tradesetbroker.setRegionID("jili成");
            tradesetbroker.setType("jili成");
            tradesetbroker.setMarketCodeIss("jili成");
            tradesetbroker.setOwnUserID("jili成");
            tradesetbroker.setAlterUserID("jili成");
            tradesetbroker.setCreateTime(new Date());
            ls.add(tradesetbroker);
        }
        return ls;
    }

    public List<TradesetCalendar> genTradesetCalendarList(int n) {
        List<TradesetCalendar> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TradesetCalendar tradesetcalendar = new TradesetCalendar();
            tradesetcalendar.setId(i);
            tradesetcalendar.setTimeStamp(new Date());
            tradesetcalendar.setRegionID("jili成");
            tradesetcalendar.setDate(new Date());
            tradesetcalendar.setIsTrade("jili成");
            tradesetcalendar.setReMark("jili成");
            tradesetcalendar.setOwnUserID("jili成");
            tradesetcalendar.setAlterUserID("jili成");
            tradesetcalendar.setCreateTime(new Date());
            ls.add(tradesetcalendar);
        }
        return ls;
    }

    public List<TradesetExchangeLimit> genTradesetExchangeLimitList(int n) {
        List<TradesetExchangeLimit> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TradesetExchangeLimit tradesetexchangelimit = new TradesetExchangeLimit();
            tradesetexchangelimit.setId(i);
            tradesetexchangelimit.setTimeStamp(new Date());
            tradesetexchangelimit.setMarketCode("jili成");
            tradesetexchangelimit.setUnderlyingObj("jili成");
            tradesetexchangelimit.setCancelLimit(i);
            tradesetexchangelimit.setOrderLimit(i);
            tradesetexchangelimit.setOnceMaxQty(i);
            tradesetexchangelimit.setReMark("jili成");
            tradesetexchangelimit.setOwnUserID("jili成");
            tradesetexchangelimit.setAlterUserID("jili成");
            tradesetexchangelimit.setCreateTime(new Date());
            ls.add(tradesetexchangelimit);
        }
        return ls;
    }

    public List<TradesetPip> genTradesetPipList(int n) {
        List<TradesetPip> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TradesetPip tradesetpip = new TradesetPip();
            tradesetpip.setPipID(i);
            tradesetpip.setTimeStamp(new Date());
            tradesetpip.setPipName("jili成");
            tradesetpip.setType("jili成");
            tradesetpip.setRegionID("jili成");
            tradesetpip.setMarketCode("jili成");
            tradesetpip.setClassCode("jil");
            tradesetpip.setBrokerID("jili成"+i);
            tradesetpip.setLimitA(i);
            tradesetpip.setLimitB(i);
            tradesetpip.setLimitC((float) 15.23);
            tradesetpip.setOwnUserID("jili成");
            tradesetpip.setAlterUserID("jili成");
            tradesetpip.setCreateTime(new Date());
            ls.add(tradesetpip);
        }
        return ls;
    }
/*
    public List<TradeSimOrderquene> genTradeSimOrderqueneList(int n) {
        List<TradeSimOrderquene> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            TradeSimOrderquene tradesimorderquene = new TradeSimOrderquene();
            tradesimorderquene.setOrderID(i);
            tradesimorderquene.setOrdertype("jili成");
            tradesimorderquene.setAccountCode("jili成");
            tradesimorderquene.setDealerUserID("jili成");
            tradesimorderquene.setTime(new Date());
            tradesimorderquene.setTradeDate(new Date());
            tradesimorderquene.setDateTime(new Date());
            tradesimorderquene.setMac("jili成");
            tradesimorderquene.setIp4(i);
            tradesimorderquene.setObj("jili成"+i);
            tradesimorderquene.setMarketCode("jili成");
            tradesimorderquene.setClassCode("jil");
            tradesimorderquene.setBs("jili成");
            tradesimorderquene.setOpenClose("jili成");
            tradesimorderquene.setPriceType("jili成");
            tradesimorderquene.setOrderPrice((float) 15.23);
            tradesimorderquene.setOrderQty(i);
            tradesimorderquene.setHedgeFlag("jili成");
            tradesimorderquene.setPosID("jili成");
            tradesimorderquene.setExexSeq(i);
            tradesimorderquene.setWorkingQty(i);
            tradesimorderquene.setUnWorkingQty(i);
            tradesimorderquene.setCancelQty(i);
            tradesimorderquene.setExeQty(i);
            ls.add(tradesimorderquene);
        }
        return ls;
    }
*/
    public List<UserAccess> genUserAccessList(int n) {
        List<UserAccess> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            UserAccess useraccess = new UserAccess();
            useraccess.setId(i);
            useraccess.setTimeStamp(new Date());
            useraccess.setUserID("jili成"+i);
            useraccess.setAccessUserID("jili成");
            useraccess.setAccessID("jili成");
            useraccess.setReMark("jili成");
            useraccess.setAlterUserID("jili成");
            useraccess.setCreateTime(new Date());
            ls.add(useraccess);
        }
        return ls;
    }

    public List<UserAccount> genUserAccountList(int n) {
        List<UserAccount> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            UserAccount useraccount = new UserAccount();
            useraccount.setId(i);
            useraccount.setTimeStamp(new Date());
            useraccount.setUserID("jili成"+i);
            useraccount.setAccountCode("jili成");
            useraccount.setAccessID("jili成");
            useraccount.setReMark("jili成");
            useraccount.setAlterUserID("jili成");
            useraccount.setCreateTime(new Date());
            ls.add(useraccount);
        }
        return ls;
    }

    public List<UserInfo> genUserInfoList(int n) {
        List<UserInfo> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            UserInfo userinfo = new UserInfo();
            userinfo.setId(i);
            userinfo.setTimeStamp(new Date());
            userinfo.setUserID("jili成"+i);
            userinfo.setUserName("jili成");
            userinfo.setPwd("jili成");
            userinfo.setStat("jili成");
            userinfo.setType("jili成");
            userinfo.setLoginStat("jili成");
            userinfo.setCorrectNum(i);
            userinfo.setIsUSBKey("jili成");
            userinfo.setRoleID("jili成");
            userinfo.setAlterUserID("jili成");
            userinfo.setCreateTime(new Date());
            ls.add(userinfo);
        }
        return ls;
    }

    public List<UserPrivateInfo> genUserPrivateInfoList(int n) {
        List<UserPrivateInfo> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            UserPrivateInfo userprivateinfo = new UserPrivateInfo();
            userprivateinfo.setId(i);
            userprivateinfo.setTimeStamp(new Date());
            userprivateinfo.setUserID("jili成"+i);
            userprivateinfo.setUserName("jili成");
            userprivateinfo.setSfID("jili成");
            userprivateinfo.setAlterUserID("jili成");
            userprivateinfo.setCreateTime(new Date());
            ls.add(userprivateinfo);
        }
        return ls;
    }

    public List<WorkFlowPath> genWorkFlowPathList(int n) {
        List<WorkFlowPath> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            WorkFlowPath workflowpath = new WorkFlowPath();
            workflowpath.setId(i);
            workflowpath.setTimeStamp(new Date());
            workflowpath.setGroupID(i);
            workflowpath.setAnchorCode("jili成");
            workflowpath.setFlowSeqNo(i);
            workflowpath.setUserID("jili成"+i);
            workflowpath.setRoleID("jili成");
            workflowpath.setReMark("jili成");
            workflowpath.setOwnUserID("jili成");
            workflowpath.setAlterUserID("jili成");
            workflowpath.setCreateTime(new Date());
            ls.add(workflowpath);
        }
        return ls;
    }

    public List<WorkGroup> genWorkGroupList(int n) {
        List<WorkGroup> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            WorkGroup workgroup = new WorkGroup();
            workgroup.setGroupID(i);
            workgroup.setTimeStamp(new Date());
            workgroup.setGroupName("jili成");
            workgroup.setManagerUserID("jili成");
            workgroup.setType("jili成");
            workgroup.setUpGroupID(i);
            workgroup.setFlag("jili成");
            workgroup.setRole("jili成");
            workgroup.setOwnUserID("jili成");
            workgroup.setAlterUserID("jili成");
            workgroup.setCreateTime(new Date());
            ls.add(workgroup);
        }
        return ls;
    }

    public List<WorkGroupUser> genWorkGroupUserList(int n) {
        List<WorkGroupUser> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            WorkGroupUser workgroupuser = new WorkGroupUser();
            workgroupuser.setId(i);
            workgroupuser.setTimeStamp(new Date());
            workgroupuser.setGroupID(i);
            workgroupuser.setUserID("jili成"+i);
            workgroupuser.setRoleID("jili成");
            workgroupuser.setReMark("jili成");
            workgroupuser.setOwnUserID("jili成");
            workgroupuser.setAlterUserID("jili成");
            workgroupuser.setCreateTime(new Date());
            ls.add(workgroupuser);
        }
        return ls;
    }

    public List<WorkProductFlow> genWorkProductFlowList(int n) {
        List<WorkProductFlow> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            WorkProductFlow workproductflow = new WorkProductFlow();
            workproductflow.setId(i);
            workproductflow.setTimeStamp(new Date());
            workproductflow.setProductID(i);
            workproductflow.setAnchorCode("jili成");
            workproductflow.setFlowSeqNo(i);
            workproductflow.setUserID("jili成"+i);
            workproductflow.setRoleID("jili成");
            workproductflow.setBlongGroupID(i);
            workproductflow.setReMark("jili成");
            workproductflow.setOwnUserID("jili成");
            workproductflow.setAlterUserID("jili成");
            workproductflow.setCreateTime(new Date());
            ls.add(workproductflow);
        }
        return ls;
    }

    public List<AccountBail> genAccountBailList(int n) {
        List<AccountBail> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            AccountBail accountbail = new AccountBail();
            accountbail.setId(i);
            accountbail.setTimeStamp(new Date());
            accountbail.setAccountCode("jili成");
            accountbail.setTempID(i);
            accountbail.setMarketCode("jili成");
            accountbail.setClassCode("jil");
            accountbail.setBs("jili成");
            accountbail.setUnderlyingCode("jili成");
            accountbail.setObj("jili成"+i);
            accountbail.setBail0((float) 15.23);
            accountbail.setBail1((float) 15.23);
            accountbail.setBail2((float) 15.23);
            accountbail.setBail0Rate((float) 15.23);
            accountbail.setBail1Rate((float) 15.23);
            accountbail.setBail2Rate((float) 15.23);
            accountbail.setOwnUserID("jili成");
            accountbail.setAlterUserID("jili成");
            accountbail.setCreateTime(new Date());
            ls.add(accountbail);
        }
        return ls;
    }

    public List<AccountFare> genAccountFareList(int n) {
        List<AccountFare> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            AccountFare accountfare = new AccountFare();
            accountfare.setId(i);
            accountfare.setTimeStamp(new Date());
            accountfare.setAccountCode("jili成");
            accountfare.setTempID(i);
            accountfare.setMarketCode("jili成");
            accountfare.setClassCode("jil");
            accountfare.setUnderlyingCode("jili成");
            accountfare.setObj("jili成"+i);
            accountfare.setBs("jili成");
            accountfare.setOpenClose("jili成");
            accountfare.setCommission0((float) 15.23);
            accountfare.setCommission1((float) 15.23);
            accountfare.setMinCommission0((float) 15.23);
            accountfare.setMinCommission1((float) 15.23);
            accountfare.setDeliverFare0((float) 15.23);
            accountfare.setDeliverFare1((float) 15.23);
            accountfare.setTax0((float) 15.23);
            accountfare.setTax1((float) 15.23);
            accountfare.setEtax1((float) 15.23);
            accountfare.setEtax0((float) 15.23);
            accountfare.setCost0((float) 15.23);
            accountfare.setCost1((float) 15.23);
            accountfare.setOwnUserID("jili成");
            accountfare.setAlterUserID("jili成");
            accountfare.setCreateTime(new Date());
            ls.add(accountfare);
        }
        return ls;
    }

    public List<AccountIcode> genAccountIcodeList(int n) {
        List<AccountIcode> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            AccountIcode accounticode = new AccountIcode();
            accounticode.setId(i);
            accounticode.setTimeStamp(new Date());
            accounticode.setIid(i);
            accounticode.setInvestID("jili成");
            accounticode.setType("jili成");
            accounticode.setCode("jili成");
            accounticode.setAlterUserID("jili成");
            accounticode.setCreateTime(new Date());
            ls.add(accounticode);
        }
        return ls;
    }

    public List<AccountInfo> genAccountInfoList(int n) {
        List<AccountInfo> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            AccountInfo accountinfo = new AccountInfo();
            accountinfo.setId(i);
            accountinfo.setTimeStamp(new Date());
            accountinfo.setAccountCode("jili成");
            accountinfo.setAccountName("jili成");
            accountinfo.setPipID(i);
            accountinfo.setPwd("jili成");
            accountinfo.setIsRisk("jili成");
            accountinfo.setIid(i);
            accountinfo.setType("jili成");
            accountinfo.setInvestID("jili成");
            accountinfo.setOwnUserID("jili成");
            accountinfo.setAlterUserID("jili成");
            accountinfo.setCreateTime(new Date());
            ls.add(accountinfo);
        }
        return ls;
    }

    public List<AccountInvest> genAccountInvestList(int n) {
        List<AccountInvest> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            AccountInvest accountinvest = new AccountInvest();
            accountinvest.setId(i);
            accountinvest.setTimeStamp(new Date());
            accountinvest.setInvestID("jili成");
            accountinvest.setRegionID("jili成");
            accountinvest.setInvestName("jili成");
            accountinvest.setBrokerID("jili成"+i);
            accountinvest.setPwd("jili成");
            accountinvest.setType("jili成");
            accountinvest.setOwnUserID("jili成");
            accountinvest.setAlterUserID("jili成");
            accountinvest.setCreateTime(new Date());
            ls.add(accountinvest);
        }
        return ls;
    }

    public List<BailTemp> genBailTempList(int n) {
        List<BailTemp> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            BailTemp bailtemp = new BailTemp();
            bailtemp.setTempID(i);
            bailtemp.setTimeStamp(new Date());
            bailtemp.setTempName("jili成");
            bailtemp.setByOwnGroupID(i);
            bailtemp.setReMark("jili成");
            bailtemp.setOwnUserID("jili成");
            bailtemp.setAlterUserID("jili成");
            bailtemp.setCreateTime(new Date());
            ls.add(bailtemp);
        }
        return ls;
    }

    public List<BailTempObj> genBailTempObjList(int n) {
        List<BailTempObj> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            BailTempObj bailtempobj = new BailTempObj();
            bailtempobj.setId(i);
            bailtempobj.setTimeStamp(new Date());
            bailtempobj.setTempID(i);
            bailtempobj.setMarketCode("jili成");
            bailtempobj.setClassCode("jil");
            bailtempobj.setBs("jili成");
            bailtempobj.setUnderlyingCode("jili成");
            bailtempobj.setObj("jili成"+i);
            bailtempobj.setBail0((float) 15.23);
            bailtempobj.setBail1((float) 15.23);
            bailtempobj.setBail2((float) 15.23);
            bailtempobj.setBail0Rate((float) 15.23);
            bailtempobj.setBail1Rate((float) 15.23);
            bailtempobj.setBail2Rate((float) 15.23);
            bailtempobj.setOwnUserID("jili成");
            bailtempobj.setAlterUserID("jili成");
            bailtempobj.setCreateTime(new Date());
            ls.add(bailtempobj);
        }
        return ls;
    }

    public List<ClearingCurrencyRates> genClearingCurrencyRatesList(int n) {
        List<ClearingCurrencyRates> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ClearingCurrencyRates clearingcurrencyrates = new ClearingCurrencyRates();
            clearingcurrencyrates.setId(i);
            clearingcurrencyrates.setTimeStamp(new Date());
            clearingcurrencyrates.setClearingDate(new Date());
            clearingcurrencyrates.setAskCurrency("jili成");
            clearingcurrencyrates.setBidCurrency("jili成");
            clearingcurrencyrates.setRatePrice((float) 15.23);
            clearingcurrencyrates.setOwnUserID("jili成");
            clearingcurrencyrates.setAlterUserID("jili成");
            clearingcurrencyrates.setCreateTime(new Date());
            ls.add(clearingcurrencyrates);
        }
        return ls;
    }

    public List<ClearingDayBook> genClearingDayBookList(int n) {
        List<ClearingDayBook> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ClearingDayBook clearingdaybook = new ClearingDayBook();
            clearingdaybook.setDayid((long)i);
            clearingdaybook.setTimeStamp(new Date());
            clearingdaybook.setBookDate(new Date());
            clearingdaybook.setAccountCode("jili成");
            clearingdaybook.setBookID(i);
            clearingdaybook.setExeTime(new Date());
            clearingdaybook.setTradeDate(new Date());
            clearingdaybook.setAccountTitle("jili成");
            clearingdaybook.setAmountReceived(45.123);
            clearingdaybook.setAmountPayed(45.123);
            clearingdaybook.setBalance(45.123);
            clearingdaybook.setMarketCode("jili成");
            clearingdaybook.setClassCode("jil");
            clearingdaybook.setObj("jili成"+i);
            clearingdaybook.setMac("jili成");
            clearingdaybook.setIp4(i);
            clearingdaybook.setOrderTypeEmit("jili成");
            clearingdaybook.setTradeOperate(i);
            clearingdaybook.setCloseType("jili成");
            clearingdaybook.setBs("jili成");
            clearingdaybook.setOpenClose("jili成");
            clearingdaybook.setHedgeFlag("jili成");
            clearingdaybook.setPosID("jili成");
            clearingdaybook.setExeSequenceNo(i);
            clearingdaybook.setOutsideExeNo("jili成");
            clearingdaybook.setExeQty(i);
            clearingdaybook.setExePrice((float) 15.23);
            clearingdaybook.setCurrency("jili成");
            clearingdaybook.setExeAmount((float) 15.23);
            clearingdaybook.setTotalFare((float) 15.23);
            clearingdaybook.setCommissionFare((float) 15.23);
            clearingdaybook.setDeliverFare((float) 15.23);
            clearingdaybook.setTax((float) 15.23);
            clearingdaybook.setOtherCost((float) 15.23);
            clearingdaybook.setEsCheckinAmount(45.123);
            clearingdaybook.setInvestID("jili成");
            clearingdaybook.setOwnUserID("jili成");
            clearingdaybook.setAlterUserID("jili成");
            clearingdaybook.setCreateTime(new Date());
            ls.add(clearingdaybook);
        }
        return ls;
    }

    public List<ClearingExRight> genClearingExRightList(int n) {
        List<ClearingExRight> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ClearingExRight clearingexright = new ClearingExRight();
            clearingexright.setId(i);
            clearingexright.setTimeStamp(new Date());
            clearingexright.setAccountCode("jili成");
            clearingexright.setClearingDate(new Date());
            clearingexright.setObj("jili成"+i);
            clearingexright.setMarketCode("jili成");
            clearingexright.setClassCode("jil");
            clearingexright.setRecordeDate(new Date());
            clearingexright.setPosQty(i);
            clearingexright.setProfitsAmount((float) 15.23);
            clearingexright.setShareA(i);
            clearingexright.setShareB(i);
            clearingexright.setTax((float) 15.23);
            clearingexright.setNetProfitsAmount((float) 15.23);
            clearingexright.setReMark("jili成");
            clearingexright.setOwnUserID("jili成");
            clearingexright.setAlterUserID("jili成");
            clearingexright.setCreateTime(new Date());
            ls.add(clearingexright);
        }
        return ls;
    }

    public List<FundState> genFundStateList(int n) {
        List<FundState> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            FundState fundstate = new FundState();
            fundstate.setId(i);
            fundstate.setTimeStamp(new Date());
            fundstate.setClearingDate(new Date());
            fundstate.setAccountCode("jili成");
            fundstate.setFundTotal((float) 15.23);
            fundstate.setFundAvl((float) 15.23);
            fundstate.setFundFroze((float) 15.23);
            fundstate.setAssetTotal((float) 15.23);
            fundstate.setAssetNet((float) 15.23);
            fundstate.setAssetCredit((float) 15.23);
            fundstate.setPosTotalValue((float) 15.23);
            fundstate.setPosNetValue((float) 15.23);
            fundstate.setPosCreditValue((float) 15.23);
            fundstate.setCurrency("jili成");
            fundstate.setRate((float) 15.23);
            fundstate.setEqualFundAvl((float) 15.23);
            fundstate.setBefAssetTotal((float) 15.23);
            fundstate.setBefFundAvl((float) 15.23);
            fundstate.setOwnUserID("jili成");
            fundstate.setAlterUserID("jili成");
            fundstate.setCreateTime(new Date());
            ls.add(fundstate);
        }
        return ls;
    }

    public List<PosDetail> genPosDetailList(int n) {
        List<PosDetail> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            PosDetail posdetail = new PosDetail();
            posdetail.setId(i);
            posdetail.setTimeStamp(new Date());
            posdetail.setClearingDate(new Date());
            posdetail.setAccountCode("jili成");
            posdetail.setPosID("jili成");
            posdetail.setObj("jili成"+i);
            posdetail.setMarketCode("jili成");
            posdetail.setClassCode("jil");
            posdetail.setBs("jili成");
            posdetail.setQty(i);
            posdetail.setAvlQty(i);
            posdetail.setFrozeQty(i);
            posdetail.setClosePrice((float) 15.23);
            posdetail.setCostPriceA((float) 15.23);
            posdetail.setCostPriceB((float) 15.23);
            posdetail.setOpencostprice((float) 15.23);
            posdetail.setAvgCostPrice((float) 15.23);
            posdetail.setHedgeFlag("jili成");
            posdetail.setPosType("jili成");
            posdetail.setPutCall("jili成");
            posdetail.setCurrency("jili成");
            posdetail.setPl((float) 15.23);
            posdetail.setCostPL((float) 15.23);
            posdetail.setTotalPL((float) 15.23);
            posdetail.setMargin((float) 15.23);
            posdetail.setTotalFare((float) 15.23);
            posdetail.setInvestID("jili成");
            posdetail.setOwnUserID("jili成");
            posdetail.setAlterUserID("jili成");
            posdetail.setCreateTime(new Date());
            ls.add(posdetail);
        }
        return ls;
    }

    public List<ClearingPrice> genClearingPriceList(int n) {
        List<ClearingPrice> ls = new ArrayList<>(n);
        for (int i = 1; i <= n; i++) {
            ClearingPrice clearingprice = new ClearingPrice();
            clearingprice.setId(i);
            clearingprice.setTimeStamp(new Date());
            clearingprice.setClearingDate(new Date());
            clearingprice.setRegionID("jili成");
            clearingprice.setObj("jili成"+i);
            clearingprice.setClassCode("jil");
            clearingprice.setMarketCode("jili成");
            clearingprice.setClosePrice((float) 15.23);
            clearingprice.setClearprice((float) 15.23);
            clearingprice.setALCPrice((float) 15.23);
            clearingprice.setOwnUserID("jili成");
            clearingprice.setAlterUserID("jili成");
            clearingprice.setCreateTime(new Date());
            ls.add(clearingprice);
        }
        return ls;
    }

}
