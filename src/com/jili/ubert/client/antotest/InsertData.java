/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.antotest;

import com.jili.ubert.dao.DaoBaseH2;
import com.jili.ubert.dao.db.OrderExecuteProgress;

/**
 *
 * @author ChengJiLi
 */
public class InsertData {
    public static void main(String[] args) {
        DaoBaseH2<OrderExecuteProgress> db = new DaoBaseH2<>(OrderExecuteProgress.class);
        GenList ls = new GenList();
        db.insert(ls.genOrderExecuteProgressList(1000));
    }
}
