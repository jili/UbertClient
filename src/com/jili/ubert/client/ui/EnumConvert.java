/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui;

import com.jili.ubert.code.OrderTypeEmit;
import com.jili.ubert.code.TradeOperate;
import com.panemu.tiwulfx.table.ComboBoxColumn;

/**
 *
 * @author ChengJiLi
 */
public class EnumConvert {

    public static void setOrderTypeEmit(ComboBoxColumn orderTypeEmit) {
        for (OrderTypeEmit a : OrderTypeEmit.values()) {
            orderTypeEmit.addItem(a.getName(), a.getCode());
        }
    }

    public static void setTradeOperate(ComboBoxColumn tradeOperate) {
        for (TradeOperate a : TradeOperate.values()) {
            tradeOperate.addItem(a.getName(), a.getCode());
        }
    }

    public static void setCurrency(ComboBoxColumn currency) {

    }

    public static void setOpenClose(ComboBoxColumn openClose) {

    }

    public static void setLoginType(ComboBoxColumn loginType) {

    }

    public static void setLogoutType(ComboBoxColumn logoutType) {

    }

    public static void setClientType(ComboBoxColumn clientType) {

    }

    public static void setOSType(ComboBoxColumn oSType) {

    }

    public static void setDirect(ComboBoxColumn direct) {

    }

    public static void setIsTrade(ComboBoxColumn isTrade) {

    }

    public static void setTradeType(ComboBoxColumn tradeType) {

    }

    public static void setClearType(ComboBoxColumn clearType) {

    }

    public static void setPutCall(ComboBoxColumn putCall) {

    }

    public static void setIsPublic(ComboBoxColumn isPublic) {

    }

    public static void setCloseType(ComboBoxColumn closeType) {

    }

    public static void setPosType(ComboBoxColumn posType) {

    }

    public static void setPriceType(ComboBoxColumn priceType) {

    }

    public static void setMsgType(ComboBoxColumn msgType) {

    }

    public static void setCalcDirect(ComboBoxColumn calcDirect) {

    }

    public static void setCanNumerator(ComboBoxColumn canNumerator) {

    }

    public static void setCanDenominator(ComboBoxColumn canDenominator) {

    }

    public static void setTraceType(ComboBoxColumn traceType) {

    }

    public static void setStat(ComboBoxColumn stat) {

    }

    public static void setLoginStat(ComboBoxColumn loginStat) {

    }

    public static void setIsUSBKey(ComboBoxColumn isUSBKey) {

    }

    public static void setFlag(ComboBoxColumn flag) {

    }

    public static void setOrdertype(ComboBoxColumn ordertype) {

    }

    public static void setHedgeFlag(ComboBoxColumn hedgeFlag) {
    }

    public static void setBs(ComboBoxColumn bs) {

    }

    public static void setAccountTitle(ComboBoxColumn accountTitle) {

    }

    public static void setCanCashRepl(ComboBoxColumn canCashRepl) {

    }

    public static void setIsEffectived(ComboBoxColumn isEffectived) {

    }

    public static void setIsForceDone(ComboBoxColumn isForceDone) {

    }

    public static void setIsForceIntercept(ComboBoxColumn isForceIntercept) {

    }

    public static void setIsHaveThreshold(ComboBoxColumn isHaveThreshold) {

    }

    public static void setIsRateOrAbs(ComboBoxColumn isRateOrAbs) {

    }

    public static void setIsRisk(ComboBoxColumn isRisk) {

    }

    public static void setIsSingleObj(ComboBoxColumn isSingleObj) {

    }

    public static void setMaketCode(ComboBoxColumn maketCode) {

    }

    public static void setMarketCode(ComboBoxColumn marketCode) {

    }

    public static void setAccessID(ComboBoxColumn accessID) {

    }

    public static void setRoleID(ComboBoxColumn roleID) {

    }

    public static void setRegionID(ComboBoxColumn regionID) {

    }

    public static void setAssetsScope(ComboBoxColumn assetsScope) {

    }

}
