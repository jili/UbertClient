/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3;

import com.jili.ubert.client.ui.AbstractNodeUI;
import com.jili.ubert.client.ui.PlatformInfo;
import com.jili.ubert.client.ui.UINode;
import com.jili.ubert.client.ui.i18n.I18N;
import com.jili.ubert.clientapi.ClientAPI;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * FXML Controller class
 *
 * @author ChengJiLi
 */
public class WorkSpaceUIController extends AbstractNodeUI implements Initializable,ControlWorkSpaceShow {
    
    private static final Log log = LogFactory.getLog(WorkSpaceUIController.class);
    ClientAPI clientapi;
    @FXML
    private AnchorPane workspace_root;

    @FXML
    private AnchorPane workspace_sub;

    @FXML
    private AnchorPane workspace_message;

    @FXML
    private AnchorPane workspace_main;

    @FXML
    private Pane workspace_menubar;

    @FXML
    private AnchorPane workspace_status;
    private AnchorPane mainregion;
    
    private final int width = PlatformInfo.getAvailableWidth();
    private final int height = PlatformInfo.getAvailableHeight();
    private final int systemheight = PlatformInfo.getWinUseHeight();
    public WorkSpaceUIController(ClientAPI clientapi) {
        super(WorkSpaceUIController.class.getResource("WorkSpaceUI.fxml"), I18N.getBundle());
        this.clientapi = clientapi;
        log.info("初始化");
    }
    private void draw(){
        workspace_root.setPrefSize(width,height-systemheight);
        workspace_sub.setPrefSize(width,height-24-systemheight);
        workspace_status.setPrefSize(width,24);
        workspace_message.setPrefSize(0,height-40-24-systemheight);
        workspace_message.setVisible(false);
        workspace_menubar.setPrefSize(width,40);
        workspace_main.setPrefSize(width,height-40-24-systemheight);
    }
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        //画大小；
        draw();
        //状态栏
        workspace_status.getChildren().add(UINode.getWorkSpace_status(clientapi).getUINode());
        //mssege and log
        workspace_message.getChildren().add(UINode.getWorkSpace_message(clientapi).getUINode());
        //工具栏
        workspace_menubar.getChildren().add(UINode.getWorkSpace_Menu(this).getUINode());
        //主功能区初始化
        mainregion=(AnchorPane)UINode.getStockTradeUI(clientapi).getUINode();
        workspace_main.getChildren().add(mainregion);
    }

    @Override
    public boolean CallCMD(String cmd) {
        boolean ret = false;
        switch (cmd){
            case "ShowMessageWin":{
                workspace_message.setVisible(true);
                workspace_main.setPrefSize(width-width/5,height-40-24-systemheight);
                mainregion.setPrefSize(width-width/5,height-40-24-systemheight);
                ret=true;
                break;
            }
            case "HideMessageWin":{
                workspace_message.setVisible(false);
                workspace_main.setPrefSize(width,height-40-24-systemheight);
                mainregion.setPrefSize(width,height-40-24-systemheight);
                ret=true;
                break;
            }
        }
        return ret;
    }
}
