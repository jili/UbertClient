/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.main2;

import com.jili.ubert.client.ui.AbstractNodeUI;
import com.jili.ubert.client.ui.EnumConvert;
import com.jili.ubert.client.ui.i18n.I18N;
import com.jili.ubert.clientapi.ClientAPI;
import com.jili.ubert.dao.db.OrderExecuteProgress;
import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import com.panemu.tiwulfx.table.ComboBoxColumn;
import com.panemu.tiwulfx.table.TableControl;
import com.panemu.tiwulfx.table.TableControl.Component;
import com.panemu.tiwulfx.table.TableController;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author ChengJiLi
 */
public class Cancel3UIController extends AbstractNodeUI implements Initializable {

    ClientAPI clientapi;
    @FXML
    private TableControl<OrderExecuteProgress> tc_canceltable;
    @FXML
    private ComboBoxColumn<OrderExecuteProgress, String> orderTypeEmit;
    @FXML
    private ComboBoxColumn<OrderExecuteProgress, Integer> tradeOperate;
    @FXML
    private ComboBoxColumn<OrderExecuteProgress, String> openClose;
    @FXML
    private ComboBoxColumn<OrderExecuteProgress, String> hedgeFlag;
    @FXML
    private ComboBoxColumn<OrderExecuteProgress, String> closeType;
    @FXML
    private ComboBoxColumn<OrderExecuteProgress, String> currency;
    //新增按钮与功能控件
    CheckBox isDoubleClik = new CheckBox(I18N.getString("table.isDoubleClickCancel"));
    CheckBox isCancelConfirm = new CheckBox(I18N.getString("table.isCancelConfirm"));
    Button bt_allCancel = new Button(I18N.getString("table.allCancel"));
    Button bt_anCancel = new Button(I18N.getString("table.anCancel"));
    Button bt_buyCancel = new Button(I18N.getString("table.buyCancel"));
    Button bt_sellCancel = new Button(I18N.getString("table.sellCancel"));
    Button bt_selectedCancel = new Button(I18N.getString("table.selectedCancel"));
    

    public Cancel3UIController(ClientAPI clientapi) {
        super(Cancel3UIController.class.getResource("Cancel3UI.fxml"), I18N.getBundle());
        this.clientapi = clientapi;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        tc_canceltable.setRecordClass(OrderExecuteProgress.class);
        tc_canceltable.setController(controller);
        tc_canceltable.setMaxRecord(500);
        tc_canceltable.setVisibleComponents(false, TableControl.Component.BUTTON_DELETE, TableControl.Component.BUTTON_EDIT, TableControl.Component.BUTTON_INSERT,
                TableControl.Component.BUTTON_SAVE);
        tc_canceltable.setIsVisibleComponents(false, TableControl.Component.BUTTON_PAGINATION);
        EnumConvert.setOrderTypeEmit(orderTypeEmit);
        EnumConvert.setTradeOperate(tradeOperate);
        EnumConvert.setOpenClose(openClose);
        EnumConvert.setHedgeFlag(hedgeFlag);
        EnumConvert.setCloseType(closeType);
        EnumConvert.setCurrency(currency);
        //新增按钮与功能
        Pane pane = new Pane();
        pane.setPrefWidth(50);
        bt_anCancel.setOnAction((ActionEvent event) -> {
        });
        bt_anCancel.setTooltip(new Tooltip(I18N.getString("table.tip.anCancel")));
        bt_selectedCancel.setOnAction((ActionEvent event) -> {
        });
        bt_selectedCancel.setTooltip(new Tooltip(I18N.getString("table.tip.selectedCancel")));
        bt_allCancel.setOnAction((ActionEvent event) -> {
        });
        bt_allCancel.setTooltip(new Tooltip(I18N.getString("table.tip.allCancel")));
        bt_buyCancel.setOnAction((ActionEvent event) -> {
        });
        bt_buyCancel.setTooltip(new Tooltip(I18N.getString("table.tip.buyCancel")));
        bt_sellCancel.setOnAction((ActionEvent event) -> {
        });
        bt_sellCancel.setTooltip(new Tooltip(I18N.getString("table.tip.sellCancel")));
        tc_canceltable.addNode(pane);
        tc_canceltable.addNode(this.isDoubleClik);
        tc_canceltable.addButton(this.bt_anCancel);
        tc_canceltable.addButton(this.bt_selectedCancel);
        tc_canceltable.addButton(this.bt_allCancel);
        tc_canceltable.addButton(this.bt_buyCancel);
        tc_canceltable.addButton(this.bt_sellCancel);
        tc_canceltable.addNode(this.isCancelConfirm);
        tc_canceltable.setOnMouseClicked((MouseEvent event) -> {
        });

    }
    private TableController<OrderExecuteProgress> controller = new TableController<OrderExecuteProgress>() {
        @Override
        public TableData<OrderExecuteProgress> loadData(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<TableColumn.SortType> sortingOrders, int maxResult) {
            TableData<OrderExecuteProgress> data = new TableData<>();
            data.setTotalRows(1);
            List<OrderExecuteProgress> ls = new ArrayList<>();
            for (int i = 1; i <= 500; i++) {
                OrderExecuteProgress or = new OrderExecuteProgress();
                or.setOrderID(i);
                or.setAccountCode("1233455jili");
                or.setAlterUserID("uij879");
                or.setBs("B");
                or.setCancelQty(100);
                or.setClassCode("A1");
                or.setCloseType("0");
                or.setCommissionFare((float) 23.5);
                or.setCompleteQty(200);
                or.setCurrency("RMB");
                or.setDateTime(new Date());
                or.setDealerUserID("jili");
                or.setDeliverFare((float) 23.5);
                or.setEsCheckinAmount(234590.0);
                or.setExePrice((float) 18.3);
                or.setExeQty(100);
                or.setExeSequenceNo(1);
                or.setExeTime(new Date());
                or.setHedgeFlag("1");
                or.setInvestID("123456");
                or.setIp4(12334);
                or.setMac("D1:D2");
                or.setMarketCode("1");

                ls.add(or);
            }
            data.setRows(ls);
            return data;
        }

        @Override
        public void exportToExcel(String title, int maxResult, TableControl<OrderExecuteProgress> tblView, List<TableCriteria> lstCriteria) {
            if (lstCriteria.size() > 0) {
                super.exportToExcel("Person Data", maxResult, tblView, lstCriteria);//I18N
            }
        }
    };

}
