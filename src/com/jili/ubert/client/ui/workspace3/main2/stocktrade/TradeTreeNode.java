/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.main2.stocktrade;

/**
 *
 * @author ChengJiLi
 */
class TradeTreeNode {
    private int nodeId;
    private String nodeText;
    public TradeTreeNode(){}
    public TradeTreeNode(int nodeId,String nodeText){
        this.nodeId = nodeId;
        this.nodeText = nodeText;
    }
    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

     public String getNodeText() {
        return nodeText;
    }

    public void setNodeText(String nodeText) {
        this.nodeText = nodeText;
    }
}