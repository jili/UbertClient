/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.main2.table;

import com.jili.ubert.client.antotest.GenList;
import com.jili.ubert.client.ui.AbstractTableNodeUI;
import com.jili.ubert.client.ui.PlatformInfo;
import com.jili.ubert.client.ui.EnumConvert;
import com.jili.ubert.dao.db.PosDetail;
import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import com.panemu.tiwulfx.table.ComboBoxColumn;
import com.panemu.tiwulfx.table.TableControl;
import com.panemu.tiwulfx.table.TableController;
import com.jili.ubert.client.ui.i18n.I18N;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import com.jili.ubert.clientapi.ClientAPI;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author ChengJiLi
 */
public class PosDetailUIController extends AbstractTableNodeUI {

	ClientAPI clientapi;

    @FXML
    private TableControl<PosDetail> tc_table;
    @FXML private ComboBoxColumn<PosDetail, String> bs;
    @FXML private ComboBoxColumn<PosDetail, String> hedgeFlag;
    @FXML private ComboBoxColumn<PosDetail, String> posType;
    @FXML private ComboBoxColumn<PosDetail, String> putCall;
    @FXML private ComboBoxColumn<PosDetail, String> currency;
    @FXML private ComboBoxColumn<PosDetail, String> marketCode;
    Button bt_alterCostPrice = new Button(I18N.getString("table.altercostprice"));

    public PosDetailUIController(ClientAPI clientapi) {
		
        super(PosDetailUIController.class.getResource("PosDetailUI.fxml"));
	this.clientapi = clientapi;
    }

    @Override
    public void tableInit() {
        tc_table.setRecordClass(PosDetail.class);
        tc_table.setController(controller);
        tc_table.setMaxRecord(PlatformInfo.rowNum);
        tc_table.setVisibleComponents(false, TableControl.Component.BUTTON_DELETE, TableControl.Component.BUTTON_EDIT, TableControl.Component.BUTTON_EDIT, TableControl.Component.BUTTON_INSERT,
                TableControl.Component.BUTTON_SAVE);
        tc_table.setIsVisibleComponents(false, TableControl.Component.BUTTON_PAGINATION);
        EnumConvert.setBs(bs);
        EnumConvert.setHedgeFlag(hedgeFlag);
        EnumConvert.setPosType(posType);
        EnumConvert.setPutCall(putCall);
        EnumConvert.setCurrency(currency);
        EnumConvert.setMarketCode(marketCode);
        
        Pane pane = new Pane();
        pane.setPrefWidth(50);
        bt_alterCostPrice.setOnAction((ActionEvent event) -> {
        });
        bt_alterCostPrice.setTooltip(new Tooltip(I18N.getString("table.tip.altercostprice")));
        tc_table.addNode(pane);
        tc_table.addNode(bt_alterCostPrice);

    }
    private TableController<PosDetail> controller = new TableController<PosDetail>() {
        @Override
        public TableData loadData(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<TableColumn.SortType> sortingOrders, int maxResult) {
            int tnum = 500;
            GenList gen = new GenList();
            List<PosDetail> ls = gen.genPosDetailList(tnum);
            TableData<PosDetail> data = new TableData<>(ls, false, tnum);
            return data;

        }

        @Override
        public void exportToExcel(String title, int maxResult, TableControl<PosDetail> tblView, List<TableCriteria> lstCriteria) {
            if (tblView.getRecords().size() > 0) {
                super.exportToExcel(I18N.getString("tablename.PosDetail"), maxResult, tblView, lstCriteria);//I18N
            }
        }
    };
}
