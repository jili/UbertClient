/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.main2.table;

import com.jili.ubert.client.antotest.GenList;
import com.jili.ubert.client.ui.AbstractTableNodeUI;
import com.jili.ubert.client.ui.PlatformInfo;
import com.jili.ubert.client.ui.EnumConvert;
import com.jili.ubert.dao.db.ClearingDayBook;
import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import com.panemu.tiwulfx.table.ComboBoxColumn;
import com.panemu.tiwulfx.table.TableControl;
import com.panemu.tiwulfx.table.TableController;
import com.jili.ubert.client.ui.i18n.I18N;
import java.util.List;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import com.jili.ubert.clientapi.ClientAPI;

/**
 * FXML Controller class
 *
 * @author ChengJiLi
 */
public class ClearingDayBookUIController extends AbstractTableNodeUI {

	ClientAPI clientapi;

    @FXML
    private TableControl<ClearingDayBook> tc_table;
    @FXML private ComboBoxColumn<ClearingDayBook,String> accountTitle;
@FXML private ComboBoxColumn<ClearingDayBook,String> orderTypeEmit;
@FXML private ComboBoxColumn<ClearingDayBook,Integer> tradeOperate;
@FXML private ComboBoxColumn<ClearingDayBook,String> closeType;
@FXML private ComboBoxColumn<ClearingDayBook,String> bs;
@FXML private ComboBoxColumn<ClearingDayBook,String> openClose;
@FXML private ComboBoxColumn<ClearingDayBook,String> hedgeFlag;
@FXML private ComboBoxColumn<ClearingDayBook,String> currency;
@FXML private ComboBoxColumn<ClearingDayBook,String> marketCode;


    public ClearingDayBookUIController(ClientAPI clientapi) {
		
        super(ClearingDayBookUIController.class.getResource("ClearingDayBookUI.fxml"));
	this.clientapi = clientapi;
    }

    @Override
    public void tableInit() {
        tc_table.setRecordClass(ClearingDayBook.class);
        tc_table.setController(controller);
        tc_table.setMaxRecord(PlatformInfo.rowNum);
        tc_table.setVisibleComponents(false, TableControl.Component.BUTTON_DELETE, TableControl.Component.BUTTON_EDIT, TableControl.Component.BUTTON_EDIT, TableControl.Component.BUTTON_INSERT,
			TableControl.Component.BUTTON_SAVE);
		tc_table.setIsVisibleComponents(false, TableControl.Component.BUTTON_PAGINATION);
		EnumConvert.setAccountTitle(accountTitle);
		EnumConvert.setMarketCode(marketCode);
EnumConvert.setOrderTypeEmit(orderTypeEmit);
EnumConvert.setTradeOperate(tradeOperate);
EnumConvert.setCloseType(closeType);
EnumConvert.setBs(bs);
EnumConvert.setOpenClose(openClose);
EnumConvert.setHedgeFlag(hedgeFlag);
EnumConvert.setCurrency(currency);
		
				
    }
    private TableController<ClearingDayBook> controller = new TableController<ClearingDayBook>() {
        @Override
        public TableData loadData(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<TableColumn.SortType> sortingOrders, int maxResult) {
            int tnum=500;GenList gen = new GenList();List<ClearingDayBook> ls = gen.genClearingDayBookList(tnum);TableData<ClearingDayBook> data = new TableData<>(ls,false,tnum);return data;

        }

        @Override
        public void exportToExcel(String title, int maxResult, TableControl<ClearingDayBook> tblView, List<TableCriteria> lstCriteria) {
            if (tblView.getRecords().size() > 0) {
                super.exportToExcel(I18N.getString("tablename.ClearingDayBook"), maxResult, tblView, lstCriteria);//I18N
            }
        }
    };
}
