/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.main2;

import com.jili.ubert.client.ui.AbstractNodeUI;
import com.jili.ubert.client.ui.i18n.I18N;
import com.jili.ubert.clientapi.ClientAPI;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import jfxtras.labs.scene.control.BigDecimalLabel;
import org.controlsfx.control.textfield.CustomTextField;
/**
 * FXML Controller class
 *
 * @author ChengJiLi
 */
public class SubmitOrderController extends AbstractNodeUI implements Initializable {
    ClientAPI clientapi;
    
    
    @FXML
    private CustomTextField obj_code;

    @FXML
    private Label obj_name;

    @FXML
    private RadioButton RB_order_sell;

    @FXML
    private CustomTextField order_price;

    @FXML
    private TextField order_qty;

    @FXML
    private RadioButton RB_order_buy;

    @FXML
    private RadioButton RB_order_close;

    @FXML
    private BigDecimalLabel acc_avlamount;

    @FXML
    private BigDecimalLabel label_acc_maxqty;

    @FXML
    private TextField order_posid;

    @FXML
    private ComboBox<?> order_accountcode;

    @FXML
    private Button buttom_setqty_all;

    @FXML
    private ChoiceBox<?> price_type;

    @FXML
    private RadioButton RB_flag_touji;

    @FXML
    private RadioButton RB_qty_4;

    @FXML
    private RadioButton RB_qty_5;

    @FXML
    private RadioButton RB_order_open;

    @FXML
    private RadioButton RB_qty_2;

    @FXML
    private RadioButton RB_qty_3;

    @FXML
    private GridPane orderGridPane;

    @FXML
    private RadioButton RB_flag_taoli;

    @FXML
    private RadioButton RB_flag_taobao;

    @FXML
    private Button button_submit;

    public SubmitOrderController(ClientAPI clientapi) {
        super(SubmitOrderController.class.getResource("SubmitOrder.fxml"), I18N.getBundle());
        this.clientapi = clientapi;
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
    }    
    
}
