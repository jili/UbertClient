/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.main2.table;

import com.jili.ubert.client.antotest.GenList;
import com.jili.ubert.client.ui.AbstractTableNodeUI;
import com.jili.ubert.client.ui.PlatformInfo;
import com.jili.ubert.client.ui.EnumConvert;
import com.jili.ubert.dao.history.HistoryTradeFundTransfer;
import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import com.panemu.tiwulfx.table.ComboBoxColumn;
import com.panemu.tiwulfx.table.TableControl;
import com.panemu.tiwulfx.table.TableController;
import com.jili.ubert.client.ui.i18n.I18N;
import java.util.List;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import com.jili.ubert.clientapi.ClientAPI;

/**
 * FXML Controller class
 *
 * @author ChengJiLi
 */
public class HistoryTradeFundTransferUIController extends AbstractTableNodeUI {

	ClientAPI clientapi;

    @FXML
    private TableControl<HistoryTradeFundTransfer> tc_table;
    @FXML private ComboBoxColumn<HistoryTradeFundTransfer,String> direct;
@FXML private ComboBoxColumn<HistoryTradeFundTransfer,String> currency;
@FXML private ComboBoxColumn<HistoryTradeFundTransfer,String> isEffectived;
@FXML private ComboBoxColumn<HistoryTradeFundTransfer,String> regionID;


    public HistoryTradeFundTransferUIController(ClientAPI clientapi) {
		
        super(HistoryTradeFundTransferUIController.class.getResource("HistoryTradeFundTransferUI.fxml"));
	this.clientapi = clientapi;
    }

    @Override
    public void tableInit() {
        tc_table.setRecordClass(HistoryTradeFundTransfer.class);
        tc_table.setController(controller);
        tc_table.setMaxRecord(PlatformInfo.rowNum);
        tc_table.setVisibleComponents(false, TableControl.Component.BUTTON_DELETE, TableControl.Component.BUTTON_EDIT, TableControl.Component.BUTTON_EDIT, TableControl.Component.BUTTON_INSERT,
			TableControl.Component.BUTTON_SAVE);
		tc_table.setIsVisibleComponents(false, TableControl.Component.BUTTON_PAGINATION);
		EnumConvert.setDirect(direct);
EnumConvert.setCurrency(currency);
EnumConvert.setIsEffectived(isEffectived);
EnumConvert.setRegionID(regionID);
		
				
    }
    private TableController<HistoryTradeFundTransfer> controller = new TableController<HistoryTradeFundTransfer>() {
        @Override
        public TableData loadData(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<TableColumn.SortType> sortingOrders, int maxResult) {
            int tnum=500;GenList gen = new GenList();List<HistoryTradeFundTransfer> ls = gen.genHistoryTradeFundTransferList(tnum);TableData<HistoryTradeFundTransfer> data = new TableData<>(ls,false,tnum);return data;

        }

        @Override
        public void exportToExcel(String title, int maxResult, TableControl<HistoryTradeFundTransfer> tblView, List<TableCriteria> lstCriteria) {
            if (tblView.getRecords().size() > 0) {
                super.exportToExcel(I18N.getString("tablename.HistoryTradeFundTransfer"), maxResult, tblView, lstCriteria);//I18N
            }
        }
    };
}
