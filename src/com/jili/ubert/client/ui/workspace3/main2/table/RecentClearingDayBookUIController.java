/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.main2.table;

import com.jili.ubert.client.antotest.GenList;
import com.jili.ubert.client.ui.AbstractTableNodeUI;
import com.jili.ubert.client.ui.PlatformInfo;
import com.jili.ubert.client.ui.EnumConvert;
import com.jili.ubert.dao.recent.RecentClearingDayBook;
import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import com.panemu.tiwulfx.table.ComboBoxColumn;
import com.panemu.tiwulfx.table.TableControl;
import com.panemu.tiwulfx.table.TableController;
import com.jili.ubert.client.ui.i18n.I18N;
import java.util.List;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import com.jili.ubert.clientapi.ClientAPI;

/**
 * FXML Controller class
 *
 * @author ChengJiLi
 */
public class RecentClearingDayBookUIController extends AbstractTableNodeUI {

	ClientAPI clientapi;

    @FXML
    private TableControl<RecentClearingDayBook> tc_table;
    @FXML private ComboBoxColumn<RecentClearingDayBook,String> accountTitle;
@FXML private ComboBoxColumn<RecentClearingDayBook,String> orderTypeEmit;
@FXML private ComboBoxColumn<RecentClearingDayBook,Integer> tradeOperate;
@FXML private ComboBoxColumn<RecentClearingDayBook,String> closeType;
@FXML private ComboBoxColumn<RecentClearingDayBook,String> bs;
@FXML private ComboBoxColumn<RecentClearingDayBook,String> openClose;
@FXML private ComboBoxColumn<RecentClearingDayBook,String> hedgeFlag;
@FXML private ComboBoxColumn<RecentClearingDayBook,String> currency;
@FXML private ComboBoxColumn<RecentClearingDayBook,String> marketCode;




    public RecentClearingDayBookUIController(ClientAPI clientapi) {
		
        super(RecentClearingDayBookUIController.class.getResource("RecentClearingDayBookUI.fxml"));
	this.clientapi = clientapi;
    }

    @Override
    public void tableInit() {
        tc_table.setRecordClass(RecentClearingDayBook.class);
        tc_table.setController(controller);
        tc_table.setMaxRecord(PlatformInfo.rowNum);
        tc_table.setVisibleComponents(false, TableControl.Component.BUTTON_DELETE, TableControl.Component.BUTTON_EDIT, TableControl.Component.BUTTON_EDIT, TableControl.Component.BUTTON_INSERT,
			TableControl.Component.BUTTON_SAVE);
		tc_table.setIsVisibleComponents(false, TableControl.Component.BUTTON_PAGINATION);
		EnumConvert.setAccountTitle(accountTitle);
EnumConvert.setOrderTypeEmit(orderTypeEmit);
EnumConvert.setTradeOperate(tradeOperate);
EnumConvert.setCloseType(closeType);
EnumConvert.setBs(bs);
EnumConvert.setOpenClose(openClose);
EnumConvert.setHedgeFlag(hedgeFlag);
EnumConvert.setCurrency(currency);
EnumConvert.setMarketCode(marketCode);


		
				
    }
    private TableController<RecentClearingDayBook> controller = new TableController<RecentClearingDayBook>() {
        @Override
        public TableData loadData(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<TableColumn.SortType> sortingOrders, int maxResult) {
            int tnum=500;GenList gen = new GenList();List<RecentClearingDayBook> ls = gen.genRecentClearingDayBookList(tnum);TableData<RecentClearingDayBook> data = new TableData<>(ls,false,tnum);return data;

        }

        @Override
        public void exportToExcel(String title, int maxResult, TableControl<RecentClearingDayBook> tblView, List<TableCriteria> lstCriteria) {
            if (tblView.getRecords().size() > 0) {
                super.exportToExcel(I18N.getString("tablename.RecentClearingDayBook"), maxResult, tblView, lstCriteria);//I18N
            }
        }
    };
}
