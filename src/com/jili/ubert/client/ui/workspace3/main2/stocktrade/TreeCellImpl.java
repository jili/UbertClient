/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.main2.stocktrade;

import javafx.scene.control.TreeCell;

/**
 *
 * @author ChengJiLi
 */
public class TreeCellImpl extends TreeCell<TradeTreeNode> {
    @Override
    public void updateItem(TradeTreeNode item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setText(null);
        } else {
            setText(item.getNodeText());
        }
    }
}

