/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.main2.table;

import com.jili.ubert.client.antotest.GenList;
import com.jili.ubert.client.ui.AbstractTableNodeUI;
import com.jili.ubert.client.ui.PlatformInfo;
import com.jili.ubert.client.ui.EnumConvert;
import com.jili.ubert.dao.trace.TraceObjUnderlying;
import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import com.panemu.tiwulfx.table.ComboBoxColumn;
import com.panemu.tiwulfx.table.TableControl;
import com.panemu.tiwulfx.table.TableController;
import com.jili.ubert.client.ui.i18n.I18N;
import java.util.List;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import com.jili.ubert.clientapi.ClientAPI;

/**
 * FXML Controller class
 *
 * @author ChengJiLi
 */
public class TraceObjUnderlyingUIController extends AbstractTableNodeUI {

	ClientAPI clientapi;

    @FXML
    private TableControl<TraceObjUnderlying> tc_table;
    @FXML private ComboBoxColumn<TraceObjUnderlying,String> traceType;
@FXML private ComboBoxColumn<TraceObjUnderlying,String> currency;
@FXML private ComboBoxColumn<TraceObjUnderlying,String> marketCode;
@FXML private ComboBoxColumn<TraceObjUnderlying,String> regionID;




    public TraceObjUnderlyingUIController(ClientAPI clientapi) {
		
        super(TraceObjUnderlyingUIController.class.getResource("TraceObjUnderlyingUI.fxml"));
	this.clientapi = clientapi;
    }

    @Override
    public void tableInit() {
        tc_table.setRecordClass(TraceObjUnderlying.class);
        tc_table.setController(controller);
        tc_table.setMaxRecord(PlatformInfo.rowNum);
        tc_table.setVisibleComponents(false, TableControl.Component.BUTTON_DELETE, TableControl.Component.BUTTON_EDIT, TableControl.Component.BUTTON_EDIT, TableControl.Component.BUTTON_INSERT,
			TableControl.Component.BUTTON_SAVE);
		tc_table.setIsVisibleComponents(false, TableControl.Component.BUTTON_PAGINATION);
		EnumConvert.setTraceType(traceType);
EnumConvert.setCurrency(currency);
EnumConvert.setMaketCode(marketCode);
EnumConvert.setRegionID(regionID);

		
				
    }
    private TableController<TraceObjUnderlying> controller = new TableController<TraceObjUnderlying>() {
        @Override
        public TableData loadData(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<TableColumn.SortType> sortingOrders, int maxResult) {
            int tnum=500;GenList gen = new GenList();List<TraceObjUnderlying> ls = gen.genTraceObjUnderlyingList(tnum);TableData<TraceObjUnderlying> data = new TableData<>(ls,false,tnum);return data;

        }

        @Override
        public void exportToExcel(String title, int maxResult, TableControl<TraceObjUnderlying> tblView, List<TableCriteria> lstCriteria) {
            if (tblView.getRecords().size() > 0) {
                super.exportToExcel(I18N.getString("tablename.TraceObjUnderlying"), maxResult, tblView, lstCriteria);//I18N
            }
        }
    };
}
