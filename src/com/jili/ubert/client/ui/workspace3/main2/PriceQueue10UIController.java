/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.main2;

import com.jili.ubert.client.ui.AbstractNodeUI;
import com.jili.ubert.client.ui.i18n.I18N;
import com.jili.ubert.client.ui.workspace3.menu1.MenuUIController;
import com.jili.ubert.clientapi.provider.MarketDataProvider;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author ChengJiLi
 */
public class PriceQueue10UIController extends AbstractNodeUI implements Initializable {

    @FXML
    private Label p5_bid6;

    @FXML
    private Label p5_ask2;

    @FXML
    private Label p5_ask9q;

    @FXML
    private Label p5_bid5;

    @FXML
    private Label p5_bid3q;

    @FXML
    private Label p5_ask1;

    @FXML
    private Label p5_bid4;

    @FXML
    private Label p5_bid10;

    @FXML
    private Label p5_bid3;

    @FXML
    private Label p5_bid1q;

    @FXML
    private Label p5_ask6;

    @FXML
    private Label p5_bid9;

    @FXML
    private Label p5_ask5;

    @FXML
    private Label p5_bid8;

    @FXML
    private Label p5_ask4;

    @FXML
    private Label p5_bid7;

    @FXML
    private Label p5_ask3;

    @FXML
    private Label p5_ask1q;

    @FXML
    private Label uplimit_price;

    @FXML
    private Label p5_ask9;

    @FXML
    private Label p5_ask10;

    @FXML
    private Label p5_ask3q;

    @FXML
    private Label p5_ask8;

    @FXML
    private Label p5_ask7;

    @FXML
    private Label p5_ask5q;

    @FXML
    private Label ratio_price;

    @FXML
    private Label p5_ask7q;

    @FXML
    private Label lastqty;

    @FXML
    private Label ratio_value;

    @FXML
    private Label p5_ask10q;

    @FXML
    private Label p5_bid2;

    @FXML
    private Label p5_bid8q;

    @FXML
    private Label lastprice;

    @FXML
    private Label p5_bid1;

    @FXML
    private Label p5_bid6q;

    @FXML
    private Label p5_bid4q;

    @FXML
    private Label p5_bid10q;

    @FXML
    private Label p5_bid2q;

    @FXML
    private Label downlimit_price;

    @FXML
    private Label p5_ask2q;

    @FXML
    private Label p5_ask4q;

    @FXML
    private Label p5_ask6q;

    @FXML
    private Label p5_ask8q;

    @FXML
    private Label p5_bid9q;

    @FXML
    private Label p5_bid7q;

    @FXML
    private Label p5_bid5q;
    private final MarketDataProvider clientapi;

    public PriceQueue10UIController(MarketDataProvider clientapi) {
        super(PriceQueue10UIController.class.getResource("PriceQueue10UI.fxml"), I18N.getBundle());
        this.clientapi = clientapi;
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
