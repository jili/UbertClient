/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.main2.table;

import com.jili.ubert.client.antotest.GenList;
import com.jili.ubert.client.ui.AbstractTableNodeUI;
import com.jili.ubert.client.ui.PlatformInfo;
import com.jili.ubert.client.ui.EnumConvert;
import com.jili.ubert.dao.trace.TraceAccountFare;
import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import com.panemu.tiwulfx.table.ComboBoxColumn;
import com.panemu.tiwulfx.table.TableControl;
import com.panemu.tiwulfx.table.TableController;
import com.jili.ubert.client.ui.i18n.I18N;
import java.util.List;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import com.jili.ubert.clientapi.ClientAPI;

/**
 * FXML Controller class
 *
 * @author ChengJiLi
 */
public class TraceAccountFareUIController extends AbstractTableNodeUI {

	ClientAPI clientapi;

    @FXML
    private TableControl<TraceAccountFare> tc_table;
    @FXML private ComboBoxColumn<TraceAccountFare,String> traceType;
@FXML private ComboBoxColumn<TraceAccountFare,String> openClose;
@FXML private ComboBoxColumn<TraceAccountFare,String> marketCode;
@FXML private ComboBoxColumn<TraceAccountFare,String> bs;



    public TraceAccountFareUIController(ClientAPI clientapi) {
		
        super(TraceAccountFareUIController.class.getResource("TraceAccountFareUI.fxml"));
	this.clientapi = clientapi;
    }

    @Override
    public void tableInit() {
        tc_table.setRecordClass(TraceAccountFare.class);
        tc_table.setController(controller);
        tc_table.setMaxRecord(PlatformInfo.rowNum);
        tc_table.setVisibleComponents(false, TableControl.Component.BUTTON_DELETE, TableControl.Component.BUTTON_EDIT, TableControl.Component.BUTTON_EDIT, TableControl.Component.BUTTON_INSERT,
			TableControl.Component.BUTTON_SAVE);
		tc_table.setIsVisibleComponents(false, TableControl.Component.BUTTON_PAGINATION);
		EnumConvert.setTraceType(traceType);
EnumConvert.setOpenClose(openClose);
		EnumConvert.setMaketCode(marketCode);
EnumConvert.setBs(bs);

				
    }
    private TableController<TraceAccountFare> controller = new TableController<TraceAccountFare>() {
        @Override
        public TableData loadData(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<TableColumn.SortType> sortingOrders, int maxResult) {
            int tnum=500;GenList gen = new GenList();List<TraceAccountFare> ls = gen.genTraceAccountFareList(tnum);TableData<TraceAccountFare> data = new TableData<>(ls,false,tnum);return data;

        }

        @Override
        public void exportToExcel(String title, int maxResult, TableControl<TraceAccountFare> tblView, List<TableCriteria> lstCriteria) {
            if (tblView.getRecords().size() > 0) {
                super.exportToExcel(I18N.getString("tablename.TraceAccountFare"), maxResult, tblView, lstCriteria);//I18N
            }
        }
    };
}
