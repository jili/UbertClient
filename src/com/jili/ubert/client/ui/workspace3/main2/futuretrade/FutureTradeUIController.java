/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.main2.futuretrade;

import com.jili.ubert.client.ui.AbstractNodeUI;
import com.jili.ubert.client.ui.i18n.I18N;
import com.jili.ubert.client.ui.workspace3.main2.Cancel3UIController;
import com.jili.ubert.clientapi.ClientAPI;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author ChengJiLi
 */
public class FutureTradeUIController extends AbstractNodeUI implements Initializable {
    ClientAPI clientapi;

    public FutureTradeUIController(ClientAPI clientapi) {
        super(FutureTradeUIController.class.getResource("FutureTradeUI.fxml"), I18N.getBundle());
        this.clientapi = clientapi;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
