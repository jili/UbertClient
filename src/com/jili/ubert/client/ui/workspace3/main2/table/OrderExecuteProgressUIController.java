/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.main2.table;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.jili.ubert.client.UbertClient;
import com.jili.ubert.client.antotest.GenList;
import com.jili.ubert.client.ui.AbstractTableNodeUI;
import com.jili.ubert.client.ui.EnumConvert;
import com.jili.ubert.client.ui.PlatformInfo;
import com.jili.ubert.dao.db.OrderExecuteProgress;
import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import com.panemu.tiwulfx.table.ComboBoxColumn;
import com.panemu.tiwulfx.table.TableControl;
import com.panemu.tiwulfx.table.TableController;
import com.jili.ubert.client.ui.i18n.I18N;
import com.jili.ubert.clientapi.until.AnswerHandler;
import com.jili.ubert.clientapi.ClientAPI;
import java.util.List;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import com.jili.ubert.clientapi.ClientAPI;
import com.jili.ubert.clientapi.provider.TradeProvider;
import com.jili.ubert.code.client2server.RequestPageData;
import com.jili.ubert.code.server2client.MsgResult;
import com.jili.ubert.dao.DaoBaseH2;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * FXML Controller class
 *
 * @author ChengJiLi
 */
public class OrderExecuteProgressUIController extends AbstractTableNodeUI {
    private static final Log log = LogFactory.getLog(OrderExecuteProgressUIController.class);

	ClientAPI clientapi;
    @FXML
    private TableControl<OrderExecuteProgress> tc_table;
    @FXML private ComboBoxColumn<OrderExecuteProgress,String> orderTypeEmit;
@FXML private ComboBoxColumn<OrderExecuteProgress,Integer> tradeOperate;
@FXML private ComboBoxColumn<OrderExecuteProgress,String> bs;
@FXML private ComboBoxColumn<OrderExecuteProgress,String> openClose;
@FXML private ComboBoxColumn<OrderExecuteProgress,String> hedgeFlag;
@FXML private ComboBoxColumn<OrderExecuteProgress,String> closeType;
@FXML private ComboBoxColumn<OrderExecuteProgress,String> currency;
@FXML private ComboBoxColumn<OrderExecuteProgress,String> marketCode;


    TradeProvider api;
    TableData<OrderExecuteProgress> data = new TableData(new ArrayList<>(), false, 0);
    public OrderExecuteProgressUIController(ClientAPI clientapi) {
		
        super(OrderExecuteProgressUIController.class.getResource("OrderExecuteProgressUI.fxml"));
	this.clientapi = clientapi;
        this.api=clientapi.getTradeProvider();
        
    }


    @Override
    public void tableInit() {
        tc_table.setRecordClass(OrderExecuteProgress.class);
        tc_table.setController(controller);
        tc_table.setMaxRecord(PlatformInfo.rowNum);
        tc_table.setVisibleComponents(false, TableControl.Component.BUTTON_DELETE, TableControl.Component.BUTTON_EDIT, TableControl.Component.BUTTON_EDIT, TableControl.Component.BUTTON_INSERT,
			TableControl.Component.BUTTON_SAVE);
		tc_table.setIsVisibleComponents(false, TableControl.Component.BUTTON_PAGINATION);
		EnumConvert.setOrderTypeEmit(orderTypeEmit);
EnumConvert.setTradeOperate(tradeOperate);
EnumConvert.setBs(bs);
EnumConvert.setOpenClose(openClose);
EnumConvert.setHedgeFlag(hedgeFlag);
EnumConvert.setCloseType(closeType);
EnumConvert.setCurrency(currency);
EnumConvert.setMarketCode(marketCode);
		
				
    }
    private TableController<OrderExecuteProgress> controller = new TableController<OrderExecuteProgress>() {
        @Override
        public TableData loadData(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<TableColumn.SortType> sortingOrders, int maxResult) {
        //    int tnum=500;GenList gen = new GenList();List<OrderExecuteProgress> ls = gen.genOrderExecuteProgressList(tnum);TableData<OrderExecuteProgress> data = new TableData<>(ls,false,tnum);return data;
        log.info("开始startIndex："+startIndex);log.info("每页maxResult"+maxResult);
        log.info("filteredColumns:"+JSON.toJSONString(filteredColumns, SerializerFeature.WriteDateUseDateFormat, SerializerFeature.WriteClassName));
            log.info("sortedColumns:"+JSON.toJSONString(sortedColumns, SerializerFeature.WriteDateUseDateFormat, SerializerFeature.WriteClassName));
            log.info("sortingOrders:"+JSON.toJSONString(sortingOrders, SerializerFeature.WriteDateUseDateFormat, SerializerFeature.WriteClassName));
            RequestPageData rpd = new RequestPageData();
            rpd.setStartIndex(startIndex);
            rpd.setMaxResult(maxResult);
            rpd.setSortedColumns(sortedColumns);
            List<String> crl = new ArrayList<>(filteredColumns.size());
            filteredColumns.stream().forEach((TableCriteria a)->{crl.add(JSON.toJSONString(a, SerializerFeature.WriteDateUseDateFormat, SerializerFeature.WriteClassName));});
    //        TableData<OrderExecuteProgress> data=null;
            try {
                api.QueryOrderExecuteProgressList(rpd, new AnswerHandler<TableData<OrderExecuteProgress>>(){
                    @Override
                    public void OnAnswer(TableData<OrderExecuteProgress> anwer) {
                        data=anwer;
                    }
                    
                    @Override
                    public void IsSuccess() {
                    }
                    
                    @Override
                    public void IsFalse(MsgResult rst) {
                        log.info("失败消息："+JSON.toJSONString(rst));
                    }
                }); } catch (IOException ex) {
                Logger.getLogger(OrderExecuteProgressUIController.class.getName()).log(Level.SEVERE, null, ex);
            }
        //    DaoBaseH2<OrderExecuteProgress> db = new DaoBaseH2<>(OrderExecuteProgress.class);
        //    TableData<OrderExecuteProgress> data = db.fetch(startIndex, filteredColumns, sortedColumns, sortingOrders, maxResult);
        if (data!=null)  {  
        if (data.isMoreRows()){
                tc_table.setIsVisibleComponents(true, TableControl.Component.BUTTON_PAGINATION);
            }else{
                tc_table.setIsVisibleComponents(false, TableControl.Component.BUTTON_PAGINATION);
            }
        }
//            log.info(JSON.toJSONString(data, SerializerFeature.WriteDateUseDateFormat, SerializerFeature.WriteClassName));
            return data;
            
        }

        @Override
        public void exportToExcel(String title, int maxResult, TableControl<OrderExecuteProgress> tblView, List<TableCriteria> lstCriteria) {
            if (tblView.getRecords().size() > 0) {
                super.exportToExcel(I18N.getString("tablename.OrderExecuteProgress"), maxResult, tblView, lstCriteria);//I18N
            }
        }
    };
}
