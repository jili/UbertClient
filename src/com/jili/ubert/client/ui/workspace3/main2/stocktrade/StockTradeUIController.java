/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.main2.stocktrade;

import com.jili.ubert.client.ui.AbstractNodeUI;
import com.jili.ubert.client.ui.PlatformInfo;
import com.jili.ubert.client.ui.i18n.I18N;
import com.jili.ubert.clientapi.ClientAPI;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.TitledPane;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * FXML Controller class
 *
 * @author ChengJiLi
 */
public class StockTradeUIController extends AbstractNodeUI implements Initializable {

    private static final Log log = LogFactory.getLog(StockTradeUIController.class);
    private final ClientAPI clientapi;

    @FXML
    private Accordion work_stock_cmd;
    @FXML
    private AnchorPane work_stock_root;
    @FXML
    private AnchorPane work_stock_work;

    @FXML
    private TreeView<TradeTreeNode> stock_general_tree;
    @FXML
    private TreeView<TradeTreeNode> stock_credit_tree;
    @FXML
    private TreeView<TradeTreeNode> stock_huigou_tree;
    @FXML
    private TreeView<TradeTreeNode> stock_option_tree;
    @FXML
    private TreeView<TradeTreeNode> stock_thirdtrade_tree;
    @FXML
    private TitledPane stock_general;
    private int AccordionMouseClick = 2;

    private void draw() {
        work_stock_root.setPrefSize(PlatformInfo.getAvailableWidth(), PlatformInfo.getAvailableHeight() - 40 - 24 - PlatformInfo.getWinUseHeight());
        work_stock_cmd.setPrefSize(235, work_stock_root.getPrefHeight());
        work_stock_work.setPrefSize(work_stock_root.getPrefWidth() - 235., work_stock_root.getPrefHeight());
        log.debug(work_stock_root.getPrefWidth() + "  高" + work_stock_root.getPrefHeight());
    }

    private void drawtree() {
        //普通委托
        /*
         11	买入
         12	卖出
         13	撤单
         14	资金持仓
         151	查询-当日委托
         152	查询-当日成交
         153	查询-当日委托成交
         154	查询-历史委托
         155	查询-历史成交
         156	查询-历史委托成交
         157	查询-历史持仓
         158	查询-出入金流水
         159	查询-净值图
         160	查询-资金流水
         161	其他-新股申购
         162	其他-基金申购
         163	其他-基金赎回
         164	其他-债转股
         165	其他-分红设定
         166	其他-基金拆分
         167	其他-基金合并
         stock_general_tree=1
         stock_general_tree_1=买入
         stock_general_tree_2=卖出
         stock_general_tree_3=撤单
         stock_general_tree_4=资金持仓
         stock_general_tree_5=查询
         stock_general_tree_6=其他
         stock_general_tree_5_1=当日委托
         stock_general_tree_5_2=当日成交
         stock_general_tree_5_3=当日委托成交
         stock_general_tree_5_4=历史委托
         stock_general_tree_5_5=历史成交
         stock_general_tree_5_6=历史委托成交
         stock_general_tree_5_7=历史持仓
         stock_general_tree_5_8=出入金流水
         stock_general_tree_5_9=净值图
         stock_general_tree_5_10=资金流水/交割单
         stock_general_tree_6_1=新股申购
         stock_general_tree_6_2=基金申购
         stock_general_tree_6_3=基金赎回
         stock_general_tree_6_4=债转股
         stock_general_tree_6_5=分红设定
         stock_general_tree_6_6=基金拆分
         stock_general_tree_6_7=基金合并
         */
        stock_general_tree.setCellFactory((TreeView<TradeTreeNode> param) -> new TreeCellImpl());
        TreeItem<TradeTreeNode> rootItem_general = new TreeItem<>(new TradeTreeNode(1, I18N.getString("stock_general_tree")));
        rootItem_general.setExpanded(true);
        for (int i = 1; i <= 6; i++) {
            TreeItem<TradeTreeNode> item = new TreeItem<>(new TradeTreeNode(10 + i, I18N.getString("stock_general_tree_" + i)));
            if (i == 5) {
                for (int j = 1; j <= 10; j++) {
                    TreeItem<TradeTreeNode> item_sub = new TreeItem<>(new TradeTreeNode(150 + j, I18N.getString("stock_general_tree_5_" + j)));
                    item.setExpanded(true);
                    item.getChildren().add(item_sub);
                }
            } else if (i == 6) {
                for (int j = 1; j <= 7; j++) {
                    TreeItem<TradeTreeNode> item_sub = new TreeItem<>(new TradeTreeNode(160 + j, I18N.getString("stock_general_tree_6_" + j)));
                    item.setExpanded(true);
                    item.getChildren().add(item_sub);
                }
            }
            rootItem_general.getChildren().add(item);
        }
        stock_general_tree.setRoot(rootItem_general);
        stock_general_tree.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseHandle_general);

        //回购交易
        stock_huigou_tree.setCellFactory((TreeView<TradeTreeNode> param) -> new TreeCellImpl());
        TreeItem<TradeTreeNode> rootItem_huigou = new TreeItem<>(new TradeTreeNode(2, I18N.getString("stock_huigou_tree")));
        rootItem_huigou.setExpanded(true);
        for (int i = 1; i <= 9; i++) {
            TreeItem<TradeTreeNode> item = new TreeItem<>(new TradeTreeNode(20 + i, I18N.getString("stock_huigou_tree_" + i)));
            if (i == 9) {
                for (int j = 1; j <= 11; j++) {
                    TreeItem<TradeTreeNode> item_sub = new TreeItem<>(new TradeTreeNode(290 + j, I18N.getString("stock_huigou_tree_9_" + j)));
                    item.setExpanded(true);
                    item.getChildren().add(item_sub);
                }
            }
            rootItem_huigou.getChildren().add(item);
        }
        stock_huigou_tree.setRoot(rootItem_huigou);
        stock_huigou_tree.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseHandle_huigou);

        //信用交易
        stock_credit_tree.setCellFactory((TreeView<TradeTreeNode> param) -> new TreeCellImpl());
        TreeItem<TradeTreeNode> rootItem_credit = new TreeItem<>(new TradeTreeNode(3, I18N.getString("stock_credit_tree")));
        rootItem_credit.setExpanded(true);
        for (int i = 1; i <= 11; i++) {
            TreeItem<TradeTreeNode> item = new TreeItem<>(new TradeTreeNode(30 + i, I18N.getString("stock_credit_tree_" + i)));
            if (i == 11) {
                for (int j = 1; j <= 11; j++) {
                    TreeItem<TradeTreeNode> item_sub = new TreeItem<>(new TradeTreeNode(310 + j, I18N.getString("stock_credit_tree_11_" + j)));
                    item.setExpanded(true);
                    item.getChildren().add(item_sub);
                }
            }
            rootItem_credit.getChildren().add(item);
        }
        stock_credit_tree.setRoot(rootItem_credit);
        stock_credit_tree.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseHandle_credit);
        //衍生品交易
        stock_option_tree.setCellFactory((TreeView<TradeTreeNode> param) -> new TreeCellImpl());
        TreeItem<TradeTreeNode> rootItem_option = new TreeItem<>(new TradeTreeNode(4, I18N.getString("stock_option_tree")));
        rootItem_option.setExpanded(true);
        for (int i = 1; i <= 12; i++) {
            TreeItem<TradeTreeNode> item;
            if (i == 10) {
                item = new TreeItem<>(new TradeTreeNode(410 + i, I18N.getString("stock_option_tree_" + i)));
            } else if (i == 11) {
                item = new TreeItem<>(new TradeTreeNode(411, I18N.getString("stock_option_tree_" + i)));
            } else {
                item = new TreeItem<>(new TradeTreeNode(40 + i, I18N.getString("stock_option_tree_" + i)));
            }
            if (i == 12) {
                for (int j = 1; j <= 12; j++) {
                    TreeItem<TradeTreeNode> item_sub = new TreeItem<>(new TradeTreeNode(4120 + j, I18N.getString("stock_option_tree_12_" + j)));
                    item.setExpanded(true);
                    item.getChildren().add(item_sub);
                }
            }
            rootItem_option.getChildren().add(item);
        }
        stock_option_tree.setRoot(rootItem_option);
        stock_option_tree.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseHandle_option);

        //新三板
    }
    //回购功能监听
    private final EventHandler<MouseEvent> mouseHandle_huigou = (MouseEvent event) -> {
        MouseButton button = event.getButton();
        int count = event.getClickCount();
        if (button == MouseButton.PRIMARY && count == AccordionMouseClick) {
            Node node = event.getPickResult().getIntersectedNode();
            // Accept clicks only on node cells, and not on empty spaces of the TreeView
            if (node instanceof Text || (node instanceof TreeCell && ((TreeCell) node).getText() != null)) {
                TradeTreeNode name = (TradeTreeNode) ((TreeItem) stock_huigou_tree.getSelectionModel().getSelectedItem()).getValue();
                log.info("mouseHandle_huigou Node click: " + name.getNodeId() + " :" + name.getNodeText());
                /*
                 21 :质押式逆回购
                 22 :债券质押入库
                 23 :债券质押出库
                 24 :质押式正回购
                 25 :买断式正回购
                 26 :买断式逆回购
                 27 :买断式回购履约申报
                 28 :撤单
                 29 :查询
                 291 :可质押入库持仓
                 292 :标的券持仓
                 293 :折算率
                 294 :回购持仓
                 295 :当日回购委托
                 296 :当日回购成交
                 297 :当日回购委托成交
                 298 :历史回购委托
                 299 :历史回购委托成交
                 300 :历史回购成交
                 301 :历史回购持仓
                 */
                switch (name.getNodeId()) {
                    case 21: {//质押式逆回购
                    }
                    case 22: {//债券质押入库
                    }
                    case 23: {//债券质押出库
                    }
                    case 24: {//质押式正回购
                    }
                    case 25: {//买断式正回购
                    }
                    case 26: {//买断式逆回购
                    }
                    case 27: {//买断式回购履约申报
                    }
                    case 28: {//撤单
                    }
                    case 29: {//查询
                    }
                    case 291: {//可质押入库持仓
                    }
                    case 292: {//标的券持仓
                    }
                    case 293: {//折算率
                    }
                    case 294: {//回购持仓
                    }
                    case 295: {//当日回购委托
                    }
                    case 296: {//当日回购成交
                    }
                    case 297: {//当日回购委托成交
                    }
                    case 298: {//历史回购委托
                    }
                    case 299: {//历史回购委托成交
                    }
                    case 300: {//历史回购成交
                    }
                    case 301: {//历史回购持仓
                    }
                }
                event.consume();
            }
        }
    };
    private final EventHandler<MouseEvent> mouseHandle_general = (MouseEvent event) -> {
        MouseButton button = event.getButton();
        Node node = event.getPickResult().getIntersectedNode();
        int count = event.getClickCount();
        if (button == MouseButton.PRIMARY && count == AccordionMouseClick) {
            // Accept clicks only on node cells, and not on empty spaces of the TreeView
            if (node instanceof Text || (node instanceof TreeCell && ((TreeCell) node).getText() != null)) {
                TradeTreeNode name = (TradeTreeNode) ((TreeItem) stock_general_tree.getSelectionModel().getSelectedItem()).getValue();
                log.info("mouseHandle_general Node click: " + name.getNodeId() + " :" + name.getNodeText());
                /*
                 11 :买入
                 12 :卖出
                 13 :撤单
                 14 :资金持仓
                 15 :查询
                 151 :当日委托
                 152 :当日成交
                 153 :当日委托成交
                 154 :历史委托
                 155 :历史成交
                 156 :历史委托成交
                 157 :历史持仓
                 158 :出入金流水
                 159 :净值图
                 160 :资金流水/交割单
                 161 :新股申购
                 162 :基金申购
                 163 :基金赎回
                 164 :债转股
                 165 :分红设定
                 166 :基金拆分
                 167 :基金合并

                 */
                switch (name.getNodeId()) {
                    case 11: {//买入
                    }
                    case 12: {//卖出
                    }
                    case 13: {//撤单
                    }
                    case 14: {//资金持仓
                    }
                    case 15: {//查询
                    }
                    case 151: {//当日委托
                    }
                    case 152: {//当日成交
                    }
                    case 153: {//当日委托成交
                    }
                    case 154: {//历史委托
                    }
                    case 155: {//历史成交
                    }
                    case 156: {//历史委托成交
                    }
                    case 157: {//历史持仓
                    }
                    case 158: {//出入金流水
                    }
                    case 159: {//净值图
                    }
                    case 160: {//资金流水/交割单
                    }
                    case 161: {//新股申购
                    }
                    case 162: {//基金申购
                    }
                    case 163: {//基金赎回
                    }
                    case 164: {//债转股
                    }
                    case 165: {//分红设定
                    }
                    case 166: {//基金拆分
                    }
                    case 167: {//基金合并
                    }
                }
                event.consume();
            }
        }
    };
    private final EventHandler<MouseEvent> mouseHandle_credit = (MouseEvent event) -> {
        MouseButton button = event.getButton();
        Node node = event.getPickResult().getIntersectedNode();
        int count = event.getClickCount();
        if (button == MouseButton.PRIMARY && count == AccordionMouseClick) {
            // Accept clicks only on node cells, and not on empty spaces of the TreeView
            if (node instanceof Text || (node instanceof TreeCell && ((TreeCell) node).getText() != null)) {
                TradeTreeNode name = (TradeTreeNode) ((TreeItem) stock_credit_tree.getSelectionModel().getSelectedItem()).getValue();
                log.info("mouseHandle_credit Node click: " + name.getNodeId() + " :" + name.getNodeText());
                /*
                 31 :融资买入
                 32 :融券卖出
                 33 :普通买入
                 34 :普通卖出
                 35 :卖券还钱
                 36 :买券还券
                 37 :直接还钱
                 38 :直接还券
                 39 :撤单
                 40 :资金持仓（信用）
                 41 :查询
                 311 :综合信用查询
                 312 :融资余额查询
                 313 :标的券查询
                 314 :非交易划拨查询
                 315 :利息费用查询
                 316 :融券合约查询
                 317 :融券余额查询
                 318 :融资合约查询
                 319 :当日委托
                 320 :当日成交
                 321 :当日委托成交
                 */
                switch (name.getNodeId()) {
                    case 31: {//融资买入
                    }
                    case 32: {//融券卖出
                    }
                    case 33: {//普通买入
                    }
                    case 34: {//普通卖出
                    }
                    case 35: {//卖券还钱
                    }
                    case 36: {//买券还券
                    }
                    case 37: {//直接还钱
                    }
                    case 38: {//直接还券
                    }
                    case 39: {//撤单
                    }
                    case 40: {//资金持仓（信用）
                    }
                    case 41: {//查询
                    }
                    case 311: {//综合信用查询
                    }
                    case 312: {//融资余额查询
                    }
                    case 313: {//标的券查询
                    }
                    case 314: {//非交易划拨查询
                    }
                    case 315: {//利息费用查询
                    }
                    case 316: {//融券合约查询
                    }
                    case 317: {//融券余额查询
                    }
                    case 318: {//融资合约查询
                    }
                    case 319: {//当日委托
                    }
                    case 320: {//当日成交
                    }
                    case 321: {//当日委托成交
                    }
                }
                event.consume();
            }
        }
    };
    private final EventHandler<MouseEvent> mouseHandle_option = (MouseEvent event) -> {
        MouseButton button = event.getButton();
        Node node = event.getPickResult().getIntersectedNode();
        int count = event.getClickCount();
        if (button == MouseButton.PRIMARY && count == AccordionMouseClick) {
            // Accept clicks only on node cells, and not on empty spaces of the TreeView
            if (node instanceof Text || (node instanceof TreeCell && ((TreeCell) node).getText() != null)) {
                TradeTreeNode name = (TradeTreeNode) ((TreeItem) stock_option_tree.getSelectionModel().getSelectedItem()).getValue();
                log.info("mouseHandle_option Node click: " + name.getNodeId() + " :" + name.getNodeText());
                /*
                 41 :买入开仓
                 42 :卖出平仓
                 43 :备兑券锁定
                 44 :备兑券解锁
                 45 :备兑开仓
                 46 :备兑平仓
                 47 :卖出开仓
                 48 :买入平仓
                 49 :行权
                 420 :自动行权
                 411 :撤单
                 52 :查询
                 4121 :行权指派查询
                 4122 :备兑股份查询
                 4123 :当日委托
                 4124 :当日成交
                 4125 :当日委托成交
                 4126 :历史委托
                 4127 :历史成交
                 4128 :历史委托成交
                 4129 :历史持仓
                 4130 :出入金流水
                 4131 :净值图
                 4132 :资金流水
                 */
                switch (name.getNodeId()) {
                    case 41: {//买入开仓
                    }
                    case 42: {//卖出平仓
                    }
                    case 43: {//备兑券锁定
                    }
                    case 44: {//备兑券解锁
                    }
                    case 45: {//备兑开仓
                    }
                    case 46: {//备兑平仓
                    }
                    case 47: {//卖出开仓
                    }
                    case 48: {//买入平仓
                    }
                    case 49: {//行权
                    }
                    case 420: {//自动行权
                    }
                    case 411: {//撤单
                    }
                    case 52: {//查询
                    }
                    case 4121: {//行权指派查询
                    }
                    case 4122: {//备兑股份查询
                    }
                    case 4123: {//当日委托
                    }
                    case 4124: {//当日成交
                    }
                    case 4125: {//当日委托成交
                    }
                    case 4126: {//历史委托
                    }
                    case 4127: {//历史成交
                    }
                    case 4128: {//历史委托成交
                    }
                    case 4129: {//历史持仓
                    }
                    case 4130: {//出入金流水
                    }
                    case 4131: {//净值图
                    }
                    case 4132: {//资金流水
                    }
                }
                event.consume();
            }
        }
    };

    /**
     * Initializes the controller class.
     *
     * @param clientapi
     */
    public StockTradeUIController(ClientAPI clientapi) {
        super(StockTradeUIController.class.getResource("StockTradeUI.fxml"), I18N.getBundle());
        this.clientapi = clientapi;
        //    log.info("初始化");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        draw();
        //显示抽屉面板；
        drawtree();

        //显示默认功能界面
        work_stock_cmd.setExpandedPane(stock_general);
    }

    public void DrawRoot(double a, double b) {
        work_stock_root.setPrefSize(a, b);
    }
}
