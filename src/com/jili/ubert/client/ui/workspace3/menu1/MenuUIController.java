/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.menu1;

import com.jili.ubert.client.ui.AbstractNodeUI;
import com.jili.ubert.client.ui.PlatformInfo;
import com.jili.ubert.client.ui.i18n.I18N;
import com.jili.ubert.client.ui.workspace3.ControlWorkSpaceShow;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author ChengJiLi
 */
public class MenuUIController extends AbstractNodeUI implements Initializable {

    @FXML
    private AnchorPane bar_root;
    @FXML
    private ToolBar bar_one;
    @FXML
    private Button bar_bolt;

    @FXML
    private Button bar_manager;

    @FXML
    private Button bar_taoli;

    @FXML
    private Button bar_stock;

    @FXML
    private Button bar_future;

    @FXML
    private Button bar_set;

    @FXML
    private Button bar_basket;

    @FXML
    private Button bar_other;

    @FXML
    private Button bar_report;

    @FXML
    private ImageView bar_image_message;
    private boolean isShowMessageWin = false;
    private final ControlWorkSpaceShow control;

    public MenuUIController(ControlWorkSpaceShow control) {
        super(MenuUIController.class.getResource("MenuUI.fxml"), I18N.getBundle());
        this.control = control;
    }

    @FXML
    void showMessageWin(ActionEvent event) {
        if (isShowMessageWin) {
            if (control.CallCMD("HideMessageWin")) {
                isShowMessageWin = false;
            }
        } else {
            isShowMessageWin = control.CallCMD("ShowMessageWin");
        }
    }

    /**
     * Initializes the controller class.
     */
    private void draw() {
        bar_root.setPrefSize(PlatformInfo.getAvailableWidth(), 40);
        bar_one.setPrefSize(PlatformInfo.getAvailableWidth() - 45, 40);
    //    bar_message.setGraphic(new ImageView(new Image(MenuUIController.class.getResourceAsStream("bar_message.jpg"))));
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        draw();
        //设置每个按钮的监听
        bar_image_message.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
            if (isShowMessageWin) {
                if (control.CallCMD("HideMessageWin")) {
                    isShowMessageWin = false;
                }
            } else {
                isShowMessageWin = control.CallCMD("ShowMessageWin");
            }
            event.consume();
        });
    }

}
