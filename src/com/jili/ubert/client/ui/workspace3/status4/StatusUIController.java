/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.status4;

import com.jili.ubert.client.ui.AbstractNodeUI;
import com.jili.ubert.client.ui.PlatformInfo;
import com.jili.ubert.client.ui.i18n.I18N;
import com.jili.ubert.client.ui.workspace3.WorkSpaceUIController;
import com.jili.ubert.clientapi.ClientAPI;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author ChengJiLi
 */
public class StatusUIController  extends AbstractNodeUI implements Initializable {

    private ClientAPI clientapi;
    @FXML
    private Label status_trade;

    @FXML
    private Label status_price;

    @FXML
    private Label status_other;

    @FXML
    private AnchorPane status_root;

    public StatusUIController(ClientAPI clientapi) {
        super(StatusUIController.class.getResource("StatusUI.fxml"), I18N.getBundle());
        this.clientapi = clientapi;
    //    log.info("初始化");
    }
    private void draw(){
        status_root.setPrefSize(PlatformInfo.getAvailableWidth(), 24);
    }
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        draw();
        //设置服务超时监听；超时后状态变红，切换图片
    }    
    
}
