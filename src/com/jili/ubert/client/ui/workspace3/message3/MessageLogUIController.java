/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.workspace3.message3;

import com.jili.ubert.client.ui.AbstractNodeUI;
import com.jili.ubert.client.ui.PlatformInfo;
import com.jili.ubert.client.ui.i18n.I18N;
import com.jili.ubert.client.ui.workspace3.WorkSpaceUIController;
import com.jili.ubert.clientapi.ClientAPI;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
/**
 * FXML Controller class
 *
 * @author ChengJiLi
 */
public class MessageLogUIController  extends AbstractNodeUI implements Initializable {

    private final ClientAPI clientapi;
    @FXML
    private Button message_bar_manger;

    @FXML
    private Button message_bar_log;

    @FXML
    private WebView message_context;

    @FXML
    private Button message_bar_msg;

    @FXML
    private Button message_bar_unread;

    @FXML
    private ListView<?> message_lists;

    @FXML
    private AnchorPane messag_root;

    @FXML
    private Button message_bar_system;

    public MessageLogUIController(ClientAPI clientapi) {
        super(MessageLogUIController.class.getResource("MessageLogUI.fxml"), I18N.getBundle());
        this.clientapi = clientapi;
    //    log.info("初始化");
    }
    
    private void draw(){
        messag_root.setPrefSize(PlatformInfo.getAvailableWidth()/5, PlatformInfo.getAvailableHeight()-40-24-PlatformInfo.getWinUseHeight());
    }

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        draw();
        //订阅消息，操作日志等等；
    }    
    
}
