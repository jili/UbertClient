/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui.i18n;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author ChengJiLi
 */
public class I18N {

    private static ResourceBundle bundle;

    public static String getString(String key) {
        return getBundle().getString(key);
    }

    public static String getString(String key, Object... arguments) {
        final String pattern = getString(key);
        return MessageFormat.format(pattern, arguments);
    }

    public static synchronized ResourceBundle getBundle() {
        if (bundle == null) {
            final String packageName = I18N.class.getPackage().getName();
            bundle = ResourceBundle.getBundle(packageName + ".ClientWord"); //NOI18N
        }

        return bundle;
    }

    public static synchronized ResourceBundle getBundle(String baseName, Locale locale) {
        final String packageName = I18N.class.getPackage().getName();
        bundle = ResourceBundle.getBundle(packageName + ".ClientWord", locale); //NOI18N
        return bundle;
    }

    private I18N() {
    }
}
