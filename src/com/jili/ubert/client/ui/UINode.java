/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui;

import com.jili.ubert.client.ui.login1.LoginUIController;
import com.jili.ubert.client.ui.workspace3.ControlWorkSpaceShow;
import com.jili.ubert.client.ui.workspace3.WorkSpaceUIController;
import com.jili.ubert.client.ui.workspace3.main2.Cancel3UIController;
import com.jili.ubert.client.ui.workspace3.main2.PriceQueue10UIController;
import com.jili.ubert.client.ui.workspace3.main2.PriceQueue5UIController;
import com.jili.ubert.client.ui.workspace3.main2.stocktrade.StockTradeUIController;
import com.jili.ubert.client.ui.workspace3.main2.table.OrderExecuteProgressUIController;
import com.jili.ubert.client.ui.workspace3.menu1.MenuUIController;
import com.jili.ubert.client.ui.workspace3.message3.MessageLogUIController;
import com.jili.ubert.client.ui.workspace3.status4.StatusUIController;
import com.jili.ubert.clientapi.ClientAPI;
import com.jili.ubert.clientapi.provider.MarketDataProvider;
import com.panemu.tiwulfx.control.DetachableTabPane;
import java.util.HashSet;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author ChengJiLi
 */
public class UINode {

    private static Stage stage;
    private static final Log log = LogFactory.getLog(UINode.class);
    private static WorkSpaceUIController workSpace;
    private static LoginUIController login;
    private final static HashSet<String> usingNode = new HashSet<>();
    private static StatusUIController workSpace_status;
    private static MessageLogUIController workSpace_message;
    private static MenuUIController workSpace_Menu;
    private static StockTradeUIController stockTradeUI;
    private static PriceQueue5UIController price5Queue;
    private static PriceQueue10UIController price10Queue;
    private static Cancel3UIController cancelUI;
    private static OrderExecuteProgressUIController orderexeUI;
    private static DetachableTabPane tabpane;

    /**
     * @param clientapi
     * @return the workSpace
     */
    public static WorkSpaceUIController getWorkSpace(ClientAPI clientapi) {
        if (workSpace == null) {
            log.info("No WorkSpaceUIController instante");
            workSpace = new WorkSpaceUIController(clientapi);
            usingNode.add("WorkSpaceUIController");
            return workSpace;
        } else if (!usingNode.contains("LoginUIController")) {
            log.info("having one WorkSpaceUIController instante");
            return workSpace;
        } else {
            log.info("more than two instante");
            return new WorkSpaceUIController(clientapi);
        }
    }

    public static DetachableTabPane getTabpane() {
        if (tabpane == null) {
            log.info("No DetachableTabPane instante");
            tabpane = new DetachableTabPane() {
            };
            usingNode.add("DetachableTabPane");
            return tabpane;
        } else if (!usingNode.contains("DetachableTabPane")) {
            log.info("having one DetachableTabPane instante");
            return tabpane;
        } else {
            log.info("more than two instante");
            return new DetachableTabPane() {
            };
        }
    }

    public static void showAsTab(DetachableTabPane tabpane0,Parent root, String label) {
        final Tab tab = new Tab(label);
        AnchorPane pane = new AnchorPane();
        pane.getChildren().add(root);
        AnchorPane.setBottomAnchor(root, 0.0);
        AnchorPane.setLeftAnchor(root, 0.0);
        AnchorPane.setRightAnchor(root, 0.0);
        AnchorPane.setTopAnchor(root, 0.0);
        tab.setClosable(true);
        tab.setContent(pane);
        tabpane0.getTabs().add(tab);
        tabpane0.getSelectionModel().select(tab);
        /**
         * Workaround for TabPane memory leak
         */
        tab.setOnClosed(new EventHandler<Event>() {

            @Override
            public void handle(Event t) {
                tab.setContent(null);
            }
        });

    }

    /**
     * @param clientapi
     * @return the login
     */
    public static LoginUIController getLogin(ClientAPI clientapi) {
        if (login == null) {
            log.info("No Login instante");
            login = new LoginUIController(clientapi);
            usingNode.add("LoginUIController");
            return login;
        } else if (!usingNode.contains("LoginUIController")) {
            log.info("having one Login instante");
            return login;
        } else {
            log.info("more than two instante");
            return new LoginUIController(clientapi);
        }
    }

    /**
     * @return the stage
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * @param aStage the stage to set
     */
    public static void setStage(Stage aStage) {
        stage = aStage;
    }

    /**
     * @param clientapi
     * @return the workSpace_status
     */
    public static StatusUIController getWorkSpace_status(ClientAPI clientapi) {
        if (workSpace_status == null) {
            workSpace_status = new StatusUIController(clientapi);
            usingNode.add("StatusUIController");
            return workSpace_status;
        } else if (!usingNode.contains("StatusUIController")) {
            return workSpace_status;
        } else {
            return new StatusUIController(clientapi);
        }
    }

    /**
     * @param clientapi
     * @return the workSpace_message
     */
    public static MessageLogUIController getWorkSpace_message(ClientAPI clientapi) {
        if (workSpace_message == null) {
            workSpace_message = new MessageLogUIController(clientapi);
            usingNode.add("MessageLogUIController");
            return workSpace_message;
        } else if (!usingNode.contains("MessageLogUIController")) {
            return workSpace_message;
        } else {
            return new MessageLogUIController(clientapi);
        }
    }

    /**
     * @param control
     * @return the workSpace_Menu
     */
    public static MenuUIController getWorkSpace_Menu(ControlWorkSpaceShow control) {
        if (workSpace_Menu == null) {
            workSpace_Menu = new MenuUIController(control);
            usingNode.add("MenuUIController");
            return workSpace_Menu;
        } else if (!usingNode.contains("MenuUIController")) {
            return workSpace_Menu;
        } else {
            return new MenuUIController(control);
        }
    }

    /**
     * @param clientapi
     * @return the stockTradeUI
     */
    public static StockTradeUIController getStockTradeUI(ClientAPI clientapi) {
        if (stockTradeUI == null) {
            stockTradeUI = new StockTradeUIController(clientapi);
            usingNode.add("StockTradeUIController");
            return stockTradeUI;
        } else if (!usingNode.contains("StockTradeUIController")) {
            return stockTradeUI;
        } else {
            return new StockTradeUIController(clientapi);
        }
    }

    /**
     * @param clientapi
     * @return the price5Queue
     */
    public static PriceQueue5UIController getPrice5Queue(MarketDataProvider clientapi) {
        if (price5Queue == null) {
            log.info("No PriceQueue5UIController instante");
            price5Queue = new PriceQueue5UIController(clientapi);
            usingNode.add("PriceQueue5UIController");
            return price5Queue;
        } else if (!usingNode.contains("PriceQueue5UIController")) {
            log.info("having one PriceQueue5UIController instante");
            return price5Queue;
        } else {
            log.info("more than two instante");
            return new PriceQueue5UIController(clientapi);
        }
    }

    /**
     * @param clientapi
     * @return the price10Queue
     */
    public static PriceQueue10UIController getPrice10Queue(MarketDataProvider clientapi) {
        if (price10Queue == null) {
            log.info("No PriceQueue10UIController instante");
            price10Queue = new PriceQueue10UIController(clientapi);
            usingNode.add("PriceQueue10UIController");
            return price10Queue;
        } else if (!usingNode.contains("PriceQueue10UIController")) {
            log.info("having one PriceQueue10UIController instante");
            return price10Queue;
        } else {
            log.info("more than two instante");
            return new PriceQueue10UIController(clientapi);
        }
    }

    public static Cancel3UIController getCancelUI(ClientAPI clientapi) {
        if (cancelUI == null) {
            log.info("No Cancel3UIController instante");
            cancelUI = new Cancel3UIController(clientapi);
            usingNode.add("Cancel3UIController");
            return cancelUI;
        } else if (!usingNode.contains("Cancel3UIController")) {
            log.info("having one Cancel3UIController instante");
            return cancelUI;
        } else {
            log.info("more than two instante");
            return new Cancel3UIController(clientapi);
        }
    }

    public static OrderExecuteProgressUIController getOrderexeUI(ClientAPI clientapi) {
        if (orderexeUI == null) {
            log.info("No OrderExecuteProgressUIController instante");
            orderexeUI = new OrderExecuteProgressUIController(clientapi);
            usingNode.add("OrderExecuteProgressUIController");
            return orderexeUI;
        } else if (!usingNode.contains("OrderExecuteProgressUIController")) {
            log.info("having one OrderExecuteProgressUIController instante");
            return orderexeUI;
        } else {
            log.info("more than two instante");
            return new OrderExecuteProgressUIController(clientapi);
        }
    }

    private UINode() {
    }
}
