/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui;

import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.util.Locale;

/**
 *
 * @author ChengJiLi
 */
public class PlatformInfo {

    private static int fullWidth;
    private static int fullHeight;
    private static int availableWidth;
    private static int availableHeight;
    private static int winUseHeight=-1;
    private static int winUseWidth=-1;
    private static final PlatformInfo s = new PlatformInfo();
    private static String oSName;
    private static final String osName = System.getProperty("os.name").toLowerCase(Locale.ROOT); //NOI18N
    public static final boolean IS_LINUX = osName.contains("linux"); //NOI18N
    public static final boolean IS_MAC = osName.contains("mac"); //NOI18N
    public static final boolean IS_WINDOWS = osName.contains("windows"); //NOI18N
    public static final int rowNum = 300;

    /**
     * @return the winUseHeight
     */
    public static int getWinUseHeight() {
        if (-1 == winUseHeight) {
            if (PlatformInfo.IS_LINUX) {
                oSName = "linux";
                winUseHeight = 0;
                winUseWidth = 0;
            }
            if (PlatformInfo.IS_MAC) {
                oSName = "mac";
                winUseHeight = 0;
                winUseWidth = 0;
            }
            if (PlatformInfo.IS_WINDOWS) {
                oSName = "windows";
                winUseHeight = 25;
                winUseWidth = 0;
            }
        }
        return winUseHeight;
    }

    /**
     * @return the winUseWidth
     */
    public static int getWinUseWidth() {
        if (-1 == winUseWidth) {
            if (PlatformInfo.IS_LINUX) {
                oSName = "linux";
                winUseHeight = 0;
                winUseWidth = 0;
            }
            if (PlatformInfo.IS_MAC) {
                oSName = "mac";
                winUseHeight = 0;
                winUseWidth = 0;
            }
            if (PlatformInfo.IS_WINDOWS) {
                oSName = "windows";
                winUseHeight = 25;
                winUseWidth = 0;
            }
        }
        return winUseWidth;
    }

    /**
     * @return the oSName
     */
    public static String getoSName() {
        if (oSName == null) {
            if (PlatformInfo.IS_LINUX) {
                oSName = "linux";
                winUseHeight = 0;
                winUseWidth = 0;
            }
            if (PlatformInfo.IS_MAC) {
                oSName = "mac";
                winUseHeight = 0;
                winUseWidth = 0;
            }
            if (PlatformInfo.IS_WINDOWS) {
                oSName = "windows";
                winUseHeight = 25;
                winUseWidth = 0;
            }
        }
        return oSName;
    }

    private PlatformInfo() {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        fullWidth = dim.width;
        fullHeight = dim.height;
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Rectangle rect = ge.getMaximumWindowBounds();
        availableWidth = rect.width;
        availableHeight = rect.height;
    }

    /**
     * @return the fullWidth
     */
    public static int getFullWidth() {
        return fullWidth;
    }

    /**
     * @return the fullHeight
     */
    public static int getFullHeight() {
        return fullHeight;
    }

    /**
     * @return the availableWidth
     */
    public static int getAvailableWidth() {
        return availableWidth;
    }

    /**
     * @return the availableHeight
     */
    public static int getAvailableHeight() {
        return availableHeight;
    }
}
