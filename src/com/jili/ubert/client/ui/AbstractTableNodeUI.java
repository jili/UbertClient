/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui;

import com.jili.ubert.client.ui.i18n.I18N;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;

/**
 *
 * @author dragon
 */
public abstract class AbstractTableNodeUI implements Initializable {

    private Parent root;
    private final URL fxmlURL;
    private final ResourceBundle resources=I18N.getBundle();

    public AbstractTableNodeUI(URL fxmlURL) {
        this.fxmlURL = fxmlURL;
    }

    public Parent getUINode() {
        System.out.println("getUINode");
        if (root == null) {
            System.out.println("UInode is null");
            FXMLLoader loader = new FXMLLoader();
            loader.setController(this);
            loader.setLocation(fxmlURL);
            loader.setResources(resources);
            try {
                root = loader.load();
            } catch (RuntimeException | IOException x) {
                System.out.println("loader.getController()=" + loader.getController());
                System.out.println("loader.getLocation()=" + loader.getLocation());
                throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
            }
        }
        return root;
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        tableInit();
    }
    public abstract void tableInit();
}
