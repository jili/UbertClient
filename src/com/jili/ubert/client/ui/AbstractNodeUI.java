/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jili.ubert.client.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 *
 * @author dragon
 */
public abstract class AbstractNodeUI {

    private Window owner;
    private Parent root;
    private Scene scene;
    private Stage stage;
    private final URL fxmlURL;
    private final ResourceBundle resources;

    public AbstractNodeUI(URL fxmlURL, ResourceBundle resources) {
        this.fxmlURL = fxmlURL;
        this.resources = resources;
    }

    public Parent getUINode() {
        System.out.println("getUINode");
        if (root == null) {
            System.out.println("UInode is null");
            FXMLLoader loader = new FXMLLoader();
            loader.setController(this);
            loader.setLocation(fxmlURL);
            loader.setResources(resources);
            try {
                System.out.println("UInode is loading");
                root = loader.load();
            } catch (RuntimeException | IOException x) {
                System.out.println("loader.getController()=" + loader.getController());
                System.out.println("loader.getLocation()=" + loader.getLocation());
                throw new RuntimeException("Failed to load " + fxmlURL.getFile(), x); //NOI18N
            }
        }
        return root;
    }
}
